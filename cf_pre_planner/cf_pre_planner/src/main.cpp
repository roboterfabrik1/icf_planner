#include <cf_pre_planner/cf_pre_planner.h>
#include "std_msgs/Float32.h"

int main(int argc, char **argv) {
    ros::init(argc, argv, "cf_pre_planner");
    ros::NodeHandle n;
    int ros_spin_frequency = 50;

    ros::Publisher planning_time_pub = n.advertise<std_msgs::Float32>("/planning_time",5);


    ros::AsyncSpinner spinner(2);
    spinner.start();
    ros::Rate ros_rate(ros_spin_frequency);

    ROS_INFO_STREAM("CF Pre Planner started");

    CFPrePlanner cf_pre_planner(n);

    std::vector<int> fap_frame_ids;
    std::vector<int> joint_types;
    std::vector<double> dh_a;
    std::vector<double> dh_alpha;
    std::vector<double> dh_d;
    std::vector<double> dh_theta;
    bool sim_only;

    double planning_time;
    double rot_vec_calc_radius;
    std::string planner_type;

    std::string ROOT_PATH = "CFPlanner/SimpleRobotDynamicSettings";

    if (n.hasParam(ROOT_PATH)) {
        n.getParam(ROOT_PATH + "/joint_types", joint_types);
        n.getParam(ROOT_PATH + "/dh_a", dh_a);
        n.getParam(ROOT_PATH + "/dh_alpha", dh_alpha);
        n.getParam(ROOT_PATH + "/dh_d", dh_d);
        n.getParam(ROOT_PATH + "/dh_theta", dh_theta);
        if (!n.param(ROOT_PATH + "/sim_only", sim_only, true)) {
            ROS_WARN_STREAM(
                "Preplanner::main  - Can not load sim_only. Falling back to default behavior: simulation only.");
        }
    }

    ROOT_PATH = "CFPlanner/CFAgentSettings";

    if (n.hasParam(ROOT_PATH)) {
        n.getParam(ROOT_PATH + "/fap_frame_ids", fap_frame_ids);
    }

    ROOT_PATH = "CFPlanner/CFPrePlannerSettings";
    if (n.hasParam(ROOT_PATH)) {
        n.getParam(ROOT_PATH + "/maximum_planning_time", planning_time);
        n.getParam(ROOT_PATH + "/planning_type", planner_type);
        n.getParam(ROOT_PATH + "/rot_vec_calc_radius", rot_vec_calc_radius);
    }

    cf_pre_planner.setFAPIds(fap_frame_ids);

    cf_pre_planner.setDHParams(dh_a, dh_d, dh_alpha, dh_theta, joint_types);

    cf_pre_planner.buildKdlChain();

    // wait until obstacle pointcloud and joint states are received
    while (!cf_pre_planner.received_pointcloud || !cf_pre_planner.received_goal_pose) {
        ros::spinOnce();
        ros_rate.sleep();
        if (!cf_pre_planner.received_pointcloud) {
            ROS_INFO_STREAM("Waiting for pointcloud");
        }
        if (!cf_pre_planner.received_goal_pose) {
            ROS_INFO_STREAM("Waiting for goal pose");
        }
    }
    // Wait for joint states of real robot
    if (!sim_only) {
        sensor_msgs::JointState::ConstPtr initial_joint_states;
        initial_joint_states = ros::topic::waitForMessage<sensor_msgs::JointState>(
            "/franka_state_controller/joint_states");
        if (initial_joint_states == NULL) {
            ROS_ERROR_STREAM("Preplanner::main - Did not receive joint states from real robot.");
        }
    }

    std::vector<float> obstacle_ids;

    int i = 2 * ros_spin_frequency - 1;
    while (ros::ok()) {
        i++;
        cf_pre_planner_msgs::RotVecArrayRobot rot_vec_array_robot_msg_simple;
        cf_pre_planner_msgs::RotVecArrayRobot rot_vec_array_robot_msg_enhanced;
        cf_pre_planner_msgs::RotVecArrayObstacles rot_vec_array_obstacles_msg_simple;
        cf_pre_planner_msgs::RotVecArrayObstacles rot_vec_array_obstacles_msg_enhanced;

        auto start = std::chrono::steady_clock::now();
        if (i == 2 * ros_spin_frequency) {
            cf_pre_planner.setPointClouds();
            cf_pre_planner.updateKdTrees();
            obstacle_ids = cf_pre_planner.getObstacleIds();
            i = 0;
        }

        moveit_msgs::RobotTrajectory trajectory_msg = cf_pre_planner.plan(planner_type, planning_time);
        std::vector<EigenSTL::vector_Vector3d> cartesian_paths(cf_pre_planner.getFAPIds().size());

        cartesian_paths = cf_pre_planner.getCartesianPathsFromTrajectory(trajectory_msg);

        RotationVectorBundle rotation_vectors_one_obstacle;

        for (auto const &obstacle_id : obstacle_ids) {
            rotation_vectors_one_obstacle = cf_pre_planner.getRotationVectorsFromPaths(cartesian_paths, obstacle_id,
                                                                                       rot_vec_calc_radius);
            int j = 0;
            for (auto const &fap_frame_id : fap_frame_ids) {
                geometry_msgs::Vector3 vector_simple;
                geometry_msgs::Vector3 vector_enhanced;

                // If no valid rotation vector could be calculated, use 0 0 0. This happens most likely on obstacles
                // which are uncritical for the movement since the generated path only contains 2 or less points.
                if (!rotation_vectors_one_obstacle.valid[j]) {
                    rot_vec_array_robot_msg_simple.rot_vecs.push_back(vector_simple);
                    rot_vec_array_robot_msg_enhanced.rot_vecs.push_back(vector_enhanced);
                    continue;
                }

                vector_simple.x = rotation_vectors_one_obstacle.simple_rot_vec_set[j][0];
                vector_simple.y = rotation_vectors_one_obstacle.simple_rot_vec_set[j][1];
                vector_simple.z = rotation_vectors_one_obstacle.simple_rot_vec_set[j][2];
                rot_vec_array_robot_msg_simple.rot_vecs.push_back(vector_simple);

                vector_enhanced.x = rotation_vectors_one_obstacle.enhanced_rot_vec_set[j][0];
                vector_enhanced.y = rotation_vectors_one_obstacle.enhanced_rot_vec_set[j][1];
                vector_enhanced.z = rotation_vectors_one_obstacle.enhanced_rot_vec_set[j][2];
                rot_vec_array_robot_msg_enhanced.rot_vecs.push_back(vector_enhanced);
                j++;
            }
            rot_vec_array_obstacles_msg_simple.rot_vecs_obstacles.push_back(rot_vec_array_robot_msg_simple);
            rot_vec_array_obstacles_msg_enhanced.rot_vecs_obstacles.push_back(rot_vec_array_robot_msg_enhanced);
        }

        auto end = std::chrono::steady_clock::now();
        std::cout << "Elapsed time in milliseconds per iteration: "
                  << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << " ms" << std::endl;

        std_msgs::Float32 planning_time_msg;
        planning_time_msg.data = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() / 1000000.0;
        planning_time_pub.publish(planning_time_msg);

        rot_vec_array_obstacles_msg_simple.rot_vec_ids = obstacle_ids;
        rot_vec_array_obstacles_msg_enhanced.rot_vec_ids = obstacle_ids;

        cf_pre_planner.publishRotVecs(rot_vec_array_obstacles_msg_simple);
        cf_pre_planner.publishRotVecs(rot_vec_array_obstacles_msg_enhanced);

        ros::spinOnce();
        ros_rate.sleep();
    }
}