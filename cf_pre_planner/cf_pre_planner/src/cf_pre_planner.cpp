#include <cf_pre_planner/cf_pre_planner.h>

CFPrePlanner::CFPrePlanner(ros::NodeHandle &node_handle) : n_(&node_handle) {
    moveit::planning_interface::MoveGroupInterface::Options opt(PLANNING_GROUP_, ROBOT_DESCRIPTION_, *n_);
    move_group_interface_ = std::make_shared<moveit::planning_interface::MoveGroupInterface>(opt);

    obstacle_sub = n_->subscribe("raw_pcl_obstacles", 1, &CFPrePlanner::pointCloudCallback, this);
    goal_pose_sub = n_->subscribe("cf_goal_pose", 1, &CFPrePlanner::goalPoseCallback, this);
    rot_vec_pub = n_->advertise<cf_pre_planner_msgs::RotVecArrayObstacles>("rotation_vector_guesses", 1);
}

void CFPrePlanner::pointCloudCallback(const pcl::PointCloud<pcl::PointXYZI>::Ptr &cloud_msg) {
    cloud_ptr_->points.resize(cloud_msg->points.size());
    cloud_ptr_->points = cloud_msg->points;
    if (!received_pointcloud) {
        received_pointcloud = true;
    }
}

void CFPrePlanner::goalPoseCallback(const geometry_msgs::PoseStamped &latest_goal_pose_msg) {
    goal_pose_ = latest_goal_pose_msg;
    if (!received_goal_pose) {
        received_goal_pose = true;
    }
}

void CFPrePlanner::publishRotVecs(cf_pre_planner_msgs::RotVecArrayObstacles &msg) { rot_vec_pub.publish(msg); }

void CFPrePlanner::setGoalPose(geometry_msgs::PoseStamped &goal_pose) { goal_pose_ = goal_pose; }

void CFPrePlanner::updateKdTrees() {
    obstacle_kd_trees_.clear();
    for (auto const &obstacle_id : getObstacleIds()) {
        pcl::KdTreeFLANN<pcl::PointXYZI> new_kd_tree;
        new_kd_tree.setInputCloud(obstacle_pointclouds_.at(obstacle_id));
        obstacle_kd_trees_.insert(std::pair<float, pcl::KdTreeFLANN<pcl::PointXYZI>>(obstacle_id, new_kd_tree));
    }
}

void CFPrePlanner::setPointClouds() {
    // Create the filtering object
    pcl::VoxelGrid<pcl::PointXYZI> sor;
    sor.setInputCloud(cloud_ptr_);
    // Downsampling to 2 cm
    sor.setLeafSize(0.02f, 0.02f, 0.02f);
    sor.filter(*cloud_ptr_);
    obstacle_pointclouds_.clear();

    for (size_t i_point = 0; i_point < cloud_ptr_->size(); i_point++) {
        // if no pointcloud exists for the found intensity, create a new one
        if (obstacle_pointclouds_.find(cloud_ptr_->at(i_point).intensity) == obstacle_pointclouds_.end()) {
            boost::shared_ptr<pcl::PointCloud<pcl::PointXYZI>> new_cloud_ptr =
                boost::make_shared<pcl::PointCloud<pcl::PointXYZI>>();
            obstacle_pointclouds_.insert(std::pair<float, boost::shared_ptr<pcl::PointCloud<pcl::PointXYZI>>>(
                cloud_ptr_->at(i_point).intensity, new_cloud_ptr));
        }
        // add the point to the corresponding point cloud
        obstacle_pointclouds_.at(cloud_ptr_->at(i_point).intensity)->push_back(cloud_ptr_->at(i_point));
    }
}

std::vector<float> CFPrePlanner::getObstacleIds() {
    std::vector<float> keys;
    for (auto const &element : obstacle_pointclouds_) {
        keys.push_back(element.first);
    }
    return keys;
}

RotationVectorBundle CFPrePlanner::getRotationVectorsFromPaths(std::vector<EigenSTL::vector_Vector3d> &cartesian_paths,
                                                               float obstacle_id, double rot_vec_calc_radius) {
    // If this is too slow, try https://github.com/neka-nat/cupoch/blob/master/src/tests/geometry/kdtree_flann.cpp
    // Performance measurements example obstacle:
    // 314 points: 0.2 ms
    // 31.416 points: 4.5 ms
    // 3.141.593 points: 1 s

    pcl::PointXYZI search_point;

    // It might be necessary to increase k_nearest_points to eliminate potential outlier errors
    int k_nearest_points = 1;
    std::vector<int> point_id_knn_search(k_nearest_points);
    std::vector<float> point_knn_squared_distances(k_nearest_points);
    std::vector<PathPointWithDistance> vector_distance_path_points;
    RotationVectorBundle rot_vec_bundle;
    rot_vec_bundle.enhanced_rot_vec_set.resize(cartesian_paths.size());
    rot_vec_bundle.simple_rot_vec_set.resize(cartesian_paths.size());

    for (size_t i_FAP = 0; i_FAP < cartesian_paths.size(); i_FAP++) {
        pcl::PointXYZI closest_point;
        int entry_point_id = -1;
        int middle_point_id = -1;
        int exit_point_id = -1;
        Eigen::Vector3d closest_distance_vector(3);
        Eigen::Vector3d velocity_vector(3);
        Eigen::Vector3d rotation_vector_simple(3);
        Eigen::Vector3d rotation_vector_enhanced(3);
        Eigen::Vector3d rotation_vector_viz(3);
        bool obstacle_vicinity = false;
        bool initial_obstacle_approach = false;
        vector_distance_path_points.clear();

        // only loop from 1 to i-1 because the direction vector is calculated with ((i+1) - (i-1) )/ 2
        for (size_t i_point = 1; i_point < cartesian_paths[i_FAP].size() - 1; i_point++) {
            search_point.x = cartesian_paths[i_FAP][i_point][0];
            search_point.y = cartesian_paths[i_FAP][i_point][1];
            search_point.z = cartesian_paths[i_FAP][i_point][2];

            obstacle_kd_trees_.at(obstacle_id)
                .nearestKSearch(search_point, k_nearest_points, point_id_knn_search, point_knn_squared_distances);

            closest_point = (*obstacle_pointclouds_.at(obstacle_id))[point_id_knn_search[0]];

            double point_knn_distance = std::sqrt(point_knn_squared_distances[0]);

            if (!obstacle_vicinity && point_knn_distance < rot_vec_calc_radius) {
                if (!initial_obstacle_approach) {
                    initial_obstacle_approach = true;
                    entry_point_id = i_point;
                }
                obstacle_vicinity = true;
            }
            if (obstacle_vicinity && point_knn_distance > rot_vec_calc_radius) {
                obstacle_vicinity = false;
                exit_point_id = i_point;
                middle_point_id = entry_point_id + ((exit_point_id - entry_point_id) / 2);
            }

            PathPointWithDistance path_point_with_distance = {point_knn_distance, i_point, closest_point};
            vector_distance_path_points.push_back(path_point_with_distance);
        }
        if (obstacle_vicinity && exit_point_id == -1) {
            exit_point_id = cartesian_paths[i_FAP].size() - 1;
            middle_point_id = entry_point_id + ((exit_point_id - entry_point_id) / 2);
        }
        std::sort(vector_distance_path_points.begin(), vector_distance_path_points.end());

        if (vector_distance_path_points.size() == 0) {
            rot_vec_bundle.valid.push_back(false);
            continue;
        }
        rot_vec_bundle.valid.push_back(true);

        closest_distance_vector[0] = vector_distance_path_points.front().closest_point.x -
                                     cartesian_paths[i_FAP][vector_distance_path_points.front().id][0];
        closest_distance_vector[1] = vector_distance_path_points.front().closest_point.y -
                                     cartesian_paths[i_FAP][vector_distance_path_points.front().id][1];
        closest_distance_vector[2] = vector_distance_path_points.front().closest_point.z -
                                     cartesian_paths[i_FAP][vector_distance_path_points.front().id][2];

        velocity_vector = cartesian_paths[i_FAP][vector_distance_path_points.front().id + 1] -
                          cartesian_paths[i_FAP][vector_distance_path_points.front().id - 1];

        // The torque is in fact defined the other way around (t = r x F). However since the rotation vector of
        // Becker2021 is defined contrary the cross product is changed here.
        rotation_vector_simple = closest_distance_vector.cross(velocity_vector).normalized();
        rot_vec_bundle.simple_rot_vec_set[i_FAP].push_back(rotation_vector_simple[0]);
        rot_vec_bundle.simple_rot_vec_set[i_FAP].push_back(rotation_vector_simple[1]);
        rot_vec_bundle.simple_rot_vec_set[i_FAP].push_back(rotation_vector_simple[2]);

        if (entry_point_id != -1 && middle_point_id != -1 && exit_point_id != -1) {
            Eigen::Vector3d middle_to_entry;
            middle_to_entry = cartesian_paths[i_FAP][entry_point_id] - cartesian_paths[i_FAP][middle_point_id];

            Eigen::Vector3d middle_to_exit;
            middle_to_exit = cartesian_paths[i_FAP][exit_point_id] - cartesian_paths[i_FAP][middle_point_id];

            rotation_vector_enhanced = middle_to_entry.cross(middle_to_exit).normalized();

            rot_vec_bundle.enhanced_rot_vec_set[i_FAP].push_back(rotation_vector_enhanced[0]);
            rot_vec_bundle.enhanced_rot_vec_set[i_FAP].push_back(rotation_vector_enhanced[1]);
            rot_vec_bundle.enhanced_rot_vec_set[i_FAP].push_back(rotation_vector_enhanced[2]);
        } else {
            rot_vec_bundle.enhanced_rot_vec_set[i_FAP].push_back(rotation_vector_simple[0]);
            rot_vec_bundle.enhanced_rot_vec_set[i_FAP].push_back(rotation_vector_simple[1]);
            rot_vec_bundle.enhanced_rot_vec_set[i_FAP].push_back(rotation_vector_simple[2]);
        }

        // Rviz visualization for EE rotation vectors
        if (i_FAP == cartesian_paths.size() - 1) {
            geometry_msgs::Point closest_point_obs;
            closest_point_obs.x = vector_distance_path_points.front().closest_point.x;
            closest_point_obs.y = vector_distance_path_points.front().closest_point.y;
            closest_point_obs.z = vector_distance_path_points.front().closest_point.z;

            geometry_msgs::Point closest_point_path;
            closest_point_path.x = cartesian_paths[i_FAP][vector_distance_path_points.front().id][0];
            closest_point_path.y = cartesian_paths[i_FAP][vector_distance_path_points.front().id][1];
            closest_point_path.z = cartesian_paths[i_FAP][vector_distance_path_points.front().id][2];

            // visual_tools_.publishArrow(closest_point_obs, closest_point_path, rviz_visual_tools::GREEN);

            geometry_msgs::Point entry_point;
            entry_point.x = cartesian_paths[i_FAP][entry_point_id][0];
            entry_point.y = cartesian_paths[i_FAP][entry_point_id][1];
            entry_point.z = cartesian_paths[i_FAP][entry_point_id][2];

            geometry_msgs::Point middle_point;
            middle_point.x = cartesian_paths[i_FAP][middle_point_id][0];
            middle_point.y = cartesian_paths[i_FAP][middle_point_id][1];
            middle_point.z = cartesian_paths[i_FAP][middle_point_id][2];

            geometry_msgs::Point exit_point;
            exit_point.x = cartesian_paths[i_FAP][exit_point_id][0];
            exit_point.y = cartesian_paths[i_FAP][exit_point_id][1];
            exit_point.z = cartesian_paths[i_FAP][exit_point_id][2];

            // visual_tools_.publishSphere(entry_point, rviz_visual_tools::GREEN, rviz_visual_tools::LARGE);
            // visual_tools_.publishSphere(middle_point, rviz_visual_tools::YELLOW, rviz_visual_tools::LARGE);
            // visual_tools_.publishSphere(exit_point, rviz_visual_tools::BLUE, rviz_visual_tools::LARGE);

            geometry_msgs::Point rot_vec_end_enhanced;
            rot_vec_end_enhanced.x = closest_point_obs.x + rotation_vector_enhanced[0];
            rot_vec_end_enhanced.y = closest_point_obs.y + rotation_vector_enhanced[1];
            rot_vec_end_enhanced.z = closest_point_obs.z + rotation_vector_enhanced[2];

            geometry_msgs::Point rot_vec_end_simple;
            rot_vec_end_simple.x = closest_point_obs.x + rotation_vector_simple[0];
            rot_vec_end_simple.y = closest_point_obs.y + rotation_vector_simple[1];
            rot_vec_end_simple.z = closest_point_obs.z + rotation_vector_simple[2];

            // visual_tools_.publishArrow(closest_point_obs, rot_vec_end_enhanced, rviz_visual_tools::RED);
            // visual_tools_.publishArrow(closest_point_obs, rot_vec_end_simple, rviz_visual_tools::ORANGE);
            // visual_tools_.trigger();
        }
    }
    return rot_vec_bundle;
}

std::vector<EigenSTL::vector_Vector3d> CFPrePlanner::getCartesianPathsFromTrajectory(
    moveit_msgs::RobotTrajectory &trajectory_msg) {
    double costs = 0.0;
    // Convert the trajectory into a series of RobotStates
    robot_trajectory::RobotTrajectory robot_trajectory(
        move_group_interface_->getRobotModel(),
        move_group_interface_->getCurrentState()->getJointModelGroup(PLANNING_GROUP_)->getName());
    robot_trajectory.setRobotTrajectoryMsg(*move_group_interface_->getCurrentState(), trajectory_msg);
    std::vector<const moveit::core::JointModel *> joint_models =
        move_group_interface_->getCurrentState()->getJointModelGroup(PLANNING_GROUP_)->getJointModels();
    std::vector<EigenSTL::vector_Vector3d> cartesian_paths(fap_ids_.size());

    for (std::size_t i_point = 0; i_point < robot_trajectory.getWayPointCount(); ++i_point) {
        std::vector<double> joint_positions;
        for (size_t i_joint = 0; i_joint < 7; i_joint++) {
            joint_positions.push_back(*robot_trajectory.getWayPoint(i_point).getJointPositions(joint_models[i_joint]));
        }
        int j = 0;
        for (const auto &i_FAP : fap_ids_) {
            cartesian_paths[j].emplace_back(getGlobalTransform(joint_positions, i_FAP).translation());
            j++;
        }
    }
    for (size_t i = fap_ids_.size() - 1; i < fap_ids_.size(); i++) {
        const double RADIUS = 0.005;
        // visual_tools_.publishPath(cartesian_paths[i], rviz_visual_tools::GREEN, RADIUS, "RRT");
        // visual_tools_.trigger();
        break;
    }
    return cartesian_paths;
}

moveit_msgs::RobotTrajectory CFPrePlanner::plan(const std::string &planner_type, const double planning_time,
                                                const double goal_tolerance) {
        moveit::core::RobotStatePtr current_state = move_group_interface_->getCurrentState();

    move_group_interface_->setStartState(*current_state);
    move_group_interface_->setPoseTarget(goal_pose_);
    move_group_interface_->setGoalPositionTolerance(goal_tolerance);
    move_group_interface_->setGoalOrientationTolerance(goal_tolerance);
    move_group_interface_->setPlannerId(planner_type);
    move_group_interface_->setPlanningTime(planning_time);
    bool success = false;
    int fail_counter = 0;
    while (!success) {
        success = (move_group_interface_->plan(plan_) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
        if (!success) {
            ROS_WARN_STREAM("The planner couldn't find a valid solution after attempt " << fail_counter
                                                                                        << ", trying again...");
            fail_counter++;
            moveit::core::RobotStatePtr current_state = move_group_interface_->getCurrentState();
            move_group_interface_->setStartState(*current_state);
        }
    }

    // visual_tools_.deleteAllMarkers();
    // visual_tools_.trigger();
    // visual_tools_.publishAxisLabeled(goal_pose_.pose, "target_pose");

    return plan_.trajectory_;
}

Eigen::Affine3d CFPrePlanner::getGlobalTransform(std::vector<double> joint_angles, int dh_frame_index) const {
    KDL::Frame cartesian_transform_kdl;
    KDL::JntArray q_array(7);
    for (size_t i = 0; i < 7; i++) {
        q_array(i) = joint_angles[i];
    }

    KDL::ChainFkSolverPos_recursive fksolver(franka_chain_);
    int success = fksolver.JntToCart(q_array, cartesian_transform_kdl, dh_frame_index);
    if (success != 0) {
        std::cout << "Solver Error Nr. " << success << " | ErrorStr: " << fksolver.strError(success) << std::endl;
    }
    Eigen::Affine3d cartesian_transform_eigen;
    tf::transformKDLToEigen(cartesian_transform_kdl, cartesian_transform_eigen);

    return cartesian_transform_eigen;
}

void CFPrePlanner::buildKdlChain() {
    for (int i = 0; i < joint_types_.size(); i++) {
        if (joint_types_[i] == 0) {
            franka_chain_.addSegment(
                KDL::Segment(KDL::Joint(KDL::Joint::None),
                             KDL::Frame::DH_Craig1989(dh_a_[i], dh_alpha_[i], dh_d_[i], dh_theta_[i])));
        } else if (joint_types_[i] == 1) {
            franka_chain_.addSegment(
                KDL::Segment(KDL::Joint(KDL::Joint::RotZ),
                             KDL::Frame::DH_Craig1989(dh_a_[i], dh_alpha_[i], dh_d_[i], dh_theta_[i])));
        }
    }
}