#include <eigen_conversions/eigen_kdl.h>
#include <geometry_msgs/Pose.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit_visual_tools/moveit_visual_tools.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_ros/point_cloud.h>
#include <std_srvs/Empty.h>
#include <tf/transform_datatypes.h>

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <chrono>
#include <kdl/chain.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl_parser/kdl_parser.hpp>
#include <string>
#include <unordered_map>
#include <vector>

#include "cf_pre_planner_msgs/RotVecArrayObstacles.h"
#include "cf_pre_planner_msgs/RotVecArrayRobot.h"

struct RotationVectorBundle {
    std::vector<std::vector<double>> enhanced_rot_vec_set;
    std::vector<std::vector<double>> simple_rot_vec_set;
    std::vector<bool> valid;
};

struct PathPointWithDistance {
    double distance;
    size_t id;
    pcl::PointXYZI closest_point;
    bool operator<(const PathPointWithDistance& a) const { return distance < a.distance; }
};

/// @brief Class for the pre planner using global motion planning
///
/// This class uses moveit sampling based planners to calculate an initial guess for the rotation vectors
class CFPrePlanner {
public:
    explicit CFPrePlanner(ros::NodeHandle& node_handle);

    /// @brief Builds a KDL chain based on the predefined dh parameters
    void buildKdlChain();

    /// @brief Updates the KD trees with the latest point cloud data
    void updateKdTrees();

    /// @brief Sets a goal pose for the pre planner
    ///
    /// @param goal_pose The desired goal pose
    void setGoalPose(geometry_msgs::PoseStamped& goal_pose);

    /// @brief Sets the internal point clouds.
    /// The point cloud is subsampled and divided into sub point clouds, depending on the intensity (id).
    /// Can be used to initialize or update the point clouds.
    void setPointClouds();

    /// @brief Calculates the global pose of different point on the robot structure from joint angles
    ///
    /// @param joint_angles The vector of joint angles
    /// @param dh_frame_index The FAP number on the structure
    /// @returns Affine transformation from panda_link0 to the FAP
    Eigen::Affine3d getGlobalTransform(std::vector<double> joint_angles, int dh_frame_index) const;

    /// @brief Calculates rotation vectors from cartesian paths
    ///
    /// @param cartesian_paths The Cartesian paths for the desired FAPs
    /// @param obstacle_id The obstacle ID
    /// @param rot_vec_calc_radius The radius for the entry and exit point calculation
    /// @returns Vector of rotation vectors for all FAPs
    RotationVectorBundle getRotationVectorsFromPaths(std::vector<EigenSTL::vector_Vector3d>& cartesian_paths,
                                                     float obstacle_id, double rot_vec_calc_radius);

    /// @brief Calculates cartesian paths from joint space trajectories
    ///
    /// @param trajectory_msg The joint space trajectory
    /// @returns Vector of Cartesian paths for each FAP
    std::vector<EigenSTL::vector_Vector3d> getCartesianPathsFromTrajectory(
        moveit_msgs::RobotTrajectory& trajectory_msg);

    /// @brief Creates a joint space trajectory using sampling based planning.
    ///
    /// @param planning_time The planning time in seconds to optimize the trajectory. Default is 1 s.
    /// @param goal_tolerance The goal tolerance. Default is 0.1.
    /// @returns The planned joint space trajectory
    moveit_msgs::RobotTrajectory plan(const std::string& planner_type, const double planning_time = 1,
                                      const double goal_tolerance = 0.1);

    /// @brief Get the number of FAPs
    ///
    /// @returns The number of FAPs
    std::vector<int> getFAPIds() { return fap_ids_; }

    /// @brief Set the number of FAPs
    ///
    /// @param fap_count The number of FAPs
    void setFAPIds(std::vector<int>& fap_ids) { fap_ids_ = fap_ids; }

    /// @brief Get the ids (intensity) of the different obstacles.
    ///
    /// @returns The a vector of obstacles ids
    std::vector<float> getObstacleIds();

    /// @brief Get the obstacle pointcloud.
    void pointCloudCallback(const pcl::PointCloud<pcl::PointXYZI>::Ptr& cloud_msg);

    /// @brief Get the goal pose.
    void goalPoseCallback(const geometry_msgs::PoseStamped& latest_goal_pose_msg);

    /// @brief Set dh params
    ///
    void setDHParams(std::vector<double>& dh_a, std::vector<double>& dh_d, std::vector<double>& dh_alpha,
                     std::vector<double>& dh_theta, std::vector<int>& joint_types) {
        dh_a_ = dh_a;
        dh_d_ = dh_d;
        dh_alpha_ = dh_alpha;
        dh_theta_ = dh_theta;
        joint_types_ = joint_types;
    }

    /// @brief Publish the rotation vector array
    void publishRotVecs(cf_pre_planner_msgs::RotVecArrayObstacles& msg);

    /// @brief Check for pointcloud initialization
    bool received_pointcloud = false;

    /// @brief Check for goal pose initialization
    bool received_goal_pose = false;

private:
    /// @brief ROS node handle
    ros::NodeHandle* n_;

    /// @brief Subscriber to the obstacle pointcloud
    ros::Subscriber obstacle_sub;

    /// @brief Subscriber to the goal
    ros::Subscriber goal_pose_sub;

    /// @brief Publisher for the rotation vectors
    ros::Publisher rot_vec_pub;

    /// @brief The ids of active FAP frames
    std::vector<int> fap_ids_;

    /// @brief The planning group string
    const std::string PLANNING_GROUP_ = "panda_arm";

    /// @brief The robot description string
    const std::string ROBOT_DESCRIPTION_ = "robot_description";

    /// @brief Move group interface
    std::shared_ptr<moveit::planning_interface::MoveGroupInterface> move_group_interface_;

    /// @brief Map of KD trees for each obstacle
    std::unordered_map<float, pcl::KdTreeFLANN<pcl::PointXYZI>> obstacle_kd_trees_;

    /// @brief Goal pose for the planner
    geometry_msgs::PoseStamped goal_pose_;

    /// @brief Plan
    moveit::planning_interface::MoveGroupInterface::Plan plan_;

    /// @brief Visual tools for rviz
    // moveit_visual_tools::MoveItVisualTools visual_tools_ = moveit_visual_tools::MoveItVisualTools("panda_link0");

    /// @brief KDL chain for the panda robot
    KDL::Chain franka_chain_;

    /// @brief Point cloud pointer for representation of all obstacles
    // boost is used here because pcl does not use std::shared:ptr
    boost::shared_ptr<pcl::PointCloud<pcl::PointXYZI>> cloud_ptr_ =
        boost::make_shared<pcl::PointCloud<pcl::PointXYZI>>();

    /// @brief Map of point cloud pointers for individual obstacle representation
    std::unordered_map<float, boost::shared_ptr<pcl::PointCloud<pcl::PointXYZI>>> obstacle_pointclouds_;

    /// @brief Goal pose for planning
    geometry_msgs::PoseStamped goal_pose;

    /// @brief DH param for the panda robot
    std::vector<double> dh_a_;

    /// @brief DH param for the panda robot
    std::vector<double> dh_d_;

    /// @brief DH param for the panda robot
    std::vector<double> dh_alpha_;

    /// @brief DH param for the panda robot
    std::vector<double> dh_theta_;

    /// @brief DH param for the panda robot
    std::vector<int> joint_types_;
};
