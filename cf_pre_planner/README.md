# cf_pre_planner

This package contains the pre-planning module for the cf-planner.
It calculates rotation vector guesses based on global planners from MoveIT! which are frequently sent to the cf-planner.