#ifndef ROBOT_STATE_H
#define ROBOT_STATE_H

#include <Eigen/Core>
#include <Eigen/StdVector>

/// @brief Parameter structure of robot data
///
/// Most parameters in this data structure can be set through rosparam at the start of the planner. See example.yaml.
/// However, there are some parameters that the CFPlanner maintains automatically.
/// If they can not be set through rosparam, this is described in their detailed documentation.
struct CFRobotState {
    CFRobotState() {
        joint_positions.setZero();
        joint_velocities.setZero();
        joint_velocities_null.setZero();
        score_min_manipulability = 1.0;
        score_joint_torques_limit = 0.0;
        score_joint_torques_total = 0.0;
        fap_jacobians = {Matrix6x7::Zero()};
        fap_poses = {Eigen::Affine3d::Identity()};
        fap_velocities = {Vector6d::Zero()};
    }

    using Vector6d = Eigen::Matrix<double, 6, 1>;
    using Vector7d = Eigen::Matrix<double, 7, 1>;
    using Matrix6x7 = Eigen::Matrix<double, 6, 7>;

    /// @brief Latest known fap poses
    std::vector<Eigen::Affine3d> fap_poses;

    /// @brief Latest known fap velocities
    std::vector<Vector6d> fap_velocities;

    /// @brief Latest known joint positions
    Vector7d joint_positions;

    /// @brief Latest known total joint velocities
    Vector7d joint_velocities;

    /// @brief Latest known null space joint velocities
    Vector7d joint_velocities_null;

    /// @brief Helper variable for the agent jla score calculation
    double score_joint_torques_limit;

    /// @brief Helper variable for the agent joint manipulability score calculation
    double score_min_manipulability;

    /// @brief Helper variable for the agent score calculation
    double score_joint_torques_total;

    /// @brief Jacobian matrices w.r.t the force application points
    std::vector<Matrix6x7> fap_jacobians;

    /// @brief Get the latest EE Pose.
    ///
    /// @return The latest EE Pose.
    Eigen::Affine3d getEEPose() const { return fap_poses.back(); }

    /// @brief Get the latest EE Velocity.
    ///
    /// @return The latest EE Velocity.
    Vector6d getEEVel() const { return fap_velocities.back(); }

    /// @brief Get the latest EE Jacobian.
    ///
    /// @return The latest EE Jacobian.
    Matrix6x7 getEEJac() const { return fap_jacobians.back(); }
};

#endif