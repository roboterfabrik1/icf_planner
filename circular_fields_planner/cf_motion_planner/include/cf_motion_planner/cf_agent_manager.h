#include <XmlRpcValue.h>

#include <Eigen/Core>
#include <QFutureWatcher>
#include <QObject>
#include <QThread>
#include <QtConcurrent>
#include <memory>
#include <utility>

#include "cf_motion_planner/cf_agent.h"
#include "cf_motion_planner/cf_agent_manager_ros_interface.h"
#include "cf_motion_planner/cf_planner_settings.h"
#include "cf_motion_planner/cf_types.h"
#include "cf_motion_planner/dynamic_settings.h"
#include "cf_motion_planner/utility.h"
#include "pcl_obstacle_generator/MovingObstacles.h"
#include "ros/ros.h"

#ifndef CF_AGENT_MANAGER_H
#define CF_AGENT_MANAGER_H

/// @brief A parameter struct, that defines the state of the best agent of the planner
///
/// This struct is used to store the measured real robot data that is signaled by the CFPlanner (pose, velocity, goal)
/// or the resulting data of the Prediction in CFAgentManager that is signaled to the CFPlanner
/// (settings, score, expected_trajectory)
///
/// It can be thought of as a clone of the state of the real robot. However, due to the way the data processing takes
/// places, it can not be guaranteed that the parameters in this struct are for any given time the same as the
/// real robot parameters. For example, the update of the pose for the real robot is usually done with a
/// much higher frequency than the signaling of a new pose to the CFAgentManager.
struct BestAgentData {
    /// @brief Desired Goal of the best agent
    Eigen::Affine3d goal_pose;

    /// @brief Prediction Score of the given settings
    double prediction_score;

    /// @brief Latest known state of the real robot
    CFRobotState latest_robot_state;

    /// @brief Agent settings, that the best agent is currently using
    CFAgentSettings settings;

    /// @brief Expected Trajectory of the best agent for the given settings
    AbstractRobotDynamicHistory expected_trajectory;
};

/// @brief Object that manages all predictive agents and their exploration for a best solution
///
/// This CFAgentManager is a central piece for the overall framework. It manages the search for a best parameter
/// set that can be used for the real robot representation, which calculates the force reaction for the robot.
///
/// In order to not interfere with or block the calculation of the real robot representation, the CFAgentManager can and
/// should run in its own thread with its own event loop. The synchronisation with the CFPlanner and the its real robot
/// representation is done via Qts signal and slot system. Please see https://doc.qt.io/qt-5/threads-qobject.html and
/// https://doc.qt.io/qt-5/signalsandslots.html for further information.
///
/// The CFAgentManager receives updates for the real robots position and velocity, a new goal
/// and new obstacle geometry from the CFPlanner. In return it only signals the planner, that it found a new best
/// parameter set for the latest situation, which the real robot representation should use.
///
/// For better debugging of the behaviour the planner can also initialize its own CFAgentManagerROSInterface. This
/// interface runs in the same thread as the CFAgentManager and does therefore not influence the CFPlanner. It should
/// only be used to publish debug data. Do not try to read real robot representation data through this interface. Always
/// fetch the data through the main ros interface inside CFPlanner.
///
/// To explore the task space and find a best parameter set for the current situation, the CFAgentManager uses two
/// components:
///
/// 1. A collection of CFGeometry objects for different obstacle_view_distances.
/// The Agents do not calculate their geometry point cloud data like minimal distances or critical vectors themself.
/// Instead they use a CFGeometry object that calculates this data for them and that wraps and processes the point cloud
/// data that is received. Due to the way this processing takes place, there has to be a geometry object
/// for each obstacle_limit_distance one agent could pursue. However, these different geometry objects are maintained by
/// this CFAgentManager class and only shared between the Agents.
/// Please note that by design it could still be possible in the future that each agent manages its own geometry object.
///
/// 2. Predictive Agents
/// One of the main tasks that is handled by this class is the creation and maintenance of the predictive agents.
/// Instead of using a regular control loop approach to start and stop predictive agents, a reactive approach is used.
/// Most interaction is therefore handled reactively through Qts signal and slot mechanism.
///
/// predictive_agents_ is a vector of size settings.max_agents that pairs a smart pointer to an agent with a
/// QFutureWatcher. A predictive agent is created only as an element of this vector and its prediction is executed via
/// QtConcurrent::run(). This API call will execute the prediction function in another thread of the applications
/// default thread pool if a thread is available. Otherwise it will queue the execution of the prediction until a
/// thread becomes available.
///
/// In a normal use case it is common that there are more predictive agents than there are
/// threads to execute them in. To allow approximately evenly distributed computation of all agents, the prediction
/// function of an agent returns to the main thread of CFAgentManager after a fixed amount of prediction steps.
/// The return will release the agents thread to be used with another agent.
/// If the agent still needs to be computed (goal not reached), it will be restarted and queued up for
/// thread execution again.
///
/// The QFutureWatcher in an element of predictive_agents_ watches the QFuture of the agents execution in
/// QtConcurrent::run(). It will automatically trigger the function managePrediction() in the main thread of
/// CFAgentManager as a reaction to a predictive agent returning to the main thread of CFAgentManager.
/// This function will - based on the return status - decide, what to do with the returned agent (Restart? Delete?).
///
/// Each agent will send signals from the prediction thread like sgnSplit() or sgnEvolve(). The signals contain a
/// snapshot of the agents settings and parameters. Since the signals are only connected to the according slots via a
/// Qt::QueuedConnection, the slots are not executed concurrently. Instead they are only processed in
/// the main thread of CFAgentManager (Please see https://doc.qt.io/qt-5/threads-qobject.html).
/// As a result a new agent is not created immediately, but only when the event loop of the
/// main thread of CFAgentManager is serviced.
///
///
/// With a call to slotValidatePrediction() the real robot representation inside CFPlanner and the representation
/// inside CFAgentManager are regularly synchronized. Furthermore, the expected best agents trajectory is validated. If
/// it does invalidate (or there is no expected trajectory, yet), the predictions are restarted. This is also the case,
/// if no predictions are running anymore.
class CFAgentManager : public QObject {
    Q_OBJECT
    using PredictiveAgentPtr = std::unique_ptr<CFAgent>;
    using AgentWatcher = QFutureWatcher<CFAgent::PredictionExitStatus>;
    using AgentWatcherPtr = std::unique_ptr<AgentWatcher>;

    /// @brief Thread to run the manager in
    QThread* manager_thread_;

    /// @brief Ros Interface to publish prediction visualization
    CFAgentManagerROSInterface* ros_interface_;

    /// @brief Settings of the planner
    CFPlannerSettings settings_;

    /// @brief Default agent settings if an agent is freshly created
    CFAgentSettings default_agent_settings_;

    /// @brief Dynamic settings used for all agents
    std::shared_ptr<AbstractRobotDynamicSettings> default_dynamic_settings_;

    /// @brief Lookup objects for all geometric calculations
    std::vector<std::shared_ptr<CFGeometry>> geometry_collection_;

    /// @brief Bool flag, if expected_best_agent_trajectory_ is valid or was already invalidated.
    ///
    /// Only if this flag is true, the invalidation reaction process is started inside calculateForceReaction()
    bool expected_best_agent_trajectory_is_valid_ = false;

    /// @brief State of the best agent
    BestAgentData best_agent_;

    /// @brief Vector with all predictive agents
    ///
    /// This vector contains the unique pointers to currently active predictive agents that are paired with
    /// a QFutureWatcher for the future, that is generated by the QtConcurrent::run call for this agent.
    ///
    /// Please note, that if an element contains the nullptr it is not actively used by an agent and can therefore be
    /// used for a generation of a new agent. The size of this vector is settings.max_agents. If no element is a
    /// nullptr, the maximum number of agents is reached.
    ///
    /// @attention Do not append an element to this vector expect in initializePredictiveAgentsVector().
    /// Treat it as an std::array with fixed size.
    std::vector<std::pair<PredictiveAgentPtr, AgentWatcherPtr>> predictive_agents_;

    /// @brief Counter that increments for each agent, that is created by the planner to generated unique agent ids.
    int last_agent_id_ = 0;

    /// @brief Step number for the latest step
    ///
    /// Could technically overflow. However due to uint64_t datatype very unlikely for most testcases.
    uint64_t time_step_ = 0;

    /// @brief Countdown of bets that need to be received, until new parameter settings are signaled to real real robot
    /// representation
    ///
    /// This counter will be reset, if the agent predictions are restarted due to an invalidation.
    int received_bets_countdown_ = 0;

    /// @brief The minimum agent id, that is accepted, to decrement the received_bets_countdown_
    int minimum_agent_id_for_bets_countdown_ = 0;

    /// @brief Flag, if the received bets are the starting bets and the received_bets_countdown_ should be set to 0.
    bool receive_direct_starting_bets_ = true;

    /// @brief Step number for duration reference time step
    ///
    /// If prediction with agents are restarted after the robot moved, they still need to use the same point in time
    /// to calculate their duration. Otherwise new predictions would always have a shorter duration (because they are
    /// started later) and therefore higher score, even if the "real" score would not be better than a previous agent.
    ///
    /// The reference time step is usually reset, if the best_agent is invalidated.
    uint64_t duration_reference_time_step_ = 0;

    /// @brief Initialize predictive_agents_
    ///
    /// Sets up the AgentWatcher callbacks with managePrediction and initializes the agent pointers with nullptr.
    void initializePredictiveAgentsVector(int max_agents);

    /// @brief Manage the agent at index of predictive_agents_
    ///
    /// This function restarts the prediction, if concurrent run returns max_prediction_steps_reached or precalculation
    /// finished.
    /// It deletes the agents for any other return status.
    ///
    /// @param index Which element in predictive_agents_ triggered this callback and needs to be managed.
    void managePrediction(int index);

    /// @brief Utility function to generate a predictive agent for an element of predictive_agents_
    ///
    /// This function creates a new agent with the given settings and history in the given element of predictive_agents_
    /// It increments last_agent_id_ to give the new agent a unique id and connects all signals and slots as required.
    /// It sets the goal pose to the real_robot_ goal_pose.
    ///
    /// @attention This function does not check, if the predictive_agent_element is already in use.
    ///
    /// @param predictive_agent_element Reference to the element in predictive_agents_, that should be modified
    /// @param settings Settings to use for the new agent
    /// @param history History for the new agent
    /// @param line_of_sight_precalculation Should an agent be started that uses calculateLineOfSightShortcuts()
    /// before actually predicting
    void createPredictiveAgent(std::pair<PredictiveAgentPtr, AgentWatcherPtr>& predictive_agent_element,
                               const CFAgentSettings& settings, const AbstractRobotDynamicHistory& history,
                               bool line_of_sight_precalculation = false);

    /// @brief Returns true if a least one predictive agent is running its prediction. False otherwise.
    bool isBusy() const;

    /// @brief Fill out a new history with the latest real robot data and time step
    ///
    /// This utility function initializes an expected empty history with the
    /// - duration_reference_time_step_
    /// - current time step
    /// - latest_ee_pose
    /// - latest_ee_velocity
    ///
    /// @param history History to initialize. Expected to be empty and freshly created.
    void initializeCleanHistoryBasedOnRealRobot(AbstractRobotDynamicHistory& history) const;

    /// @brief Try to restart the predictions
    ///
    /// This function tries to find an empty slot in predictive_agents_ and generate two new agents at the current
    /// real robot position. It will initialize the first new agent with the current real robot settings if the best agent is invalid
    /// the second new agent is initalized with the real robot settings but the default calculation method as if the best agent would be valid.
    ///
    /// If the best agent is valid, only one agent will be generated.
    void restartPredictions();

    /// @brief Get a pointer to the geometry inside geometry_collection_ that has obstacle_limit_distance
    ///
    /// @returns Copy of element or nullptr if no match.
    std::shared_ptr<CFGeometry> getGeometryForObstacleLimitDistance(double obstacle_limit_distance) const;

    /// @brief Check, if the best agents trajectory does collied with an obstacle due to changed obstacle data.
    ///
    /// For every timed point of the best agents trajectory a critical distance query is conducted with the
    /// latest data. If the distance is once lesser than best_agent_.settings.collision_distance true is returned.
    /// Otherwise false is returned.
    ///
    /// This utility function is used inside slotValidatePrediction() regularly in order to allow for early invalidation
    /// of the existing best trajectory.
    ///
    /// Please note, that an invalidation of the best agent with this function does not necessarily mean, that the
    /// used parameter set is not the best, anymore. It just does register, that the prediction on the old data does
    /// not match the prediction on new obstacle data.
    ///
    /// @returns true, if best agent trajectory is colliding, false otherwise.
    bool checkExpectedBestAgentTrajectoryForCollision() const;

public:
    /// @brief Constructor
    ///
    /// @param manager_thread Thread to run the manager in.
    /// @param planner_settings Settings for this planner
    /// @param agent_settings Agent settings for start of the simulation
    /// @param robot_dynamic_settings Settings for a robot dynamic to use for force reaction and prediction calculation
    CFAgentManager(QThread* manager_thread, CFPlannerSettings& planner_settings, const CFAgentSettings& agent_settings,
                   const std::shared_ptr<AbstractRobotDynamicSettings>& dynamic_settings);

    /// @brief Trigger deletion of ros_interface
    ~CFAgentManager() override;

    /// @brief Utility function that calculates the new smoothing strategy to use while smoothing and the index
    ///
    /// This function implements the behaviour, that is described by CFPlannerSettings::smoothing_strategy
    ///
    /// This function could be private and non static. However, it is public and static for easier unit testing.
    ///
    /// @param smoothing_strategies Smoothing strategy vector of an agent manager (settings_.smoothing_strategy)
    /// @param previous_smoothing_strategy_command_index Index in settings_.smoothing_strategy of the previous strategy
    /// @param smoothing_strategy_reference_score Reference score of the latest agent to test improvement against
    /// @param latest_score Score of the latest agent
    ///
    /// @returns std::pair<new_strategy, new_smoothing_strategy_command_index>:
    /// new_strategy is a coded strategy as described by CFPlannerSettings::smoothing_strategy,
    /// new_smoothing_strategy_command_index is the index that is currently used in smoothing_strategy or -1 if invalid
    static std::pair<SmoothingStrategy, int> determineSmoothingStrategy(
        const std::vector<SmoothingStrategy>& smoothing_strategies, int previous_smoothing_strategy_command_index,
        double smoothing_strategy_reference_score, double latest_score);

    /// @brief Calculate a new rotation vector that is rotated around an objects normal
    ///
    /// Please see Becker21 page 4 for a general description of the algorithm used. Please note that the
    /// rotation_axis is generalized in this implementation.
    ///
    /// Agent numbering of new agents starts at 0.
    /// Example for a rotation around the x axis with total_number_of_new_agents = 3:
    /// for_agent_number = 0 => 90° rotation of original_rotation_vector
    /// for_agent_number = 1 => 180° rotation of original_rotation_vector
    /// for_agent_number = 2 => 270° rotation of original_rotation_vector
    ///
    /// @param for_agent_number Iteration from 0 to total_number_of_new_agents-1
    /// @param total_number_of_new_agents Total number of new agents rotations are calculated for.
    /// @param original_rotation_vector The vector to rotate around rotation_axis
    /// @param rotation_axis The axis to rotate original_rotation_vector around.
    ///
    /// @return Eigen::Vector3d Returns calculated rotated rotation vector
    static Eigen::Vector3d calculateRotatedRotationVector(int for_agent_number, int total_number_of_new_agents,
                                                          const Eigen::Vector3d& original_rotation_vector,
                                                          Eigen::Vector3d rotation_axis);

public slots:  // NOLINT(readability-redundant-access-specifiers) : Slot access specifier

    /// @brief If possible, create one or more new agents with given settings and history
    ///
    /// The rotation vectors for the new agent for the last added object will be changed
    ///
    /// @attention Please note that for a 3D Agent the rotation axis used to calculate the rotated rotation vector for a
    /// splitted agent differs from Becker21. Becker21 uses the normal of the nearest point of the new object.
    /// This implementation uses the latest velocity (or if vel=0 the vector to the goal).
    /// Therefore, the rotated rotation vector also stands perpendicular to the latest velocity.
    ///
    /// @param settings Settings to use for new agents. Latest rotation vector will be altered.
    /// @param history Agent history till the split point to clone.
    /// @param rotation_vectors The set of rotation vectors for the new agent.
    void slotCreateAgent(const CFAgentSettings& settings, const RotationVectorMapVector& rotation_vectors);

    /// @brief If possible, create on or more new agents with given settings, starting at real pose
    ///
    /// A new agent with the next obstacle distance inside settings_.allowed_obstacle_limit_distances will be created.
    /// This basic behaviour is planned to be replaced by a more advanced behaviour in the future.
    /// @attention Is not implemented for robotic manipulator and only works for point mass robots. Using this functionality requires significant adaptions of this version.
    void slotEvolveAgent(const CFAgentSettings& settings);

    /// @brief If possible, create on or more new agents but using the trace of the previous agent as a moving goal
    ///
    /// If an agent reached the goal it usually signals the agent manager, that it would like to be smoothed. This
    /// slot creates depending on settings_.smoothing_strategy a new agent that will be smoothed.
    /// Two modes of smoothing are possible:
    ///
    /// - SmoothingStrategy::TIME_OFFSET uses the previous history as a moving goal. As a result, the new agent tries to
    ///   follow the by n steps into the future transformed trajectory of the old agent. This operation is best used to
    ///   gain small improvements on the trajectory e.g. rounding a sharp corner.
    ///
    /// - SmoothingStrategy::LINE_OF_SIGHT tries to calculate intermediate goals based on the previous history that are
    ///   in the direct line of sight of the current robot pose. This operation is best used to make big shortcuts and
    ///   react early if an obstacle would be discovered later. Please see CFAgent::calculateLineOfSightShortcuts() for
    ///   a detailed explanation. Please also note that a new agent with this smoothing mode is not started directly
    ///   with CFAgent::predict() but with CFAgent::calculateLineOfSightShortcuts() which will then trigger
    ///   CFAgent::predict() if it returns once to the agent manager.
    ///
    /// Smoothing is an expensive operation. It should only be allowed for the most promising candidates.
    /// Therefore, before each smoothing is started, the score of the best agent in its non smoothed version is compared
    /// to score of the non smoothed version of the agent that request smoothing. The smoothing is then only performed
    /// if best_agent.non_smoothed_reference_score <= this_agent.non_smoothed_reference_score *
    /// allow_smoothing_delta_score_threshold_factor
    ///
    /// @param settings Settings to use for new agents.
    /// @param history Agent history to be used to generate the moving goal
    /// @param score Score of the given history
    /// @attention Is not implemented for robotic manipulator and only works for point mass robots. Using this functionality requires significant adaptions of this version.
    void slotSmoothAgent(const CFAgentSettings& settings, const AbstractRobotDynamicHistory& history, double score);

    /// @brief Receive a bet for the best agent settings and change the settings, if it actually has a higher score
    ///
    /// ... and is still a valid trajectory.
    ///
    /// This function will check if an bet is better than the latest best agent parameters and signal the new
    /// parameters to the real robot representation in CFPlanner.
    ///
    /// However, if the prediction of the best agent is invalidated, the real robot representation parameters should
    /// only be changed, if at least minimum_bet_number_before_invalid_settings_change bets were received. This should
    /// be done in order to avoid following a suboptimal trajectory, just because it was the first bet received after an
    /// invalidation.
    ///
    /// Please note that the underlying behaviour also depends on
    /// only_consider_goal_reached_for_invalid_settings_change_bet_number. If this is true, only bets are counted
    /// towards this setting, that reached the goal. Otherwise all freshly generated agents after invalidation are
    /// counted.
    void slotReceiveBet(double score, const CFAgentSettings& settings, const AbstractRobotDynamicHistory& history);

    /// @brief Check, if the expected_best_agent_trajectory_ is the real robots behaviour & start new predictions if
    /// not.
    ///
    /// Furthermore checks, if the prediction is idling / no prediction is running and restarts it the same way.
    /// Also checks regularly, every n-th cycle, if the best agents trajectory does collide with obstacles in the
    /// future, due to a possible change in the obstacle data.
    ///
    /// In a first step updates best_agent_.latest_ee_pose and best_agent_.latest_ee_velocity and increments time_step_.
    /// Only checks and reacts, if expected_best_agent_trajectory_is_valid_
    /// Otherwise returns immediately.
    ///
    /// Reaction to a deviation is a call to restartPredictions() which will create 2 new agents:
    /// 0. The latest best agent settings with the moving goal predicted from the real pose and velocity.
    /// 1. The latest best agent settings predicted from the real pose and velocity.
    ///
    /// It will only be created if it has non default rotation vectors in it.
    ///
    /// If agents are restarted because of a deviation, the minimum_agent_id_for_bets_countdown_ is also initialized.
    ///
    /// Furthermore, if the agents are restarting because of idling and there was previous not any agent, that found
    /// a way to the goal, the allowed_obstacle_limit_distances are rotated in order to use an alternate goal distance
    /// as the standard setting. This is for example necessary, if the trap system moves in the direction of the point
    /// mass. For a to small obstacle_view_distance, the agent can nor escape the trap anymore.
    ///
    /// @attention Please note that there will be no reaction, if the maximum number of agents is already exceeded.
    ///
    void slotValidatePrediction(const CFRobotState& robot_state);

    /// @brief Update the goal pose of best_agent_ & trigger validation through expected_best_agent_trajectory_is_valid_
    ///
    /// Also set receive_direct_starting_bets_ to true;
    ///
    /// @param goal_pose New ee goal pose to pursue
    void slotChangeBestAgentGoalPose(const Eigen::Affine3d& goal_pose);

    /// @brief Set the new point cloud data to all geometry objects in geometry_collection_
    ///
    /// @param cloud New obstacle cloud data with points, object ids (as intensity) and normals
    void slotSetObstacles(const pcl_obstacle_generator::MovingObstacles::Ptr& latest_obstacle_msg) const;

    /// @brief Set the rotation vectors of the initial best agent
    ///
    /// @param rotation_vectors The first set of rotation vectors
    void slotInitRotVec(const RotationVectorMapVector& rotation_vectors);

signals:

    /// @brief Signal that the Agent Manager has found better settings for the best agent
    void sgnBetterBestAgentSettingsFound(const CFAgentSettings& new_settings);

    /// @brief Signal, that a prediction result is ready for broadcasting / visualization
    ///
    /// This signal is simply used as a relay to connect CFAgent::sgn_prediction_result() with
    /// CFPlannerNode::slot_publishPredictionResult()
    ///
    /// @param agent_id Id of the agent
    /// @param score Latest score of this prediction trajectory
    /// @param history Trajectory history of the dynamic
    void sgnPredictionResult(int agent_id, double score, const AbstractRobotDynamicHistory& history);

    /// @brief Mark the given position in rviz
    ///
    /// Used for debugging only.
    void sgnMarkPosition(const Eigen::Vector3d& position, DSColor color);
};
#endif