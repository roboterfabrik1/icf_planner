#ifndef CF_ROBOT_CALCULATIONS_H
#define CF_ROBOT_CALCULATIONS_H

#include <eigen_conversions/eigen_msg.h>
#include <nav_msgs/Path.h>
#include <sensor_msgs/JointState.h>
#include <tf2/LinearMath/Quaternion.h>

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <Eigen/StdVector>
#include <boost/algorithm/clamp.hpp>
#include <iostream>

#include "cf_motion_planner/cf_agent_settings.h"
#include "cf_motion_planner/kdl_dynamics.h"
#include "cf_motion_planner/robot_state.h"
#include "eigen_conversions/eigen_kdl.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/Wrench.h"
#include "geometry_msgs/WrenchStamped.h"
#include "ros/ros.h"
#include "visualization_msgs/Marker.h"

using Vector6d = Eigen::Matrix<double, 6, 1>;
using Vector7d = Eigen::Matrix<double, 7, 1>;
using Matrix6x7 = Eigen::Matrix<double, 6, 7>;

/// @brief Class for calculating localization and velocity of a robot receiving virtual forces.
///
///
/// Please see the documentation of the member variables with the same name.
///
/// The main calculation of this node can be found in publishDynamics().
class CFRobotCalculations {
    /// @brief KDL chain
    RobotKDL chainKDL_;

    /// @name Simulation parameters
    /// These parameters are configurable via rosparam and are loaded in getParams()
    /// @{
    /// @brief Step time in seconds
    double step_time_;  // NOLINT

    /// @brief Max velocity cap for robot in m/s
    double max_vel_ = 0.5;  // NOLINT

    /// @}

    /// @brief The goal pose of the planner
    Eigen::Affine3d goal_pose_;

    /// @brief Vector containing the lower joint limits of the robot
    std::vector<double> q_min_;

    /// @brief Vector containing the upper joint limits of the robot
    std::vector<double> q_max_;

    /// @brief Vector containing the joint velocity limits of the robot
    std::vector<double> qd_max_;

    /// @brief Vector containing the joint acceleration limits of the robot
    std::vector<double> qdd_max_;

    /// @brief Calculates joint torques towards a configuration with higher manipulability
    ///
    /// The torque is determined by the gradient of the manipulability index
    ///
    /// @param robot_state Current robot state
    ///
    /// @returns A Vector of joint torques towards a configuration with a high manipulability
    Vector7d calculateManipulabilityOptimization(const CFRobotState& robot_state);

    /// @brief Calculates avoidance torques acting on the joints if they approach their predefined limits
    ///
    /// @param joint_pos Current joint positions
    /// @param joint_vel Vector of joint velocities
    /// @param agent_settings Current agent settings
    ///
    /// @returns A vector containing the avoidance torques
    Vector7d calculateJointLimitAvoidance(const Vector7d& joint_pos, const Vector7d& joint_vel,
                                                const CFAgentSettings& agent_settings);

    /// @brief Build the jacobian based on joint positions
    ///
    /// @param robot_state The current robot state
    /// @param agent_settings The current agent settings
    ///
    /// @returns A 6x7 Jacobian matrix at the current joint angles
    void buildFAPJacobians(CFRobotState& robot_state, const CFAgentSettings& agent_settings);

    /// @brief Calculates the nullspace projector for the EE Jacobian matrix
    /// @attention Use the version which takes the pseudo inverse as input
    /// parameter if the pseudo inverse was already calculated to save
    /// computational resources
    ///
    /// @param jacobian The Jacobian matrix
    /// @param jacobian The pseudo inverse of Jacobian matrix
    ///
    /// @returns The pseudoinverse
    Eigen::MatrixXd calculateVelocityNullspaceProjector(
        const Eigen::MatrixXd& jacobian,
        const Eigen::MatrixXd& jacobian_pinv) const;

    /// @brief Calculates the nullspace projector for a given Jacobian matrix
    /// @attention Use the version which takes the pseudo inverse as input
    /// parameter if the pseudo inverse was already calculated to save
    /// computational resources
    ///
    /// @param jacobian The Jacobian matrix
    ///
    /// @returns The pseudoinverse
    Eigen::MatrixXd calculateVelocityNullspaceProjector(
        const Eigen::MatrixXd& jacobian) const;

    /// @brief Calculates the nullspace projector for a given FAP Jacobian matrix
    ///
    /// @param jacobian The Jacobian matrix
    /// @param jacobian The pseudo inverse of Jacobian matrix
    ///
    /// @returns The pseudoinverse
    Eigen::MatrixXd calculateVelocityNullspaceProjectorFAP(
        const Eigen::MatrixXd& jacobian,
        const Eigen::MatrixXd& jacobian_pinv, 
        CFRobotState& robot_state) const;
    
    /// @brief Calculates the Moore Penrose pseudoinverse
    ///
    /// @param jacobian The jacobian matrix to invert
    ///
    /// @returns The pseudoinverse
    Eigen::MatrixXd calculateMoorePenroseInverse(
        const Eigen::MatrixXd& jacobian, int mode = 1) const;

    /// @brief Calculates the inverse of a matrix using different methods
    /// 
    /// Default method uses SVD
    ///
    /// @param jacobian The matrix to invert
    /// @param large_threshold Boolean wether to use a large or small threshold
    /// for eliminating small singular values
    /// @param mode Defines the calculation method
    ///
    /// @returns The inverse
    Eigen::MatrixXd calculateMatrixInverse(const Eigen::MatrixXd& matrix, bool large_threshold = true,
                                           int mode = 1) const;

    /// @brief Calculates resulting rotation from angular velocity and step time
    ///
    /// @param ang_vel A vector of angular velocities
    /// @param delta_t The time for applying the angular velocity
    /// @param approx Boolean for defining if approximate method should be used
    ///
    /// @returns The combined null space torque vector
    Eigen::Quaterniond deltaRotation(const Vector3d& ang_vel,
                                     const double delta_t, bool approx = true);

    /// @brief Calculates the orientational distance to the goal
    ///
    /// Two methods are provided to calculate the rotational distance.
    /// They represent the imaginary delta parts for the quaternion
    /// difference between the two rotations: "Two coordinate systems coincide
    /// if, and only if, delta_q = 0, where delta_q is the vector component of
    /// the quaternion" that describes the relative orientation.
    /// The first method is based on "Closed-loop manipulator control
    /// using quaternion feedback" (Yuan, 1988)
    /// The second method is based on "Resolved-acceleration control of robot
    /// manipulators: A critical review with experiments" (Caccavale et al,
    /// 1998).
    static Eigen::Vector3d calculateOrientationGoalDistance(
        const Eigen::Affine3d& current_pose, const Eigen::Affine3d& goal_pose,
        const CFAgentSettings& agent_settings);

    /// @brief Calculates the total joint velocities based on the last joint
    /// velocities. Is based on desired task accelerations
    ///
    /// @param wrenches A vector containing wrenches (end effector and fap
    /// forces and torques)
    /// @param robot_state Structure contains the entire robot state
    /// @param agent_settings The current agent settings
    void calculateJointVelocityCmd(const std::vector<Vector6d>& wrenches, CFRobotState& robot_state,
                                             const CFAgentSettings& agent_settings);

    /// @brief Calculates the total cartesian velocities for all force application points including the end effector,
    /// based on the joint velocities
    ///
    /// @param robot_state The current robot state
    void calculateFAPVelocities(CFRobotState& robot_state) const;

    /// @brief Calculates the cartesian positions of all joints, i.e. link starting positions
    ///
    /// @param cart_pos Cartesian positions of all segment start positions
    /// @param joint_pos Joint angles
    /// @param agent_settings The current agent settings
    void calculateCartesianPositions(std::vector<Eigen::Affine3d>& cart_pos, const Vector7d& joint_pos,
                                     const CFAgentSettings& agent_settings);

    /// @brief Calculates the total cartesian positions for all force application points including the end effector,
    /// based on the joint positions
    ///
    /// @param robot_state The entire robot state
    /// @param agent_settings The current agent settings
    void calculateTotalPositions(CFRobotState& robot_state, const CFAgentSettings& agent_settings);

    /// @}

public:
    /// @brief Construct a new Robot Dynamics object
    ///
    CFRobotCalculations();

    /// @brief Set goal pose
    ///
    /// @param goal_pose_msg The goal pose
    void setGoalPose(geometry_msgs::PoseStamped& goal_pose_msg);

    /// @brief Set all the required parameters for a class object
    ///
    /// @param step_time The step time in seconds
    /// @param joints_lower The lower joint position limits
    /// @param joints_upper The upper joint position limits
    /// @param joints_max_vel The upper joint velocity limits
    /// @param joints_max_acc The upper joint acceleration limits
    void setParams(const double& step_time, const std::vector<double>& joints_lower,
                   const std::vector<double>& joints_upper, const std::vector<double>& joints_max_vel,
                   const std::vector<double>& joints_max_acc);

    /// @brief Build the robot's kinematic chain using the KDL library and DH parameters
    ///
    /// @param joint_types Types of joints to be added (0 = Fixed, 1 = RotZ)
    /// @param a A Vector containing the DH parameter "a"
    /// @param alpha A Vector containing the DH parameter "alpha"
    /// @param d A Vector containing the DH parameter "d"
    /// @param theta A Vector containing the DH parameter "theta"
    void buildChain(const std::vector<int>& joint_types, const std::vector<double>& a, const std::vector<double>& alpha,
                    const std::vector<double>& d, const std::vector<double>& theta);

    /// @brief Calculate one step of the dynamic.
    ///
    /// This function updates current_vel_ and current_pos_ based upon the incoming wrenches
    ///
    /// @param wrench A vector containing wrenches (end effector and fap forces and torques)
    /// @param robot_state The current robot state
    /// @param agent_settings The current agent settings
    void calculateDynamicStep(const std::vector<Vector6d>& wrench, CFRobotState& robot_state,
                              const CFAgentSettings& agent_settings);

    /// @brief Calculates the end effector pose given the joint positions
    ///
    /// @param q_in Vector containing the current joint positions
    /// @param pos_ee_out Vector containing the end effector positions as output
    void calculatePoseFromJoints(const Vector7d& q_in, std::pair<Eigen::Affine3d, int>& pos_ee_out);
};

#endif  // CF_ROBOT_CALCULATIONS_H
