#include <pcl/point_types.h>
#include <pcl_ros/point_cloud.h>

#ifndef PC_TYPES_H
#define PC_TYPES_H

using PointCloud = pcl::PointCloud<pcl::PointXYZI>;
using PointCloudNormal = pcl::PointCloud<pcl::Normal>;
using PointCloudObject = pcl::PointCloud<pcl::PointXYZINormal>;

/// @brief Data structure that describes a point in R^3 at an optional time_step t
class TimedPoint {
public:
    TimedPoint(){};
    TimedPoint(const Eigen::Vector3d& p) : point(p){};
    boost::optional<uint64_t> time_step = boost::none;
    Eigen::Vector3d point;

    operator Eigen::Vector3d() const { return point; };
};

#endif