#include <boost/algorithm/clamp.hpp>

#include "cf_motion_planner/abstract_robot_dynamic.h"
#include "cf_motion_planner/cf_robot_calculations.h"
#include "cf_motion_planner/dynamic_settings.h"
#include "eigen_conversions/eigen_kdl.h"

#pragma once

/// @brief Simulation of the kinematic chain of a Franka Emika Panda robot. It assumes, that the robot can perfectly
/// follow a given joint state trajectory. This class is used to generate the behaviour of the predictive agents
/// reacting to the CF forces.
///
///
class SimpleRobotDynamic : public AbstractRobotDynamic {
    CFRobotCalculations cf_robot_calculator_;

public:
    /// @brief Constructor with settings to use
    explicit SimpleRobotDynamic(const SimpleRobotDynamicSettings& settings);

    /// @brief Default destructor
    ~SimpleRobotDynamic() override = default;

    /// @name Simulation
    /// @{

    /// @brief Simulate one dynamic step under the influence of a endeffector_wrench in N and Nm
    ///
    /// This function calculates latest_pose_, latest_velocity_, latest_acceleration_,
    /// latest_joint_torques_ and latest_joint_velocities_.
    ///
    /// @param ee_wrench Force to apply to robot end effector in robot_CS
    void calculateStep(const std::vector<Vector6d>& fap_wrenches, const CFAgentSettings& agent_settings) override;

    /// @}
};