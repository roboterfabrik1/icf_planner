#include <eigen_conversions/eigen_msg.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/WrenchStamped.h>
#include <ros/callback_queue.h>
#include <visualization_msgs/MarkerArray.h>

#include <Eigen/Core>
#include <QObject>
#include <QTimer>
#include <boost/range/adaptor/reversed.hpp>
#include <stdexcept>
#include <unordered_map>

#include "cf_motion_planner/abstract_robot_dynamic_history.h"
#include "cf_motion_planner/cf_agent.h"
#include "cf_motion_planner/cf_planner_settings.h"
#include "cf_motion_planner/cf_types.h"
#include "cf_motion_planner/dynamic_settings.h"
#include "cf_planner_msgs/AgentSettings.h"
#include "cf_planner_msgs/ForceApplicationPoints.h"
#include "cf_planner_msgs/RobotState.h"
#include "cf_planner_msgs/WrenchArray.h"
#include "cf_pre_planner_msgs/RotVecArrayObstacles.h"
#include "pcl_obstacle_generator/MovingObstacles.h"
#include "ros/ros.h"
#include "std_msgs/Float32.h"


#ifndef CF_PLANNER_ROS_INTERFACE_H
#define CF_PLANNER_ROS_INTERFACE_H

/// @brief A class that is the interface to ROS of a CfPlanner
///
/// This class is the ros interface for a CfPlanner
/// It delivers all Subscribers, Publishers and Timers for ros interaction
/// and preprocesses messages and parameters.
///
/// Because Qt has its own Event Loop, ros::spin() is not used in this class.
/// Instead the ROScallbackQueue of the node is serviced manually with
/// CFPlannerNode::spinOnce. This member function is called through a QTimer.
///
/// If you are not familiar with QT. A QT app basically implements its own
/// ros::spin() (called Qt Event Loop) to service callbacks between classes
/// within the app. These callbacks are connected via so called signals and
/// slots. If one class emits a signal and this signal is connected to a slot,
/// the call to this slot will be added to the Qt Event Loop. It will be called
/// when the Event Loop/Queue is serviced.
///
/// @see https://doc.qt.io/qt-5/signalsandslots.html
/// on an introduction to Signals and Slots
/// @see https://doc.qt.io/qt-5/threads-qobject.html
/// on an introduction to the QT Event Loop
class CFPlannerROSInterface : public QObject {
    Q_OBJECT

    /// @name Ros Utility
    /// Properties for ros interaction
    /// @{
    /// @brief ROS node handle
    ros::NodeHandle node_handle_;

    /// @brief Subscriber to the robot state
    ros::Subscriber robot_state_subscriber_;

    /// @brief Subscribes to obstacle msgs
    ///
    /// Adds new obstacles to the obstacles list. Initiates a new agent for new obstacles.
    ros::Subscriber obstacle_subscriber_;

    /// @brief Subscribes to new goal poses.
    ros::Subscriber goal_pose_subscriber_;

    /// @brief Subscribes to new rotation vectors.
    ros::Subscriber rotation_vector_subscriber_;

    /// @brief Publisher for the forces acting on the FAP
    ros::Publisher forces_publisher_fap_;

    /// @brief Publisher for the robot settings e.g. turn on null space projection
    ros::Publisher robot_settings_publisher_;

    /// @brief Publisher for the vectors between robot joints to the critical object
    ros::Publisher fap_positions_publisher_;

    /// @brief Publisher for the step time
    ros::Publisher step_time_publisher_;

    /// @brief A Timer to call the ros callback queue and main control loop periodically
    QTimer spin_timer_;

    /// @brief Time interval in which the spin_timer_ fires.
    ///
    /// Will be overwritten by planner_settings.control_loop_cycle_duration * 1000
    int spin_timer_time_ms_;
    /// @}

    /// @brief Status of the initialization.
    ///
    /// This variable is interpreted as a binary unsigned int.
    /// Each bit represents, if at least on message of an input was received.
    /// If not all messages were received, the node does not calculate and publish the force response.
    ///
    /// lsb: Goal message received
    /// bit 1: rotation vector guess received
    /// bit 2: Obstacle cloud received
    /// msb: real robot state received
    unsigned char initialization_status_ = 0;

    /// @brief Initialize the node by starting subscribers and timers.
    ///
    /// The init function is used instead of the constructor, because the timers need a event loop to
    /// be started.
    void init();

    /// @brief Utility function to construct the publishers and subscribers
    ///
    /// Only subscribes if cf_planner_ is not nullptr
    void constructPublishersAndSubscribers();

    /// @brief Get the ROScallbackQueue, call its functions and then calculateAndPublishForceResponse()
    ///
    /// This function is called periodically from the #spin_timer_
    void spinOnce();

    /// @brief Callback for the robot state, that registers, if the state of the real robot changed.
    void robotStateCallback(const cf_planner_msgs::RobotState& latest_robot_state_msg);

    // @brief Callback for goal_pose_subscriber_, that registers, if the goal pose of the real robot changed.
    void goalPoseChangedCallback(const geometry_msgs::PoseStamped& latest_goal_pose_msg);

    // @brief Callback for rotation_vector_subscriber_, that registers, if the rotation vectors has changed.
    void rotationVectorGuessesCallback(const cf_pre_planner_msgs::RotVecArrayObstacles& rot_vec_guesses_msg);

    // @brief Callback for obstacle_subscriber_, that registers, if the obstacle point cloud changed.
    void obstaclesCallback(const pcl_obstacle_generator::MovingObstacles::Ptr& latest_obstacle_msg);

public:
    /// @brief Construct a new CfPlanner object
    CFPlannerROSInterface();

    /// @brief Destruct the planner node, stop the #spin_timer_.
    ~CFPlannerROSInterface() override;

    /// @brief Load the settings structures from the ros parameter server
    ///
    /// Parameters are output only. They will be overriden.
    ///
    /// If multiple types of dynamic_settings are found on the ros parameter server. Only one is used.
    /// However, the order is undefined.
    ///
    /// If no settings for a type are found, the value will be unchanged.
    ///
    /// Technically, this function could be private. However, it has to be public for integration testing.
    ///
    /// @param planner_settings The settings for the CFPlanner
    /// @param agent_settings The settings for the agents
    /// @param dynamic_settings Settings for the dynamics. Please note that this pointer may point to a subclass.
    void loadSettings(CFPlannerSettings& planner_settings, CFAgentSettings& agent_settings,
                      std::shared_ptr<AbstractRobotDynamicSettings>& dynamic_settings);

public slots:  // NOLINT(readability-redundant-access-specifiers) : Slot access specifier

    /// @brief Publish the FAP-Forces to apply to the real robot
    void slotPublishFAPForces(const std::vector<Vector6d>& fap_forces);

    /// @brief Publish the robot settings used by the real robot representation (= best agent settings)
    /// to apply to the real robot
    void slotPublishRealRobotSettings(const CFAgentSettings& real_robot_settings);

    void slotPublishStepTime(float step_time);

signals:

    /// @brief A new robot state was received through ROS
    void sgnNewRobotState(const CFRobotState& robot_state);

    /// @brief A new goal pose was received through ROS
    void sgnNewGoalPose(const Eigen::Affine3d& robot_ee_pose);

    /// @brief A new obstacle msg was received through ROS
    void sgnNewObstacleMsg(const pcl_obstacle_generator::MovingObstacles::Ptr& obstacle_msg);

    // /// @brief A new rotation vectors set msg was received through ROS
    void sgnNewRotVecMsg(const RotationVectorMapVector& rot_vec_map);

    /// @brief Trigger the force reaction calculation after the callback queue was serviced
    void sgnRequestForceReactionCalculation();
};
#endif
