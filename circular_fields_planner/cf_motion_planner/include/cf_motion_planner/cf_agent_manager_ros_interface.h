#include <eigen_conversions/eigen_msg.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/WrenchStamped.h>
#include <ros/callback_queue.h>
#include <visualization_msgs/MarkerArray.h>

#include <Eigen/Core>
#include <QObject>
#include <QThread>
#include <QTimer>
#include <boost/circular_buffer.hpp>
#include <boost/range/adaptor/reversed.hpp>
#include <memory>

#include "cf_motion_planner/abstract_robot_dynamic_history.h"
#include "cf_motion_planner/ds_color.h"
#include "ros/ros.h"

#ifndef CF_AGENT_MANAGER_ROS_INTERFACE_H
#define CF_AGENT_MANAGER_ROS_INTERFACE_H

/// @brief A small ros interface that is an optional part of the CFAgenManager and enables it to publish debug data
///
/// This class creates a small ros interface in the given thread and with a separate callback queue that can be used
/// to publish some debug or visualization data of the CFAgent Manager
///
/// Please see the slots section for information on which data can be published.
class CFAgentManagerROSInterface : public QObject {
    Q_OBJECT

    /// @brief Thread to run the interface in
    QThread* thread_;

    /// @brief A Timer to call the ros callback queue and main control loop periodically
    QTimer* spin_timer_;

    /// @brief Time interval in which the spin_timer_ fires.
    int spin_timer_time_ms_ = 5;

    /// @name Ros Utility
    /// Properties for ros interaction
    /// @{
    /// @brief ROS node handle
    ros::NodeHandle node_handle_;

    /// @brief Unique callback queue to use with the node handle of this node.
    ros::CallbackQueue callback_queue_;

    /// @brief Prediction Publisher to publish prediction trace visualization to rviz
    ros::Publisher prediction_trace_publisher_;

    /// @brief Prediction Publisher to publish best prediction trace visualization to rviz
    ros::Publisher best_prediction_trace_publisher_;

    /// @brief Prediction Publisher to publish prediction label visualization to rviz
    ros::Publisher prediction_label_publisher_;

    /// @brief Prediction Publisher to publish best prediction label visualization to rviz
    ros::Publisher best_prediction_label_publisher_;

    /// @brief Prediction Publisher to publish prediction forces visualization to rviz
    ros::Publisher prediction_forces_publisher_;

    /// @brief Publisher, to visualize points in rviz at which the replanning took place
    ros::Publisher marked_positions_publisher_;
    /// @}

    /// @brief Counter to generate unique ids for the position markers of slotMarkPosition()
    int marked_position_counter_ = 0;

    /// @brief Initialize the node by starting subscribers and timers.
    ///
    /// The init function is used instead of the constructor, because the timers need a event loop to
    /// be started.
    void init();

    /// @brief Utility function to construct the publishers and subscribers
    void constructPublishersAndSubscribers();

    /// @brief Get the local ROScallbackQueue and call its functions
    ///
    /// This function is called periodically from the #spin_timer_
    void spinOnce();

    /// @brief Publish the trace (translation part of poses) of one agent
    ///
    /// Special color for agent_id < 0 a.k.a best agent with special message name best_prediction_trace
    ///
    /// @param agent_id Id of the agent for rviz namespacing
    /// @param states list of robot states with poses to generated the trace from
    /// @param lifetime_s Lifetime of Trace in rviz in seconds
    void publishPredictionTrace(int agent_id, const boost::circular_buffer<CFRobotState>& states,
                                double lifetime_s) const;

    /// @brief Publish the label for a trace of one agent positioned at 2/3 of the trace
    ///
    /// Special color for agent_id < 0 a.k.a best agent with special message name best_prediction_label
    ///
    /// @param agent_id Id of the agent
    /// @param score Score of the agent
    /// @param states list of robot states with poses to generated the trace from
    /// @param lifetime_s Lifetime of Trace in rviz in seconds
    void publishPredictionLabel(int agent_id, double score, const boost::circular_buffer<CFRobotState>& states,
                                double lifetime_s) const;

    /// @brief Publish the forces for each trace point of one agent
    ///
    /// The vectors are anchored at each trace point and point int the direction of the force at this point.
    /// Please note, that the torques of the wrenches are not displayed.
    ///
    /// @attention This function only publishes the result if number_of_forces == number_of_poses || number_of_forces ==
    /// number_of_poses - 1. Otherwise the forces can not be matched to the anchor points points the trace.
    ///
    /// @param agent_id Id of the agent for rviz namespacing
    /// @param states list of robot states with poses to generate the visualization from
    /// @param wrenches ee-wrenches to generate the visualization from
    /// @param lifetime_s Lifetime of Trace in rviz in seconds
    void publishPredictionForces(int agent_id, const boost::circular_buffer<CFRobotState>& states,
                                 const boost::circular_buffer<std::vector<Eigen::Matrix<double, 6, 1>>>& wrenches,
                                 double lifetime_s) const;

public:
    /// @brief Construct a new CfPlanner object, that is run in the given thread
    explicit CFAgentManagerROSInterface(QThread* manager_thread);

    /// @brief Destruct the interface, stop the #spin_timer_.
    ~CFAgentManagerROSInterface() override;

public slots:  // NOLINT(readability-redundant-access-specifiers) : Slot access specifier

    /// @brief Publish the prediction result of an agent as a visualization_msgs::Marker
    ///
    /// This function publishes a visualization_msgs::Marker for visualization of a prediction in rviz:
    /// - position trace
    /// - label at 2/3 of position trace with score and id
    /// - force vector for each position trace point if possible
    ///
    /// Negative agent ids are interpreted as the best agent and are displayed with a special color and published on a
    /// standalone topic for trace and label
    ///
    /// @param agent_id Id of this agent
    /// @param score Latest score of this prediction trajectory
    /// @param history Trajectory history of the dynamic
    void slotPublishPredictionResult(int agent_id, double score, const AbstractRobotDynamicHistory& history);

    /// @brief Mark the given position with a small sphere and the given color
    ///
    /// This slot publishes a visualization_msgs::Marker wit a unique id to mark the position in rviz.
    /// Use different colors to differ different situations in which a position should be marked.
    ///
    /// @param position Position in cartesian space in m to mark
    /// @param color color style to use for the marker
    void slotMarkPosition(const Eigen::Vector3d& position, DSColor color);
};
#endif