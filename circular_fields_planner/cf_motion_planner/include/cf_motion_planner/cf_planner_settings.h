#include <XmlRpcValue.h>

#include "cf_motion_planner/utility.h"
#include "ros/ros.h"

#ifndef CF_PLANNER_SETTINGS_H
#define CF_PLANNER_SETTINGS_H

/// @brief Enum for different possible smoothing strategy states
///
/// Please see CFPlannerSettings::smoothing_strategy for a detailed explanation
enum class SmoothingStrategy { REDO_PREVIOUS_IF_IMPROVED = 0, TIME_OFFSET = 1, LINE_OF_SIGHT = 2, INVALID = -1 };

/// @brief Parameter structure of settings for a CFPlanner and CFAgentManager
///
/// This parameter structure contains settings for a CFPlanner and CFAgentManager object.
/// Although it could technically be splitted into two separate settings objects for the two different classes,
/// this is not done in order to not break the existing settings API, in which CFPlanner and CFAgentManager used to be
/// one class.
///
/// It may be done in the future, if the settings for each class get more complex.
struct CFPlannerSettings {
    /// @brief Periodic time of the main control loop that calculates the force reaction in s
    ///
    /// Please note that this variable is not processed directly inside CFPlanner but inside CFPlannerNode.
    /// Therefore, it will also influence how often the ros callback queue is serviced.
    double control_loop_cycle_duration = 0.01;  // NOLINT(readability-magic-numbers) : Default value initialization

    /// @brief Multiple of control_loop_cycle_duration that is used to set RobotDynamicSettings.step_time
    ///
    /// The trajectories of predictive agents are validated regularly. However, this validation process does not need
    /// to take place every control_loop_cycle. It can be every n-th cycle. As a result the agents also can calculate
    /// their dynamic with a n-times slower step_time. This variable determines this factor.
    int prediction_step_time_multiple = 5;

    /// @brief Maximum Number of Agents
    int max_agents = 5;

    /// @brief Number of agents that are created when new obstacle is detected
    int new_agents_at_split = 1;

    /// @brief Number of agents that are created when agent reached its goal and now tries to evolve its params
    int new_agents_at_evolution = 1;

    /// @brief Number of prediction steps, which one agent performs in one execution of predict()
    ///
    /// Although as much agents as possible should run their prediction in parallel, the real number of parallel agents
    /// is limited by the ideal thread count (hypercores). If more agents try to predict their path, QtConcurrent will
    /// queue their execution until a thread of the thread pool becomes available e.g. if another agent finished its
    /// prediction.
    ///
    /// If all agents would predict endlessly, the maximum number of agents, that are actually run would be the size of
    /// the QtConcurrent Thread Pool. Therefore, the agents have to return after a defined number of predictions steps,
    /// so other agents that are queued for thread execution can use the now free thread.
    /// If the returned agent has not reached its goal, it will queue up for thread execution immediately.
    ///
    /// This parameter determines how many of the prediction steps are calculated before, it returns to the main thread.
    /// It should be choosen in a way, that every agent that exists has the chance to update its prediction at least
    /// once during one control cycle of the real robot representation.
    ///
    /// Negative values allow the agent to run until it is killed, collided or reached the goal.
    int continuous_prediction_steps = 100;  // NOLINT(readability-magic-numbers) : Default value initialization

    /// @brief Maximum euclidean distance in m of which a predicted trajectory at t can deviate from the real system
    ///
    /// ... to be valid. If the predicted trajectory of any agent for time t deviates more than this the agent and its
    /// parameter settings are treated as invalid. Please note that some deviation is normal due to plant model
    /// mismatch.
    double maximum_trajectory_deviation = 0.1;  // NOLINT(readability-magic-numbers) : Default value initialization

    /// @brief Vector of discrete obstacle_limit_distances to use while exploring.
    ///
    /// For each distance a geometry object is created.
    /// The first element is always the start and default setting for an agent.
    /// @attention Using more than one obstacle limit distance is not implemented for robotic manipulator and only works for point mass robots. Using this functionality requires significant adaptions of this version.
    std::vector<double> allowed_obstacle_limit_distances;

    /// @brief Maximum number of obstacle points that are considered for current and distance calculations
    ///
    /// This factor limits the maximum number of points for each object, that are used to calculate the distances and
    /// current vectors. A value of 1 means, that only the minimal distance is used for current calculation.
    ///
    /// The default value of 0 means, that no limit is enforced and all points inside the used obstacle_limit_distance
    /// are used
    int maximum_number_considered_points_per_object = 0;

    /// @brief Restart the prediction if all predictive agents did finish.
    bool restart_prediction_if_idling = true;

    /// @brief Spawn one additional agent with all body movements projected into the null space
    bool explore_nullspace = true;

    /// @brief Spawn one additional agent with null space movements in the direction of high manipulability
    bool explore_manipulability = true;

    /// @brief Vector of discrete smoothing offset time steps to use while smoothing a found trajectory.
    ///
    /// If an agent reached the goal, the agent manager may - depending on smoothing_strategy - try to smooth its
    /// trajectory by creating a new agent, that has a moving goal which is the position of the original agent by an
    /// offset time step in the future. This vector defines all "future" offsets that are tried out.
    /// @attention Is not implemented for robotic manipulator and only works for point mass robots. Using this functionality requires significant adaptions of this version.
    std::vector<uint64_t> smoothing_offsets;

    /// @brief Smoothing strategy to use for smoothing
    ///
    /// If one trajectory is found to the goal it should be smoothed. This vector defines the (recursive) strategy that
    /// is used. It consist of a list of operations coded as integers. The following operations are available
    /// 0 = Redo previous operation unless there is no improvement
    /// 1 = Smooth by time offset: Please note, that more than one number in smoothing_offsets will spawn more than
    /// one agent.
    /// 2 = Smooth through line of sight shortcuts.
    ///
    /// An empty vector means no smoothing at all.
    ///
    /// Example:
    /// [2, 1, 0, 2]
    /// Based on one normal agent:
    /// - Simulate an agent, with line of sight shortcuts
    /// - Smooth the line of sight shortcut solution by time offset until one agent is not better than the previous
    /// generation
    /// - Smooth the time offset solution again with line of sight shortcuts
    /// @attention Is not implemented for robotic manipulator and only works for point mass robots. Using this functionality requires significant adaptions of this version.
    std::vector<SmoothingStrategy> smoothing_strategy;

    /// @brief Threshold score to allow smoothing
    ///
    /// Smoothing is an expensive operation. It should only be allowed for the most promising candidates.
    /// Therefore, before each smoothing is started, the score of the best agent in its non smoothed version is compared
    /// to score of the non smoothed version of the agent that request smoothing.
    /// The smoothing is then only performed if
    /// best_agent.non_smoothed_reference_score <= this_agent.non_smoothed_reference_score *
    /// allow_smoothing_delta_score_threshold_factor
    ///
    /// Therefore, this factor should generally larger than 1.0.
    /// @attention Is not implemented for robotic manipulator and only works for point mass robots. Using this functionality requires significant adaptions of this version.
    double allow_smoothing_delta_score_threshold_factor = 1.0;

    /// @brief Minumum number of bets the AgentManager should receive, before signaling new settings after invalidation
    ///
    /// If the prediction of the best agent is invalidated, the best agents parameters should only be changed, if
    /// at least minimum_bet_number_before_invalid_settings_change bets were received. This should be done in order
    /// to avoid following a suboptimal trajectory, just because it was the first bet received after an invalidation.
    ///
    /// Please note that the underlying behaviour also depends on
    /// only_consider_goal_reached_for_invalid_settings_change_bet_number. If this is true, only bets are counted
    /// towards this setting, that reached the goal. Otherwise all freshly generated agents after invalidation are
    /// counted.
    ///
    /// @attention Seems to have a bug. Only set to zero
    int minimum_bet_number_before_invalid_settings_change = 0;

    /// @brief Flag, if only agents that reached the goal should be counted towards
    /// minimum_bet_number_before_invalid_settings_change
    bool only_consider_goal_reached_for_invalid_settings_change_bet_number = true;

    /// @brief Equality operator.
    bool operator==(const CFPlannerSettings& rhs) const;

    /// @brief Constructing parsing function for a entry structure on the ros parameter server
    ///
    /// This function expects a XmlRpcValue that from a design standpoint consist of a dict (std::pair)
    /// with the keys and values for the parameters of this struct.
    static CFPlannerSettings generateFromRosParameterEntry(const XmlRpc::XmlRpcValue& ros_parameter_entry);
};

#endif