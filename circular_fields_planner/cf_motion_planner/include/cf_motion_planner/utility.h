#include <Eigen/Core>
#include <algorithm>
#include <cmath>
#include <limits>
#include <vector>

#include "cf_motion_planner/pc_types.h"

#ifndef DS_UTILITY_H
#define DS_UTILITY_H

/// @brief Compare, if a double is almost equal.
///
/// The epsilon needs to be changed depending on the size of the numbers it is expected to compare.
///
/// See: https://stackoverflow.com/questions/17333/what-is-the-most-effective-way-for-float-and-double-comparison
/// See: https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
bool almostEqualRelative(double a, double b, double epsilon = std::numeric_limits<double>::epsilon());

/// @brief Wrapper around almostEqualRelative() for Eigen::Vector3d
///
/// This function compares if each component is almostEqualRelative()
bool vectorsAreAlmostEqual(const Eigen::Vector3d& vector1, const Eigen::Vector3d& vector2,
                           double epsilon = std::numeric_limits<double>::epsilon());

/// @brief Wrapper around almostEqualRelative() for std::vector<double>
///
/// This function compares if each component is almostEqualRelative()
bool stdVectorsAreAlmostEqual(const std::vector<double>& vector1, const std::vector<double>& vector2,
                              double epsilon = std::numeric_limits<double>::epsilon());

/// @brief Wrapper around almostEqualRelative() for TimedPoint
///
/// This function compares if each component is almostEqualRelative()
bool timedPointsAreAlmostEqual(const TimedPoint& point1, const TimedPoint& point2,
                               double epsilon = std::numeric_limits<double>::epsilon());
#endif