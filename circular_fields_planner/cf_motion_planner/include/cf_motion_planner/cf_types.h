#ifndef CF_TYPES_H
#define CF_TYPES_H

#include <pcl/point_types.h>
#include <pcl_ros/point_cloud.h>

#include <Eigen/Core>
#include <QMetaType>
#include <map>
#include <vector>

#include "cf_motion_planner/robot_state.h"
#include "pcl_obstacle_generator/MovingObstacles.h"

using Vector6d = Eigen::Matrix<double, 6, 1>;
using Vector7d = Eigen::Matrix<double, 7, 1>;
using Vector3d = Eigen::Vector3d;
using Matrix6x7 = Eigen::Matrix<double, 6, 7>;
using RotationVectorMapVector = std::vector<std::map<int, Vector3d>>;

Q_DECLARE_METATYPE(pcl::PointCloud<pcl::PointXYZINormal>::Ptr);
Q_DECLARE_METATYPE(Eigen::Affine3d);
Q_DECLARE_METATYPE(Vector6d);
Q_DECLARE_METATYPE(Vector7d);
Q_DECLARE_METATYPE(CFRobotState);
Q_DECLARE_METATYPE(pcl_obstacle_generator::MovingObstacles::Ptr);
Q_DECLARE_METATYPE(RotationVectorMapVector);

#endif