#ifndef KDL_DYNAMICS_H
#define KDL_DYNAMICS_H

#include <math.h>
#include <stdio.h>

#include <iostream>
#include <kdl/chain.hpp>
#include <kdl/chaindynparam.hpp>
#include <kdl/chainfksolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainiksolver.hpp>
#include <kdl/chainiksolvervel_pinv.hpp>
#include <kdl/chainjnttojacdotsolver.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/frames.hpp>
#include <kdl/frames_io.hpp>

class RobotKDL {
public:
    RobotKDL();
    ~RobotKDL();

    /// @brief Build the kinematic chain based on Denavit Hartenberg parameters using Craig's convention [see
    /// Introduction to Robotics: Mechanics and Control (3rd Edition)]
    /// @param joint_types Type of joint to be added (0 = Fixed, 1 = RotZ)
    /// @param a DH Parameter a
    /// @param alpha DH Parameter alpha
    /// @param d DH Parameter d
    /// @param theta DH Parameter theta
    void buildChain(const std::vector<int> &joint_types, const std::vector<double> &a, const std::vector<double> &alpha,
                    const std::vector<double> &d, const std::vector<double> &theta);

    /// @brief Calculate the jacobian of the end effector based on the current joint positions
    ///
    /// @param q_in Current joint positions as input
    /// @param jac_out Jacobian for the end effector as output
    int getJacobianEE(const KDL::JntArray &q_in, KDL::Jacobian &jac_out);

    /// @brief Calculate the differential jacobian of the end effector based on the current joint positions
    ///
    /// @param q_in Current joint positions as input
    /// @param j_dot Jacobian for the end effector as output
    int getJacobianDot(const KDL::JntArrayVel &q_in, KDL::Jacobian &j_dot);

    /// @brief Calculate the joint velocity given an end effector velocity based on the current joint positions
    ///
    /// @param q_in Current joint positions as input
    /// @param v_in Jacobian for the end effector as output
    /// @param qdot_out Joint velocities as output
    int getVelocity(const KDL::JntArray &q_in, const KDL::Twist &v_in, KDL::JntArray &qdot_out);

    /// @brief Calculate the end effector pose based on the current joint positions
    ///
    /// @param q_in Current joint positions as input
    /// @param cart_out End effector pose as output
    int getJntToCartEE(const KDL::JntArray &q_in, KDL::Frame &cart_out);

    /// @brief Calculate the cartesian pose of a segment based on the current joint positions
    ///
    /// @param q_in Current joint positions as input
    /// @param segment_id Index of the current segment
    /// @param cart_out End effector pose as output
    int getJntToCartFAP(const KDL::JntArray &q_in, int segment_id, KDL::Frame &cart_out);

    /// @brief Calculate the jacobian of a point on a given segment based on the current joint positions
    ///
    /// @param q_in Current joint positions as input
    /// @param segment_id Index of the current segment
    /// @param jac_out Jacobian for the FAP as output
    int getJacobianFAP(const KDL::JntArray &q_in, int segment_id, KDL::Jacobian &jac_out);

    /// @brief Returns the number of segments in the robot chain
    int getNrOfSegments();

    /// @brief Returns the number of joints in the robot chain
    int getNrOfJoints();

private:
    KDL::Chain chainEE_;
    KDL::ChainJntToJacSolver *jacSolverEE_;
    KDL::ChainIkSolverVel *velSolver_;
    KDL::ChainFkSolverPos_recursive *posFKSolverEE_;
    KDL::ChainFkSolverPos_recursive *posFKSolverFAP_;
    KDL::ChainJntToJacDotSolver *jacDotSolver_;
    KDL::ChainJntToJacSolver *jacSolverFAP_;
};

#endif  // KDL_DYNAMICS_H