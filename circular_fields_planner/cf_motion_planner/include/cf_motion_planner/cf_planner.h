#ifndef CF_PLANNER_H
#define CF_PLANNER_H

#include <XmlRpcValue.h>

#include <Eigen/Core>
#include <QFutureWatcher>
#include <QObject>
#include <QThread>
#include <QtConcurrent>
#include <memory>

#include "cf_motion_planner/cf_agent.h"
#include "cf_motion_planner/cf_agent_manager.h"
#include "cf_motion_planner/cf_planner_ros_interface.h"
#include "cf_motion_planner/cf_planner_settings.h"
#include "cf_motion_planner/cf_robot_calculations.h"
#include "cf_motion_planner/cf_types.h"
#include "cf_motion_planner/ds_stopwatch.h"
#include "cf_motion_planner/dynamic_settings.h"
#include "cf_motion_planner/utility.h"
#include "pcl_obstacle_generator/MovingObstacles.h"
#include "ros/ros.h"

/// @brief Main class of the circular fields planner
///
/// This class is the main class of the circular fields planner.
/// It manages the real robot representation that calculates the force reaction for the current situation and passes
/// new data to the CFAgentManager in another thread. The CFAgentManager explores a best parameter setting for
/// the real robot representation through the usage of predictive agents.
///
/// The real robot representation is an agent that will be used for the force reaction calculation that is published to
/// the real system. This force reaction is calculated for every control loop with a cycle duration of
/// settings.control_loop_cycle_duration. If the planner receives new position or velocity data from the real system,
/// the real robot representation is also updated accordingly. Therefore, the trajectory of the real system is not
/// necessarily equal to the trajectory of a predictive agent with the same parameter settings.
///
/// The settings of the real robot representation are updated, if a new best parameter set is received, from the
/// CFAgentManager.
///
/// The CFAgentManager is run in a separate thread in order to not interfere or block the time constrained calculation
/// of the real robot representation. Since the predictive agents use another slower time interval to update their
/// position, the CFPlanner triggers a validation of the predictions in comparison to real robot representation position
/// every n-th control cycle. This is done inside slotCalculateForceReaction().
class CFPlanner : public QObject {
    Q_OBJECT

    /// @brief Thread in which the agent manager is executed
    QThread* agent_manager_thread_;

    /// @brief The agent manager, that handles all the predictive agents and generates the best fitting parameter set
    CFAgentManager* agent_manager_;

    /// @brief ROS Interface to get data from an publish the reaction force to
    CFPlannerROSInterface* ros_interface_;

    /// @brief Settings of the planner
    CFPlannerSettings settings_;

    /// @brief Default agent settings if an agent is freshly created
    CFAgentSettings default_agent_settings_;

    /// @brief Dynamic settings used for all agents
    std::shared_ptr<AbstractRobotDynamicSettings> default_dynamic_settings_;

    /// @brief Lookup objects for all geometric calculations
    std::vector<std::shared_ptr<CFGeometry>> geometry_collection_;

    /// @brief Agent, that represents the real robot and is used for cf force feedback calculation
    std::unique_ptr<CFAgent> real_robot_representation_;

    /// @brief Counter, which counts how many control_cycles passed since the last time validateReactPrediction()
    unsigned int control_loop_validate_cycle_counter_ = 0;

    /// @brief Time step of the prediction. Used to time reference the geometry data.
    uint64_t time_step_ = 0;

    /// @brief Helper for initializing the real robot representation with the first received rotation vector set
    bool rot_vec_init_ = false;

    /// @brief Stopwatch object to measure various execution times.
    ///
    /// This variable is for debugging purposes only and is not part of the planner itself.
    /// It can be removed safely anytime without changing the planner behaviour.
    std::unique_ptr<DSStopwatch> debug_stopwatch_;

    /// @brief Stopwach for step time calculation
    std::unique_ptr<DSStopwatch> step_time_stopwatch_;

    /// @brief Watchdog object to measure the main loop execution faithfulness
    ///
    /// This variable is for debugging purposes only and is not part of the planner itself.
    /// It can be removed safely anytime without changing the planner behaviour.
    std::unique_ptr<DSWatchdog> control_loop_watchdog_;

    /// @brief Utility function that connects all signals and slots between this class and the agent manager
    void connectPlannerWithAgentManager() const;

    /// @brief Utility function that connects all signals and slots between this class and the ROS interface
    void connectPlannerWithROSInterface() const;

    /// @brief Get a pointer to the geometry inside geometry_collection_ that has obstacle_limit_distance
    ///
    /// @returns Copy of element or nullptr if no match.
    std::shared_ptr<CFGeometry> getGeometryForObstacleLimitDistance(double obstacle_limit_distance) const;

    /// @brief Utility function to update the moving goal inside slotCalculateForceReaction()
    ///
    /// Reaction depends on the operation mode. Expects, that real_robot_representation_->getSettings().moving_goal is
    /// not none.
    void updateMovingGoal();

public:
    /// @brief Constructor
    ///
    /// @param planner_settings Settings for this planner
    /// @param agent_settings Agent settings for start of the simulation
    /// @param robot_dynamic_settings Settings for a robot dynamic to use for force reaction and prediction calculation
    CFPlanner(CFPlannerROSInterface* ros_interface, const CFPlannerSettings& planner_settings,
              const CFAgentSettings& agent_settings,
              const std::shared_ptr<AbstractRobotDynamicSettings>& dynamic_settings);

    /// @brief Destructor that cleans up the agent_manager_thread_
    ~CFPlanner() override;

    /// @brief Get a temporary reference to the real robot representation in order to simulate one dynamic step and
    /// collect the results
    std::unique_ptr<CFAgent>& getRealRobotRepresentation() { return real_robot_representation_; };

public slots:  // NOLINT(readability-redundant-access-specifiers) : Slot access specifier

    /// @brief Update the robot state
    ///
    /// The robot state contains joint states, force application point poses and velocities, as well as the jacobian
    /// matrices
    ///
    /// This function modifies real robot representation to calculate the real reaction
    ///
    /// @param robot_state Robot state
    void slotSetRobotState(const CFRobotState& robot_state) const;

    /// @brief Update the goal pose of the planner to goal_pose
    ///
    /// @param goal_pose Affine transformation from world_CS to EE_CS
    void slotSetGoalPose(const Eigen::Affine3d& goal_pose);

    /// @brief Set the new point cloud data to all geometry objects in geometry_collection_
    ///
    /// @param cloud New obstacle cloud data with points, object ids (as intensity) and normals
    void slotSetObstacles(const pcl_obstacle_generator::MovingObstacles::Ptr& latest_obstacle_msg);

    /// @brief Calculate the force reaction based on the real robot representation latest settings.
    ///
    /// Also increment the time_step for predictive agent trajectory validation. Furthermore, increment the time step
    /// for the moving goal of the real robot representation every n-th iteration, if required.
    ///
    /// This function will emit sgnPredictionStep() every settings_.prediction_step_time_multiple to trigger the
    /// validation of the predictive agents inside CFAgentManager.
    void slotCalculateForceReaction();

    /// @brief Update the real robot representation settings and change the used geometry, if required
    void slotUpdateRealRobotRepresentationSettings(const CFAgentSettings& new_settings);

    /// @brief Initialize the real robot representation and create new agents for every new rotation vector set
    void slotSetRotVecs(const RotationVectorMapVector& rotation_vectors);

signals:
    /// @brief Signal, that a time step for a prediction has passed and the AgentManger should validate its predictions
    void sgnPredictionStep(const CFRobotState& latest_robot_state);

    /// @brief The goal of the real robot representation did change (to the agent manager)
    void sgnGoalPoseChanged(const Eigen::Affine3d& goal_pose);

    /// @brief Signal (to the agent manager) that a new agent with a new rotation vector set should be spawned
    ///
    /// @param settings Current settings of the original agent
    /// @param rotation_vectors Set of rotation vectors
    void sgnCreateNewAgent(const CFAgentSettings& settings, const RotationVectorMapVector& rotation_vectors);

    /// @brief Signal (to the agent manager) that a new obstacle cloud was received
    void sgnNewObstacleData(const pcl_obstacle_generator::MovingObstacles::Ptr& latest_obstacle_msg);

    /// @brief Signal (to the agent manager) that the first rotation vector set was received
    void sgnFirstRotVecData(const RotationVectorMapVector& rotation_vectors);

    /// @brief Signal (to the ros interface) that a new force reaction was calculated
    void sgnFAPForces(const std::vector<Vector6d>& fap_forces);

    /// @brief Signal (to the ros interface) that new settings are available
    void sgnRealRobotSettings(const CFAgentSettings& real_robot_settings);

    /// @brief Signal (to ros interface) that a new step time is available for publish
    void sgnStepTime(float step_time);

};

#endif