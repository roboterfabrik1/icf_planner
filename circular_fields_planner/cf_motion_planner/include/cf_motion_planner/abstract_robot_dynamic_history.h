#include <Eigen/Core>
#include <Eigen/Geometry>
#include <QMetaType>
#include <boost/circular_buffer.hpp>
#include <boost/optional.hpp>

#include "cf_motion_planner/cf_types.h"
#include "ros/ros.h"

#ifndef ABSTRACT_ROBOT_DYNAMIC_HISTORY_H
#define ABSTRACT_ROBOT_DYNAMIC_HISTORY_H

/// @brief Class to store the history of an AbstractRobotDynamic and its subclasses.
///
/// The AbstractRobotDynamicHistory is a simple container used to store the state and calculation results of an
/// AbstractRobotDynamic over time. By design a value in time_steps_ should match and assign a time_step number to a
/// value in any other buffer with the same index. However, the code that maintains the history has to make sure
/// this is the case.
///
/// Example:
/// time_steps[i] => 42, poses_[i] => Home
/// The robot was at time_step 42 at Home.
///
/// For a defintion, which data is saved in the variables with which unit and in which coordinate system, please see
/// the description of the vars with the same name in the class you are generating the history for.
///
/// Data is sorted ascending by age: The latest element in a buffer is always the first. Adding more data steps than
/// the history_buffer_size will overwrite the oldest data.
///
/// As an addition and required for the score calculation of an agent, the minimal_obstacle_distance_ for a
/// trajectory can also be saved. This is usually not done by the AbstractRobotDynamic but by the agent, the
/// AbstractRobotDynamic is included in. Furthermore, the duration since an arbitrary time step can be tracked.
class AbstractRobotDynamicHistory {
    using Vector6d = Eigen::Matrix<double, 6, 1>;
    using Vector7d = Eigen::Matrix<double, 7, 1>;

    /// @brief Buffer size to use inside the constructor without arguments
    ///
    /// If more elements are added, the oldest will be overwritten
    static const int DEFAULT_HISTORY_BUFFER_SIZE = 10000;

    /// @brief Time step the history calculates its duration against
    ///
    /// Unless explicitly set with setDurationReferenceTimeStep() this is equal to the time step of the first recorded
    /// value of this history. Then, if no value in the buffer is ever overwritten, this is equal to time_steps_.tail().
    ///
    /// Used for agent score calculation.
    boost::optional<uint64_t> duration_reference_time_step_ = boost::none;

    /// @brief First recorded step time of this history that was added via incrementTimeStep()
    boost::optional<uint64_t> start_time_step_ = boost::none;

    /// @brief Overall number of the calculation step for a value in these buffers
    ///
    /// Could technically overflow. However, due to uint64_t datatype very unlikely for most testcases.
    boost::circular_buffer<uint64_t> time_steps_;

    /// @brief Buffer of robot state
    boost::circular_buffer<CFRobotState> robot_state_;

    /// @brief Buffer of all applied wrenches_
    boost::circular_buffer<std::vector<Vector6d>> wrenches_;

    /// @brief State for the minimal_obstacle_distance the agent has seen during its runtime
    ///
    /// This is used for the score calculation of an agent and only set by the agent itself, not the dynamic.
    boost::optional<double> minimal_obstacle_distance_ = boost::none;

public:
    /// @brief Constructor, that sets the buffers sizes to DEFAULT_HISTORY_BUFFER_SIZE
    AbstractRobotDynamicHistory();

    /// @brief Constructor, that sets the buffers sizes to history_buffer_size
    ///
    /// If more elements than history_buffer_size are added to the record of one value, the oldest
    /// values is overwritten.
    ///
    /// @param history_buffer_size Size of all buffers
    explicit AbstractRobotDynamicHistory(int history_buffer_size);

    /// @name Element setters
    /// @{

    /// @brief Add a time step to the record that is an increment of the latest step
    ///
    /// Uses start_step if there is no previous time step
    /// May also set duration_reference_time_step_ to the given value, if it is not set manually, yet.
    void incrementTimeStep(uint64_t start_step = 0);

    /// @brief Set the time step, that the duration in getTrackedDuration() is calculated from
    ///
    /// incrementTimeStep() sets the duration_reference_time_step automatically if no duration_reference_time_step
    /// was set previously. Therefore, the use of this function is not necessary, if your reference point
    /// for the duration is the first time step of a recorded history.
    ///
    /// Only use this function if you would like to calculate the duration against a fixed time_step, that may be not
    /// part of this history itself.
    ///
    /// @param duration_reference_time_step time step to use as a reference point.
    void setDurationReferenceTimeStep(uint64_t duration_reference_time_step);

    /// @brief Add a robot state to the record
    void addRobotState(const CFRobotState& robot_state);

    /// @brief Add an applied wrench to the record
    void addWrench(const std::vector<Vector6d>& latest_wrench);

    /// @brief Check if obstacle_distance < minimal_obstacle_distance_ and add it if necessary
    void checkAddMinimalObstacleDistance(double obstacle_distance);

    /// @}

    /// @name History getters
    /// @{

    /// @brief Get first recorded step time of this history that was added via incrementTimeStep()
    boost::optional<uint64_t> getStartTimeStep() const { return start_time_step_; }

    /// @brief Get poses history
    boost::circular_buffer<uint64_t> getTimeSteps() const { return time_steps_; }

    /// @brief Get last recorded step time of this history that was added via incrementTimeStep()
    boost::optional<uint64_t> getLatestTimeStep() const;

    /// @brief Get robot state history
    const boost::circular_buffer<CFRobotState>& getRobotStates() const { return robot_state_; }

    /// @brief Get applied ee_wrenches history
    const boost::circular_buffer<std::vector<Vector6d>>& getWrenches() const { return wrenches_; }

    /// @brief Get the optional minimal obstacle distance of this history
    ///
    /// Is boost::None if it was never set.
    boost::optional<double> getMinimalObstacleDistance() const { return minimal_obstacle_distance_; };

    /// @}

    /// @brief Reset all history buffers and start a new record of values
    void clear();

    /// @brief Check, if the history has a similar trajectory point at time_step
    ///
    /// This function can be used to check, if a prediction that is saved inside this object is still valid in
    /// comparison to  the real robot representation. It will return true, if the translational part of the pose for
    /// time_step is in a euclidean range delta around point. It will return false otherwise.
    ///
    /// @attention Please note, that, if the buffers are choosen to small, the information for the oldest points are
    /// overwritten. A query for a overwritten point will also return false, although it might did have a similar point
    /// once.
    ///
    /// @param time_step Time step to search for
    /// @param point Point that should be similar to the value in history at time_step
    /// @param delta Euclidean distance in m for which the points are interpreted as equal
    ///
    /// @returns true, if the history has trans. pose component at time_step, whose distance to point is <= delta
    bool hasSimilarTrajectoryPoint(uint64_t time_step, const Eigen::Vector3d& point, double delta) const;

    /// @brief Return the duration this history keeps track of
    ///
    /// Please note that this is not necessarily equal to the amount of values in the buffers or the number of time
    /// steps this history actually recorded since its creation.
    /// If the buffers begin to overflow and rotate, this number still increases, although the history for the
    /// oldest values is not accessible anymore.
    /// Furthermore, the reference point can be set to any arbitrary value with setDurationReferenceTimeStep().
    uint64_t getTrackedDuration() const;
};

Q_DECLARE_METATYPE(AbstractRobotDynamicHistory);

#endif