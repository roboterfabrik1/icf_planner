#include <Eigen/Core>
#include <Eigen/Geometry>
#include <Eigen/StdVector>

#include "cf_motion_planner/abstract_robot_dynamic_history.h"
#include "cf_motion_planner/cf_robot_calculations.h"

#ifndef ABSTRACT_ROBOT_DYNAMIC_H
#define ABSTRACT_ROBOT_DYNAMIC_H

/// @brief Abstract base class for a arbitrary robot dynamic
///
/// A robot dynamic in a context of this framework is a simulation rule, that calculates FAP positions, velocities
/// and accelerations (and optionally) joint torques and joint velocities that result under the influence
/// of a control wrench to the FAPs.
///
/// The class uses a calculate once and then get values design: Use calculateStep() to simulate on step and then
/// use the getters to get the different aspects of the calculation result.
///
/// Please note, that this class is not thread safe!
class AbstractRobotDynamic {
protected:
    using Vector6d = Eigen::Matrix<double, 6, 1>;
    using Vector7d = Eigen::Matrix<double, 7, 1>;

    /// @brief State information of the robot for the current position
    ///
    /// This includes the speed and pose of the force application points as well as the joint states and jacobian
    /// matrices.
    CFRobotState latest_robot_state_;

    /// @brief Buffer size of the history_: How many steps can the history contain?
    static const int HISTORY_BUFFER_SIZE = 10000;

    /// @brief Historic value cache of most properties
    ///
    /// @attention Subclasses need to implement the maintenance of this history manually.
    /// In general, they should always add a value to this history, if one value is updated.
    ///
    /// Please also note, that wrench is treated as an input to the system and therefore has usually one value less than
    /// the others. x(n) = f(wrench(n),x(n-1))
    /// => Wrench at the top is the input to get any other value at the top, if applied to the value one step before
    AbstractRobotDynamicHistory history_;

    /// @brief Add all latest_*_ to history_
    ///
    /// Adds an entry for robot_state to history.
    void makeHistorySnapshot();

public:
    /// @brief Initializes all vectors as zero vectors.
    AbstractRobotDynamic();
    virtual ~AbstractRobotDynamic() = default;

    /// @name Setters
    /// Please use this functions only to set the starting behaviour or if you want to modify the
    /// existing position or velocity. Otherwise use calculateStep() to calculate these values
    /// based on the dynamics model.
    /// @{

    /// @brief Set the state information of the robot
    void setRobotState(const CFRobotState& robot_state);

    /// @brief Set the robot history
    ///
    /// This will also set the latest_* values with the head of the value in the history buffer, if it exists.
    ///
    /// This is used for spliting or evolution operation
    void setHistory(const AbstractRobotDynamicHistory& history);
    /// @}

    /// @name Getters
    /// Get results of latest calculation. Should only be used, after calculateStep() was called.
    /// @{

    /// @brief Get the latest robot state
    CFRobotState getLatestRobotState() const { return latest_robot_state_; };

    /// @brief Return the history of this dynamic
    const AbstractRobotDynamicHistory& getHistory() const { return history_; };

    /// @brief Return the history of this dynamic (Editable Overload)
    AbstractRobotDynamicHistory& getHistory() { return history_; };
    /// @}

    /// @name Simulation
    /// @{

    /// @brief Simulate one dynamic step under the influence of a wrench in N and Nm
    ///
    /// This function calculates the latest_robot_state_
    virtual void calculateStep(const std::vector<Vector6d>& fap_wrenches, const CFAgentSettings& agent_settings) = 0;

    /// @}
};

#endif