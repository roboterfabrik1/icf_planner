#include "cf_motion_planner/abstract_robot_dynamic.h"
#include "cf_motion_planner/dynamic_settings.h"

#ifndef POINT_MASS_ROBOT_DYNAMIC_H
#define POINT_MASS_ROBOT_DYNAMIC_H

/// @brief Dynamic model for a point mass
///
/// @attention EE-Torques and therefore rotational calculations are ignored.
/// Joint values are not calculated and will return a 0 vector.
///
/// In consequence only the first three elements of each Vector6d will return valid results and can be
/// used for further calculations.
class PointMassRobotDynamic : public AbstractRobotDynamic {
    /// @brief Dynamic Settings
    PointMassRobotDynamicSettings settings_;

public:
    /// @brief Constructor with settings to use
    explicit PointMassRobotDynamic(const PointMassRobotDynamicSettings& settings);

    /// @brief Default destructor
    ~PointMassRobotDynamic() override = default;

    /// @name Simulation
    /// @{

    /// @brief Simulate one dynamic step under the influence of a endeffector_wrench in N and Nm
    ///
    /// This function calculates latest_pose_, latest_velocity_, latest_acceleration_,
    /// latest_joint_torques_ and latest_joint_velocities_ are not supported.
    ///
    /// @attention Torques and therefore rotational calculations are ignored.
    ///
    /// @param ee_wrench Force to apply to point mass in world_CS, torques are ignored
    void calculateStep(const std::vector<Vector6d>& fap_wrenches, const CFAgentSettings& agent_settings) override;
    /// @}
};

#endif