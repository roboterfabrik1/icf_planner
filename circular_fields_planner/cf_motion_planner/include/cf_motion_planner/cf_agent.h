#include <XmlRpcValue.h>

#include <QMetaType>
#include <QObject>
#include <exception>
#include <memory>
#include <utility>

#include "cf_motion_planner/abstract_robot_dynamic.h"
#include "cf_motion_planner/abstract_robot_dynamic_history.h"
#include "cf_motion_planner/cf_agent_settings.h"
#include "cf_motion_planner/cf_geometry.h"
#include "cf_motion_planner/dynamic_settings.h"
#include "cf_motion_planner/moving_goal.h"
#include "cf_motion_planner/pc_object.h"
#include "cf_motion_planner/pc_types.h"
#include "cf_motion_planner/point_mass_robot_dynamic.h"
#include "cf_motion_planner/simple_robot_dynamic.h"
#include "cf_motion_planner/utility.h"
#include "ros/ros.h"

#ifndef CF_AGENT_H
#define CF_AGENT_H

/// @brief Class for an Agent, that handles the calculation of dynamic and force reaction
///
/// An agent is an entity, that represents a state of the real world system.
/// Agents can either act as a prediction of the system in the future or as the "real" system.
///
/// With an agent, it is possible to:
/// - calculate the latest force reaction to the real system, if the dynamic parameters are manually set to the
/// latest real system state
/// - predict and simulate the future behaviour of the robot for a given setting and dynamic.
///
/// The Agent uses two other classes to provide its services:
///
/// - The robot_dynamic handles the update of the velocity, and pose in one simulation step.
/// It is designed as an abstract class to support a plug and play solution if another dynamic
/// (e.g. Franka instead of point mass) needs to be used.
/// The agent does not need to know the implementation details of the dynamic.
///
/// - The geometry class handles the calculation of all obstacle geometry related data calculations
/// (e.g minimal distance). By design, it theoretically can be shared among agents, to improve calculation efforts.
/// However, each agent could construct its own geometry class, if required.
///
///
/// The function predict() is designed to be run in a parallel thread to the main thread. It is a blocking function that
/// returns after a defined number of calculation steps. Please note however, that the class CFAgent itself is not
/// thread safe (with the exception of killPrediction()). Do not try to change parameters of an agent that is running in
/// another thread. Only change parameters of agents that returned from their prediction calculation. Also note that a
/// shared resource between agents like a shared geometry class needs to be thread safe.
class CFAgent : public QObject {
    Q_OBJECT
    using Vector6d = Eigen::Matrix<double, 6, 1>;
    using Vector7d = Eigen::Matrix<double, 7, 1>;

    /// @name Agent components
    /// @{

    /// @brief Latest settings for this agent
    CFAgentSettings settings_;

    /// @brief Geometry object to use for obstacle geometry related data calculations
    std::shared_ptr<CFGeometry> geometry_;

    /// @brief Robot dynamic to use for force reaction and prediction calculation
    std::unique_ptr<AbstractRobotDynamic> robot_dynamic_;
    /// @}

    /// @brief Flag, if the prediction in predict() shall be run or aborted
    std::atomic<bool> predict_;

    /// @brief Is this the best agent?
    ///
    /// The best agent does not use timed points when requesting its geometry data, because it should always work only
    /// on the latest data.
    bool is_best_agent_ = false;

    /// @brief Validate, that all rotation vectors are configured for objects inside obstacle_limit_distance
    ///
    /// Otherwise define a new rotation vector for a new object and trigger a split.
    ///
    /// Because geometry_->getElectricCurrentsAndSquareDistances() returns only values for known and defined
    /// rotation vectors, you need to update the rotations vectors through this function before a call
    /// to calculateCFForceEndEffector(). Otherwise a new object could be inside obstacle_limit_distance for which no
    /// rotation vector is defined and that is therefore not considered for the force calculation.
    void validateAddRotationVectorsEndEffector();

    /// @brief Validate, that all rotation vectors are configured for objects inside obstacle_limit_distance
    ///
    /// Otherwise define a new rotation vector for a new object and trigger a split.
    /// Here, the distance calculation and split is performed for all force application points on the robot body.
    /// Because geometry_->getElectricCurrentsAndSquareDistances() returns only values for known and defined
    /// rotation vectors, you need to update the rotations vectors through this function before a call
    /// to calculateCFForce(). Otherwise a new object could be inside obstacle_limit_distance for which no
    /// rotation vector is defined and that is therefore not considered for the force calculation.
    /// In contrast
    void validateAddRotationVectorsBody();

    /// @brief Utility function to update the moving goal inside calculateStep()
    ///
    /// Reaction depends on the operation mode. Expects, that settings_.moving_goal is not none.
    void updateMovingGoal();

    /// @brief Utility function to get the latest goal pose
    ///
    /// Snippet can be used to determine, if the moving goal should be used in the calculations or the overall global
    /// goal
    ///
    /// @param is_moving_goal Optional out flag that returns if the returned goal is part of a moving goal
    ///
    /// @returns Goal pose to use in calculations.
    const Eigen::Affine3d& getLatestGoalPose(bool* is_moving_goal = nullptr) const;

public:
    /// @brief Constructor
    ///
    /// The type of robot_dynamic, that is created as a component of this class is automatically choosen from
    /// the real non abstract type of robot_dynamic_settings.
    /// E.g if robot_dynamic_settings points to a PointMassRobotDynamicSettings object, a PointMassRobotDynamic will be
    /// created as robot_dynamic_.
    ///
    /// @param settings Settings for this agent
    /// @param geometry Geometry object to use for obstacle geometry related data calculations
    /// @param robot_dynamic_settings Settings for a robot dynamic to use for force reaction and prediction calculation
    /// @param id Unique Agent id
    /// @param is_best_agent Is this the best agent, which should only query the latest (non interpolated) geometry
    /// data
    CFAgent(const CFAgentSettings& settings, std::shared_ptr<CFGeometry>& geometry,
            const std::shared_ptr<AbstractRobotDynamicSettings>& robot_dynamic_settings, int id = 0,
            bool is_best_agent = false);

    /// @brief Set the geometry object, from which this agent queries its geometry data.
    ///
    /// This function is not thread safe. Please make sure, you only change geometry, if no prediction is running.
    ///
    /// @attention Possible bug ahead: The obstacle_limit_distance in the settings defines the geometry class that has
    /// to be used. If the obstacle_limit_distance is not maintained when the geometry changes and vice versa,
    /// it may differ from the real value used by the geometry and therefore by the agent.
    void setGeometry(std::shared_ptr<CFGeometry>& geometry) { geometry_ = geometry; };

    /// @brief Set the desired goal pose for this agent
    ///
    /// @param goal_pose Transformation from World_CS to EE_CS
    void setGoalPose(const Eigen::Affine3d& goal_pose) { settings_.internal.goal_pose = goal_pose; };

    /// @brief Get the desired goal pose for this agent
    ///
    /// @returns goal_pose Transformation from World_CS to EE_CS
    Eigen::Affine3d getGoalPose() const { return settings_.internal.goal_pose; };

    /// @brief Set the settings of this agent to new settings
    ///
    /// @attention Possible bug ahead: The obstacle_limit_distance in the settings defines the geometry class that has
    /// to be used. If the obstacle_limit_distance is not maintained when the geometry changes and vice versa,
    /// it may differ from the real value used by the geometry and therefore by the agent.
    void setSettings(const CFAgentSettings& settings) { settings_ = settings; };

    /// @brief Get the latest agent settings
    const CFAgentSettings& getSettings() const { return settings_; };

    /// @brief Get the latest agent settings (Non const overload for operator chaining)
    CFAgentSettings& getSettings() { return settings_; };

    /// @brief Set the lookup for object ids to rotation vector for this agent
    ///
    /// @param rotation_vectors New lookup table, that assigns a rotation vector to each known object with id
    void setRotationVectors(const std::vector<std::map<int, Eigen::Vector3d>>& rotation_vectors);

    /// @brief Calculate the velocity limiting controller (VLC) force
    ///
    /// The force calculation depends on the current position and velocity of robot_dynamic_ and state of geometry_.
    ///
    /// Behaviour can be modified through:
    /// - settings_.k_g
    /// - settings_.k_d
    /// - settings_.translational_velocity_limit
    /// - settings_.rotational_velocity_limit
    ///
    /// The VLC is the only component inside the agent, that calculates torques for the ee_wrench.
    /// The solution proposed by Yuan1988 is used, that calculates a artificial quaternion difference,
    /// which is transformed to ee_torques by the controller.
    ///
    /// If the agent settings do have a moving_goal and it is not the last time step of it or near the global goal,
    /// this is used for the goal pose regarding the translation of this calculation. Otherwise and for the rotation the
    /// static global goal_pose of the settings is used. Make sure, that the moving_goals active time_step that is used
    /// for getGoalPose() is set to the correct time before calling this function. This is usually done inside
    /// calculateStep().
    ///
    /// Regarding the desired velocity for translation when using moving goals:
    /// To avoid a deceleration at intermediate goals only the velocity direction to the intermediate goals is used.
    /// The artificial_desired_translation_velocity is scaled with the magnitude of the desired velocity to the global
    /// goal (often vmax). Therefore, in most cases the agent does not decelerate, even if the intermediate goal is
    /// already almost reached, because it still is to far away from the global goal.
    ///
    /// @returns EE-Wrench in World_CS [N,Nm]
    Vector6d calculateVLCForce() const;

    Vector3d calculateSelfCollisionAvoidanceForce() const;



    /// @brief Calculate the scaling factors of the goal force according to Ataka et al.
    /// "Reactive magnetic-field-inspired navigation method for robots in unknown 
    /// convex 3-d environments", 2018
    ///
    /// The combined factor k_gr is the product of four factors w1-w3.
    ///
    /// w1: Scaling s.t. the attractive force gets smaller the closer the obstacle is.
    /// w2: Scaling s.t. the attractive force increases if no obstacle is
    /// between the robot and its goal and decreases otherwise.
    ///
    /// @attention Factor is 1, if no object inside settings_.obstacle_limit_distance.
    /// The factor does not scale continuous with the distance to an object but "jumps" directly to < 0.96
    /// (caused by w1(minimal_obstacle_distance == settings_.obstacle_limit_distance)) if one object is found.
    /// The jump threshold can be configured by the factor alpha of w1. To minimize the jump but still have a "flat"
    /// exponential decrease, alpha is set to 0.3. An alpha of 1 would led to a jump to 0.63.
    ///
    /// @attention This scaling factor only uses translation!
    ///
    /// @attention To minimize calculation effort, this function does update the minimal_obstacle_distance for the
    /// dynamic history as a side effect.
    ///
    /// If the agent settings do have a moving_goal and it is not the last time step of it,
    /// this is used for the goal pose of this calculation. Otherwise the static goal_pose of the settings is used.
    /// Make sure, that the moving_goals active time_step that is used for getGoalPose()
    /// is set for to the correct time before calling this function. This is usually done inside calculateStep().
    ///
    /// @return k_gr, the scaling factor to weaken the influence of the VLC for the calculation of the combined force
    double calculateAtakaGoalRelaxationFactors(double minimal_obstacle_distance) const;

    /// @brief Calculate the scaling factor w3 of the goal force according to 
    /// Becker et al. "Circular fields and predictive multi-agents for online 
    /// global trajectory planning", 2021
    ///
    /// Reduce the calculated VLC force factor if it acts against the latest velocity.
    ///
    /// @attention This scaling factor only uses translation!
    ///
    /// @param translation_vlc_force First three components for translation of latest vlc force
    /// @param circular_fields_force Latest force based on circular fields. If this is 0, the result automatically 1
    ///
    /// @return k_gr, the scaling factor to weaken the influence of the VLC for the calculation of the combined force.
    /// Multiply this with the result of calculateAtakaGoalRelaxationFactors() to get the real k_gr.
    double calculateNonConvexGoalRelaxationFactor(const Eigen::Vector3d& translation_vlc_force,
                                               const Eigen::Vector3d& circular_fields_force) const;

    /// @brief Calculate the CF forces for all FAPs as well as the VLC force for the EE for the current position and
    /// velocity of robot_dynamic_ and state of geometry_
    ///
    ///
    /// Because geometry_->getForceCalculationData() returns only values for known and defined
    /// rotation vectors, you need to update the rotations vectors through validateAddRotationVectorsEndEffector()
    /// before a call to this function. Otherwise a new object could be inside obstacle_limit_distance for which no
    /// rotation vector is defined and that is therefore not considered for the force calculation.
    ///
    /// Not all points are considered. If the agent moves in the direction of the (intermediate) goal, all points from
    /// which the agent moves at least in a 5° angle away from are ignored for the force calculation.
    /// See  PCObject::getForceCalculationData() for a detailed explanation.
    ///
    /// @returns A vector containing FAP wrenches in World_CS
    std::vector<Vector6d> calculateTotalForces();

    /// @brief Calculate one simulation step of the robot
    ///
    /// This function calculates F_VLC and F_CF and simulates one step
    /// of the dynamic under the influence of these forces.
    ///
    /// Please update the position and velocity of the dynamic and the point cloud in geometry if you
    /// want to get the reaction to the real system state.
    ///
    /// The results of this calculation can be used by calling this.robotDynamic().get<value>().
    ///
    /// The underlying functions calculateVLCForce() and calculateAtakaGoalRelaxationFactors() use the moving_goal
    /// of the settings if it is available. This function increments the time_step of the moving goal before calling the
    /// other operations.
    void calculateStep();

    /// @brief Return a temporary reference to the RobotDynamic.
    ///
    /// This reference can be used to get calculation results of the dynamic.
    /// Exposing this reference to call the getters and setters on robotDynamic() minimizes gluecode.
    /// Otherwise the getters and setters of dynamic would need to be mirrored inside this class.
    ///
    /// @attention Do not cache the reference result of this function.
    /// The reference is only temporary and may become invalid.
    std::unique_ptr<AbstractRobotDynamic>& robotDynamic() { return robot_dynamic_; };

    /// @brief Possible exit status for predict()
    enum class PredictionExitStatus {
        MAX_PREDICTION_STEPS_REACHED,
        PRECALCULATION_FINISHED,
        ABORTED,
        GOAL_REACHED,
        COLLIDED,
    };

    /// @brief Run the prediction
    ///
    /// This function is designed to be run in another thread and calculate no_of_steps of the robot dynamic under the
    /// influence of the circular fields reaction.
    /// It is thread safe, if no other functions with the exception of killPrediction() are called from the main thread
    /// and all signals are connected via a QueuedConnection.
    ///
    /// This function is able to emit the signals sgn_split(), sgn_inherit(), sgn_bet() and sgn_prediction_result()
    /// during its runtime.:
    ///
    /// - sgn_bet() is emitted if the prediction returns because it reached a goal or the no_of_steps are reached
    /// - sgn_evolve() is emitted if the prediction reached the goal
    /// - sgn_split() is emitted if a new obstacle was discovered
    /// - sgn_prediction_result() is emitted if the prediction returns because it reached a goal or the no_of_steps are
    /// reached
    ///
    /// @attention evolve and split are not implemented for robotic manipulator and only work for point mass robots. Using theses functionalities requires significant adaptions of this version.
    ///
    /// The function is blocking and returns under the following conditions:
    /// - killPrediction() is called from another thread
    /// - no_of_steps is reached
    /// - The agent reached its goal.
    /// - the agent detects a collision with an object
    /// It will return the according exit status.
    ///
    /// @param no_of_steps Number of steps to predict. Any negative value means unlimited / until goal reached / killed
    /// / collided.
    ///
    /// @returns The exit status
    PredictionExitStatus predict(int no_of_steps);

    /// @brief Calculate a new moving goal based on an existing moving goal, that uses shortcuts
    ///
    /// ...if a position is in the line of sight.
    ///
    /// The function tries to calculate shortcuts for an existing moving goal with operation mode TIME. The intermediate
    /// goal poses will overwrite the existing moving_goal in the settings with operation mode POSITION.
    ///
    /// For a (start) pose the agent is in, a binary search is conducted, which tries to find an intermediate goal pose
    /// in the line of sight of the start pose. A direct line from start to goal position (rotation is ignored for this
    /// calculation) shall be found, under the following conditions:
    /// - The intermediate pose is time-step-wise as close to the original goal as possible. / Skip as much as possible.
    /// - The line has no collision with a point cloud / The minimal distance between the line and the point cloud is
    /// larger than a collision threshold.
    /// - The lines minimal distance d_k(*) to a point of the point cloud is never
    /// less than a*max(d_k(intermediate_goal),d_k(latest_pose)) with 'a' beeing a scaling factor. / Do not cut corners
    /// if under the influence of the circular fields force.
    ///
    /// The search will be repeated from every intermediate goal until the goal is reached or MAX_LOS_SHORTCUTS
    /// exceeded.
    ///
    /// This function is designed to be run asynchronously to the agent manager thread. It can be treated as a
    /// pre step to predict().
    /// It is thread safe, if no other functions with the exception of killPrediction() are called from the main thread
    /// and all signals are connected via a QueuedConnection.
    ///
    /// The function is blocking and returns under the following conditions:
    /// - killPrediction() is called from another thread
    /// - MAX_LOS_SHORTCUTS is reached
    /// - A shortcut found a way to the goal
    /// The function wil either return PredictionExitStatus::ABORTED for the first case or
    /// PredictionExitStatus::PRECALCULATION_FINISHED for the other cases.
    ///
    /// To predict the agent that uses the calculated shortcuts, simply call predict() after this function. The agent
    /// is fully configured after calculateLineOfSightShortcuts() to start its prediction.
    /// @attention Is not implemented for robotic manipulator and only works for point mass robots. Using this functionality requires significant adaptions of this version.
    PredictionExitStatus calculateLineOfSightShortcuts();

    /// @brief For a list of poses find an intermediate pose that is in the line of sight from the start_pose
    ///
    /// poses is a list of poses from a previous moving goal.
    ///
    /// For a (start) pose the agent is in, a binary search is conducted, which tries to find an intermediate goal pose
    /// in the line of sight of the start pose. A direct line from start to goal position (rotation is ignored for this
    /// calculation) shall be found, under the following conditions:
    /// - The intermediate pose is time-step-wise as close to the original goal as possible. / Skip as much as possible.
    /// - The line has no collision with a point cloud / The minimal distance between the line and the point cloud is
    /// larger than a collision threshold.
    /// - The lines minimal distance d_k(*) to a point of the point cloud is never
    /// less than a*max(d_k(intermediate_goal),d_k(latest_pose)) with 'a' beeing a scaling factor. / Do not cut corners
    /// if under the influence of the circular fields force.
    ///
    /// Fallback solution if the criteria can not be met is poses[start_index-1];
    ///
    /// @attention Please note, that the poses as described in the MovingGoal class are sorted ascending by age.
    /// The latest (e.g. t = 42) pose is in the front of the buffer (index = 0). The oldest pose (t = 0) is at the end
    /// (index = 42).
    ///
    /// @param poses List of poses of a previous trajectory ordered ascending by age.
    /// @param time_steps List of time_steps of a previous trajectory ordered ascending by age or empty buffer if no
    /// time_steps are known. Each index should map to a pose in poses.
    /// @param start_index Index of the pose in poses that is the start pose for the search
    /// @param max_search_iterations Maximum search iterations to find the final solution. If no final solution is found
    /// to this point, the returned result is an approximation.
    ///
    /// @returns {Best found goal pose, Index of this pose in poses}
    ///
    /// @throws std::invalid_argument("Given start_index is out of bounds or poses is smaller than 2 elements.");
    /// @attention Is not implemented for robotic manipulator and only works for point mass robots. Using this functionality requires significant adaptions of this version.
    std::pair<Eigen::Affine3d, int> lineOfSightBinarySearch(const boost::circular_buffer<Eigen::Affine3d>& poses,
                                                            const boost::circular_buffer<uint64_t>& time_steps,
                                                            int start_index, int max_search_iterations) const;

    /// @brief Utility function to calculate the 'Do not cut corners' threshold of calculateLineOfSightShortcuts()
    ///
    /// @attention Please note, that the poses as described in the MovingGoal class are sorted ascending by age.
    /// The latest (e.g. t = 42) pose is in the front of the buffer (index = 0). The oldest pose (t = 0) is at the end
    /// (index = 42).
    ///
    /// @param poses List of poses of a previous trajectory ordered ascending by age.
    /// @param time_steps List of time_steps of a previous trajectory ordered ascending by age or empty buffer if no
    /// time_steps are known. Each index should map to a pose in poses.
    /// @param start_index Index of the pose in poses that is the start pose for the search
    /// @param end_index Index of the pose in poses that is the end candidate pose for the search
    ///
    /// @returns {threshold, start_position, end_position}
    /// @attention Is not implemented for robotic manipulator and only works for point mass robots. Using this functionality requires significant adaptions of this version.
    std::tuple<double, TimedPoint, TimedPoint> calculateThresholdStartEndPoses(
        const boost::circular_buffer<Eigen::Affine3d>& poses, const boost::circular_buffer<uint64_t>& time_steps,
        int start_index, int end_index) const;

    /// @brief Calculate the score of the latest trajectory for this agent
    ///
    /// The score consists of 7 rules that usually are summed up:
    ///
    /// 1. If a collision ocurred a.k.a.the minimal distance to an object is smaller than collision_distance the
    /// complete score is 0 - regardless what the other score components would unfold to.
    ///
    /// 2. If the goal is reached with the prediction, a.k.a the translational distance to the goal is smaller than
    /// a threshold 100k points are added to the score. The score is always (except 1.) greater than 100k if the
    /// goal was reached and smaller than 100k if it was not reached.
    ///
    /// 3. If the goal is not reached, the remaining distance to the goal will be scored exponentially in a range
    /// between 0 and 10k.
    /// Score = 10000*e^(-(remaining_distance / goal_influence_limit_distance))
    /// It will converge to 10k, if the remaining_distance decreases,
    /// be ~3670 for remaining_distance == goal_influence_limit_distance
    /// and converge to 0 for larger remaining distances.
    /// The goal_influence_limit_distance is used to scale the distance score, because it is usually recommended to
    /// be similar to the distance from start to goal. This makes for a good reference scale point without the need
    /// to calculate and save the actual distance inside CFAgent. However, for future uses the reference scale point
    /// can be replaced with any other fitting value if necessary.
    ///
    /// 4. The duration of the current prediction is substracted from the base score in a range from 5000 to 0.
    /// For each calculation step one point is subtracted from the start score of 1000.
    /// For a prediction step_time of 0.1 seconds the worst distinguishable duration would be 20s.
    ///
    /// 5. If no joint limit avoidance movements occured, the full score 500 is added. Otherwise the score is lowered
    /// by the percentage of joint limit avoidance movements from the total motions.
    ///
    /// 6. We use the manipulability index to avoid singularities by rewarding a
    /// higher minimum manipulability of the agent trajectory
    ///
    /// 7. If no collision occurred, the minimal distance of a trajectory to any object is scored between 0 and 200.
    /// Score = 10(1-e^(collision_distance-minimal_object_distance))
    /// It will be zero for minimal_object_distance == collision_distance
    /// and increase exponentially for any value greater than collision_distance to a maximum of 10.
    ///
    ///
    /// By setting extreme_estimates_only to true, there exists the possibility to only check for conditions 1
    /// and 2. If neither of this extreme conditions is fulfilled, the value 1.0 is returned.
    double calculatePredictionScore(bool extreme_estimates_only = false) const;

    /// @brief Kill a running prediction loop of predict()
    ///
    /// If this function is called from another thread, predict() will return with PredictionExitStatus::aborted after
    /// the current calculation step finished.
    void killPrediction() { predict_ = false; };

    /// @brief Calculates a new rotation vector regarding the robot end effector from the current velocity (3D only)
    ///
    /// @attention Note that it is expected that robot_vel is not zero.
    /// Therefore, you usually have to replace robot_vel with a substitute value like xg- x or similar, if this is the
    /// case.
    ///
    /// @param robot_vel Current translational end effector velocity
    /// @return Eigen::Vector3d Returns calculated rotation vector
    static Eigen::Vector3d calculateNewRotationVectorEndEffector(Eigen::Vector3d robot_vel);

    /// @brief Calculates a new rotation vector for a force application point based on the compliance matrix of the
    /// robot, considering the force application point
    ///
    /// The rotation vector is chosen, so that the resulting current vector is equivalent to the current vector from
    /// [Ataka2018], thus a projection of the current velocity onto the object surface.
    ///
    /// @param fap_vel Current velocity of the Force Application Point
    /// @param object_distance_vector Minimal distance vector from the FAP to the object
    ///
    /// @returns Calculated rotation vector
    Eigen::Vector3d calculateNewRotationVectorBody(Eigen::Vector3d fap_vel,
                                                   Eigen::Vector3d object_distance_vector) const;

signals:

    /// @brief Signal to inherit the current settings to a new agent and start prediction from the latest robot position
    ///
    /// This signal is usually emitted, if this agent reached the goal. It signals, that another agent with the same
    /// base settings, especially rotation vectors, should be generated with some minor changes.
    ///
    /// @param settings Current settings of the original agent
    /// @attention Is not implemented for robotic manipulator and only works for point mass robots. Using this functionality requires significant adaptions of this version.
    void sgnEvolve(const CFAgentSettings& settings);

    /// @brief Signal that the current settings reached the goal and the smooth operation should be started.
    ///
    /// @param settings Current settings of the original agent
    /// @param history Dynamic history to use as a moving goal to the new agents
    /// @param score Score of the given settings with history
    /// @attention Is not implemented for robotic manipulator and only works for point mass robots. Using this functionality requires significant adaptions of this version.
    void sgnSmooth(const CFAgentSettings& settings, const AbstractRobotDynamicHistory& history, double score);

    /// @brief Bet on the best agent settings with the current agent settings and score.
    ///
    /// Please see calculatePredictionScore() on how the score is calculated.
    ///
    /// @param score Score of the given settings with history
    /// @param settings Current settings of the original agent
    /// @param history Current history of the original agent
    void sgnBet(double score, const CFAgentSettings& settings, const AbstractRobotDynamicHistory& history);

    /// @brief Signal, that a prediction result is ready for broadcasting / visualization
    ///
    /// @param agent_id Id of this agent
    /// @param score Latest score of this prediction trajectory
    /// @param history Trajectory history of the dynamic
    void sgnPredictionResult(int agent_id, double score, const AbstractRobotDynamicHistory& history);
};
#endif
