#include <boost/circular_buffer.hpp>
#include <boost/optional.hpp>
#include <numeric>

#include "ros/ros.h"
#ifndef DS_STOPWATCH_H
#define DS_STOPWATCH_H

/// @brief Measure the execution time between tic() and toc() n time and print the mean
class DSStopwatch {
    /// @brief How many samples should this class take, before printing?
    const int MEAN_SAMPLES_ = 1;

    /// @brief Counter of how many samples were already taken
    int sample_counter_ = 0;

    /// @brief Time point of last tic call
    std::chrono::_V2::system_clock::time_point tic_start_ = std::chrono::system_clock::now();

    /// @brief Buffer of all measured durations between tic and toc.
    boost::circular_buffer<std::chrono::microseconds> duration_buffer_;

    /// @brief Name of what is measured with this object
    ///
    /// Will be used for the printing
    std::string measurement_name_ = "";

    /// @brief Should an error message be printed, if triggered?
    const bool PRINT_RESULT_;

public:
    /// @brief Create a  stopwatch
    ///
    /// @param mean_samples How many samples should this stopwatch take, before printing the result.mean_samples. def 1
    /// @param measurement_name Name of the measurement this stopwatch takes. Default: Empty string.
    /// @param print_result Print the result if available. This should only be false in unit tests.
    explicit DSStopwatch(int mean_samples = 1, const std::string& measurement_name = "", bool print_result = true);
    ~DSStopwatch() = default;

    /// @brief Start the measurement of one iteration
    void tic();

    /// @brief Measure the time form last tic() call and print the mean value if mean_samples are taken.
    ///
    /// If not mean_samples are already taken only takes the time from last tic() call.
    ///
    /// The message will be: <measurement_name_> took average <mean_value> µs over <mean_samples> iterations.
    ///
    /// @returns The mean value printed or boost::none, if nothing printed.
    boost::optional<std::chrono::microseconds> toc();
};

///@brief Special version of DSStopwatch that can be used in loops to monitor the loop time faithfulness
class DSWatchdog {
    /// @brief Stopwatch to use for stopping the loop time
    DSStopwatch stopwatch_;

    /// @brief Duration, which the check function checks and warns against
    std::chrono::microseconds trigger_time_mus_;

    /// @brief check() was at least called once and time measurement startet
    ///
    /// If this is false, no evaluation takes place during check()
    bool first_check_called_ = false;

    /// @brief How many samples should this class take to calculate the mean
    const int MEAN_SAMPLES_ = 1;

    /// @brief Name of what is measured with this object
    ///
    /// Will be used for the printing
    ///
    std::string measurement_name_ = "Watchdog";

    /// @brief Should an error message be printed, if triggered?
    const bool PRINT_RESULT_;

public:
    /// @brief Create a watchdog
    ///
    /// @param trigger_time_mus Time in micro seconds at which the watchdog triggers
    /// @param measurement_name Name of the measurement this watchdog watches. Default: Watchdog.
    /// @param mean_samples How many samples should this watchdog take, before printing checking the result. Default: 1
    /// @param print_result Print the result if available. This should only be false in unit tests.
    explicit DSWatchdog(std::chrono::microseconds trigger_time_mus, const std::string& measurement_name = "Watchdog",
                        int mean_samples = 1, bool print_result = true);
    ~DSWatchdog() = default;

    /// @brief Check, that the duration between two check calls (in a loop) is smaller than trigger_time_mus
    ///
    /// Otherwise print a ROS_ERROR (if print_result == true) and return false.
    ///
    /// To use the watchdog, simply include it in a loop function you would like to measure. If the time, the loop takes
    /// till it reaches the same code line, takes longer than trigger_time_mus, the watchdog triggers the error message.
    ///
    /// The message will be: <measurement_name_> triggered. Execution took <measurement> µs over <mean_samples>
    /// iterations. Allowed were <trigger_time_mus_> µs.
    bool check();
};

#endif