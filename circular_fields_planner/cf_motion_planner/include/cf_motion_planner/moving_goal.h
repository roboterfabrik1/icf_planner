#include <Eigen/Core>
#include <Eigen/Geometry>
#include <boost/circular_buffer.hpp>
#include <boost/optional.hpp>

#include "cf_motion_planner/abstract_robot_dynamic_history.h"
#include "ros/ros.h"

#ifndef MOVING_GOAL_H
#define MOVING_GOAL_H
/// @brief Datastructure, that represents the desired goal pose (over time)
///
/// Similarly to the #AbstractRobotDynamicHistory the MovingGoal stores in the default mode time step numbers and poses_
/// in a circular buffer. By design a value in time_steps_ should match and assign a time_step number to a
/// value in poses_ with the same index. However, the code that maintains the MovingGoal has to make sure this is
/// the case.
///
/// Example:
/// time_steps[i] => 42, poses_[i] => Home
/// The robot should be at time_step 42 at Home.
///
/// incrementTimeStep() is used to increment the time step. If no value was set previously the argument value is
/// searched for. Therefore, offsets between the best agent time step and the returned goal are possible. After
/// incrementing the time step the goal for this time_step can be queried through getGoalPose().
///
/// Example:
/// The best agent's latest time step is number 42. It asks a moving goal object, that has some data and was never used
/// before, what the goal is. If incrementTimeStep(42) is called, it will return the value for the same time step. In
/// the next iteration (best agents time step is 43) a call to incrementTimeStep(42) will return the data for
/// time_step 43, because the argument is ignored.
/// However, using incrementTimeStep(52) as the first call will always return the data 10 steps "in the future".
/// As a result a second call with incrementTimeStep(52) will return the results in this data structure for
/// time_step = 53.
///
/// Requesting time_steps that would be out of bounds will return either bounds. Therefore, incrementTimeStep() can be
/// called endlessly. If there are not more known values, it will always return the latest value.
///
/// As an addition an alternate operation_mode can be used. The POSITION operation mode does not use the time_steps_
/// buffer. This can be used to increment poses after an agent reached these poses. Instead of incrementTimeStep()
/// the function nextGoalPose() has to be used. However, the functions isLatestTimeStep() and resetTimeStep() can still
/// be used to check, if it is the last pose in the sequence or to reset the index, respectively.
class MovingGoal {
public:
    /// @brief Operation Modes of the Moving Goal
    enum class MovingGoalOperationMode { TIME, POSITION };

private:
    /// @brief Buffer size to use inside the constructor without arguments
    static const int DEFAULT_HISTORY_BUFFER_SIZE = 10000;

    /// @brief Operation mode of this goal. Default TIME
    MovingGoalOperationMode operation_mode_ = MovingGoalOperationMode::TIME;

    /// @brief Time step, that was first used in incrementTimeStep
    boost::optional<uint64_t> output_start_time_step_;

    /// @brief Index, which time_step is currently used for getGoalPose()
    ///
    /// Can be incremented by incrementTimeStep()
    boost::optional<int> time_step_index_;

    /// @brief Overall number of the calculation step for a value in these buffers
    ///
    /// Could technically overflow. However, due to uint64_t datatype very unlikely for most testcases.
    boost::circular_buffer<uint64_t> time_steps_;

    /// @brief Buffer of poses
    boost::circular_buffer<Eigen::Affine3d> poses_;

    /// @brief The offset that was used in the first call of incrementTimeStep()
    uint64_t used_time_offset_ = 0;

public:
    /// @brief Constructor, that sets the buffers sizes to DEFAULT_HISTORY_BUFFER_SIZE
    MovingGoal();

    /// @brief Constructor, that sets the buffers sizes to history_buffer_size
    ///
    /// @param history_buffer_size Size of all buffers
    explicit MovingGoal(int history_buffer_size);

    /// @name Element setters
    /// @{

    /// @brief Change the operation mode to a new_mode.
    ///
    /// This function should usually be called after construction of a new MovingGoal. Calling it on an already used
    /// data structure may lead to undefined behaviour.
    ///
    /// Please also note that the default mode is TIME. Therefore a change is only necessary if you would like to use
    /// POSITION
    void setOperationMode(MovingGoalOperationMode new_mode) { operation_mode_ = new_mode; };

    /// @brief Return the operation mode of this moving goal.
    MovingGoalOperationMode getOperationMode() const { return operation_mode_; };

    /// @brief Set the time step buffer this data structure should use
    ///
    /// @attention The structure should have the same size as poses_ and increase its step elements only by 1.
    /// The latest / greatest element is expected to be at the front.
    /// The oldest / smallest element is expected to be at the back.
    /// However, these conditions are not checked.
    ///
    /// @attention Only needed for MovingGoalOperationMode::TIME
    void setTimeSteps(const boost::circular_buffer<uint64_t>& time_steps) { time_steps_ = time_steps; };

    /// @brief Set the poses buffer this data structure should use
    ///
    /// @attention The structure should have the same size as time_steps_ and match the time_step value with the same
    /// index in time_step it is used for.
    /// However, these conditions are not checked.
    void setPoses(const boost::circular_buffer<Eigen::Affine3d>& poses) { poses_ = poses; };

    /// @brief Increment the time step by one that is used for getGoalPose()
    ///
    /// Uses start_step if there is no previous time step. Optionally explicitly record a time offset.
    /// The real used start step is start_step + offset
    ///
    /// Requesting time_steps that would be out of bounds will return either bounds.
    /// Therefore, incrementTimeStep() can be called endlessly.
    /// If there are not more known values, it will always return the latest value.
    ///
    /// @attention Only allowed for MovingGoalOperationMode::TIME. Use nextGoalPose() for POSITION.
    void incrementTimeStep(uint64_t start_step = 0, uint64_t offset = 0);

    /// @brief Increment the goal used for used for getGoalPose()
    ///
    /// nextGoalPose() can be called endlessly.
    /// If there are not more known values, it will always return the latest value.
    ///
    /// @attention Only allowed for MovingGoalOperationMode::POSITION. Use incrementTimeStep() for TIME.
    void nextGoalPose();

    /// @brief Get the Goal Pose for the latest through incrementTimeStep() modified time_step
    const Eigen::Affine3d& getGoalPose() const;

    /// @brief Return true, if the incrementation already reached the latest known timestep of the underlying history
    ///
    /// If this is true a call to incrementTimeStep() will change nothing.
    bool isLatestTimeStep() const;

    /// @brief Reset the time step
    ///
    /// A subsequent call to incrementTimeStep() will start at start_step with offset again
    void resetTimeStep();

    /// @brief Return, if the given time_step can be found in the history
    ///
    /// @attention Only allowed for MovingGoalOperationMode::TIME
    bool isValidTimeStep(uint64_t time_step);
    /// @}

    /// @name History getters
    /// @{

    /// @brief Get Time step, that was first used in incrementTimeStep (start_step + offset)
    boost::optional<uint64_t> getOutputStartTimeStep() const { return output_start_time_step_; }

    /// @brief Get time steps history
    boost::circular_buffer<uint64_t> getTimeSteps() const { return time_steps_; }

    /// @brief Get poses history
    boost::circular_buffer<Eigen::Affine3d> getPoses() const { return poses_; }

    /// @brief Get the offset that was used in the first call of incrementTimeStep()
    uint64_t getTimeOffset() const { return used_time_offset_; }
    /// @}

    /// @brief Construct a MovingGoal object from a AbstractRobotDynamicHistory
    ///
    /// This pseudo constructor basically copies the history poses and time_steps
    static std::unique_ptr<MovingGoal> fromHistory(const AbstractRobotDynamicHistory& history);
};
#endif