#include <std_msgs/ColorRGBA.h>

#ifndef DS_COLOR_H
#define DS_COLOR_H

/// @brief Enum of known and defined colors
enum class DSColor { PRIMARY, SECONDARY, SUCCESS, DANGER, WARNING, INFO, LIGHT, DARK };

/// @brief Converter Class that converts DSColor to RGBA values
class DSColorTheme {
protected:
    /// @brief Utility function that acts as simplified a constructor of std_msgs::ColorRGBA
    static std_msgs::ColorRGBA createRGBA(double r, double g, double b, double a);

public:
    /// @brief Convert a DSColor to std_msgs::ColorRGBA
    ///
    /// The values of RGBA depend on the choosen color theme.
    /// Returns White, if color is unknown or not implemented.
    virtual std_msgs::ColorRGBA toRGBA(DSColor color) = 0;
};

/// @brief Default color theme
///
/// The theme is based on the IMESORANGE and IMESBLUE as PRIMARY and SECONDARY
/// and Bootstrap 4 Background color for everything else.
class DSColorStandardTheme : DSColorTheme {
public:
    /// @brief Convert a DSColor to std_msgs::ColorRGBA
    ///
    /// The values of RGBA depend on the choosen color theme.
    /// Returns White, if color is unknown or not implemented.
    std_msgs::ColorRGBA toRGBA(DSColor color) override;
};

#endif