#include <XmlRpcValue.h>

#include <string>
#include <vector>

#include "cf_motion_planner/utility.h"
#include "ros/ros.h"

#ifndef DYNAMIC_SETTINGS_H
#define DYNAMIC_SETTINGS_H

/// @brief Abstract base class for any kind of dynamic settings
///
/// This class delivers blob storage for double, bool, int and string.
/// Any dynamic class may inherit this class for its settings and define
/// named getters and setters for the blob elements
class AbstractRobotDynamicSettings {
protected:
    std::string settings_type_;
    std::vector<double> double_blob_;
    std::vector<bool> bool_blob_;
    std::vector<int> int_blob_;
    std::vector<std::string> string_blob_;

public:
    AbstractRobotDynamicSettings() = default;
    virtual ~AbstractRobotDynamicSettings() = default;

    /// @brief Set Step time of the predictive agents in seconds
    virtual void setStepTime(double step_time) = 0;

    /// @brief Get Step time of the predictive agents in seconds
    virtual double getStepTime() const = 0;

    /// @brief Equality operator. Makes use of almostEqualRelative() for double comparison.
    bool operator==(const AbstractRobotDynamicSettings& rhs) const;

    /// @brief Return the name of the settings type e.g. PointMassRobotDynamicSettings
    std::string getSettingsType() const { return settings_type_; };
};

/// @brief Settings for a PointMassRobotDynamic
class PointMassRobotDynamicSettings : public AbstractRobotDynamicSettings {
public:
    PointMassRobotDynamicSettings();
    ~PointMassRobotDynamicSettings() override = default;

    /// @brief Set Step time of the predictive agents in seconds
    void setStepTime(double step_time) override { double_blob_[0] = step_time; };

    /// @brief Get Step time of the predictive agents in seconds
    double getStepTime() const override { return double_blob_[0]; };

    /// @brief Set mass in kg
    void setMass(double mass_kg) { double_blob_[1] = mass_kg; };

    /// @brief Get mass in kg
    double getMass() const { return double_blob_[1]; };

    /// @brief Set mass in kg
    void setMaxVelocity(double vel_ms) { double_blob_[2] = vel_ms; };

    /// @brief Get mass in kg
    double getMaxVelocity() const { return double_blob_[2]; };

    /// @brief Constructing parsing function for a entry structure on the ros parameter server
    ///
    /// This function expects a XmlRpcValue that from a design standpoint consist of a dict (std::pair)
    /// with the keys and values for the parameters of this struct.
    ///
    /// Since the step time should be automatically set based on the planner settings, it is removed from parsing.
    static PointMassRobotDynamicSettings generateFromRosParameterEntry(const XmlRpc::XmlRpcValue& ros_parameter_entry);

    /// @brief Check, if the given abstract settings are actually of type PointMassRobotDynamicSettings
    static bool isType(AbstractRobotDynamicSettings* settings);
};

/// @brief Settings for a RealRobotDynamic
class SimpleRobotDynamicSettings : public AbstractRobotDynamicSettings {
private:
    std::vector<double> joint_limits_qmin_;
    std::vector<double> joint_limits_qmax_;
    std::vector<double> joint_limits_qd_;
    std::vector<double> joint_limits_qdd_;
    std::vector<int> joint_types_;
    std::vector<double> DH_params_a_;
    std::vector<double> DH_params_alpha_;
    std::vector<double> DH_params_d_;
    std::vector<double> DH_params_theta_;
    double k_jla_;
    double k_man_;
    double d_null_;
    double d_jla_;
    bool scale_dynamic_;
    bool nullspace_projection_;
    bool manipulability_gradient_;

public:
    SimpleRobotDynamicSettings();
    ~SimpleRobotDynamicSettings() override = default;

    /// @brief Set Step time of the predictive agents in seconds
    void setStepTime(double step_time) override { double_blob_[0] = step_time; };

    /// @brief Get Step time of the predictive agents in seconds
    double getStepTime() const override { return double_blob_[0]; };

    /// @brief Set Step time of the predictive agents in seconds
    void setGainsKJLA(double k_jla) { k_jla_ = k_jla; };

    /// @brief Get Step time of the predictive agents in seconds
    double getGainsKJLA() const { return k_jla_; };

    /// @brief Set Step time of the predictive agents in seconds
    void setGainsKMAN(double k_man) { k_man_ = k_man; };

    /// @brief Get Step time of the predictive agents in seconds
    double getGainsKMAN() const { return k_man_; };

    /// @brief Set Step time of the predictive agents in seconds
    void setGainsDJLA(double d_jla) { d_jla_ = d_jla; };

    /// @brief Get Step time of the predictive agents in seconds
    double getGainsDJLA() const { return d_jla_; };

    /// @brief Set Step time of the predictive agents in seconds
    void setGainsDNULL(double d_null) { d_null_ = d_null; };

    /// @brief Get Step time of the predictive agents in seconds
    double getGainsDNULL() const { return d_null_; };

    /// @brief Get max joint positions
    std::vector<double> getMaxJointPositions() const { return joint_limits_qmax_; };

    /// @brief Set max joint positions
    void setMaxJointPosisions(std::vector<double> joint_limits_qmax) { joint_limits_qmax_ = joint_limits_qmax; };

    /// @brief Get min joint positions
    std::vector<double> getMinJointPositions() const { return joint_limits_qmin_; };

    /// @brief Set min joint positions
    void setMinJointPositions(std::vector<double> joint_limits_qmin) { joint_limits_qmin_ = joint_limits_qmin; };

    /// @brief Get max joint velocities
    std::vector<double> getMaxJointVelocities() const { return joint_limits_qd_; };

    /// @brief Set max joint velocities
    void setMaxJointVelocities(std::vector<double> joint_limits_qd) { joint_limits_qd_ = joint_limits_qd; };

    /// @brief Get max joint torques
    std::vector<double> getMaxJointTorques() const { return joint_limits_qdd_; };

    /// @brief Set max joint torques
    void setMaxJointTorques(std::vector<double> joint_limits_qdd) { joint_limits_qdd_ = joint_limits_qdd; };

    /// @brief Set joint types
    void setJointTypes(std::vector<int> joint_types) { joint_types = joint_types; };

    /// @brief Get Denavit Hartenberg parameter a
    std::vector<int> getJointTypes() const { return joint_types_; };

    /// @brief Set Denavit Hartenberg parameter a
    void setDHParameterA(std::vector<double> a) { DH_params_a_ = a; };

    /// @brief Get Denavit Hartenberg parameter a
    std::vector<double> getDHParameterA() const { return DH_params_a_; };

    /// @brief Set Denavit Hartenberg parameter alpha
    void setDHParameterAlpha(std::vector<double> alpha) { DH_params_alpha_ = alpha; };

    /// @brief Get Denavit Hartenberg parameter alpha
    std::vector<double> getDHParameterAlpha() const { return DH_params_alpha_; };

    /// @brief Set Denavit Hartenberg parameter d
    void setDHParameterD(std::vector<double> d) { DH_params_d_ = d; };

    /// @brief Get Denavit Hartenberg parameter d
    std::vector<double> getDHParameterD() const { return DH_params_d_; };

    /// @brief Set Denavit Hartenberg parameter theta
    void setDHParameterTheta(std::vector<double> theta) { DH_params_theta_ = theta; };

    /// @brief Get Denavit Hartenberg parameter theta
    std::vector<double> getDHParameterTheta() const { return DH_params_theta_; };

    /// @brief Get bool flag for manipulability gradient
    bool getManipulability() const { return manipulability_gradient_; };

    /// @brief Get bool flag for manipulability gradient
    bool getNullspace() const { return nullspace_projection_; };

    /// @brief Get bool flag for manipulability gradient
    bool getScale() const { return scale_dynamic_; };

    /// @brief Constructing parsing function for a entry structure on the ros parameter server
    ///
    /// This function expects a XmlRpcValue that from a design standpoint consist of a dict (std::pair)
    /// with the keys and values for the parameters of this struct.
    ///
    /// Since the step time should be automatically set based on the planner settings, it is removed from parsing.
    static SimpleRobotDynamicSettings generateFromRosParameterEntry(const XmlRpc::XmlRpcValue& ros_parameter_entry);

    /// @brief Check, if the given abstract settings are actually of type RealRobotDynamicSettings
    static bool isType(AbstractRobotDynamicSettings* settings);
};
#endif