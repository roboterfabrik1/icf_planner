#include <pcl/point_types.h>
#include <pcl_ros/point_cloud.h>

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <QReadLocker>
#include <QReadWriteLock>
#include <QWriteLocker>
#include <map>
#include <utility>

#include "cf_motion_planner/pc_object.h"
#include "cf_motion_planner/pc_types.h"
#include "pcl_obstacle_generator/MovingObstacles.h"

#ifndef CF_GEOMETRY_H
#define CF_GEOMETRY_H

/// @brief Class for all geometric calculations
///
/// This class represents the taskspace of the system and all obstacles in it.
/// It can be used to get distance calculations and values for a given robot pose.
///
/// Please see CFObject for a detailed explanation, how interpolation of object position is handled with the
/// TimedPoint data structure.
///
/// This class uses a circular_buffer to cache the last 10 results of getCriticalObject().
///
/// The non static member functions are thread safe.
class CFGeometry {
    using Vector6d = Eigen::Matrix<double, 6, 1>;
    using PointCloud = pcl::PointCloud<pcl::PointXYZ>;
    using PointCloudNormal = pcl::PointCloud<pcl::Normal>;

    /// @brief Vector of all collision objects that can be used for distance calculations.
    std::vector<std::unique_ptr<PCObject>> pc_objects_;

    /// @brief Size of the critical object cache.
    static const int CRITICAL_OBJECT_CACHE_SIZE = 10;

    /// @brief Step time that is used for linear interpolation of future object position.
    ///
    /// See PCObject for a detailed explanation
    double step_time_s_ = 0.1;  // NOLINT(readability-magic-numbers) : Default value initialization

    /// @brief Cache for last 10 calculated values of getCriticalObject
    ///
    /// Is cleared if setNewData is called.
    boost::circular_buffer<std::pair<TimedPoint, PCObject*>> critical_object_cache_;

    /// @brief Advanced Mutex, that allows concurrent read actions of critical_object_cache_
    ///
    /// The critical_object_cache_ has to be guarded against concurrent manipulation.
    /// It is locked, if it is reset or appended to. Manipulations of the PCObjects in it do
    /// not have to be guarded explicitly because PCObject itself is thread safe.
    QReadWriteLock critical_object_cache_lock_;

    /// @brief Cache for last 10 calculated values of getCriticalObject
    ///
    /// Is cleared if setNewData is called.
    boost::circular_buffer<std::pair<TimedPoint, std::set<int>>> active_object_ids_cache_;

    /// @brief Advanced Mutex, that allows concurrent read actions of active_object_ids_cache_
    ///
    /// The critical_object_cache_ has to be guarded against concurrent manipulation.
    /// It is locked, if it is reset or appended to. Manipulations of the PCObjects in it do
    /// not have to be guarded explicitly because PCObject itself is thread safe.
    QReadWriteLock active_object_ids_cache_lock_;

    /// @brief Block computation of all requests if setNewData() is called.
    ///
    /// Set new data invalidates all running calculations. Therefore, the computation of setNewData() is exclusive
    /// against all other functions. The other functions however, are allowed to be called concurrently, because
    /// the underlying QObject queries are thread safe itself.
    ///
    /// Please note, that technically even the code behind the read lock could do write operations on a PCObject.
    QReadWriteLock new_data_lock_;

    /// @brief Maximum distance for distance calculation
    ///
    /// Also known as radius of the kd radius search
    /// If no point is inside this distance around the for_point argument, the returned values will be None.
    double maximum_search_distance_ = 1.0;

    /// @brief Maximum number of points to consider in the kdTree radius search of each object. 0 means unlimited.
    ///
    /// Example: if this value is 1 only the nearest point is used for all calculations, especially the current
    /// calculation
    int maximum_number_considered_points_per_object_ = 0;

    /// @brief Get the critical object for for_point, if distance less than maximum_search_distance.
    ///
    /// The critical object is the object for each point in 3DSpace, that has the minimum distance between the for_point
    /// and one of its vertices in comparison to all other objects.
    /// In other words, it is the one object, in which for_point is part of the voronoi region.
    ///
    /// The exposed PCObject can then be used to get the value of minimal distance or the vector to the critical vertex.
    ///
    /// @returns Pointer to the critical object for for_point, if lesser than maximum_search_distance, nullptr otherwise
    PCObject* getCriticalObject(const TimedPoint& for_point);

public:
    /// @brief Default constructor with a maximum_search_distance_ of 1 and critical_object_cache_ size of 10
    CFGeometry();

    /// @brief Constructor with a variable maximum_search_distance_ and critical_object_cache_ size of 10
    ///
    /// @param maximum_search_distance Maximum distance to search in for obstacles
    /// @param step_time_s Step time in seconds to use for object position interpolation (should match agent settings).
    /// @param maximum_number_considered_points_per_object Limit the number of points that shall be found for each
    /// single obstacle. Default value is 0, which means no limit.
    explicit CFGeometry(double maximum_search_distance, double step_time_s,
                        int maximum_number_considered_points_per_object = 0);

    /// @brief Returns the maximum search distance of this geometry object
    double getMaximumSearchDistance() const { return maximum_search_distance_; };

    /// @brief Update and set the point cloud object data
    ///
    /// This function parses the MovingObstacles message that is received through ros
    /// into an internal vector of PCObject. These PCObjects are then used for geometry calculation inside the planner.
    ///
    /// Three things can happen:
    /// 1. If a PCObject for a given id already exists, PCObject::setNewData is called on this object.
    /// 2. If no PCObject for a given id exists, it will be created.
    /// 3. If PCObject exists, but no data for this id is given in cloud, it will be deleted.
    ///
    /// Also resets critical_object_cache_.
    ///
    /// This function blocks the concurrent execution of getCriticalObject()
    /// and getElectricCurrentsAndSquareDistances().
    ///
    /// @param latest_obstacle_msg New obstacle cloud data
    /// @param corresponding_time_step The time step this data relates to. Relevant for object position interpolation
    /// inside PCObject. Should be time_step of CFAgentManager or CFPlanner.
    void setNewData(const pcl_obstacle_generator::MovingObstacles::Ptr& latest_obstacle_msg,
                    uint64_t corresponding_time_step);

    boost::optional<double> getCriticalObjectMinimalDistance(const TimedPoint& for_point);

    boost::optional<Eigen::Vector3d> getCriticalObjectMinimalVector(const TimedPoint& for_point);
    boost::optional<double> getCriticalObjectSquareDistance(const TimedPoint& for_point);

    /// @brief Get the object with id, if it exists.
    ///
    /// The exposed PCObject can then be used to query more values.
    ///
    /// @returns Pointer to the object with id, nullptr if not found
    PCObject* getObjectByID(int id);

    /// @brief Check, if the minimal distance of a point of this object to a line is never less than threshold
    ///
    /// This is basically a wrapper around CFObject::minimalDistanceToLineIsNotLesserThan() for every object of the
    /// geometry. Please see the mentioned function for implementation details.
    ///
    /// Let there be a line defined by a start and end point in R^3. This function checks, if there could exist a point
    /// of this object whose distance to the line is smaller than the given threshold.
    ///
    /// If true is returned, there is definitely no point that has a smaller distance to the line than threshold.
    /// However, due to the way the check works, the other statement is not true. If false is returned, there is a
    /// candidate point that could have a smaller distance than threshold, but it could also be a false positive.
    /// Therefore, the function only acts as an estimate.
    ///
    /// @attention Unfortunately, this method has side effects. It is possible, that false positives are calculated,
    /// because the used threshold to classify candidates is greater than threshold. However, in the worst case a false
    /// positive can only occur with a real distance that is 2/sqrt(3)*threshold which is an error about 15%. Please
    /// keep this statistic in mind when using this function.
    ///
    /// This function also supports the usage of TimedPoint in conjunction with moving obstacles.
    /// The time_steps that are used for the search points are a linearly interpolated between
    /// line_start_point.time_step and line_end_point.time_step.
    ///
    /// @param threshold Threshold distance to the line in m that the real distance should never be below.
    /// @param line_start_point Start point of the line
    /// @param line_end_point End point of the line
    ///
    /// @returns True if the distance to the line is never lesser than threshold. False, if it could
    /// be lesser (No final solution).
    bool minimalDistanceToLineIsNotLesserThan(double threshold, const TimedPoint& line_start_point,
                                              const TimedPoint& line_end_point);

    /// @brief Return the object ids of all those objects, that are inside maximum_search_distance
    std::set<int> getActiveObjectIds(const TimedPoint& for_point);

    /// @brief Get electric currents, square distances and relative velocities for each point and each object
    ///
    /// This function will return a list of tuples with electric current vectors, squared distances and relative
    /// velocity for each point, that is inside maximum_search_distance around for_point and whose normal points in the
    /// direction of for_point (a.k.a faces it). Furthermore, only those points are
    /// considered, from which the robot moves at least in an angle of 5° away from, unless the robot moves (relatively
    /// away from the goal).
    /// The electric currents also depend on the rotation vector for_rotation_vector.
    ///
    /// Results should be expected to be unordered.
    ///
    /// For a detailed explanation of the underlying function, see PCObject::getElectricCurrentsAndSquareDistances().
    ///
    /// @param for_point Position of robot in cartesian space to get the results for.
    /// @param for_object_ids_rotation_vectors Map from object ids to rotation vector for this object to use for
    /// electric current calculation.
    /// @param robot_velocity Not normalized robot velocity.
    /// @param goal_position Latest goal position the agent is pursuing
    ///
    /// @returns List of electric current vectors paired with squared distances and normalized relative velocity
    /// for each point and object facing for_point and satisfying relevant_direction_vector condition.
    std::vector<std::tuple<Eigen::Vector3d, double, Eigen::Vector3d>> getForceCalculationData(
        const TimedPoint& for_point, const std::map<int, Eigen::Vector3d>& for_object_ids_rotation_vectors,
        const Eigen::Vector3d& robot_velocity, const Eigen::Vector3d& goal_position);

    /// @brief Get electric currents, square distances and relative velocities for each point on a certain object
    ///
    /// This is essentially the same function as above, but performs only on a single Object
    ///
    /// @param for_point Position of robot in cartesian space to get the results for.
    /// @param for_object_ids_rotation_vectors Map from object ids to rotation vector for this object to use for
    /// electric current calculation.
    /// @param robot_velocity Not normalized robot velocity.
    /// @param goal_position Latest goal position the agent is pursuing
    ///
    /// @returns List of electric current vectors paired with squared distances and normalized relative velocity
    /// for each point and object facing for_point and satisfying relevant_direction_vector condition.
    std::vector<std::tuple<Eigen::Vector3d, double, Eigen::Vector3d>> getObjectForceCalculationData(
        const TimedPoint& for_point, const std::pair<int, Eigen::Vector3d>& for_object_ids_rotation_vectors,
        const Eigen::Vector3d& robot_velocity, const Eigen::Vector3d& goal_position);

    /// @brief Get the minimal vector to an obstacle for a given point at a given time
    ///
    /// @param for_point Position for which the vector should be determined
    /// @param object_id Id of the considered object
    ///
    /// @returns An optional 3D distance vector (none if not in limit distance)
    boost::optional<Eigen::Vector3d> getObjectMinimalVector(const TimedPoint& for_point, const int object_id);

    /// @brief Calculate the distance for each component from a current_pose to a goal_pose
    ///
    /// The first three components of the return value represent the simple difference between the
    /// translation vectors. E.g. Vector6d[0-2] = Delta Translation.
    ///
    /// Two methods are provided to calculate the rotational distance.
    /// They represent the imaginary delta parts for the quaternion
    /// difference between the two rotations: "Two coordinate systems coincide
    /// if, and only if, delta_q = 0, where delta_q is the vector component of
    /// the quaternion" that describes the relative orientation.
    /// The first method is based on "Closed-loop manipulator control
    /// using quaternion feedback" (Yuan, 1988)
    /// The second method is based on "Resolved-acceleration control of robot
    /// manipulators: A critical review with experiments" (Caccavale et al,
    /// 1998).
    /// Initial tests indicated that it works equally well. Test which one is faster.
    ///
    /// @attention Because of the quaterion difference in the result, the returned vector is not geometrically
    /// interpretable.
    ///
    ///
    /// With calculateTranslationGoalDistance() and calculateRotationGoalDistance() there exist two pseudo
    /// overloaded function that only calculate one specific component.
    static Vector6d calculateGoalDistance(const Eigen::Affine3d& current_pose, const Eigen::Affine3d& goal_pose);

    /// @brief Subcomponent of calculateGoalDistance that only calculates the translation part
    ///
    /// See calculateGoalDistance() for detailed documentation
    static Eigen::Vector3d calculateTranslationGoalDistance(const Eigen::Affine3d& current_pose,
                                                            const Eigen::Affine3d& goal_pose);

    /// @brief Subcomponent of calculateGoalDistance that only calculates the rotation part
    ///
    /// See calculateGoalDistance() for detailed documentation
    static Eigen::Vector3d calculateRotationGoalDistance(const Eigen::Affine3d& current_pose,
                                                         const Eigen::Affine3d& goal_pose);
};
#endif