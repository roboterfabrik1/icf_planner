#include "cf_motion_planner/cf_agent_settings.h"

bool CFAgentSettings::operator==(const CFAgentSettings& rhs) const {
    return only_two_dimensions == rhs.only_two_dimensions && almostEqualRelative(k_g, rhs.k_g) &&
           almostEqualRelative(k_d, rhs.k_d) && almostEqualRelative(k_cf_ee, rhs.k_cf_ee) &&
           almostEqualRelative(obstacle_limit_distance, rhs.obstacle_limit_distance) &&
           almostEqualRelative(collision_distance, rhs.collision_distance) &&
           almostEqualRelative(goal_influence_limit_distance, rhs.goal_influence_limit_distance) &&
           almostEqualRelative(intermediate_goal_reached_distance, rhs.intermediate_goal_reached_distance) &&
           maximum_line_of_sight_shortcuts == rhs.maximum_line_of_sight_shortcuts &&
           maximum_line_of_sight_binary_search_iterations == rhs.maximum_line_of_sight_binary_search_iterations &&
           almostEqualRelative(shortcut_under_obstacle_influence_weight,
                               rhs.shortcut_under_obstacle_influence_weight) &&
           almostEqualRelative(translational_velocity_limit, rhs.translational_velocity_limit) &&
           almostEqualRelative(rotational_velocity_limit, rhs.rotational_velocity_limit);
}

CFAgentSettings CFAgentSettings::generateFromRosParameterEntry(const XmlRpc::XmlRpcValue& ros_parameter_entry) {
    CFAgentSettings result;

    for (const auto& element : ros_parameter_entry) {
        std::string key = element.first;
        if (key == "only_two_dimensions") {
            result.only_two_dimensions = static_cast<bool>(element.second);
        } else if (key == "k_g") {
            result.k_g = static_cast<double>(element.second);
        } else if (key == "k_d") {
            result.k_d = static_cast<double>(element.second);
        } else if (key == "k_cf_ee") {
            result.k_cf_ee = static_cast<double>(element.second);
        } else if (key == "k_cf_body") {
            result.k_cf_body = static_cast<double>(element.second);
        } else if (key == "k_cf_rep") {
            result.k_cf_rep = static_cast<double>(element.second);
        } else if (key == "k_pot_rep") {
            result.k_pot_rep = static_cast<double>(element.second);
        } else if (key == "d_null") {
            result.d_null = static_cast<double>(element.second);
        } else if (key == "d_jla") {
            result.d_jla = static_cast<double>(element.second);
        } else if (key == "k_jla") {
            result.k_jla = static_cast<double>(element.second);
        } else if (key == "k_man") {
            result.k_man = static_cast<double>(element.second);
        } else if (key == "obstacle_limit_distance_repulsive") {
          result.obstacle_limit_distance_repulsive =
              static_cast<double>(element.second);
        } else if (key == "obstacle_limit_distance_safety") {
          result.obstacle_limit_distance_safety =
              static_cast<double>(element.second);
        } else if (key == "manipulability_gradient") {
          result.manipulability_gradient = static_cast<bool>(element.second);
        } else if (key == "nullspace_projection") {
          result.nullspace_projection = static_cast<bool>(element.second);
        } else if (key == "scale_dynamic") {
          result.scale_dynamic = static_cast<bool>(element.second);
        } else if (key == "collision_distance") {
          result.collision_distance = static_cast<double>(element.second);
        } else if (key == "goal_influence_limit_distance") {
          result.goal_influence_limit_distance =
              static_cast<double>(element.second);
        } else if (key == "intermediate_goal_reached_distance") {
          result.intermediate_goal_reached_distance =
              static_cast<double>(element.second);
        } else if (key == "maximum_line_of_sight_shortcuts") {
          result.maximum_line_of_sight_shortcuts =
              static_cast<int>(element.second);
        } else if (key == "maximum_line_of_sight_binary_search_iterations") {
          result.maximum_line_of_sight_binary_search_iterations =
              static_cast<int>(element.second);
        } else if (key == "shortcut_under_obstacle_influence_weight") {
          result.shortcut_under_obstacle_influence_weight =
              static_cast<double>(element.second);
        } else if (key == "translational_velocity_limit") {
          result.translational_velocity_limit =
              static_cast<double>(element.second);
        } else if (key == "rotational_velocity_limit") {
          result.rotational_velocity_limit =
              static_cast<double>(element.second);
        } else if (key == "fap_frame_ids") {
          for (int i = 0; i < element.second.size(); i++) {
            result.fap_frame_ids.push_back(static_cast<int>(element.second[i]));
          }
        } else {
          ROS_WARN_STREAM(
              "CFAgentSettings::generateFromRosParameterEntry - key '"
              << key << "' is unknown.");
        }
    }

    return result;
}
