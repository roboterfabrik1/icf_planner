#include "cf_motion_planner/cf_planner_ros_interface.h"

CFPlannerROSInterface::CFPlannerROSInterface() {
    connect(&spin_timer_, &QTimer::timeout, this, &CFPlannerROSInterface::spinOnce);

    QTimer::singleShot(0, this, &CFPlannerROSInterface::init);
}

CFPlannerROSInterface::~CFPlannerROSInterface() { spin_timer_.stop(); }

void CFPlannerROSInterface::init() {
    constructPublishersAndSubscribers();
    spin_timer_.start(spin_timer_time_ms_);
}

void CFPlannerROSInterface::constructPublishersAndSubscribers() {
    // Subscriber
    obstacle_subscriber_ = node_handle_.subscribe("pcl_obstacle_objects", 1, &CFPlannerROSInterface::obstaclesCallback,
                                                  this, ros::TransportHints().tcpNoDelay());
    goal_pose_subscriber_ = node_handle_.subscribe("cf_goal_pose", 1, &CFPlannerROSInterface::goalPoseChangedCallback,
                                                   this, ros::TransportHints().tcpNoDelay());
    robot_state_subscriber_ = node_handle_.subscribe("cf_robot_state", 1, &CFPlannerROSInterface::robotStateCallback,
                                                     this, ros::TransportHints().tcpNoDelay());
    rotation_vector_subscriber_ = node_handle_.subscribe("rotation_vector_guesses", 1,
                                                         &CFPlannerROSInterface::rotationVectorGuessesCallback, this,
                                                         ros::TransportHints().tcpNoDelay());

    forces_publisher_fap_ = node_handle_.advertise<cf_planner_msgs::WrenchArray>("cf_virtual_fap_forces", 1);

    robot_settings_publisher_ = node_handle_.advertise<cf_planner_msgs::AgentSettings>("cf_real_robot_settings", 1);

    step_time_publisher_ = node_handle_.advertise<std_msgs::Float32>("step_time", 5);
}

void CFPlannerROSInterface::spinOnce() {
    ros::getGlobalCallbackQueue()->callAvailable();  // Handle all subscriber data
    const unsigned char ALL_MESSAGES_RECEIVED_ONCE = 0b1111;
    if (initialization_status_ == ALL_MESSAGES_RECEIVED_ONCE) {
        emit sgnRequestForceReactionCalculation();
    }
}

void CFPlannerROSInterface::loadSettings(CFPlannerSettings& planner_settings, CFAgentSettings& agent_settings,
                                         std::shared_ptr<AbstractRobotDynamicSettings>& dynamic_settings) {
    XmlRpc::XmlRpcValue result;
    const std::string ROOT_PATH = "CFPlanner/";
    if (node_handle_.hasParam(ROOT_PATH)) {
        node_handle_.getParam(ROOT_PATH, result);
        for (auto& element : result) {
            const std::string KEY = element.first;
            if (KEY == "PointMassRobotDynamicSettings") {
                PointMassRobotDynamicSettings temp_dynamic_settings =
                    PointMassRobotDynamicSettings::generateFromRosParameterEntry(element.second);
                dynamic_settings = std::dynamic_pointer_cast<AbstractRobotDynamicSettings>(
                    std::make_shared<PointMassRobotDynamicSettings>(temp_dynamic_settings));
            } else if (KEY == "SimpleRobotDynamicSettings") {
                SimpleRobotDynamicSettings temp_dynamic_settings =
                    SimpleRobotDynamicSettings::generateFromRosParameterEntry(element.second);
                dynamic_settings = std::dynamic_pointer_cast<AbstractRobotDynamicSettings>(
                    std::make_shared<SimpleRobotDynamicSettings>(temp_dynamic_settings));
            } else if (KEY == "CFPlannerSettings") {
                planner_settings = CFPlannerSettings::generateFromRosParameterEntry(element.second);
            } else if (KEY == "CFAgentSettings") {
                agent_settings = CFAgentSettings::generateFromRosParameterEntry(element.second);
            } else if (KEY == "CFPrePlannerSettings") {
                // Do nothing. Key is only used in preplanner.
            } else {
                ROS_WARN_STREAM("CFPlannerNode::loadSettings - KEY '" << KEY << "' is unknown.");
            }
        }
    } else {
        ROS_ERROR_STREAM("CFPlannerNode::loadSettings - Can not load objects. Root Path '"
                         << ROOT_PATH << "' is not found on the ros parameter server.");
    }

    const int S_TO_MS_FACTOR = 1000;
    spin_timer_time_ms_ = std::roundl(planner_settings.control_loop_cycle_duration * S_TO_MS_FACTOR);
}

void CFPlannerROSInterface::goalPoseChangedCallback(const geometry_msgs::PoseStamped& latest_goal_pose_msg) {
    Eigen::Affine3d goal_ee_pose;
    tf::poseMsgToEigen(latest_goal_pose_msg.pose, goal_ee_pose);
    emit sgnNewGoalPose(goal_ee_pose);
    const unsigned char GOAL_RECEIVED_ONCE = 0b0001;
    initialization_status_ |= GOAL_RECEIVED_ONCE;
}

void CFPlannerROSInterface::rotationVectorGuessesCallback(
    const cf_pre_planner_msgs::RotVecArrayObstacles& rot_vec_guesses_msg) {
    // This variable holds a vector of maps with the obstacle ids and the rotation vector. One map per fap.
    RotationVectorMapVector rot_vecs_all_obstacles;
    Eigen::Vector3d rot_tmp;
    std::map<int, Eigen::Vector3d> tmp_map;

    for (size_t i_FAP = 0; i_FAP < rot_vec_guesses_msg.rot_vecs_obstacles[0].rot_vecs.size(); i_FAP++) {
        for (size_t i_obstacle = 0; i_obstacle < rot_vec_guesses_msg.rot_vec_ids.size(); i_obstacle++) {
            int obstacle_id = (int)rot_vec_guesses_msg.rot_vec_ids[i_obstacle];
            cf_pre_planner_msgs::RotVecArrayRobot rot_vec_array_robot_msg =
                rot_vec_guesses_msg.rot_vecs_obstacles[i_obstacle];
            tf::vectorMsgToEigen(rot_vec_array_robot_msg.rot_vecs[i_FAP], rot_tmp);
            tmp_map.insert(std::pair<int, Eigen::Vector3d>(obstacle_id, rot_tmp));
        }
        rot_vecs_all_obstacles.push_back(tmp_map);
        tmp_map.clear();
    }

    emit sgnNewRotVecMsg(rot_vecs_all_obstacles);
    const unsigned char ROT_VEC_RECEIVED_ONCE = 0b0010;
    initialization_status_ |= ROT_VEC_RECEIVED_ONCE;
}

void CFPlannerROSInterface::obstaclesCallback(const pcl_obstacle_generator::MovingObstacles::Ptr& latest_obstacle_msg) {
    emit sgnNewObstacleMsg(latest_obstacle_msg);

    const unsigned char OBSTACLE_RECEIVED_ONCE = 0b0100;
    initialization_status_ |= OBSTACLE_RECEIVED_ONCE;

    // =============================== EXPERIMENTAL ========================================
    // obstacle_subscriber_.shutdown();  // Only use obstacle once => Static environment.
    // =============================== EXPERIMENTAL ========================================
}

void CFPlannerROSInterface::robotStateCallback(const cf_planner_msgs::RobotState& latest_robot_state_msg) {
    CFRobotState robot_state;
    std::vector<double> joint_pos = latest_robot_state_msg.joint_positions;
    std::vector<double> joint_vel = latest_robot_state_msg.joint_velocities;
    std::vector<double> joint_vel_null = latest_robot_state_msg.joint_velocities_null;

    Eigen::Matrix<double, 7, 1> q(joint_pos.data());
    Eigen::Matrix<double, 7, 1> dq(joint_vel.data());
    Eigen::Matrix<double, 7, 1> dq_null(joint_vel_null.data());

    robot_state.joint_positions = q;
    robot_state.joint_velocities = dq;
    robot_state.joint_velocities_null = dq_null;

    std::vector<Eigen::Affine3d> fap_poses;
    for (const auto pose : latest_robot_state_msg.fap_poses) {
        Eigen::Affine3d poseAffine;
        tf::poseMsgToEigen(pose, poseAffine);
        fap_poses.push_back(poseAffine);
    }
    robot_state.fap_poses = fap_poses;

    std::vector<Vector6d> fap_vels;
    for (const auto vel_msg : latest_robot_state_msg.fap_velocities) {
        Vector6d vel;
        tf::twistMsgToEigen(vel_msg, vel);
        fap_vels.push_back(vel);
    }
    robot_state.fap_velocities = fap_vels;

    emit sgnNewRobotState(robot_state);

    const unsigned char ROBOT_STATE_RECEIVED_ONCE = 0b1000;
    initialization_status_ |= ROBOT_STATE_RECEIVED_ONCE;
}

void CFPlannerROSInterface::slotPublishFAPForces(const std::vector<Vector6d>& fap_wrenches) {
    cf_planner_msgs::WrenchArray fap_force_msg;
    fap_force_msg.header.stamp = ros::Time::now();
    fap_force_msg.header.frame_id = "map";
    for (auto& fap_wrench : fap_wrenches) {
        geometry_msgs::Wrench temp_msg;
        tf::wrenchEigenToMsg(fap_wrench, temp_msg);
        fap_force_msg.wrenches.push_back(temp_msg);
    }
    forces_publisher_fap_.publish(fap_force_msg);
}

void CFPlannerROSInterface::slotPublishRealRobotSettings(const CFAgentSettings& real_robot_settings) {
    cf_planner_msgs::AgentSettings real_robot_settings_msg;
    real_robot_settings_msg.k_jla = real_robot_settings.k_jla;
    real_robot_settings_msg.k_man = real_robot_settings.k_man;
    real_robot_settings_msg.d_null = real_robot_settings.d_null;
    real_robot_settings_msg.d_jla = real_robot_settings.d_jla;
    real_robot_settings_msg.scale_dynamic = real_robot_settings.scale_dynamic;
    real_robot_settings_msg.nullspace_projection = real_robot_settings.nullspace_projection;
    real_robot_settings_msg.manipulability_gradient = real_robot_settings.manipulability_gradient;
    real_robot_settings_msg.invalid_best_agent = real_robot_settings.invalid_best_agent;
    real_robot_settings_msg.safety_joint_vel_calculation_method =
        real_robot_settings.safety_joint_vel_calculation_method;
    real_robot_settings_msg.close_control_point =
        real_robot_settings.close_control_point;
    int i = 0;
    for (auto& frame_id : real_robot_settings.fap_frame_ids) {
        real_robot_settings_msg.fap_frame_ids.push_back(frame_id);
        i++;
    }

    robot_settings_publisher_.publish(real_robot_settings_msg);
}

void CFPlannerROSInterface::slotPublishStepTime(float step_time) {
    std_msgs::Float32 float_msg;
    float_msg.data = step_time;
    step_time_publisher_.publish(float_msg);
}
