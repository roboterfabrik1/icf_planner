#include "cf_motion_planner/moving_goal.h"

MovingGoal::MovingGoal() : time_steps_(DEFAULT_HISTORY_BUFFER_SIZE), poses_(DEFAULT_HISTORY_BUFFER_SIZE) {}

MovingGoal::MovingGoal(int history_buffer_size) : time_steps_(history_buffer_size), poses_(history_buffer_size) {}

void MovingGoal::incrementTimeStep(uint64_t start_step, uint64_t offset) {
    if (operation_mode_ != MovingGoalOperationMode::TIME) {
        ROS_ERROR_STREAM("MovingGoal::incrementTimeStep - "
                         << "This operation is only allowed with MovingGoalOperationMode::Time. Aborting.");
        return;
    }

    if (!output_start_time_step_) {
        output_start_time_step_ = start_step + offset;
        used_time_offset_ = offset;
    }

    if (!time_step_index_) {  // init
        if (output_start_time_step_ < time_steps_.back()) {
            ROS_WARN_STREAM("MovingGoal::incrementTimeStep - Desired start step is not in time_steps_."
                            << " It is older than the oldest element. Replacing with the oldest known element.");
            output_start_time_step_ = time_steps_.back();
        } else if (output_start_time_step_ > time_steps_.front()) {
            ROS_WARN_STREAM("MovingGoal::incrementTimeStep - Desired start step is not in time_steps_."
                            << " It is younger than the latest element. Replacing with the latest known element.");
            output_start_time_step_ = time_steps_.front();
        }

        for (int i = time_steps_.size() - 1; i >= 0; i--) {
            if (time_steps_.at(i) == output_start_time_step_.get()) {
                time_step_index_ = i;
            }
        }

        return;
    }

    // normal decrement
    if (time_step_index_.get() > 0) {
        if (time_steps_.at(time_step_index_.get() - 1) - time_steps_.at(time_step_index_.get()) != 1) {
            ROS_ERROR_STREAM("MovingGoal::incrementTimeStep - Two consecutive steps have a delta that is not 1.");
        }
        time_step_index_ = time_step_index_.get() - 1;
    } else {
        time_step_index_ = 0;
    }
}

void MovingGoal::nextGoalPose() {
    if (operation_mode_ != MovingGoalOperationMode::POSITION) {
        ROS_ERROR_STREAM("MovingGoal::nextGoalPose - "
                         << "This operation is only allowed with MovingGoalOperationMode::POSITION. Aborting.");
        return;
    }

    if (poses_.empty()) {
        ROS_WARN_STREAM("MovingGoal::nextGoalPose - Not possible poses_ is empty.");
    }

    if (!time_step_index_) {  // init
        time_step_index_ = poses_.size() - 1;
        return;
    }

    // normal decrement
    if (time_step_index_.get() > 0) {
        time_step_index_ = time_step_index_.get() - 1;
    } else {
        time_step_index_ = 0;
    }
}

const Eigen::Affine3d& MovingGoal::getGoalPose() const { return poses_.at(time_step_index_.get()); }

bool MovingGoal::isLatestTimeStep() const { return time_step_index_ == 0; };

void MovingGoal::resetTimeStep() {
    output_start_time_step_ = boost::none;
    time_step_index_ = boost::none;
    used_time_offset_ = 0;
};

bool MovingGoal::isValidTimeStep(uint64_t time_step) {
    if (operation_mode_ != MovingGoalOperationMode::TIME) {
        ROS_ERROR_STREAM("MovingGoal::isValidTimeStep - "
                         << "This operation is only allowed with MovingGoalOperationMode::Time. Aborting.");
        return false;
    }
    return time_step <= time_steps_.front() && time_step >= time_steps_.back();
}

std::unique_ptr<MovingGoal> MovingGoal::fromHistory(const AbstractRobotDynamicHistory& history) {
    auto moving_goal = std::make_unique<MovingGoal>();

    boost::circular_buffer<Eigen::Affine3d> poses(10000);
    for (auto& robot_state : history.getRobotStates()) {
        Eigen::Affine3d ee_pose = robot_state.getEEPose();
        poses.push_front(ee_pose);
    }
    moving_goal->setPoses(poses);
    moving_goal->setTimeSteps(history.getTimeSteps());
    return moving_goal;
}