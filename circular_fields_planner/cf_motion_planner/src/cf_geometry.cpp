#include "cf_motion_planner/cf_geometry.h"

CFGeometry::CFGeometry() : critical_object_cache_(CRITICAL_OBJECT_CACHE_SIZE) {}

CFGeometry::CFGeometry(double maximum_search_distance, double step_time_s,
                       int maximum_number_considered_points_per_object)
    : critical_object_cache_(CRITICAL_OBJECT_CACHE_SIZE),
      maximum_search_distance_(maximum_search_distance),
      step_time_s_(step_time_s),
      maximum_number_considered_points_per_object_(maximum_number_considered_points_per_object) {}

void CFGeometry::setNewData(const pcl_obstacle_generator::MovingObstacles::Ptr& latest_obstacle_msg,
                            uint64_t corresponding_time_step) {
    QWriteLocker new_data_locker(&new_data_lock_);

    // Reset cache
    QWriteLocker critical_object_cache_write_locker(&critical_object_cache_lock_);
    critical_object_cache_.clear();
    critical_object_cache_write_locker.unlock();

    std::vector<bool> object_changed;
    object_changed.resize(pc_objects_.size(), false);

    for (const auto& object : latest_obstacle_msg->obstacles) {
        int id = object.obstacle_id;
        int object_index = -1;
        for (size_t i = 0; i < pc_objects_.size(); i++) {
            if (pc_objects_.at(i)->getID() == id) {
                object_index = i;
            }
        }

        if (object_index >= 0) {  // modify data of existing object with id
            pc_objects_.at(object_index)->setNewData(object, corresponding_time_step);
            object_changed.at(object_index) = true;
        } else {  // Create new PCObject for id
            pc_objects_.emplace_back(std::make_unique<PCObject>(maximum_search_distance_, step_time_s_, id,
                                                                maximum_number_considered_points_per_object_));
            pc_objects_.back()->setNewData(object, corresponding_time_step);
            object_changed.push_back(true);
        }
    }

    // Delete unused objects
    int max_size = pc_objects_.size();
    int i = 0;
    while (i < max_size) {
        if (object_changed.at(i)) {
            i++;
        } else {
            pc_objects_.erase(pc_objects_.begin() + i);
            object_changed.erase(object_changed.begin() + i);
            max_size = max_size - 1;
        }
    }
}

PCObject* CFGeometry::getCriticalObject(const TimedPoint& for_point) {

    QReadLocker critical_object_cache_read_locker(&critical_object_cache_lock_);
    // Use cache. Cleared by call to setNewData()
    for (auto& cached_point : critical_object_cache_) {
        if (timedPointsAreAlmostEqual(for_point, cached_point.first)) {
            return cached_point.second;
        }
    }
    critical_object_cache_read_locker.unlock();

    // No cache found
    boost::optional<double> min_squared_distance;
    PCObject* crit_object = nullptr;
    for (auto& object : pc_objects_) {
        boost::optional<double> object_distance = object->getMinimalSquareDistance(for_point);
        if (object_distance) {
            if (min_squared_distance) {
                if (object_distance.get() < min_squared_distance.get()) {
                    min_squared_distance = object_distance;
                    crit_object = object.get();
                }
            } else {
                min_squared_distance = object_distance;
                crit_object = object.get();
            }
        }
    }

    if (min_squared_distance) {
        QWriteLocker critical_object_cache_write_locker(&critical_object_cache_lock_);
        critical_object_cache_.push_front(std::pair<TimedPoint, PCObject*>{for_point, crit_object});
        return crit_object;
    }

    return nullptr;
}

PCObject* CFGeometry::getObjectByID(int id) {
    QReadLocker new_data_locker(&new_data_lock_);

    for (auto& object : pc_objects_) {
        if (object->getID() == id) {
            return object.get();
        }
    }
    return nullptr;
}

bool CFGeometry::minimalDistanceToLineIsNotLesserThan(double threshold, const TimedPoint& line_start_point,
                                                      const TimedPoint& line_end_point) {
    QReadLocker new_data_locker(&new_data_lock_);
    for (auto& object : pc_objects_) {
        bool critical = !object->minimalDistanceToLineIsNotLesserThan(threshold, line_start_point, line_end_point);
        if (critical) {
            return false;
        }
    }
    return true;
}

std::set<int> CFGeometry::getActiveObjectIds(const TimedPoint& for_point) {
    QReadLocker new_data_locker(&new_data_lock_);

    QReadLocker active_object_ids_read_locker(&active_object_ids_cache_lock_);
    // Use cache. Cleared by call to setNewData()
    for (auto& cached_point : active_object_ids_cache_) {
        if (timedPointsAreAlmostEqual(for_point, cached_point.first)) {
            return cached_point.second;
        }
    }
    active_object_ids_read_locker.unlock();

    // No cache found
    std::set<int> new_active_ids_set;
    for (auto& object : pc_objects_) {
        boost::optional<double> object_distance = object->getMinimalSquareDistance(for_point);
        if (object_distance) {
            new_active_ids_set.insert(object->getID());
        }
    }

    QWriteLocker active_object_ids_write_locker(&active_object_ids_cache_lock_);
    active_object_ids_cache_.push_front(std::pair<TimedPoint, std::set<int>>{for_point, new_active_ids_set});
    return new_active_ids_set;
}

std::vector<std::tuple<Eigen::Vector3d, double, Eigen::Vector3d>> CFGeometry::getForceCalculationData(
    const TimedPoint& for_point, const std::map<int, Eigen::Vector3d>& for_object_ids_rotation_vectors,
    const Eigen::Vector3d& robot_velocity, const Eigen::Vector3d& goal_position) {
    QReadLocker new_data_locker(&new_data_lock_);
    std::vector<std::tuple<Eigen::Vector3d, double, Eigen::Vector3d>> result;

    for (const auto& entry : for_object_ids_rotation_vectors) {
        bool found = false;

        for (auto& object : pc_objects_) {
            if (object->getID() == entry.first) {
                std::vector<std::tuple<Eigen::Vector3d, double, Eigen::Vector3d>> object_result =
                    object->getForceCalculationData(for_point, entry.second, robot_velocity, goal_position);
                result.insert(result.end(), object_result.begin(), object_result.end());
                found = true;
                break;
            }
        }

        if (!found) {
            ROS_WARN_STREAM("CFGeometry::getForceCalculationData - No object with id '"
                            << entry.first << "' could be found."
                            << "The electric current vectors for this object are not used.");
        }
    }

    return result;
}

std::vector<std::tuple<Eigen::Vector3d, double, Eigen::Vector3d>> CFGeometry::getObjectForceCalculationData(
    const TimedPoint& for_point, const std::pair<int, Eigen::Vector3d>& for_object_ids_rotation_vectors,
    const Eigen::Vector3d& robot_velocity, const Eigen::Vector3d& goal_position) {
    QReadLocker new_data_locker(&new_data_lock_);
    std::vector<std::tuple<Eigen::Vector3d, double, Eigen::Vector3d>> result;

    bool found = false;

    for (auto& object : pc_objects_) {
        if (object->getID() == for_object_ids_rotation_vectors.first) {
            std::vector<std::tuple<Eigen::Vector3d, double, Eigen::Vector3d>> object_result =
                object->getForceCalculationData(for_point, for_object_ids_rotation_vectors.second, robot_velocity,
                                                goal_position);
            result.insert(result.end(), object_result.begin(), object_result.end());
            found = true;
            break;
        }
    }

    if (!found) {
        ROS_WARN_STREAM("CFGeometry::getForceCalculationData - No object with id '"
                        << for_object_ids_rotation_vectors.first << "' could be found."
                        << "The electric current vectors for this object are not used.");
    }

    return result;
}

CFGeometry::Vector6d CFGeometry::calculateGoalDistance(const Eigen::Affine3d& current_pose,
                                                       const Eigen::Affine3d& goal_pose) {
    Vector6d result;

    result.head(3) = calculateTranslationGoalDistance(current_pose, goal_pose);
    result.tail(3) = calculateRotationGoalDistance(current_pose, goal_pose);

    return result;
}

Eigen::Vector3d CFGeometry::calculateTranslationGoalDistance(const Eigen::Affine3d& current_pose,
                                                             const Eigen::Affine3d& goal_pose) {
    return goal_pose.translation() - current_pose.translation();
}

Eigen::Vector3d CFGeometry::calculateRotationGoalDistance(const Eigen::Affine3d& current_pose,
                                                          const Eigen::Affine3d& goal_pose) {
  // Yuan, 1988
  Eigen::Quaterniond q_p(current_pose.rotation());
  Eigen::Quaterniond q_g(goal_pose.rotation());

  Eigen::Vector3d rotation_result =
      q_p.w() * q_g.vec() - q_g.w() * q_p.vec() - q_p.vec().cross(q_g.vec());

    rotation_result =
      current_pose.rotation().inverse() *
      rotation_result;  // Transform quaternion difference into world_CS

  // Caccavale et al., 1998
  // Eigen::Matrix3d rotation_mat =
  //     current_pose.rotation().transpose() * goal_pose.rotation();
  // Eigen::Quaterniond q_eeg(rotation_mat);
  // q_eeg.normalize();
  // Eigen::Vector3d rotation_result =
  //     current_pose.rotation().transpose() * q_eeg.vec();

  return rotation_result;
}

boost::optional<Eigen::Vector3d> CFGeometry::getObjectMinimalVector(const TimedPoint& for_point, const int object_id) {
    for (auto& object : pc_objects_) {
        if (object->getID() == object_id) {
            auto object_result = object->getMinimalVector(for_point);
            if (object_result) {
                return object_result.get();
            }
        }
    }
    return boost::none;
}

boost::optional<double> CFGeometry::getCriticalObjectMinimalDistance(const TimedPoint& for_point){
    QReadLocker new_data_locker(&new_data_lock_);
    auto pc_object = this->getCriticalObject(for_point);
    if(pc_object){
        return pc_object->getMinimalDistance(for_point);
    }
    return boost::none;
}

boost::optional<Eigen::Vector3d> CFGeometry::getCriticalObjectMinimalVector(const TimedPoint& for_point){
    QReadLocker new_data_locker(&new_data_lock_);
    auto pc_object = this->getCriticalObject(for_point);
    if(pc_object){
        return pc_object->getMinimalVector(for_point);
    }
    return boost::none;

}

  

boost::optional<double> CFGeometry::getCriticalObjectSquareDistance(const TimedPoint& for_point){
    QReadLocker new_data_locker(&new_data_lock_);
    auto pc_object = this->getCriticalObject(for_point);
    if(pc_object){
        return pc_object->getMinimalSquareDistance(for_point);
    }
    return boost::none;
}