#include "cf_motion_planner/ds_color.h"

std_msgs::ColorRGBA DSColorTheme::createRGBA(double r, double g, double b, double a) {
    std_msgs::ColorRGBA rgba;
    rgba.r = static_cast<float>(r);
    rgba.g = static_cast<float>(g);
    rgba.b = static_cast<float>(b);
    rgba.a = static_cast<float>(a);
    return rgba;
}

std_msgs::ColorRGBA DSColorStandardTheme::toRGBA(DSColor color) {
    switch (color) {
        case DSColor::PRIMARY:
            return createRGBA(0.905, 0.482, 0.161, 1.0);  // NOLINT(readability-magic-numbers) : Style initialization
            break;
        case DSColor::SECONDARY:
            return createRGBA(0.0, 0.314, 0.608, 1.0);  // NOLINT(readability-magic-numbers) : Style initialization
            break;
        case DSColor::SUCCESS:
            return createRGBA(0.157, 0.655, 0.271, 1.0);  // NOLINT(readability-magic-numbers) : Style initialization
            break;
        case DSColor::DANGER:
            return createRGBA(0.863, 0.208, 0.271, 1.0);  // NOLINT(readability-magic-numbers) : Style initialization
            break;
        case DSColor::WARNING:
            return createRGBA(1.0, 0.757, 0.027, 1.0);  // NOLINT(readability-magic-numbers) : Style initialization
            break;
        case DSColor::INFO:
            return createRGBA(0.09, 0.635, 0.722, 1.0);  // NOLINT(readability-magic-numbers) : Style initialization
            break;
        case DSColor::LIGHT:
            return createRGBA(0.973, 0.976, 0.98, 1.0);  // NOLINT(readability-magic-numbers) : Style initialization
            break;
        case DSColor::DARK:
            return createRGBA(0.204, 0.227, 0.251, 1.0);  // NOLINT(readability-magic-numbers) : Style initialization
            break;
        default:
            return createRGBA(1.0, 1.0, 1.0, 1.0);  // NOLINT(readability-magic-numbers) : Style initialization
    }
}