#include "cf_motion_planner/cf_agent_manager.h"

CFAgentManager::CFAgentManager(QThread* manager_thread, CFPlannerSettings& planner_settings,
                               const CFAgentSettings& agent_settings,
                               const std::shared_ptr<AbstractRobotDynamicSettings>& dynamic_settings)
    : settings_(planner_settings) {
    default_agent_settings_ = agent_settings;
    default_dynamic_settings_ = dynamic_settings;

    best_agent_.settings = default_agent_settings_;
    best_agent_.prediction_score = 0;
    const double PREDICTION_STEP_TIME = settings_.prediction_step_time_multiple * settings_.control_loop_cycle_duration;
    for (const double& obstacle_limit_distance : settings_.allowed_obstacle_limit_distances) {
        geometry_collection_.emplace_back(std::make_shared<CFGeometry>(
            obstacle_limit_distance, PREDICTION_STEP_TIME, settings_.maximum_number_considered_points_per_object));
    }

    initializePredictiveAgentsVector(settings_.max_agents);

    manager_thread_ = manager_thread;
    this->moveToThread(manager_thread_);

    ros_interface_ = new CFAgentManagerROSInterface(manager_thread_);
    connect(this, &CFAgentManager::sgnPredictionResult, ros_interface_,
            &CFAgentManagerROSInterface::slotPublishPredictionResult);
    connect(this, &CFAgentManager::sgnMarkPosition, ros_interface_, &CFAgentManagerROSInterface::slotMarkPosition);
}

CFAgentManager::~CFAgentManager() { ros_interface_->deleteLater(); }

void CFAgentManager::initializePredictiveAgentsVector(int max_agents) {
    predictive_agents_.reserve(max_agents);
    for (size_t i = 0; i < max_agents; i++) {
        predictive_agents_.emplace_back(
            std::pair<PredictiveAgentPtr, AgentWatcherPtr>{nullptr, std::make_unique<AgentWatcher>()});
        connect(predictive_agents_.at(i).second.get(), &AgentWatcher::finished, this,
                [=]() { this->managePrediction(i); });
    }
}

void CFAgentManager::managePrediction(int index) {
    PredictiveAgentPtr& agent = predictive_agents_.at(index).first;
    AgentWatcherPtr& watcher = predictive_agents_.at(index).second;
    CFAgent::PredictionExitStatus exit_status = watcher->result();

    if (exit_status == CFAgent::PredictionExitStatus::MAX_PREDICTION_STEPS_REACHED) {
        Eigen::Vector3d real_point = best_agent_.latest_robot_state.getEEPose().translation();

        if (agent->robotDynamic()->getHistory().hasSimilarTrajectoryPoint(time_step_, real_point,
                                                                          settings_.maximum_trajectory_deviation)) {
            QFuture<CFAgent::PredictionExitStatus> future = QtConcurrent::run(agent.get(), &CFAgent::predict,
                                                                              settings_.continuous_prediction_steps);
            watcher->setFuture(future);
        } else {
            agent = nullptr;
        }
    } else if (exit_status == CFAgent::PredictionExitStatus::PRECALCULATION_FINISHED) {
        QFuture<CFAgent::PredictionExitStatus> future = QtConcurrent::run(agent.get(), &CFAgent::predict,
                                                                          settings_.continuous_prediction_steps);
        watcher->setFuture(future);
    } else if (exit_status == CFAgent::PredictionExitStatus::ABORTED) {
        agent = nullptr;
    } else if (exit_status == CFAgent::PredictionExitStatus::GOAL_REACHED) {
        agent = nullptr;
    } else if (exit_status == CFAgent::PredictionExitStatus::COLLIDED) {
        agent = nullptr;
    }
}

void CFAgentManager::slotCreateAgent(const CFAgentSettings& settings, const RotationVectorMapVector& rotation_vectors) {
    if (duration_reference_time_step_ == 0) return;
    AbstractRobotDynamicHistory history;
    initializeCleanHistoryBasedOnRealRobot(history);
    std::vector<CFAgentSettings> new_settings_queue;

    CFAgentSettings old_settings = settings;
    CFAgentSettings new_settings = old_settings;
    new_settings.internal.rotation_vectors = rotation_vectors;
    new_settings.invalid_best_agent = false;
    CFAgentSettings safety_settings = new_settings;
    safety_settings.invalid_best_agent = true;

    // If the number of obstacles has changed, do not spawn the old settings again.
    if (old_settings.internal.rotation_vectors.front().size() !=
        new_settings.internal.rotation_vectors.front().size()) {
        if (settings_.explore_manipulability && !settings_.explore_nullspace) {
            new_settings_queue.push_back(new_settings);
            new_settings.manipulability_gradient = !new_settings.manipulability_gradient;
            new_settings_queue.push_back(new_settings);

        } else if (settings_.explore_nullspace && !settings_.explore_manipulability) {
            new_settings_queue.push_back(new_settings);
            new_settings.nullspace_projection = !new_settings.nullspace_projection;
            new_settings_queue.push_back(new_settings);

        } else if (settings_.explore_nullspace && settings_.explore_manipulability) {
            new_settings.nullspace_projection = !new_settings.nullspace_projection;
            new_settings.manipulability_gradient = !new_settings.manipulability_gradient;
            new_settings_queue.push_back(new_settings);

            new_settings.manipulability_gradient = !new_settings.manipulability_gradient;
            new_settings_queue.push_back(new_settings);

            new_settings.nullspace_projection = !new_settings.nullspace_projection;
            new_settings_queue.push_back(new_settings);

            new_settings.manipulability_gradient = !new_settings.manipulability_gradient;
            new_settings_queue.push_back(new_settings);
        } else {
            new_settings_queue.push_back(new_settings);
        }
    } else {
        if (settings_.explore_manipulability && !settings_.explore_nullspace) {
            new_settings_queue.push_back(new_settings);
            new_settings_queue.push_back(old_settings);
            new_settings.manipulability_gradient = !new_settings.manipulability_gradient;
            old_settings.manipulability_gradient = !old_settings.manipulability_gradient;
            new_settings_queue.push_back(new_settings);
            new_settings_queue.push_back(old_settings);

        } else if (settings_.explore_nullspace && !settings_.explore_manipulability) {
            new_settings_queue.push_back(new_settings);
            new_settings_queue.push_back(old_settings);
            new_settings.nullspace_projection = !new_settings.nullspace_projection;
            old_settings.nullspace_projection = !old_settings.nullspace_projection;
            new_settings_queue.push_back(new_settings);
            new_settings_queue.push_back(old_settings);

        } else if (settings_.explore_nullspace && settings_.explore_manipulability) {
            new_settings.nullspace_projection = !new_settings.nullspace_projection;
            old_settings.nullspace_projection = !old_settings.nullspace_projection;
            new_settings.manipulability_gradient = !new_settings.manipulability_gradient;
            old_settings.manipulability_gradient = !old_settings.manipulability_gradient;
            new_settings_queue.push_back(new_settings);
            new_settings_queue.push_back(old_settings);

            new_settings.manipulability_gradient = !new_settings.manipulability_gradient;
            old_settings.manipulability_gradient = !old_settings.manipulability_gradient;
            new_settings_queue.push_back(new_settings);
            new_settings_queue.push_back(old_settings);

            new_settings.nullspace_projection = !new_settings.nullspace_projection;
            old_settings.nullspace_projection = !old_settings.nullspace_projection;
            new_settings_queue.push_back(new_settings);
            new_settings_queue.push_back(old_settings);

            new_settings.manipulability_gradient = !new_settings.manipulability_gradient;
            old_settings.manipulability_gradient = !old_settings.manipulability_gradient;
            new_settings_queue.push_back(new_settings);
            new_settings_queue.push_back(old_settings);
        } else {
            new_settings_queue.push_back(new_settings);
            new_settings_queue.push_back(old_settings);
        }
    }

    int k = 0;
    for (auto& agent_element : predictive_agents_) {
        if (agent_element.first == nullptr) {
            createPredictiveAgent(agent_element, new_settings_queue[k], history);
            k++;
            if (k >= new_settings_queue.size() || k >= settings_.max_agents) {
                return;
            }
        }
    }
}

void CFAgentManager::slotEvolveAgent(const CFAgentSettings& settings) {
    int created = 0;

    AbstractRobotDynamicHistory history;
    initializeCleanHistoryBasedOnRealRobot(history);

    CFAgentSettings new_settings = settings;

    for (auto& agent_element : predictive_agents_) {
        if (created == settings_.new_agents_at_evolution) {
            break;
        }

        if (agent_element.first == nullptr) {
            int size = settings_.allowed_obstacle_limit_distances.size();
            bool create = false;
            for (size_t i = 0; i < size; i++) {
                if (new_settings.obstacle_limit_distance == settings_.allowed_obstacle_limit_distances[i] &&
                    (i + 1 < size)) {
                    new_settings.obstacle_limit_distance = settings_.allowed_obstacle_limit_distances[i + 1];
                    create = true;
                    break;
                }
            }
            if (create) {
                createPredictiveAgent(agent_element, new_settings, history);
                created++;
            } else {
                break;
            }
        }
    }
}

std::pair<SmoothingStrategy, int> CFAgentManager::determineSmoothingStrategy(
    const std::vector<SmoothingStrategy>& smoothing_strategies, int previous_smoothing_strategy_command_index,
    double smoothing_strategy_reference_score, double latest_score) {
    SmoothingStrategy new_strategy;
    int new_smoothing_strategy_command_index;

    while (true) {
        if (previous_smoothing_strategy_command_index + 1 < smoothing_strategies.size()) {
            new_smoothing_strategy_command_index = previous_smoothing_strategy_command_index + 1;
            new_strategy = smoothing_strategies.at(new_smoothing_strategy_command_index);
        } else {
            return {SmoothingStrategy::INVALID, -1};
        }

        if (new_strategy == SmoothingStrategy::REDO_PREVIOUS_IF_IMPROVED) {
            if (latest_score > smoothing_strategy_reference_score) {
                new_smoothing_strategy_command_index = previous_smoothing_strategy_command_index;
                new_strategy = smoothing_strategies.at(new_smoothing_strategy_command_index);
                return {new_strategy, new_smoothing_strategy_command_index};
            }
            previous_smoothing_strategy_command_index = new_smoothing_strategy_command_index;
        } else {
            return {new_strategy, new_smoothing_strategy_command_index};
        }
    }
}

void CFAgentManager::slotSmoothAgent(const CFAgentSettings& settings, const AbstractRobotDynamicHistory& history,
                                     double score) {
    SmoothingStrategy new_strategy;
    int new_smoothing_strategy_command_index;
    std::tie(new_strategy, new_smoothing_strategy_command_index) = determineSmoothingStrategy(
        settings_.smoothing_strategy, settings.internal.smoothing_strategy_command_index,
        settings.internal.smoothing_strategy_reference_score, score);

    if (new_strategy == SmoothingStrategy::INVALID) {
        return;
    }

    double non_smoothed_reference_score = settings.internal.non_smoothed_reference_score;
    if (settings.internal.smoothing_strategy_command_index == -1) {
        non_smoothed_reference_score = score;
    }
    if (best_agent_.settings.internal.non_smoothed_reference_score >
        non_smoothed_reference_score * settings_.allow_smoothing_delta_score_threshold_factor) {
        return;
    }

    int max_creations = 1;
    int creation = 0;

    if (new_strategy == SmoothingStrategy::TIME_OFFSET) {
        max_creations = settings_.smoothing_offsets.size();
    }

    AbstractRobotDynamicHistory clean_history;
    initializeCleanHistoryBasedOnRealRobot(clean_history);
    clean_history.setDurationReferenceTimeStep(duration_reference_time_step_);

    for (auto& agent_element : predictive_agents_) {
        if (creation == max_creations) {
            break;
        }
        if (agent_element.first == nullptr) {
            CFAgentSettings new_settings = settings;
            new_settings.internal.moving_goal = *MovingGoal::fromHistory(history);
            new_settings.internal.non_smoothed_reference_score = non_smoothed_reference_score;
            new_settings.internal.smoothing_strategy_reference_score = score;
            new_settings.internal.smoothing_strategy_command_index = new_smoothing_strategy_command_index;
            new_settings.internal.rotation_vectors = best_agent_.settings.internal.rotation_vectors;
            if (new_strategy == SmoothingStrategy::TIME_OFFSET) {
                const uint64_t TIME_STEP_OFFSET = settings_.smoothing_offsets.at(creation);
                if (new_settings.internal.moving_goal.get().isValidTimeStep(time_step_ + TIME_STEP_OFFSET)) {
                    new_settings.internal.moving_goal.get().incrementTimeStep(time_step_, TIME_STEP_OFFSET);
                    createPredictiveAgent(agent_element, new_settings, clean_history, false);
                }
            } else if (new_strategy == SmoothingStrategy::LINE_OF_SIGHT) {
                new_settings.internal.moving_goal.get().incrementTimeStep(time_step_);
                createPredictiveAgent(agent_element, new_settings, clean_history, true);
            }
            creation++;
        }
    }
}

void CFAgentManager::slotReceiveBet(double score, const CFAgentSettings& settings,
                                    const AbstractRobotDynamicHistory& history) {
    bool signal_score = false;
    const double GOAL_REACHED_MIN_SCORE = 100000.0;
    if (minimum_agent_id_for_bets_countdown_ >= settings.internal.id && received_bets_countdown_ > 0 &&
        (!settings_.only_consider_goal_reached_for_invalid_settings_change_bet_number ||
         score > GOAL_REACHED_MIN_SCORE)) {
        received_bets_countdown_--;
        if (received_bets_countdown_ == 0) {
            signal_score = true;
        }
    }

    // @ToDo: This needs fine tuning and maybe a refactoring for better readability
    const double ROT_VEC_CONTINUITY = 100.0;
    double difference = 0;

    if (best_agent_.settings.internal.rotation_vectors[0].size() == settings.internal.rotation_vectors[0].size()) {
        for (size_t i_fap = 0; i_fap < best_agent_.settings.internal.rotation_vectors.size(); i_fap++) {
            for (size_t i_obstacle = 0; i_obstacle < best_agent_.settings.internal.rotation_vectors[i_fap].size();
                 i_obstacle++) {
                auto difference_vec = settings.internal.rotation_vectors[i_fap].at(i_obstacle) -
                                      best_agent_.settings.internal.rotation_vectors[i_fap].at(i_obstacle);
                difference += difference_vec.squaredNorm();
            }
            difference /= best_agent_.settings.internal.rotation_vectors[i_fap].size();
        }
        difference /= best_agent_.settings.internal.rotation_vectors.size();

        difference *= ROT_VEC_CONTINUITY;

        // Penalize even small changes
        if (difference != 0) {
            difference += 10.0;
        }
    }
    double continuity_score = ROT_VEC_CONTINUITY - 2 * difference;
    continuity_score = std::max(0.0, continuity_score);
    continuity_score = std::min(ROT_VEC_CONTINUITY, continuity_score);

    score += continuity_score;

    if (score > best_agent_.prediction_score) {
        Eigen::Vector3d real_point = best_agent_.latest_robot_state.getEEPose().translation();
        if (history.hasSimilarTrajectoryPoint(time_step_, real_point, settings_.maximum_trajectory_deviation)) {
            CFAgentSettings new_settings = settings;
            if (new_settings.internal.moving_goal) {
                new_settings.internal.moving_goal->resetTimeStep();
                if (new_settings.internal.moving_goal->getOperationMode() ==
                    MovingGoal::MovingGoalOperationMode::POSITION) {
                    new_settings.internal.moving_goal->nextGoalPose();
                } else {
                    new_settings.internal.moving_goal->incrementTimeStep(
                        time_step_, settings.internal.moving_goal->getTimeOffset());
                }
            }

            best_agent_.prediction_score = score;
            best_agent_.settings = new_settings;
            best_agent_.expected_trajectory = history;
            expected_best_agent_trajectory_is_valid_ = true;

            if (received_bets_countdown_ == 0) {
                signal_score = true;
            }
        }
    }

    if (signal_score) {
        emit sgnBetterBestAgentSettingsFound(best_agent_.settings);
        emit sgnMarkPosition(best_agent_.latest_robot_state.getEEPose().translation(), DSColor::INFO);
        const int BEST_AGENT_ID = -1;
        // VIZ: This is only needed for debug purpose and should be deactivated
        // if not needed, since it is quite heavy emit
        sgnPredictionResult(BEST_AGENT_ID, best_agent_.prediction_score,
                            best_agent_.expected_trajectory);
    }
}

void CFAgentManager::slotValidatePrediction(const CFRobotState& real_robot_state) {
    best_agent_.latest_robot_state = real_robot_state;
    time_step_++;

    if (expected_best_agent_trajectory_is_valid_) {
        Eigen::Vector3d real_point = best_agent_.latest_robot_state.getEEPose().translation();
        Eigen::Vector3d real_goal = best_agent_.goal_pose.translation();
        bool no_deviation = best_agent_.expected_trajectory.hasSimilarTrajectoryPoint(
            time_step_, real_point, settings_.maximum_trajectory_deviation);
        if (!no_deviation) {
            ROS_WARN_THROTTLE(1, "Deviation from best agent trajectory. Restarting");
        }
        bool expect_collision = false;
        const int BEST_AGENT_CHECK_CYCLE_MULTIPLE = 10;
        if (time_step_ % BEST_AGENT_CHECK_CYCLE_MULTIPLE == 0) {
            expect_collision = checkExpectedBestAgentTrajectoryForCollision();
            if (expect_collision) {
                ROS_WARN_STREAM("The best agent expects a collision due to new geometry data. Restarting.");
            }
        }

        bool is_busy = isBusy();
        bool deviation = !no_deviation || expect_collision;
        const double GOAL_REACHED_SQUARED_DELTA = settings_.maximum_trajectory_deviation *
                                                  settings_.maximum_trajectory_deviation;
        bool goal_reached = (real_goal - real_point).squaredNorm() < GOAL_REACHED_SQUARED_DELTA;

        if (goal_reached ||
            (no_deviation && !expect_collision && (is_busy || !settings_.restart_prediction_if_idling))) {
            return;
        }
        if (deviation) {
          duration_reference_time_step_ = time_step_;
          minimum_agent_id_for_bets_countdown_ = last_agent_id_ + 1;
          received_bets_countdown_ =
              settings_.minimum_bet_number_before_invalid_settings_change;
          if (receive_direct_starting_bets_) {
            received_bets_countdown_ = 0;
            receive_direct_starting_bets_ = false;
          }
            emit sgnMarkPosition(real_point, DSColor::DANGER);
        } else {
            const double GOAL_REACHED_SCORE = 100000;
            if (best_agent_.prediction_score < GOAL_REACHED_SCORE &&
                settings_.allowed_obstacle_limit_distances.size() > 1) {
                std::rotate(settings_.allowed_obstacle_limit_distances.begin(),
                            settings_.allowed_obstacle_limit_distances.begin() + 1,
                            settings_.allowed_obstacle_limit_distances.end());

                default_agent_settings_.obstacle_limit_distance = settings_.allowed_obstacle_limit_distances.front();
                ROS_WARN_STREAM(
                    "No solution found with the current starting obstacle_limit_distance. Rotating used "
                    "distances.");
            }

            emit sgnMarkPosition(real_point, DSColor::WARNING);
        }

        restartPredictions();

        if (no_deviation && !expect_collision) {
            return;
        }
    }
    expected_best_agent_trajectory_is_valid_ = false;
    best_agent_.prediction_score = 0;

    // Activate safe mode if the best agent is invalid
    best_agent_.settings.invalid_best_agent = true;
    emit sgnBetterBestAgentSettingsFound(best_agent_.settings);
}

void CFAgentManager::slotChangeBestAgentGoalPose(const Eigen::Affine3d& goal_pose) {
    best_agent_.goal_pose = goal_pose;
    expected_best_agent_trajectory_is_valid_ = true;
    receive_direct_starting_bets_ = true;
}

void CFAgentManager::createPredictiveAgent(std::pair<PredictiveAgentPtr, AgentWatcherPtr>& predictive_agent_element,
                                           const CFAgentSettings& settings, const AbstractRobotDynamicHistory& history,
                                           bool line_of_sight_precalculation) {
    last_agent_id_++;

    PredictiveAgentPtr& agent = predictive_agent_element.first;
    AgentWatcherPtr& watcher = predictive_agent_element.second;

    std::shared_ptr<CFGeometry> matching_geometry = getGeometryForObstacleLimitDistance(
        settings.obstacle_limit_distance);
    if (matching_geometry == nullptr) {
        matching_geometry = geometry_collection_.front();
    }

    agent = std::make_unique<CFAgent>(settings, matching_geometry, default_dynamic_settings_, last_agent_id_);

    connect(agent.get(), &CFAgent::sgnPredictionResult, this, &CFAgentManager::sgnPredictionResult,
            Qt::QueuedConnection);

    if (settings_.new_agents_at_evolution > 0) {
        connect(agent.get(), &CFAgent::sgnEvolve, this, &CFAgentManager::slotEvolveAgent, Qt::QueuedConnection);
    }
    if (!settings_.smoothing_offsets.empty()) {
        connect(agent.get(), &CFAgent::sgnSmooth, this, &CFAgentManager::slotSmoothAgent, Qt::QueuedConnection);
    }
    connect(agent.get(), &CFAgent::sgnBet, this, &CFAgentManager::slotReceiveBet, Qt::QueuedConnection);

    agent->setGoalPose(best_agent_.goal_pose);
    agent->robotDynamic()->setHistory(history);

    if (line_of_sight_precalculation) {
        QFuture<CFAgent::PredictionExitStatus> future = QtConcurrent::run(agent.get(),
                                                                          &CFAgent::calculateLineOfSightShortcuts);
        watcher->setFuture(future);
    } else {
        QFuture<CFAgent::PredictionExitStatus> future = QtConcurrent::run(agent.get(), &CFAgent::predict,
                                                                          settings_.continuous_prediction_steps);
        watcher->setFuture(future);
    }
}

bool CFAgentManager::isBusy() const {
    for (auto& agent_element : predictive_agents_) {
        if (agent_element.first != nullptr) {
            return true;
        }
    }

    return false;
}

void CFAgentManager::initializeCleanHistoryBasedOnRealRobot(AbstractRobotDynamicHistory& history) const {
    history.clear();
    history.setDurationReferenceTimeStep(duration_reference_time_step_);
    history.incrementTimeStep(time_step_);
    history.addRobotState(best_agent_.latest_robot_state);
}

// Use this function if you need the smoothing and evolve functionality for point mass robots
// void CFAgentManager::restartPredictions() {
//     enum class CreationType { UNINITIALIZED, DEFAULT, REAL_NON_SMOOTHED, REAL_SMOOTHED, INVALID };
//     AbstractRobotDynamicHistory history;
//     initializeCleanHistoryBasedOnRealRobot(history);
//     CreationType creation_state = CreationType::REAL_SMOOTHED;
//     CFAgentSettings best_settings = best_agent_.settings;
//     if (best_settings == default_agent_settings_) {
//         creation_state = CreationType::INVALID;
//     } else if (!best_settings.internal.moving_goal) {
//         creation_state = CreationType::REAL_NON_SMOOTHED;
//     }

//     for (auto& agent_element : predictive_agents_) {
//         if (agent_element.first == nullptr) {
//             if (creation_state == CreationType::REAL_SMOOTHED) {
//                 CFAgentSettings new_settings = best_settings;
//                 new_settings.invalid_best_agent = true;
//                 new_settings.safety_joint_vel_calculation_method = true;
//                 new_settings.internal.moving_goal->resetTimeStep();
//                 if (new_settings.internal.moving_goal->getOperationMode() ==
//                     MovingGoal::MovingGoalOperationMode::POSITION) {
//                   new_settings.internal.moving_goal->nextGoalPose();
//                 } else {
//                   new_settings.internal.moving_goal->incrementTimeStep(
//                       time_step_,
//                       best_settings.internal.moving_goal->getTimeOffset());
//                 }
//                 new_settings.internal.smoothing_strategy_reference_score = 0;
//                 createPredictiveAgent(agent_element, new_settings, history);
//                 creation_state = CreationType::REAL_NON_SMOOTHED;
//             } else if (creation_state == CreationType::REAL_NON_SMOOTHED) {
//               CFAgentSettings new_settings = best_settings;
//               new_settings.invalid_best_agent = true;
//               new_settings.safety_joint_vel_calculation_method = true;
//               new_settings.internal.moving_goal = boost::none;
//               new_settings.internal.smoothing_strategy_command_index = -1;
//               new_settings.internal.smoothing_strategy_reference_score = 0;
//               new_settings.internal.non_smoothed_reference_score = 0;
//               createPredictiveAgent(agent_element, new_settings, history);
//               creation_state = CreationType::INVALID;
//             }
//         }

//         if (creation_state == CreationType::INVALID) {
//             break;
//         }
//     }
// }

void CFAgentManager::restartPredictions() {
    enum class CreationType { UNINITIALIZED, DEFAULT, SAFETY, INVALID };
    AbstractRobotDynamicHistory history;
    initializeCleanHistoryBasedOnRealRobot(history);
    CreationType creation_state = CreationType::UNINITIALIZED;
    CFAgentSettings best_settings = best_agent_.settings;
    if (best_settings.invalid_best_agent) {
        creation_state = CreationType::SAFETY; 
    } else {
        creation_state = CreationType::DEFAULT; 
    }

    for (auto& agent_element : predictive_agents_) {
        if (agent_element.first == nullptr) {
            if (creation_state == CreationType::SAFETY) {
                CFAgentSettings new_settings = best_settings;
                new_settings.internal.moving_goal = boost::none;
                new_settings.internal.smoothing_strategy_command_index = -1;
                new_settings.internal.smoothing_strategy_reference_score = 0;
                new_settings.internal.smoothing_strategy_reference_score = 0;
                createPredictiveAgent(agent_element, new_settings, history);
                creation_state = CreationType::DEFAULT; 
            } else if (creation_state == CreationType::DEFAULT) {
                CFAgentSettings new_settings = best_settings;
                new_settings.invalid_best_agent = false;
                new_settings.internal.moving_goal = boost::none;
                new_settings.internal.smoothing_strategy_command_index = -1;
                new_settings.internal.smoothing_strategy_reference_score = 0;
                new_settings.internal.non_smoothed_reference_score = 0;
                createPredictiveAgent(agent_element, new_settings, history);
                creation_state = CreationType::INVALID;
            }
        }

        if (creation_state == CreationType::INVALID) {
            break;
        }
    }
}

void CFAgentManager::slotSetObstacles(const pcl_obstacle_generator::MovingObstacles::Ptr& latest_obstacle_msg) const {
    for (auto& geometry : geometry_collection_) {
        geometry->setNewData(latest_obstacle_msg, time_step_);
    }
}

void CFAgentManager::slotInitRotVec(const RotationVectorMapVector& rotation_vectors) {
    best_agent_.settings.internal.rotation_vectors = rotation_vectors;
}

std::shared_ptr<CFGeometry> CFAgentManager::getGeometryForObstacleLimitDistance(double obstacle_limit_distance) const {
    std::shared_ptr<CFGeometry> matching_geometry;
    for (auto& geometry : geometry_collection_) {
        if (almostEqualRelative(geometry->getMaximumSearchDistance(), obstacle_limit_distance)) {
            matching_geometry = geometry;
            break;
        }
    }
    return matching_geometry;
}

bool CFAgentManager::checkExpectedBestAgentTrajectoryForCollision() const {
    boost::circular_buffer<uint64_t> time_steps = best_agent_.expected_trajectory.getTimeSteps();
    const boost::circular_buffer<CFRobotState>& states = best_agent_.expected_trajectory.getRobotStates();
    int i = 0;
    int max_i = time_steps.size() - 1;
    std::shared_ptr<CFGeometry> geometry = getGeometryForObstacleLimitDistance(
        best_agent_.settings.obstacle_limit_distance);
    const double SQUARED_COLLISION_DISTANCE = best_agent_.settings.collision_distance *
                                              best_agent_.settings.collision_distance;

    std::vector<double> fap_obstacle_distances = {99.0};

    while (time_steps[i] >= time_step_ && i < max_i) {
        for (const auto& fap_pose : states.at(i).fap_poses) {
            auto position = static_cast<TimedPoint>(fap_pose.translation());

            position.time_step = time_steps.at(i);

            auto fap_obstacle_distance = geometry->getCriticalObjectSquareDistance(position);
            if (fap_obstacle_distance.has_value()) {
                fap_obstacle_distances.push_back(fap_obstacle_distance.value());
            }
            
        }
        i++;
        std::sort(fap_obstacle_distances.begin(), fap_obstacle_distances.end());
        double minimal_obstacle_distance = fap_obstacle_distances.front();
        if (SQUARED_COLLISION_DISTANCE >= minimal_obstacle_distance) {
            return true;
        }
    }

    return false;
}

Eigen::Vector3d CFAgentManager::calculateRotatedRotationVector(int for_agent_number, int total_number_of_new_agents,
                                                               const Eigen::Vector3d& original_rotation_vector,
                                                               Eigen::Vector3d rotation_axis) {
    rotation_axis.normalize();
    if (vectorsAreAlmostEqual(original_rotation_vector, rotation_axis)) {
        double const OFFSET = 0.1;
        rotation_axis.x() += rotation_axis.x() + OFFSET;
    }

    // Rotation angle = angle around which the rotation vector is rotated
    double theta = (for_agent_number + 1) * 2 * M_PI / (total_number_of_new_agents + 1);

    // Rodriguez Rotation Formula
    return original_rotation_vector * std::cos(theta) +
           rotation_axis.cross(original_rotation_vector) * std::sin(theta) +
           rotation_axis * (rotation_axis.dot(original_rotation_vector)) * (1 - std::cos(theta));
}