#include "cf_motion_planner/point_mass_robot_dynamic.h"

PointMassRobotDynamic::PointMassRobotDynamic(const PointMassRobotDynamicSettings& settings) : settings_(settings) {}

void PointMassRobotDynamic::calculateStep(const std::vector<Vector6d>& fap_wrenches,
                                          const CFAgentSettings& agent_settings) {
    double mass_kg = settings_.getMass();
    double step_time = settings_.getStepTime();

    Vector6d latest_acceleration = fap_wrenches.back().head(3) / mass_kg;

    Vector6d latest_velocity;
    latest_velocity.head(3) = latest_velocity.head(3) + (latest_acceleration.head(3) * step_time);

    latest_robot_state_.getEEVel() = latest_velocity;

    double vel_norm = latest_velocity.head(3).norm();
    double max_vel = settings_.getMaxVelocity();
    if (vel_norm > max_vel) {
        latest_velocity.head(3) = latest_velocity.head(3) * max_vel / vel_norm;
    }

    latest_robot_state_.getEEPose().translation() += latest_velocity.head(3) * step_time;

    history_.addWrench(fap_wrenches);
    makeHistorySnapshot();
}