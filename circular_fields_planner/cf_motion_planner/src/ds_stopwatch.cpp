#include "cf_motion_planner/ds_stopwatch.h"

DSStopwatch::DSStopwatch(int mean_samples, const std::string& measurement_name, bool print_result)
    : MEAN_SAMPLES_(mean_samples),
      duration_buffer_(mean_samples),
      measurement_name_(measurement_name),
      PRINT_RESULT_(print_result) {}

void DSStopwatch::tic() {
    tic_start_ = std::chrono::system_clock::now();
    duration_buffer_.clear();
}

boost::optional<std::chrono::microseconds> DSStopwatch::toc() {
    auto end = std::chrono::system_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::microseconds>(end - tic_start_);
    duration_buffer_.push_back(elapsed);
    sample_counter_++;

    if (sample_counter_ == MEAN_SAMPLES_) {
        auto mean = std::accumulate(duration_buffer_.begin(), duration_buffer_.end(), std::chrono::microseconds(0)) /
                    duration_buffer_.size();
        sample_counter_ = 0;
        if (PRINT_RESULT_) {
            ROS_INFO_STREAM(measurement_name_ << " took average " << mean.count() << " µs over " << MEAN_SAMPLES_
                                              << " iterations.");
        }
        return std::chrono::duration_cast<std::chrono::microseconds>(mean);
    }

    return boost::none;
}

DSWatchdog::DSWatchdog(std::chrono::microseconds trigger_time_mus, const std::string& measurement_name, int mean_samples,
                       bool print_result)
    : stopwatch_(mean_samples, measurement_name, false),
      trigger_time_mus_(trigger_time_mus),
      MEAN_SAMPLES_(mean_samples),
      measurement_name_(measurement_name),
      PRINT_RESULT_(print_result) {}

bool DSWatchdog::check() {
    boost::optional<std::chrono::microseconds> result;
    if (first_check_called_) {
        result = stopwatch_.toc();
    }
    stopwatch_.tic();

    first_check_called_ = true;

    if (result and result > trigger_time_mus_) {
        if (PRINT_RESULT_) {
            ROS_ERROR_STREAM(measurement_name_ << " triggered. Execution took " << result.get().count() << " µs over "
                                               << MEAN_SAMPLES_ << " iterations. Allowed were "
                                               << trigger_time_mus_.count() << " µs.");
        }
        return false;
    }

    return true;
}