#include "cf_motion_planner/utility.h"

bool almostEqualRelative(double a, double b, double epsilon) {
    return std::abs(a - b) <= (std::max(std::abs(a), std::abs(b)) * epsilon);
};

bool vectorsAreAlmostEqual(const Eigen::Vector3d& vector1, const Eigen::Vector3d& vector2, double epsilon) {
    for (size_t i = 0; i < 3; i++) {
        if (!almostEqualRelative(vector1[i], vector2[i], epsilon)) {
            return false;
        }
    }
    return true;
}

bool stdVectorsAreAlmostEqual(const std::vector<double>& vector1, const std::vector<double>& vector2, double epsilon) {
    const int SIZE = vector1.size();
    if (SIZE != vector2.size()) {
        return false;
    }
    for (int i = 0; i < SIZE; i++) {
        if (!almostEqualRelative(vector1[i], vector2[i], epsilon)) {
            return false;
        }
    }
    return true;
}

bool timedPointsAreAlmostEqual(const TimedPoint& point1, const TimedPoint& point2, double epsilon) {
    return vectorsAreAlmostEqual(point1.point, point2.point, epsilon) && point1.time_step == point2.time_step;
}
