#include "cf_motion_planner/cf_planner_settings.h"

bool CFPlannerSettings::operator==(const CFPlannerSettings& rhs) const {
    return almostEqualRelative(control_loop_cycle_duration, rhs.control_loop_cycle_duration) &&
           prediction_step_time_multiple == rhs.prediction_step_time_multiple && max_agents == rhs.max_agents &&
           new_agents_at_split == rhs.new_agents_at_split && new_agents_at_evolution == rhs.new_agents_at_evolution &&
           continuous_prediction_steps == rhs.continuous_prediction_steps &&
           almostEqualRelative(maximum_trajectory_deviation, rhs.maximum_trajectory_deviation) &&
           stdVectorsAreAlmostEqual(allowed_obstacle_limit_distances, rhs.allowed_obstacle_limit_distances) &&
           maximum_number_considered_points_per_object == rhs.maximum_number_considered_points_per_object &&
           restart_prediction_if_idling == rhs.restart_prediction_if_idling &&
           smoothing_offsets == rhs.smoothing_offsets && smoothing_strategy == rhs.smoothing_strategy &&
           almostEqualRelative(allow_smoothing_delta_score_threshold_factor,
                               rhs.allow_smoothing_delta_score_threshold_factor) &&
           minimum_bet_number_before_invalid_settings_change == rhs.minimum_bet_number_before_invalid_settings_change &&
           only_consider_goal_reached_for_invalid_settings_change_bet_number ==
               rhs.only_consider_goal_reached_for_invalid_settings_change_bet_number;
}

CFPlannerSettings CFPlannerSettings::generateFromRosParameterEntry(const XmlRpc::XmlRpcValue& ros_parameter_entry) {
    CFPlannerSettings result;

    for (const auto& element : ros_parameter_entry) {
        std::string key = element.first;
        if (key == "control_loop_cycle_duration") {
            result.control_loop_cycle_duration = static_cast<double>(element.second);
        } else if (key == "prediction_step_time_multiple") {
            result.prediction_step_time_multiple = static_cast<int>(element.second);
        } else if (key == "max_agents") {
            result.max_agents = static_cast<int>(element.second);
        } else if (key == "new_agents_at_split") {
            result.new_agents_at_split = static_cast<int>(element.second);
        } else if (key == "new_agents_at_evolution") {
            result.new_agents_at_evolution = static_cast<int>(element.second);
        } else if (key == "explore_nullspace") {
            result.explore_nullspace = static_cast<bool>(element.second);
        } else if (key == "explore_manipulability") {
            result.explore_manipulability = static_cast<bool>(element.second);
        } else if (key == "continuous_prediction_steps") {
            result.continuous_prediction_steps = static_cast<int>(element.second);
        } else if (key == "maximum_trajectory_deviation") {
            result.maximum_trajectory_deviation = static_cast<double>(element.second);
        } else if (key == "allowed_obstacle_limit_distances") {
            int array_size = element.second.size();
            for (size_t i = 0; i < array_size; i++) {
                auto num = static_cast<double>(element.second[i]);
                result.allowed_obstacle_limit_distances.push_back(num);
            }
        } else if (key == "maximum_number_considered_points_per_object") {
            result.maximum_number_considered_points_per_object = static_cast<int>(element.second);
        } else if (key == "restart_prediction_if_idling") {
            result.restart_prediction_if_idling = static_cast<bool>(element.second);
        } else if (key == "smoothing_offsets") {
            int array_size = element.second.size();
            for (size_t i = 0; i < array_size; i++) {
                auto num = static_cast<uint64_t>((static_cast<int>(element.second[i])));
                result.smoothing_offsets.push_back(num);
            }
        } else if (key == "smoothing_strategy") {
            int array_size = element.second.size();
            for (size_t i = 0; i < array_size; i++) {
                auto num = static_cast<SmoothingStrategy>(static_cast<int>(element.second[i]));
                result.smoothing_strategy.push_back(num);
            }
        } else if (key == "allow_smoothing_delta_score_threshold_factor") {
            result.allow_smoothing_delta_score_threshold_factor = static_cast<double>(element.second);
        } else if (key == "minimum_bet_number_before_invalid_settings_change") {
            result.minimum_bet_number_before_invalid_settings_change = static_cast<int>(element.second);
        } else if (key == "only_consider_goal_reached_for_invalid_settings_change_bet_number") {
            result.only_consider_goal_reached_for_invalid_settings_change_bet_number = static_cast<bool>(
                element.second);
        } else {
            ROS_WARN_STREAM("CFPlannerSettings::generateFromRosParameterEntry - key '" << key << "' is unknown.");
        }
    }

    return result;
}
