#include "cf_motion_planner/kdl_dynamics.h"

using namespace KDL;

RobotKDL::RobotKDL() {}

RobotKDL::~RobotKDL() {
    delete jacSolverEE_;
    delete jacSolverFAP_;
    delete jacDotSolver_;
    delete velSolver_;
    delete posFKSolverEE_;
    delete posFKSolverFAP_;
}

void RobotKDL::buildChain(const std::vector<int> &joint_types, const std::vector<double> &a,
                          const std::vector<double> &alpha, const std::vector<double> &d,
                          const std::vector<double> &theta) {
    int num_segments = joint_types.size();
    for (int i = 0; i < num_segments; i++) {
        if (joint_types[i] == 0) {
            chainEE_.addSegment(Segment(Joint(Joint::None), Frame::DH_Craig1989(a[i], alpha[i], d[i], theta[i])));
        } else if (joint_types[i] == 1) {
            chainEE_.addSegment(Segment(Joint(Joint::RotZ), Frame::DH_Craig1989(a[i], alpha[i], d[i], theta[i])));
        }
    }
    jacSolverEE_ = new KDL::ChainJntToJacSolver(chainEE_);
    jacDotSolver_ = new KDL::ChainJntToJacDotSolver(chainEE_);
    velSolver_ = new KDL::ChainIkSolverVel_pinv(chainEE_);
    posFKSolverEE_ = new KDL::ChainFkSolverPos_recursive(chainEE_);
    posFKSolverFAP_ = new KDL::ChainFkSolverPos_recursive(chainEE_);
    jacSolverFAP_ = new KDL::ChainJntToJacSolver(chainEE_);
}

int RobotKDL::getNrOfSegments() { return chainEE_.getNrOfSegments(); }

int RobotKDL::getNrOfJoints() { return chainEE_.getNrOfJoints(); }

int RobotKDL::getJacobianDot(const KDL::JntArrayVel &q_in, KDL::Jacobian &j_dot) {
    int ret = jacDotSolver_->JntToJacDot(q_in, j_dot);

    if (ret != 0) {
        std::cout << "Solver Error Nr. " << ret << " | ErrorStr: " << this->jacDotSolver_->strError(ret) << std::endl;
    }
    return ret;
}

int RobotKDL::getVelocity(const KDL::JntArray &q_in, const KDL::Twist &v_in, KDL::JntArray &qdot_out) {
    int ret = velSolver_->CartToJnt(q_in, v_in, qdot_out);
    if (ret != 0) {
        std::cout << "Solver Error Nr. " << ret << " | ErrorStr: " << this->velSolver_->strError(ret) << std::endl;
    }

    return ret;
}

int RobotKDL::getJntToCartFAP(const KDL::JntArray &q_in, int segment_id, KDL::Frame &cart_out) {
    int ret = posFKSolverFAP_->JntToCart(q_in, cart_out, segment_id);
    if (ret != 0) {
        std::cout << "Solver Error Nr. " << ret << " | ErrorStr: " << this->posFKSolverFAP_->strError(ret) << std::endl;
    }

    return ret;
}

int RobotKDL::getJacobianFAP(const KDL::JntArray &q_in, int segment_id, KDL::Jacobian &jac_out) {
    int ret = jacSolverFAP_->JntToJac(q_in, jac_out, segment_id);

    if (ret != 0) {
        std::cout << "Solver Error Nr. " << ret << " | ErrorStr: " << this->jacSolverFAP_->strError(ret) << std::endl;
    }
    return ret;
}
