#include "cf_motion_planner/cf_agent_manager_ros_interface.h"

CFAgentManagerROSInterface::CFAgentManagerROSInterface(QThread* manager_thread) {
    thread_ = manager_thread;
    this->moveToThread(thread_);

    node_handle_.setCallbackQueue(&callback_queue_);

    spin_timer_ = new QTimer;
    spin_timer_->moveToThread(thread_);
    connect(spin_timer_, &QTimer::timeout, this, &CFAgentManagerROSInterface::spinOnce);

    QTimer::singleShot(0, this, &CFAgentManagerROSInterface::init);
}

CFAgentManagerROSInterface::~CFAgentManagerROSInterface() {
    spin_timer_->stop();
    spin_timer_->deleteLater();
}

void CFAgentManagerROSInterface::init() {
    constructPublishersAndSubscribers();
    spin_timer_->start(spin_timer_time_ms_);
}

void CFAgentManagerROSInterface::constructPublishersAndSubscribers() {
    // Publisher

    const int VISUALIZATION_QUEUE_SIZE = 10;
    prediction_trace_publisher_ = node_handle_.advertise<visualization_msgs::Marker>("prediction_trace",
                                                                                     VISUALIZATION_QUEUE_SIZE);
    best_prediction_trace_publisher_ = node_handle_.advertise<visualization_msgs::Marker>("best_prediction_trace",
                                                                                          VISUALIZATION_QUEUE_SIZE);
    prediction_label_publisher_ = node_handle_.advertise<visualization_msgs::Marker>("prediction_label",
                                                                                     VISUALIZATION_QUEUE_SIZE);
    best_prediction_label_publisher_ = node_handle_.advertise<visualization_msgs::Marker>("best_prediction_label",
                                                                                          VISUALIZATION_QUEUE_SIZE);
    prediction_forces_publisher_ = node_handle_.advertise<visualization_msgs::Marker>("prediction_forces",
                                                                                      VISUALIZATION_QUEUE_SIZE);
    marked_positions_publisher_ = node_handle_.advertise<visualization_msgs::Marker>("marked_positions",
                                                                                     VISUALIZATION_QUEUE_SIZE);
}

void CFAgentManagerROSInterface::spinOnce() { callback_queue_.callAvailable(); }

void CFAgentManagerROSInterface::publishPredictionTrace(int agent_id,
                                                        const boost::circular_buffer<CFRobotState>& states,
                                                        double lifetime_s) const {
    visualization_msgs::Marker trace;
    trace.header.frame_id = "map";
    trace.header.stamp = ros::Time::now();
    trace.lifetime = ros::Duration(lifetime_s);
    const bool IS_BEST_AGENT = agent_id < 0;
    if (IS_BEST_AGENT) {
        trace.scale.x = 0.009;  // NOLINT(readability-magic-numbers) : Style value initialization
        trace.color.a = 1.0;
        trace.color.r = 1.0;
        trace.color.g = 0.0;
        trace.color.b = 0.0;
        trace.pose.orientation.w = 1.0;
    } else {
        trace.scale.x = 0.008;  // NOLINT(readability-magic-numbers) : Style value initialization
        trace.color.a = 1.0;
        trace.color.r = 0.0;
        trace.color.g = 0.0;
        trace.color.b = 0.9;  // NOLINT(readability-magic-numbers) : Style value initialization
        trace.pose.orientation.w = 1.0;
    }

    trace.ns = "trace_agent" + std::to_string(agent_id);
    trace.id = agent_id;
    trace.action = visualization_msgs::Marker::ADD;
    trace.type = visualization_msgs::Marker::LINE_STRIP;

    int subsampling_factor = 100;
    int number_of_poses = states.size() / subsampling_factor;
    trace.points.reserve(number_of_poses);
    int counter = 0;
    for (const auto& state : boost::adaptors::reverse(states)) {
        if (counter % subsampling_factor == 0) {
            geometry_msgs::Point point_position;
            tf::pointEigenToMsg(state.getEEPose().translation(), point_position);
            trace.points.push_back(point_position);
        }
    }

    if (IS_BEST_AGENT) {
        best_prediction_trace_publisher_.publish(trace);
    } else {
        prediction_trace_publisher_.publish(trace);
    }
}

void CFAgentManagerROSInterface::publishPredictionLabel(int agent_id, double score,
                                                        const boost::circular_buffer<CFRobotState>& states,
                                                        double lifetime_s) const {
    visualization_msgs::Marker label;
    label.header.frame_id = "map";
    label.header.stamp = ros::Time::now();
    label.lifetime = ros::Duration(lifetime_s);
    const bool IS_BEST_AGENT = agent_id < 0;
    if (IS_BEST_AGENT) {
        label.scale.z = 0.05;  // NOLINT(readability-magic-numbers) : Style value initialization
        label.color.a = 1.0;
        label.color.r = 1.0;
        label.color.g = 0.0;
        label.color.b = 0.0;
        label.pose.orientation.w = 1.0;
    } else {
        label.scale.z = 0.05;  // NOLINT(readability-magic-numbers) : Style value initialization
        label.color.a = 1.0;
        label.color.r = 0.0;
        label.color.g = 0.0;
        label.color.b = 0.9;  // NOLINT(readability-magic-numbers) : Style value initialization
        label.pose.orientation.w = 1.0;
    }

    label.ns = "label_agent" + std::to_string(agent_id);
    label.id = agent_id;
    label.action = visualization_msgs::Marker::ADD;
    label.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
    std::stringstream textstream;
    textstream << "ID: " << agent_id << " Score: " << score;
    label.text = textstream.str();

    int number_of_poses = states.size();
    if (number_of_poses <= 0) {
        return;
    }
    const double THIRD = 0.33333333;
    int anchor_pose_index = std::roundl((number_of_poses - 1) * THIRD);
    Eigen::Affine3d anchor_pose = states.at(anchor_pose_index).getEEPose();
    geometry_msgs::Pose anchor_pose_msg;
    tf::poseEigenToMsg(anchor_pose, anchor_pose_msg);
    label.pose = anchor_pose_msg;

    if (IS_BEST_AGENT) {
        best_prediction_label_publisher_.publish(label);
    } else {
        prediction_label_publisher_.publish(label);
    }
}

void CFAgentManagerROSInterface::publishPredictionForces(
    int agent_id, const boost::circular_buffer<CFRobotState>& states,
    const boost::circular_buffer<std::vector<Eigen::Matrix<double, 6, 1>>>& wrenches, double lifetime_s) const {
    int number_of_poses = states.size();
    int number_of_forces = wrenches.size();
    if (number_of_forces == number_of_poses || number_of_forces == number_of_poses - 1) {
        visualization_msgs::Marker sampled_forces;
        sampled_forces.header.frame_id = "map";
        sampled_forces.header.stamp = ros::Time::now();
        sampled_forces.lifetime = ros::Duration(lifetime_s);
        sampled_forces.scale.x = 0.01;  // NOLINT(readability-magic-numbers) : Style value initialization
        sampled_forces.color.a = 1.0;
        sampled_forces.color.r = 0.9;  // NOLINT(readability-magic-numbers) : Style value initialization
        sampled_forces.color.g = 0.6;  // NOLINT(readability-magic-numbers) : Style value initialization
        sampled_forces.color.b = 0.0;
        sampled_forces.pose.orientation.w = 1.0;

        sampled_forces.ns = "sampled_forces_agent" + std::to_string(agent_id);
        sampled_forces.id = agent_id;
        sampled_forces.action = visualization_msgs::Marker::ADD;
        sampled_forces.type = visualization_msgs::Marker::LINE_LIST;

        int point_index = number_of_poses - 1;
        sampled_forces.points.reserve(2 * number_of_forces);

        for (const auto& wrench : boost::adaptors::reverse(wrenches)) {
            Eigen::Vector3d anchor_point = states[point_index].getEEPose().translation();
            geometry_msgs::Point anchor_point_msg;
            tf::pointEigenToMsg(anchor_point, anchor_point_msg);
            sampled_forces.points.push_back(anchor_point_msg);

            Eigen::Vector3d head_point = anchor_point + wrench.back().head(3);
            geometry_msgs::Point head_point_msg;
            tf::pointEigenToMsg(head_point, head_point_msg);
            sampled_forces.points.push_back(head_point_msg);

            point_index--;
        }
        prediction_forces_publisher_.publish(sampled_forces);
    } else {
        ROS_WARN_STREAM("CFPlannerNode::publishPredictionForces "
                        << "- Number of forces and poses in history does not match. Can not display forces.");
    }
}

void CFAgentManagerROSInterface::slotPublishPredictionResult(int agent_id, double score,
                                                             const AbstractRobotDynamicHistory& history) {
    double lifetime_s = 0.8;
    auto& states = history.getRobotStates();
    auto& wrenches = history.getWrenches();
    if (agent_id < 0) {
        agent_id = -1;
        lifetime_s = 20;  // NOLINT(readability-magic-numbers) : Settings value initialization
    }

    publishPredictionTrace(agent_id, states, lifetime_s);
    publishPredictionLabel(agent_id, score, states, lifetime_s);
    publishPredictionForces(agent_id, states, wrenches, lifetime_s);
}

void CFAgentManagerROSInterface::slotMarkPosition(const Eigen::Vector3d& position, DSColor color) {
    const double LIFETIME_S = 60.0;

    visualization_msgs::Marker marker;
    marker.action = visualization_msgs::Marker::ADD;
    marker.type = visualization_msgs::Marker::SPHERE;
    marker.header.frame_id = "map";
    marker.header.stamp = ros::Time::now();
    marker.lifetime = ros::Duration(LIFETIME_S);
    double scale = 0.05;  // NOLINT(readability-magic-numbers) : Style value initialization
    if (color == DSColor::DANGER) {
        scale = 0.06;  // NOLINT(readability-magic-numbers) : Style value override for special case
    }
    marker.scale.x = scale;
    marker.scale.y = scale;
    marker.scale.z = scale;

    DSColorStandardTheme theme;
    marker.color = theme.toRGBA(color);

    marker.pose.position.x = position.x();
    marker.pose.position.y = position.y();
    marker.pose.position.z = position.z();
    marker.pose.orientation.w = 1.0;

    marker.ns = "marked_position" + std::to_string(marked_position_counter_);
    marker.id = marked_position_counter_;

    marked_positions_publisher_.publish(marker);
    marked_position_counter_++;
}
