#include "cf_motion_planner/simple_robot_dynamic.h"

SimpleRobotDynamic::SimpleRobotDynamic(const SimpleRobotDynamicSettings& settings) {
    std::vector<double> a, alpha, d, theta, q_min, q_max, qd_max, qdd_max;
    std::vector<int> joint_types;
    double step_time = settings.getStepTime();
    q_min = settings.getMinJointPositions();
    q_max = settings.getMaxJointPositions();
    qd_max = settings.getMaxJointVelocities();
    qdd_max = settings.getMaxJointTorques();
    joint_types = settings.getJointTypes();
    a = settings.getDHParameterA();
    alpha = settings.getDHParameterAlpha();
    d = settings.getDHParameterD();
    theta = settings.getDHParameterTheta();

    cf_robot_calculator_.buildChain(joint_types, a, alpha, d, theta);
    cf_robot_calculator_.setParams(step_time, q_min, q_max, qd_max, qdd_max);
}

void SimpleRobotDynamic::calculateStep(const std::vector<Vector6d>& fap_wrenches,
                                       const CFAgentSettings& agent_settings) {
    cf_robot_calculator_.calculateDynamicStep(fap_wrenches, latest_robot_state_, agent_settings);

    history_.addWrench(fap_wrenches);
    makeHistorySnapshot();
}