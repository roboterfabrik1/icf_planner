#include "cf_motion_planner/cf_planner.h"

CFPlanner::CFPlanner(CFPlannerROSInterface* ros_interface, const CFPlannerSettings& planner_settings,
                     const CFAgentSettings& agent_settings,
                     const std::shared_ptr<AbstractRobotDynamicSettings>& dynamic_settings)
    : ros_interface_(ros_interface), settings_(planner_settings) {
    agent_manager_thread_ = new QThread;
    connect(agent_manager_thread_, &QThread::finished, agent_manager_thread_, &QThread::deleteLater);

    default_agent_settings_ = agent_settings;
    dynamic_settings->setStepTime(planner_settings.control_loop_cycle_duration *
                                  planner_settings.prediction_step_time_multiple);
    default_dynamic_settings_ = dynamic_settings;

    Vector3d unit_z_3d;
    unit_z_3d << 0, 0, 1;
    std::map<int, Vector3d> tmp_map;
    tmp_map.insert(std::pair<int, Vector3d>(0, unit_z_3d));
    for (size_t i = 0; i < default_agent_settings_.fap_frame_ids.size(); i++) {
        default_agent_settings_.internal.rotation_vectors.push_back(tmp_map);
    }

    default_agent_settings_.obstacle_limit_distance = settings_.allowed_obstacle_limit_distances.front();
    for (const double& obstacle_limit_distance : settings_.allowed_obstacle_limit_distances) {
        geometry_collection_.emplace_back(
            std::make_shared<CFGeometry>(obstacle_limit_distance, settings_.control_loop_cycle_duration,
                                         settings_.maximum_number_considered_points_per_object));
    }
    real_robot_representation_ = std::make_unique<CFAgent>(default_agent_settings_, geometry_collection_.front(),
                                                           default_dynamic_settings_, 0, true);

    agent_manager_ =
        new CFAgentManager(agent_manager_thread_, settings_, default_agent_settings_, default_dynamic_settings_);

    connectPlannerWithAgentManager();
    connectPlannerWithROSInterface();

    // The following code sets up the stopwatch and timer. It is for debug only.
    // VVVVVVVVVVVVVVVVVVVVVVVVVVV
    const int MEAN_SAMPLES = 3000;
    const std::string MEASUREMENT_NAME = "Reaction calculation";
    debug_stopwatch_ = std::make_unique<DSStopwatch>(MEAN_SAMPLES, MEASUREMENT_NAME);

    const double MILLION = 1000000.0;
    const double WATCHDOG_THRESHOLD_MULTIPLE = 1.2;
    std::chrono::microseconds trigger_time_mus(
        std::lround(planner_settings.control_loop_cycle_duration * MILLION * WATCHDOG_THRESHOLD_MULTIPLE));
    const std::string WATCHDOG_NAME = "Watchdog";
    const int WATCHDOG_MEAN_SAMPLES = 10;
    control_loop_watchdog_ = std::make_unique<DSWatchdog>(trigger_time_mus, WATCHDOG_NAME, WATCHDOG_MEAN_SAMPLES);
    // ^^^^^^^^^^^^^^^^^^^^^^^^^^^

    const int MEAN_SAMPLES2 = 1;
    const std::string MEASUREMENT_NAME2 = "Step time";
    step_time_stopwatch_ = std::make_unique<DSStopwatch>(MEAN_SAMPLES2, MEASUREMENT_NAME2, false);

    // Start thread
    agent_manager_thread_->start();
}

CFPlanner::~CFPlanner() {
    agent_manager_->deleteLater();
    // This will also trigger deletion of thread through connected signal finished => delete later
    agent_manager_thread_->quit();
}

void CFPlanner::connectPlannerWithAgentManager() const {
    connect(this, &CFPlanner::sgnPredictionStep, agent_manager_, &CFAgentManager::slotValidatePrediction);
    connect(this, &CFPlanner::sgnGoalPoseChanged, agent_manager_, &CFAgentManager::slotChangeBestAgentGoalPose);
    connect(this, &CFPlanner::sgnNewObstacleData, agent_manager_, &CFAgentManager::slotSetObstacles);
    connect(this, &CFPlanner::sgnFirstRotVecData, agent_manager_, &CFAgentManager::slotInitRotVec);
    connect(this, &CFPlanner::sgnCreateNewAgent, agent_manager_, &CFAgentManager::slotCreateAgent);

    connect(agent_manager_, &CFAgentManager::sgnBetterBestAgentSettingsFound, this,
            &CFPlanner::slotUpdateRealRobotRepresentationSettings);
}

void CFPlanner::connectPlannerWithROSInterface() const {
    connect(this, &CFPlanner::sgnFAPForces, ros_interface_, &CFPlannerROSInterface::slotPublishFAPForces);
    connect(this, &CFPlanner::sgnRealRobotSettings, ros_interface_,
            &CFPlannerROSInterface::slotPublishRealRobotSettings);
    connect(this, &CFPlanner::sgnStepTime, ros_interface_, &CFPlannerROSInterface::slotPublishStepTime);
    connect(ros_interface_, &CFPlannerROSInterface::sgnNewRobotState, this, &CFPlanner::slotSetRobotState);
    connect(ros_interface_, &CFPlannerROSInterface::sgnNewGoalPose, this, &CFPlanner::slotSetGoalPose);
    connect(ros_interface_, &CFPlannerROSInterface::sgnNewObstacleMsg, this, &CFPlanner::slotSetObstacles);
    connect(ros_interface_, &CFPlannerROSInterface::sgnNewRotVecMsg, this, &CFPlanner::slotSetRotVecs);
    connect(ros_interface_, &CFPlannerROSInterface::sgnRequestForceReactionCalculation, this,
            &CFPlanner::slotCalculateForceReaction);
}

void CFPlanner::slotSetRobotState(const CFRobotState& robot_state) const {
    real_robot_representation_->robotDynamic()->setRobotState(robot_state);
}

void CFPlanner::slotSetRotVecs(const RotationVectorMapVector& rotation_vectors) {
    if (!rot_vec_init_) {
        real_robot_representation_->setRotationVectors(rotation_vectors);
        emit sgnFirstRotVecData(rotation_vectors);
        rot_vec_init_ = true;
    }
    emit sgnCreateNewAgent(real_robot_representation_->getSettings(), rotation_vectors);
}

void CFPlanner::slotSetGoalPose(const Eigen::Affine3d& goal_pose) {
    real_robot_representation_->setGoalPose(goal_pose);
    emit sgnGoalPoseChanged(goal_pose);
}

void CFPlanner::slotSetObstacles(const pcl_obstacle_generator::MovingObstacles::Ptr& latest_obstacle_msg) {
    for (auto& geometry : geometry_collection_) {
        geometry->setNewData(latest_obstacle_msg, time_step_);

    }
    emit sgnNewObstacleData(latest_obstacle_msg);
}

void CFPlanner::slotCalculateForceReaction() {
    control_loop_watchdog_->check();

    if (control_loop_validate_cycle_counter_ == 0) {
        emit sgnPredictionStep(real_robot_representation_->robotDynamic()->getLatestRobotState());

        time_step_++;
        if (real_robot_representation_->getSettings().internal.moving_goal) {
            updateMovingGoal();
        }
    }
    control_loop_validate_cycle_counter_ =
        (control_loop_validate_cycle_counter_ + 1) % (settings_.prediction_step_time_multiple);

    debug_stopwatch_->tic();
    step_time_stopwatch_->tic();
    std::vector<Vector6d> fap_forces = real_robot_representation_->calculateTotalForces();
    CFAgentSettings real_robot_settings = real_robot_representation_->getSettings();
    debug_stopwatch_->toc();
    auto duration_in_us = step_time_stopwatch_->toc();

    if (duration_in_us.has_value()) {
        const float MICROSECONDS_TO_SECONDS = 1000000.0;
        emit sgnStepTime(duration_in_us.value().count() / MICROSECONDS_TO_SECONDS);
    }


    emit sgnFAPForces(fap_forces);
    emit sgnRealRobotSettings(real_robot_settings);
}

void CFPlanner::slotUpdateRealRobotRepresentationSettings(const CFAgentSettings& new_settings) {
    if (real_robot_representation_->getSettings().obstacle_limit_distance != new_settings.obstacle_limit_distance) {
        std::shared_ptr<CFGeometry> matching_geometry =
            getGeometryForObstacleLimitDistance(new_settings.obstacle_limit_distance);
        real_robot_representation_->setGeometry(matching_geometry);
    }
    real_robot_representation_->setSettings(new_settings);
}

std::shared_ptr<CFGeometry> CFPlanner::getGeometryForObstacleLimitDistance(double obstacle_limit_distance) const {
    std::shared_ptr<CFGeometry> matching_geometry;
    for (auto& geometry : geometry_collection_) {
        if (almostEqualRelative(geometry->getMaximumSearchDistance(), obstacle_limit_distance)) {
            matching_geometry = geometry;
            break;
        }
    }
    return matching_geometry;
}

void CFPlanner::updateMovingGoal() {
    MovingGoal* moving_goal = &real_robot_representation_->getSettings().internal.moving_goal.get();
    Eigen::Vector3d real_pos =
        real_robot_representation_->robotDynamic()->getLatestRobotState().getEEPose().translation();
    Eigen::Vector3d latest_goal = moving_goal->getGoalPose().translation();
    double const INTERMEDIATE_GOAL_REACHED_DISTANCE =
        real_robot_representation_->getSettings().intermediate_goal_reached_distance;
    double const INTERMEDIATE_GOAL_REACHED_DISTANCE_SQUARED =
        INTERMEDIATE_GOAL_REACHED_DISTANCE * INTERMEDIATE_GOAL_REACHED_DISTANCE;
    Eigen::Vector3d diff = latest_goal - real_pos;
    double norm = diff.squaredNorm();

    if (moving_goal->getOperationMode() == MovingGoal::MovingGoalOperationMode::TIME) {
        if (norm <= INTERMEDIATE_GOAL_REACHED_DISTANCE_SQUARED) {
            const double INCREMENT_GOAL_REACHED_MULTIPLE = 1.5;
            while (norm <= INCREMENT_GOAL_REACHED_MULTIPLE * INTERMEDIATE_GOAL_REACHED_DISTANCE_SQUARED &&
                   !moving_goal->isLatestTimeStep()) {
                moving_goal->incrementTimeStep();
                latest_goal = moving_goal->getGoalPose().translation();
                diff = latest_goal - real_pos;
                norm = diff.squaredNorm();
            }
        } else {
            moving_goal->incrementTimeStep();
        }
    } else {
        if (norm <= INTERMEDIATE_GOAL_REACHED_DISTANCE_SQUARED) {
            moving_goal->nextGoalPose();
        }
    }
}