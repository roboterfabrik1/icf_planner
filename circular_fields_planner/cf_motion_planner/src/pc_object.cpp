#include "cf_motion_planner/pc_object.h"

PCObject::PCObject(double maximum_search_distance, double step_time_s, int id, int maximum_number_considered_points)
    : id_(id),
      distance_cache_(CACHE_SIZE),
      electric_current_cache_(CACHE_SIZE),
      maximum_search_distance_(maximum_search_distance),
      step_time_s_(step_time_s),
      maximum_number_considered_points_(maximum_number_considered_points) {}

void PCObject::invalidateCachedData() {
    if (!kd_tree_is_valid_) {
        kd_tree_ = std::make_unique<pcl::KdTreeFLANN<pcl::PointXYZ>>();
        kd_tree_->setInputCloud(points_);
    }
    
    distance_cache_.clear();
    electric_current_cache_.clear();
    cache_is_valid_ = true;
    kd_tree_is_valid_ = true;
}

PCObject::Vector6d PCObject::getTwist() {
    QMutexLocker locker(&mutex_);
    return global_twist_;
};

void PCObject::setNewData(const pcl_obstacle_generator::MovingObstacle& obstacle_data,
                          uint64_t corresponding_time_step) {
    QMutexLocker locker(&mutex_);

    const bool EVERYTHING_UNINITIALIZED = (points_ == nullptr);

    data_time_step_ = corresponding_time_step;

    if (static_cast<bool>(obstacle_data.local_point_cloud_did_change) || EVERYTHING_UNINITIALIZED) {
        if (points_ == nullptr) {
            points_ = pcl::make_shared<PointCloud>();
        }
        if (normals_ == nullptr) {
            normals_ = pcl::make_shared<PointCloudNormal>();
        }

        pcl::fromROSMsg(obstacle_data.local_point_cloud, *points_);
        pcl::fromROSMsg(obstacle_data.local_normal_cloud, *normals_);
        kd_tree_is_valid_ = false;
        cache_is_valid_ = false;
    }

    if (static_cast<bool>(obstacle_data.global_transform_did_change) || EVERYTHING_UNINITIALIZED) {
        global_transform_.setIdentity();
        global_transform_.translation().x() = obstacle_data.global_transform.translation.x;
        global_transform_.translation().y() = obstacle_data.global_transform.translation.y;
        global_transform_.translation().z() = obstacle_data.global_transform.translation.z;
        Eigen::Quaterniond Qua_local_to_global(obstacle_data.global_transform.rotation.w, obstacle_data.global_transform.rotation.x,
                                             obstacle_data.global_transform.rotation.y, obstacle_data.global_transform.rotation.z);
        Eigen::Matrix3d R_local_to_global = Qua_local_to_global.toRotationMatrix();

        T_local_to_global_ << R_local_to_global(0,0), R_local_to_global(0,1), R_local_to_global(0,2), obstacle_data.global_transform.translation.x,
                              R_local_to_global(1,0), R_local_to_global(1,1), R_local_to_global(1,2), obstacle_data.global_transform.translation.y,
                              R_local_to_global(2,0), R_local_to_global(2,1), R_local_to_global(2,2), obstacle_data.global_transform.translation.z,
                              0,0,0,1;


        cache_is_valid_ = false;
    }

    if (static_cast<bool>(obstacle_data.global_twist_did_change) || EVERYTHING_UNINITIALIZED) {
        global_twist_ << obstacle_data.global_twist.linear.x, obstacle_data.global_twist.linear.y,
            obstacle_data.global_twist.linear.z, obstacle_data.global_twist.angular.x,
            obstacle_data.global_twist.angular.y, obstacle_data.global_twist.angular.z;
        cache_is_valid_ = false;
    }
}

PCObjectDistanceCacheEntry& PCObject::kdRadiusSearch(const TimedPoint& for_point) {
    Eigen::Vector3d local_point = transformTimedPointToLocalPoint(for_point);

    pcl::PointXYZ point(local_point.x(), local_point.y(), local_point.z());
    PCObjectDistanceCacheEntry cache_entry;
    cache_entry.for_point = for_point;

    // The returned point_indices and distances are sorted ascending, if kd_tree constructed with arg sorted=true
    // (default). => .front contains the minimum distance.
    int num_neighbors = kd_tree_->radiusSearch(point, maximum_search_distance_,
                                               cache_entry.squared_distance_point_indices,
                                               cache_entry.squared_distances, maximum_number_considered_points_);

    if (num_neighbors > 0) {
        cache_entry.minimal_squared_distance = static_cast<double>(cache_entry.squared_distances.front());
        int min_point_index = cache_entry.squared_distance_point_indices.front();
        cache_entry.minimal_vector = points_->at(min_point_index).getVector3fMap().cast<double>() - local_point;
    }

    distance_cache_.push_front(cache_entry);
    return distance_cache_.front();
}

Eigen::Vector3d PCObject::transformTimedPointToLocalPoint(const TimedPoint& timed_point) {
    Eigen::Vector3d translation = global_transform_.translation();

    u_int64_t delta_time_steps = 0;

    Eigen::Vector4d global_timed_point(timed_point.point.x(), timed_point.point.y(), timed_point.point.z(), 1);
    Eigen::Matrix4d T_global_to_local = T_local_to_global_.inverse();
    Eigen::Vector4d local_timed_point_T = T_global_to_local * global_timed_point;
    Eigen::Vector3d local_timed_point_3d(local_timed_point_T(0), local_timed_point_T(1), local_timed_point_T(2));

    if (timed_point.time_step && timed_point.time_step.get() >= data_time_step_) {
        delta_time_steps = timed_point.time_step.get() - data_time_step_;
    }
    for (size_t i = 0; i < 3; i++) {
        translation[i] = translation[i] + ((global_twist_[i] * step_time_s_) * static_cast<double>(delta_time_steps));
    }
    return timed_point.point - translation;
}

PCObjectDistanceCacheEntry* PCObject::findValidatedDistanceCacheEntry(const TimedPoint& for_point) {
    if (!cache_is_valid_ || !kd_tree_is_valid_) {
        invalidateCachedData();
        return nullptr;
    }
    for (PCObjectDistanceCacheEntry& cached_object : distance_cache_) {
        if (timedPointsAreAlmostEqual(for_point, cached_object.for_point)) {
            return &cached_object;
        }
    }
    return nullptr;
}

PCObjectElectricCurrentVectorCacheEntry* PCObject::findElectricCurrentCacheEntry(
    const Eigen::Vector3d& for_rotation_vector) {
    for (PCObjectElectricCurrentVectorCacheEntry& entry : electric_current_cache_) {
        if (vectorsAreAlmostEqual(for_rotation_vector, entry.for_rotation_vector)) {
            return &entry;
        }
    }
    return nullptr;
}

PCObjectElectricCurrentVectorCacheEntry& PCObject::createNewElectricCurrentCacheEntry(
    const Eigen::Vector3d& for_rotation_vector) {
    PCObjectElectricCurrentVectorCacheEntry cache_entry;
    cache_entry.for_rotation_vector = for_rotation_vector;

    electric_current_cache_.push_front(cache_entry);

    electric_current_cache_.front().electric_current_vectors.resize(points_->size(), boost::none);

    return electric_current_cache_.front();
}

boost::optional<double> PCObject::getMinimalDistance(const TimedPoint& for_point) {
    QMutexLocker locker(&mutex_);
    PCObjectDistanceCacheEntry* distance_cache_entry = findValidatedDistanceCacheEntry(for_point);
    if (distance_cache_entry == nullptr) {
        distance_cache_entry = &(kdRadiusSearch(for_point));
    }

    if (!distance_cache_entry->minimal_squared_distance) {
        return boost::none;  // If no squared distance, normal distance not calculable
    }

    if (distance_cache_entry->minimal_distance) {
        return distance_cache_entry->minimal_distance;  // cached value exists
    }

    // cached value can be calculated based on minimal_squared_distance
    distance_cache_entry->minimal_distance = std::sqrt(distance_cache_entry->minimal_squared_distance.get());
    return distance_cache_entry->minimal_distance;
}

boost::optional<double> PCObject::getMinimalSquareDistance(const TimedPoint& for_point) {
    QMutexLocker locker(&mutex_);
    PCObjectDistanceCacheEntry* distance_cache_entry = findValidatedDistanceCacheEntry(for_point);
    if (distance_cache_entry == nullptr) {
        distance_cache_entry = &(kdRadiusSearch(for_point));
    }

    return distance_cache_entry->minimal_squared_distance;
}

boost::optional<Eigen::Vector3d> PCObject::getMinimalVector(const TimedPoint& for_point) {
    QMutexLocker locker(&mutex_);
    PCObjectDistanceCacheEntry* distance_cache_entry = findValidatedDistanceCacheEntry(for_point);
    if (distance_cache_entry == nullptr) {
        distance_cache_entry = &(kdRadiusSearch(for_point));
    }

    return distance_cache_entry->minimal_vector;
}

bool PCObject::minimalDistanceToLineIsNotLesserThan(double threshold, const TimedPoint& line_start_point,
                                                    const TimedPoint& line_end_point) {
    Eigen::Vector3d line_vector = line_end_point.point - line_start_point.point;
    u_int64_t delta_time_steps = 0;
    bool has_time_component = (line_start_point.time_step && line_end_point.time_step);
    if (has_time_component) {
        delta_time_steps = line_end_point.time_step.get() - line_start_point.time_step.get();
    }

    double line_length = line_vector.norm();
    line_vector.normalize();
    const double TWO_PER_ROOT_THREE = 1.154700538;
    double line_sampling_delta = TWO_PER_ROOT_THREE * threshold;

    if (line_sampling_delta > maximum_search_distance_) {
        ROS_ERROR_STREAM("PCObject::minimalDistanceToLineIsNotLesserThan - "
                         << "line_sampling_delta is larger than maximum_search_distance_."
                         << " The function will most likely not behave as expected.");
    }

    // Preview: Check with the maximum possible sampling delta, if the object is near the line
    const double PREVIEW_FACTOR = 1.5;
    if (line_sampling_delta * PREVIEW_FACTOR < maximum_search_distance_) {
        bool object_near_line = false;
        double preview_line_sampling_delta = maximum_search_distance_;
        int required_iterations = std::lround(std::ceil(line_length / preview_line_sampling_delta));
        double time_sampling_delta = static_cast<double>(delta_time_steps) / static_cast<double>(required_iterations);

        for (size_t i = 0; i < required_iterations; i++) {
            TimedPoint latest_point;
            latest_point.point = line_start_point.point + (i * preview_line_sampling_delta) * line_vector;
            if (has_time_component) {
                latest_point.time_step = line_start_point.time_step.get() +
                                         std::lround(static_cast<double>(i) * time_sampling_delta);
            }

            boost::optional<double> distance = getMinimalSquareDistance(latest_point);
            if (distance) {
                object_near_line = true;
                break;
            }
        }
        if (!object_near_line) {
            boost::optional<double> distance = getMinimalSquareDistance(line_end_point);
            if (distance) {
                object_near_line = true;
            }
        }
        if (!object_near_line) {
            return true;
        }
    }

    // Real check
    double real_threshold = line_sampling_delta * line_sampling_delta;
    int required_iterations = std::lround(std::ceil(line_length / line_sampling_delta));
    double time_sampling_delta = static_cast<double>(delta_time_steps) / static_cast<double>(required_iterations);

    for (size_t i = 0; i < required_iterations; i++) {
        TimedPoint latest_point;
        latest_point.point = line_start_point.point + (i * line_sampling_delta) * line_vector;
        if (has_time_component) {
            latest_point.time_step = line_start_point.time_step.get() +
                                     std::lround(static_cast<double>(i) * time_sampling_delta);
        }
        boost::optional<double> distance = getMinimalSquareDistance(latest_point);

        if (distance && (distance.get() < real_threshold)) {
            return false;
        }
    }

    boost::optional<double> distance = getMinimalSquareDistance(line_end_point);
    return !(distance && (distance.get() < real_threshold));
}

std::vector<std::tuple<Eigen::Vector3d, double, Eigen::Vector3d>> PCObject::getForceCalculationData(
    const TimedPoint& for_point, const Eigen::Vector3d& for_rotation_vector, const Eigen::Vector3d& robot_velocity,
    const Eigen::Vector3d& goal_position) {
    QMutexLocker locker(&mutex_);
    PCObjectDistanceCacheEntry* distance_cache_entry = findValidatedDistanceCacheEntry(for_point);
    if (distance_cache_entry == nullptr) {
        distance_cache_entry = &(kdRadiusSearch(for_point));
    }

    PCObjectElectricCurrentVectorCacheEntry* electric_current_cache_entry = findElectricCurrentCacheEntry(
        for_rotation_vector);

    if (electric_current_cache_entry == nullptr) {
        electric_current_cache_entry = &(createNewElectricCurrentCacheEntry(for_rotation_vector));
    }

    Eigen::Vector3d goal_vector = goal_position - for_point.point;
    Eigen::Vector3d rel_trans_velocity = (robot_velocity - global_twist_.head(3)).normalized();
    bool moves_into_goal_direction = goal_vector.dot(rel_trans_velocity) > 0;

    std::vector<std::tuple<Eigen::Vector3d, double, Eigen::Vector3d>> result;
    result.reserve(distance_cache_entry->squared_distance_point_indices.size());
    int i = 0;
    Eigen::Vector3d local_point = transformTimedPointToLocalPoint(for_point);
    for (const int& point_index : distance_cache_entry->squared_distance_point_indices) {
        Eigen::Vector3d normal = normals_->at(point_index).getNormalVector3fMap().cast<double>();
        Eigen::Vector3d vector_to_point = points_->at(point_index).getVector3fMap().cast<double>() - local_point;

        // Check if the normal of the obstacle point is facing the robot and only use facing points.
        // Furthermore check if normal is facing the relevant_direction_vector (rel_vel) and only use facing points.
        // SCALAR_THRESHOLD is cos(85°). Therefore, all points with normals are considered that point against
        // relevant_direction_vector in an 190° radius.
        const double FACING_RELEVANT_DIRECTION_VECTOR_SCALAR_THRESHOLD = 0.08715574275;  // => cos(85°)
        bool faces_point = normal.dot(vector_to_point) < 0;
        bool moves_not_away_from_obstacle = normal.dot(rel_trans_velocity) <
                                            FACING_RELEVANT_DIRECTION_VECTOR_SCALAR_THRESHOLD;
        if (faces_point && (!moves_into_goal_direction || moves_not_away_from_obstacle)) {
            boost::optional<Eigen::Vector3d>* electric_current_vector = &(
                electric_current_cache_entry->electric_current_vectors.at(point_index));

            if (*electric_current_vector == boost::none) {
                *electric_current_vector = (normal.cross(for_rotation_vector));
                // @ToDo: Investigate if this should be normalized
                // *electric_current_vector = (normal.cross(for_rotation_vector)).normalized();
            }

            result.emplace_back(std::tuple<Eigen::Vector3d, double, Eigen::Vector3d>{
                electric_current_vector->get(), distance_cache_entry->squared_distances.at(i), rel_trans_velocity});
        }

        i++;
    }

    return result;
}
