#include "cf_motion_planner/cf_robot_calculations.h"

CFRobotCalculations::CFRobotCalculations() : chainKDL_() {}

void CFRobotCalculations::buildChain(const std::vector<int>& joint_types, const std::vector<double>& dh_a,
                                     const std::vector<double>& dh_alpha, const std::vector<double>& dh_d,
                                     const std::vector<double>& dh_theta) {
    chainKDL_.buildChain(joint_types, dh_a, dh_alpha, dh_d, dh_theta);
}

void CFRobotCalculations::setParams(const double& step_time, const std::vector<double>& joints_lower,
                                    const std::vector<double>& joints_upper, const std::vector<double>& joints_max_vel,
                                    const std::vector<double>& joints_max_acc) {
    q_min_ = joints_lower;
    q_max_ = joints_upper;
    qd_max_ = joints_max_vel;
    qdd_max_ = joints_max_acc;
    step_time_ = step_time;
}

void CFRobotCalculations::setGoalPose(
    geometry_msgs::PoseStamped& goal_pose_msg) {
  Eigen::Translation3d translation(goal_pose_msg.pose.position.x,
                                   goal_pose_msg.pose.position.y,
                                   goal_pose_msg.pose.position.z);
  Eigen::Quaterniond quaternion(
      goal_pose_msg.pose.orientation.w, goal_pose_msg.pose.orientation.x,
      goal_pose_msg.pose.orientation.y, goal_pose_msg.pose.orientation.z);
  goal_pose_ = translation * quaternion;
}

void CFRobotCalculations::calculatePoseFromJoints(const Vector7d& q_in, std::pair<Eigen::Affine3d, int>& pos_ee_out) {
    KDL::JntArray q_KDL;
    KDL::Frame pos_ee_KDL;
    Eigen::VectorXd q_KDL_helper = q_in.col(0);
    Eigen::Affine3d& pos_ee = pos_ee_out.first;
    int link_index = pos_ee_out.second;
    q_KDL.data = q_KDL_helper;

    chainKDL_.getJntToCartFAP(q_KDL, link_index, pos_ee_KDL);
    tf::transformKDLToEigen(pos_ee_KDL, pos_ee);
}

Eigen::MatrixXd CFRobotCalculations::calculateVelocityNullspaceProjector(
    const Eigen::MatrixXd& jacobian) const {
  Eigen::MatrixXd pinv = calculateMoorePenroseInverse(jacobian);
  return calculateVelocityNullspaceProjector(jacobian, pinv);
}

Eigen::MatrixXd CFRobotCalculations::calculateVelocityNullspaceProjector(
    const Eigen::MatrixXd& jacobian,
    const Eigen::MatrixXd& jacobian_pinv) const {
  int n = jacobian.cols();
  return (Eigen::MatrixXd::Identity(n, n) - jacobian_pinv * jacobian);
}

Eigen::MatrixXd CFRobotCalculations::calculateVelocityNullspaceProjectorFAP(
    const Eigen::MatrixXd& jacobian,
    const Eigen::MatrixXd& jacobian_pinv, 
    CFRobotState& robot_state) const {
  int n = jacobian.cols();
  int m = jacobian.rows();
  int n_robot = robot_state.joint_positions.size();
  
  if (n < n_robot) {
    Eigen::MatrixXd jacobian_full = Eigen::MatrixXd::Identity(m, n_robot); 
    Eigen::MatrixXd jacobian_pinv_full = Eigen::MatrixXd::Identity(n_robot, m); 
    jacobian_full.topLeftCorner(m,n) = jacobian;
    jacobian_pinv_full.topLeftCorner(n,m) = jacobian_pinv;
    return (Eigen::MatrixXd::Identity(n_robot, n_robot) - jacobian_pinv_full * jacobian_full);
  } else {
    return (Eigen::MatrixXd::Identity(n, n) - jacobian_pinv * jacobian);
  }
}

Eigen::MatrixXd CFRobotCalculations::calculateMoorePenroseInverse(
    const Eigen::MatrixXd& jacobian, int mode) const {
  Eigen::MatrixXd temp_jac = jacobian * jacobian.transpose();
  Eigen::MatrixXd temp_jac_inverse =
      calculateMatrixInverse(temp_jac, false, mode);

  return jacobian.transpose() * temp_jac_inverse;
}

Eigen::MatrixXd CFRobotCalculations::calculateMatrixInverse(const Eigen::MatrixXd& matrix, bool large_threshold,
                                                            int mode) const {
    Eigen::MatrixXd p_inv;
    Eigen::JacobiSVD<Eigen::MatrixXd> svd(matrix, Eigen::ComputeFullU | Eigen::ComputeFullV);

    Eigen::JacobiSVD<Eigen::MatrixXd>::SingularValuesType sing_vals = svd.singularValues();
    Eigen::MatrixXd S_inv = matrix;  // copying the dimensions of matrix, its content is not needed.
    S_inv.setZero();
    if (mode == 0) {
        p_inv = matrix.inverse();
    } else if (mode == 1) {
        double lambda_ = large_threshold ? 0.05 : 0.005;
        for (int i = 0; i < sing_vals.size(); i++) {
            if (sing_vals(i) < lambda_) {
                S_inv(i, i) = 0;
            } else {
                S_inv(i, i) = 1 / sing_vals(i);
            }
        }
        p_inv = svd.matrixV() * S_inv.transpose() * svd.matrixU().transpose();

    } else if (mode == 2) {
        double lambda_ = large_threshold ? 0.1 : 0.05;
        for (int i = 0; i < sing_vals.size(); i++) {
            S_inv(i, i) = (sing_vals(i)) / (sing_vals(i) * sing_vals(i) + lambda_ * lambda_);
        }
        p_inv = svd.matrixV() * S_inv.transpose() * svd.matrixU().transpose();

    } else if (mode == 3) {
        double lambda_ = large_threshold ? 0.05 : 0.005;
        double sigma_crit = sing_vals.maxCoeff() * lambda_;
        for (int i = 0; i < sing_vals.size(); i++) {
            // Singularity avoidance from Zsolt KEMÉNY
            if (sing_vals(i) < sigma_crit) {
                sing_vals(i) = (std::pow(sing_vals(i), 2) / (4 * sigma_crit)) + sigma_crit;
            }
            S_inv(i, i) = 1 / sing_vals(i);
            // }
        }
        p_inv = svd.matrixV() * S_inv.transpose() * svd.matrixU().transpose();
    }

    for (int i = 0; i < p_inv.rows(); i++) {
        for (size_t j = 0; j < p_inv.cols(); j++) {
            if (std::isnan(p_inv(i, j))) {
                p_inv(i, j) = 0;
                ROS_WARN_STREAM("Replacing NaN value in pseudo inverse with 0");
            }
        }
    }

    return p_inv;
}

Vector7d CFRobotCalculations::calculateJointLimitAvoidance(const Vector7d& joint_pos, const Vector7d& joint_vel,
                                                                 const CFAgentSettings& agent_settings) {
    double k_jla = agent_settings.k_jla;
    Vector7d tau_joint_avoidance = Vector7d::Zero();

    // Method 1
    Vector7d joint_limits_shifted;
    std::vector<double> joint_pos_shifted(7);
    // map joint positions to be symmetric in a range from -1 to 1
    for (int i = 0; i < joint_pos_shifted.size(); i++) {
      joint_pos_shifted.at(i) = -1 + (2 / (q_max_.at(i) - q_min_.at(i))) *
                                         (joint_pos(i, 0) - q_min_.at(i));

      if (joint_pos_shifted.at(i) < 0) {
        tau_joint_avoidance(i) =
            k_jla *
            (0.5 + 0.5 * std::tanh(8.0*(0.5 - std::sqrt(1 -std::abs(joint_pos_shifted.at(i))))) 
            + 1*(0.5 + 0.5 * std::tanh(30.0*(0.2 - std::sqrt(1 -std::abs(joint_pos_shifted.at(i)))))));
      } else {
        tau_joint_avoidance(i) =
            -k_jla *
            (0.5 + 0.5 * std::tanh(8.0*(0.5 - std::sqrt(1 -std::abs(joint_pos_shifted.at(i)))))
            + 1*(0.5 + 0.5 * std::tanh(30.0*(0.2 - std::sqrt(1 -std::abs(joint_pos_shifted.at(i)))))));
      }
    }

    // // Method 2
    // double joint_dist_lim = 0.25;
    // for (size_t i = 0; i < joint_pos.size(); i++) {
    //   double upper_dist = q_max_.at(i) - joint_pos(i);
    //   double lower_dist = joint_pos(i) - q_min_.at(i);
    //   if (upper_dist < joint_dist_lim) {
    //     tau_joint_avoidance(i) = -agent_settings.k_jla *
    //                              (1 / upper_dist - 1 / joint_dist_lim) /
    //                              (upper_dist * upper_dist);
    //   } else if (lower_dist < joint_dist_lim) {
    //     tau_joint_avoidance(i) = agent_settings.k_jla *
    //                              (1 / lower_dist - 1 / joint_dist_lim) /
    //                              (lower_dist * lower_dist);
    //   } else {
    //     tau_joint_avoidance(i) = 0.0;
    //   }
    // }

    // // Method 3
    // double activation_threshold = 0.85;
    // for (size_t i = 0; i < joint_pos.size(); i++) {
    //   double joint_center = (q_max_.at(i) + q_min_.at(i)) / 2;
    //   // double scaling = 2 * std::abs(joint_center - joint_pos(i)) /
    //   double scaling =
    //       2 * (joint_center - joint_pos(i)) / (q_max_.at(i) - q_min_.at(i));
    //   if (std::abs(scaling) <= (1 - activation_threshold)) {
    //     tau_joint_avoidance(i) = 0;
    //     continue;
    //   } else if (scaling > (1 - activation_threshold)) {
    //     scaling -= (1 - activation_threshold);
    //   } else if (scaling < (activation_threshold - 1)) {
    //     scaling += (1 - activation_threshold);
    //   }
    //   tau_joint_avoidance(i) =
    //       agent_settings.k_jla / activation_threshold * scaling;
    // }

    return tau_joint_avoidance;
}

Vector7d CFRobotCalculations::calculateManipulabilityOptimization(const CFRobotState& robot_state) {
    Vector7d joints = robot_state.joint_positions;
    Vector7d gradient = Vector7d::Zero();
    double eps = 0.001;

    for (int i = 0; i < joints.size(); i++) {
        Vector7d temp_joints_upper = joints;
        Vector7d temp_joints_lower = joints;
        temp_joints_lower(i) -= eps;
        temp_joints_upper(i) += eps;

        KDL::JntArray q(7);
        KDL::Jacobian jac_ee(7);

        for (size_t j = 0; j < joints.size(); j++) {
          q(j) = temp_joints_lower(j);
        }
        chainKDL_.getJacobianFAP(q, -1, jac_ee);
        Matrix6x7 jacobian_lower = jac_ee.data;

        for (size_t j = 0; j < joints.size(); j++) {
          q(j) = temp_joints_upper(j);
        }
        chainKDL_.getJacobianFAP(q, -1, jac_ee);
        Matrix6x7 jacobian_upper = jac_ee.data;

        double m_lower = sqrt((jacobian_lower * jacobian_lower.transpose()).determinant());
        double m_upper = sqrt((jacobian_upper * jacobian_upper.transpose()).determinant());

        gradient(i) = (m_upper - m_lower) / (2 * eps);
    }
    return gradient;
}

void CFRobotCalculations::buildFAPJacobians(CFRobotState& robot_state, const CFAgentSettings& agent_settings) {
    KDL::JntArray q(7);
    KDL::Jacobian jac(7);
    std::vector<Matrix6x7> jacobians;

    for (size_t i_joint = 0; i_joint < robot_state.joint_positions.size(); i_joint++) {
        q(i_joint) = robot_state.joint_positions(i_joint, 0);
    }

    for (auto frame_id : agent_settings.fap_frame_ids) {
        chainKDL_.getJacobianFAP(q, frame_id, jac);
        jacobians.emplace_back(jac.data);
    }

    robot_state.fap_jacobians = jacobians;
}

Eigen::Quaterniond CFRobotCalculations::deltaRotation(const Vector3d& ang_vel,
                                                      const double delta_t,
                                                      bool approx) {
  Vector3d half_angle = ang_vel * delta_t * 0.5;
  if (approx) {
    return Eigen::Quaterniond(1.0, half_angle.x(), half_angle.y(),
                              half_angle.z());
  }
  double l = half_angle.norm();  // magnitude
  if (l > 0) {
    half_angle *= std::sin(l) / l;
  }
  return Eigen::Quaterniond(cos(l), half_angle.x(), half_angle.y(),
                            half_angle.z());
}

Eigen::Vector3d CFRobotCalculations::calculateOrientationGoalDistance(
    const Eigen::Affine3d& current_pose, const Eigen::Affine3d& goal_pose,
    const CFAgentSettings& agent_settings) {
  // Yuan, 1988
  Eigen::Quaterniond q_p(current_pose.rotation());
  Eigen::Quaterniond q_g(goal_pose.rotation());
  Eigen::Vector3d rotation_result =
      q_p.w() * q_g.vec() - q_g.w() * q_p.vec() - q_p.vec().cross(q_g.vec());

  rotation_result =
      current_pose.rotation().inverse() *
      rotation_result;  // Transform quaternion difference into world_CS


  // Caccavale et al., 1998
  // Eigen::Matrix3d rotation_mat =
  //     current_pose.rotation().transpose() * goal_pose.rotation();
  // Eigen::Quaterniond q_eeg(rotation_mat);
  // q_eeg.normalize();
  // Eigen::Vector3d rotation_result =
  //     current_pose.rotation().transpose() * q_eeg.vec();

  return rotation_result;
}

void CFRobotCalculations::calculateJointVelocityCmd(
    const std::vector<Vector6d>& wrenches, CFRobotState& robot_state,
    const CFAgentSettings& agent_settings) {
  Vector7d joint_vel_last_time_step = robot_state.joint_velocities;

  // EE Acceleration
  Eigen::Matrix<double, 7, 6> jac_ee_mp_inv =
      calculateMoorePenroseInverse(robot_state.getEEJac());
  Vector7d joint_acc_ee = jac_ee_mp_inv*wrenches.back();

  // Body Acceleration
  Vector7d joint_acc_body = Vector7d::Zero();
  double max_norm = 0.0;
  int max_norm_index = 0;
  for (int i = 0; i < robot_state.fap_jacobians.size() - 1; i++) {
    joint_acc_body +=
        robot_state.fap_jacobians.at(i).transpose() * wrenches.at(i);
    if (wrenches.at(i).norm() > max_norm) {
        max_norm = wrenches.at(i).norm();
        max_norm_index = i;
    }
  }

  Vector7d joint_acc_total = joint_acc_ee + joint_acc_body;

  for (size_t i = 0; i < robot_state.joint_positions.size(); i++) {
    joint_acc_total(i) = boost::algorithm::clamp(
        joint_acc_total(i), -qdd_max_.at(i), qdd_max_.at(i));
  }

  // Joint limit avoidance
  Vector7d joint_acc_limit = Vector7d::Zero();
  joint_acc_limit = calculateJointLimitAvoidance(
      robot_state.joint_positions, robot_state.joint_velocities,
      agent_settings);

  // Manipulability velocity vector
  Vector7d joint_vel_manip = Vector7d::Zero();
  if (agent_settings.manipulability_gradient) {     
    joint_vel_manip +=
        agent_settings.k_man * calculateManipulabilityOptimization(robot_state);
  }

  // Damping joint motions
  Vector7d joint_vel_damp =
      -agent_settings.d_null * robot_state.joint_velocities;

  Vector7d joint_vel_to_null = joint_vel_manip + joint_vel_damp + joint_acc_limit;

  // FAP translational position Jacobian
  Eigen::Matrix<double, 3, 7> jac_fap_trans = robot_state.fap_jacobians.at(max_norm_index).topRows(3);
  // EE translational position Jacobian
  Eigen::Matrix<double, 3, 7> jac_ee_trans = robot_state.getEEJac().topRows(3);

  // Joined safety jacobian NS projector
  Eigen::Matrix<double, 6, 7>  jac_safety;
  jac_safety << jac_ee_trans, jac_fap_trans;
  Eigen::MatrixXd jac_safety_pinv =
            calculateMoorePenroseInverse(jac_safety);
  Eigen::Matrix<double, 7, 7> ns_projector_safety =
        calculateVelocityNullspaceProjectorFAP(jac_safety, jac_safety_pinv, robot_state);

  // EE Pose NS Projector
  Eigen::Matrix<double, 7, 7> ns_projector_ee =
        calculateVelocityNullspaceProjector(robot_state.getEEJac(), jac_ee_mp_inv);

  if (agent_settings.safety_joint_vel_calculation_method) {
    robot_state.joint_velocities_null = ns_projector_safety*joint_vel_to_null;
  } else {
    robot_state.joint_velocities_null = ns_projector_ee*joint_vel_to_null;
  }

  for (size_t i = 0; i < robot_state.joint_velocities_null.size(); i++) {
    robot_state.joint_velocities_null(i) = boost::algorithm::clamp(
        robot_state.joint_velocities_null(i), -qd_max_.at(i), qd_max_.at(i));
  }

  robot_state.joint_velocities = joint_vel_last_time_step +
                                 joint_acc_total * step_time_ +
                                 robot_state.joint_velocities_null;
  
  for (size_t i = 0; i < robot_state.joint_velocities.size(); i++) {
    robot_state.joint_velocities(i) = boost::algorithm::clamp(
        robot_state.joint_velocities(i), -qd_max_.at(i), qd_max_.at(i));
  }

  double manipulability = (robot_state.getEEJac() * 
                            robot_state.getEEJac().transpose()).determinant();
  if (manipulability < robot_state.score_min_manipulability) {
    robot_state.score_min_manipulability = manipulability;
  }
  if (agent_settings.internal.id == 0) {
  }
  
  robot_state.score_joint_torques_limit +=
      joint_acc_limit.cwiseAbs().maxCoeff();   
  robot_state.score_joint_torques_total +=
      robot_state.joint_velocities.cwiseAbs().sum();
}

void CFRobotCalculations::calculateFAPVelocities(CFRobotState& robot_state) const {
    for (int i = 0; i < robot_state.fap_jacobians.size(); i++) {
        robot_state.fap_velocities.at(i) = robot_state.fap_jacobians.at(i) * robot_state.joint_velocities;
    }
}

void CFRobotCalculations::calculateCartesianPositions(std::vector<Eigen::Affine3d>& cart_pos, const Vector7d& joint_pos,
                                                      const CFAgentSettings& agent_settings) {
    int i = 0;
    for (auto frame_id : agent_settings.fap_frame_ids) {
        auto pair = std::make_pair(cart_pos.at(i), frame_id);
        calculatePoseFromJoints(joint_pos, pair);
        cart_pos.at(i) = pair.first;
        i++;
    }
}

void CFRobotCalculations::calculateTotalPositions(CFRobotState& robot_state, const CFAgentSettings& agent_settings) {
    Vector7d joint_vels = robot_state.joint_velocities;

    robot_state.joint_positions += joint_vels * step_time_;

    for (size_t i = 0; i < robot_state.joint_positions.size(); i++) {
        robot_state.joint_positions(i) = boost::algorithm::clamp(robot_state.joint_positions(i), q_min_.at(i),
                                                                 q_max_.at(i));
    }

    calculateCartesianPositions(robot_state.fap_poses, robot_state.joint_positions, agent_settings);
}

void CFRobotCalculations::calculateDynamicStep(
    const std::vector<Vector6d>& wrenches, CFRobotState& robot_state,
    const CFAgentSettings& agent_settings) {
  buildFAPJacobians(robot_state, agent_settings);

  calculateJointVelocityCmd(wrenches, robot_state, agent_settings);
  calculateFAPVelocities(robot_state);
  calculateTotalPositions(robot_state, agent_settings);
}
