#include "cf_motion_planner/abstract_robot_dynamic.h"

AbstractRobotDynamic::AbstractRobotDynamic() : history_(HISTORY_BUFFER_SIZE) {
    latest_robot_state_.joint_positions.setZero();
    latest_robot_state_.joint_velocities.setZero();
    latest_robot_state_.joint_velocities_null.setZero();
    latest_robot_state_.score_joint_torques_limit = 0.0;
    latest_robot_state_.score_min_manipulability = 1.0;
    latest_robot_state_.score_joint_torques_total = 0.0;
}

void AbstractRobotDynamic::makeHistorySnapshot() {
    history_.incrementTimeStep();
    history_.addRobotState(latest_robot_state_);
}
void AbstractRobotDynamic::setRobotState(const CFRobotState& robot_state) {
    latest_robot_state_ = robot_state;
    history_.incrementTimeStep();
    history_.addRobotState(robot_state);
}

void AbstractRobotDynamic::setHistory(const AbstractRobotDynamicHistory& history) {
    history_ = history;
    if (!history.getRobotStates().empty()) {
        latest_robot_state_ = history_.getRobotStates().front();
    }
};
