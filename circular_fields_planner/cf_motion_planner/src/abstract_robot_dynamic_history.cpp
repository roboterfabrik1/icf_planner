#include "cf_motion_planner/abstract_robot_dynamic_history.h"

AbstractRobotDynamicHistory::AbstractRobotDynamicHistory()
    : time_steps_(DEFAULT_HISTORY_BUFFER_SIZE),
      wrenches_(DEFAULT_HISTORY_BUFFER_SIZE),
      robot_state_(DEFAULT_HISTORY_BUFFER_SIZE) {}

AbstractRobotDynamicHistory::AbstractRobotDynamicHistory(int history_buffer_size)
    : time_steps_(history_buffer_size), wrenches_(history_buffer_size), robot_state_(history_buffer_size) {}

void AbstractRobotDynamicHistory::incrementTimeStep(uint64_t start_step) {
    if (time_steps_.empty()) {
        time_steps_.push_front(start_step);
        if (!duration_reference_time_step_) {
            duration_reference_time_step_ = start_step;
        }
        if (!start_time_step_) {
            start_time_step_ = start_step;
        }
    } else {
        time_steps_.push_front(time_steps_.front() + 1);
    }
}

void AbstractRobotDynamicHistory::setDurationReferenceTimeStep(uint64_t duration_reference_time_step) {
    duration_reference_time_step_ = duration_reference_time_step;
}
void AbstractRobotDynamicHistory::addRobotState(const CFRobotState& robot_state) {
    robot_state_.push_front(robot_state);
}

void AbstractRobotDynamicHistory::addWrench(const std::vector<Vector6d>& latest_wrench) {
    wrenches_.push_front(latest_wrench);
}

void AbstractRobotDynamicHistory::checkAddMinimalObstacleDistance(double obstacle_distance) {
    if (minimal_obstacle_distance_) {
        if (minimal_obstacle_distance_.get() <= obstacle_distance) {
            return;
        }
    }
    minimal_obstacle_distance_ = obstacle_distance;
}

boost::optional<uint64_t> AbstractRobotDynamicHistory::getLatestTimeStep() const {
    if (time_steps_.empty()) {
        return boost::none;
    }
    return time_steps_.front();
}

void AbstractRobotDynamicHistory::clear() {
    time_steps_.clear();
    wrenches_.clear();
    robot_state_.clear();
    minimal_obstacle_distance_ = boost::none;
    duration_reference_time_step_ = boost::none;
    start_time_step_ = boost::none;
}

bool AbstractRobotDynamicHistory::hasSimilarTrajectoryPoint(uint64_t time_step, const Eigen::Vector3d& point,
                                                            double delta) const {
    int time_size = time_steps_.size();
    if (time_size == 0) {
        return false;
    }

    uint64_t time_front = time_steps_.front();
    uint64_t time_back = time_steps_.back();

    if (time_front < time_step || time_back > time_step) {
        return false;
    }

    if (time_front - time_back != time_size - 1) {
        ROS_WARN_STREAM("AbstractRobotDynamicHistory::hasSimilarTrajectoryPoint"
                        << "- Given history seems to be out of order. Function will possibly return wrong results.");
    }

    int index = time_front - time_step;
    try {
        if (time_steps_.at(index) == time_step) {
            Eigen::Vector3d history_point = robot_state_.at(index).getEEPose().translation();
            Eigen::Vector3d diff = history_point - point;
            return diff.norm() <= delta;
        }
        ROS_WARN_STREAM("AbstractRobotDynamicHistory::hasSimilarTrajectoryPoint - "
                        << "Index is not time step.");
    } catch (const std::out_of_range& e) {
        ROS_ERROR_STREAM("AbstractRobotDynamicHistory::hasSimilarTrajectoryPoint - " << e.what());
    }

    return false;
}

uint64_t AbstractRobotDynamicHistory::getTrackedDuration() const {
    if (!time_steps_.empty() && duration_reference_time_step_) {
        return time_steps_.front() - duration_reference_time_step_.get();
    }
    return 0;
}