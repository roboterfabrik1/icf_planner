#include "cf_motion_planner/dynamic_settings.h"

bool AbstractRobotDynamicSettings::operator==(const AbstractRobotDynamicSettings& rhs) const {
    if (double_blob_.size() != rhs.double_blob_.size() || bool_blob_.size() != rhs.bool_blob_.size() ||
        int_blob_.size() != rhs.int_blob_.size() || string_blob_.size() != rhs.string_blob_.size()) {
        return false;
    }

    for (size_t i = 0; i < double_blob_.size(); i++) {
        if (!almostEqualRelative(double_blob_.at(i), rhs.double_blob_.at(i))) {
            return false;
        }
    }

    return bool_blob_ == rhs.bool_blob_ && int_blob_ == rhs.int_blob_ && string_blob_ == rhs.string_blob_;
}

//=======================================
// PointMass
//=======================================

PointMassRobotDynamicSettings::PointMassRobotDynamicSettings() {
    const int DOUBLE_BLOB_SIZE = 3;
    double_blob_.resize(DOUBLE_BLOB_SIZE);
    settings_type_ = "PointMassRobotDynamicSettings";
}

PointMassRobotDynamicSettings PointMassRobotDynamicSettings::generateFromRosParameterEntry(
    const XmlRpc::XmlRpcValue& ros_parameter_entry) {
    PointMassRobotDynamicSettings result;
    const int REQUIRED_PARAMETERS = 2;
    if (ros_parameter_entry.size() < REQUIRED_PARAMETERS) {
        ROS_WARN_STREAM("PointMassRobotDynamicSettings::generateFromRosParameterEntry"
                        << " - Less parameters fetched from ros parameter server than expected. "
                        << "Please check your settings. The planner will probably misbehave.");
    }

    for (const auto& element : ros_parameter_entry) {
        std::string key = element.first;
        if (key == "mass_kg") {
            result.setMass(static_cast<double>(element.second));
        } else if (key == "max_velocity_ms") {
            result.setMaxVelocity(static_cast<double>(element.second));
        } else {
            ROS_WARN_STREAM("PointMassRobotDynamicSettings::generateFromRosParameterEntry - key '" << key
                                                                                                   << "' is unknown.");
        }
    }
    return result;
}

bool PointMassRobotDynamicSettings::isType(AbstractRobotDynamicSettings* settings) {
    return settings->getSettingsType() == "PointMassRobotDynamicSettings";
}

//=======================================
// Simple Robot
//=======================================

SimpleRobotDynamicSettings::SimpleRobotDynamicSettings() {
    const int DOUBLE_BLOB_SIZE = 3;
    double_blob_.resize(DOUBLE_BLOB_SIZE);
    settings_type_ = "SimpleRobotDynamicSettings";
}

SimpleRobotDynamicSettings SimpleRobotDynamicSettings::generateFromRosParameterEntry(
    const XmlRpc::XmlRpcValue& ros_parameter_entry) {
    SimpleRobotDynamicSettings result;

    for (const auto& element : ros_parameter_entry) {
        std::string key = element.first;
        std::vector<double> vals;

        if (key == "q_max") {
            for (int i = 0; i < element.second.size(); i++) {
                result.joint_limits_qmax_.push_back(static_cast<double>(element.second[i]));
            }
        } else if (key == "q_min") {
            for (int i = 0; i < element.second.size(); i++) {
                result.joint_limits_qmin_.push_back(static_cast<double>(element.second[i]));
            }
        } else if (key == "dq_max") {
            for (int i = 0; i < element.second.size(); i++) {
                result.joint_limits_qd_.push_back(static_cast<double>(element.second[i]));
            }
        } else if (key == "ddq_max") {
            for (int i = 0; i < element.second.size(); i++) {
                result.joint_limits_qdd_.push_back(static_cast<double>(element.second[i]));
            }
        } else if (key == "joint_types") {
            for (int i = 0; i < element.second.size(); i++) {
                result.joint_types_.push_back(static_cast<int>(element.second[i]));
            }
        } else if (key == "dh_a") {
            for (int i = 0; i < element.second.size(); i++) {
                result.DH_params_a_.push_back(static_cast<double>(element.second[i]));
            }
        } else if (key == "dh_alpha") {
            for (int i = 0; i < element.second.size(); i++) {
                result.DH_params_alpha_.push_back(static_cast<double>(element.second[i]));
            }
        } else if (key == "dh_d") {
            for (int i = 0; i < element.second.size(); i++) {
                result.DH_params_d_.push_back(static_cast<double>(element.second[i]));
            }
        } else if (key == "dh_theta") {
            for (int i = 0; i < element.second.size(); i++) {
                result.DH_params_theta_.push_back(static_cast<double>(element.second[i]));
            }
        } else if (key == "k_man") {
            result.setGainsKMAN(static_cast<double>(element.second));
        } else if (key == "k_jla") {
            result.setGainsKJLA(static_cast<double>(element.second));
        } else if (key == "d_jla") {
            result.setGainsDJLA(static_cast<double>(element.second));
        } else if (key == "d_null") {
            result.setGainsDNULL(static_cast<double>(element.second));
        } else if (key == "scale_dynamic") {
            result.scale_dynamic_ = static_cast<bool>(element.second);
        } else if (key == "manipulability_gradient") {
            result.manipulability_gradient_ = static_cast<bool>(element.second);
        } else if (key == "nullspace_projection") {
            result.nullspace_projection_ = static_cast<bool>(element.second);
        } else if (key == "sim_only") {
            // Do nothing. Key is only used in preplanner.
        } else {
            ROS_WARN_STREAM("SimpleRobotDynamicSettings::generateFromRosParameterEntry - key '" << key
                                                                                                << "' is unknown.");
        }
    }
    return result;
}

bool SimpleRobotDynamicSettings::isType(AbstractRobotDynamicSettings* settings) {
    return settings->getSettingsType() == "SimpleRobotDynamicSettings";
}