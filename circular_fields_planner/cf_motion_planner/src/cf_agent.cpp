#include "cf_motion_planner/cf_agent.h"

CFAgent::CFAgent(const CFAgentSettings& settings, std::shared_ptr<CFGeometry>& geometry,
                 const std::shared_ptr<AbstractRobotDynamicSettings>& robot_dynamic_settings, int id,
                 bool is_best_agent)
    : settings_(settings), geometry_(geometry), is_best_agent_(is_best_agent) {
    settings_.internal.id = id;
    try {
        if (PointMassRobotDynamicSettings::isType(robot_dynamic_settings.get())) {
            std::shared_ptr<PointMassRobotDynamicSettings> point_mass_settings =
                std::dynamic_pointer_cast<PointMassRobotDynamicSettings>(robot_dynamic_settings);
            robot_dynamic_ = std::unique_ptr<AbstractRobotDynamic>(
                std::make_unique<PointMassRobotDynamic>(*point_mass_settings));
        } else if (SimpleRobotDynamicSettings::isType(robot_dynamic_settings.get())) {
            std::shared_ptr<SimpleRobotDynamicSettings> robot_settings =
                std::dynamic_pointer_cast<SimpleRobotDynamicSettings>(robot_dynamic_settings);
            robot_dynamic_ = std::unique_ptr<AbstractRobotDynamic>(
                std::make_unique<SimpleRobotDynamic>(*robot_settings));
        }
    } catch (const std::exception& e) {
        ROS_ERROR_STREAM("CFAgent::CFAgent - Could not infer robot_dynamic_ from robot_dynamic_settings."
                         << " Original error: " << e.what());
    }
}

void CFAgent::setRotationVectors(const std::vector<std::map<int, Eigen::Vector3d>>& rotation_vectors) {
    settings_.internal.rotation_vectors = rotation_vectors;
};

CFAgent::Vector6d CFAgent::calculateVLCForce() const {
    Vector6d vlc_force;

    // Calculate translational
    Eigen::Affine3d ee_pose = robot_dynamic_->getLatestRobotState().getEEPose();

    bool is_moving_goal;
    Eigen::Affine3d goal = getLatestGoalPose(&is_moving_goal);
    const double K_G_THROUGH_K_V = (settings_.k_g / settings_.k_d);  // k_g = Position Gain, k_v = velocity gain,

    // Translation (taking moving goal into account, scaling velocity with global value to avoid stop at intermediate)
    Eigen::Vector3d artificial_translation_goal_distance = CFGeometry::calculateTranslationGoalDistance(ee_pose, goal);
    Eigen::Vector3d artificial_desired_translation_velocity = K_G_THROUGH_K_V * artificial_translation_goal_distance;
    double artificial_desired_translation_velocity_norm = artificial_desired_translation_velocity.norm();
    if (is_moving_goal) {
        Eigen::Vector3d global_artificial_translation_goal_distance = CFGeometry::calculateTranslationGoalDistance(
            ee_pose, settings_.internal.goal_pose);
        Eigen::Vector3d global_artificial_desired_translation_velocity = K_G_THROUGH_K_V *
                                                                         global_artificial_translation_goal_distance;
        double global_norm = global_artificial_desired_translation_velocity.norm();
        artificial_desired_translation_velocity = artificial_desired_translation_velocity *
                                                  (global_norm / artificial_desired_translation_velocity_norm);
        artificial_desired_translation_velocity_norm = global_norm;
    }
    double scale_lim = std::min(1.0,
                                settings_.translational_velocity_limit / artificial_desired_translation_velocity_norm);

    // Rotation (Ignoring moving goal, only global goal)
    Eigen::Vector3d artificial_rotation_goal_distance =
    CFGeometry::calculateRotationGoalDistance(
        ee_pose, settings_.internal.goal_pose);
    Eigen::Vector3d artificial_desired_rotation_velocity = K_G_THROUGH_K_V *
    artificial_rotation_goal_distance; double scale_lim_rot = std::min(1.0,
                                    settings_.rotational_velocity_limit /
                                    artificial_desired_rotation_velocity.norm());

    // Combination
    Vector6d artificial_desired_velocity;
    artificial_desired_velocity.head(3) << scale_lim * artificial_desired_translation_velocity;
    artificial_desired_velocity.tail(3) << scale_lim_rot *
    artificial_desired_rotation_velocity;

    vlc_force = -settings_.k_d * (robot_dynamic_->getLatestRobotState().getEEVel() - artificial_desired_velocity);
    return vlc_force;
}


Vector3d CFAgent::calculateSelfCollisionAvoidanceForce() const{
    Eigen::Affine3d ee_pose = robot_dynamic_->getLatestRobotState().getEEPose();

    Vector3d collision_sphere;
    collision_sphere << 0.0, 0.0, 0.25;
    double radius = 0.15;
    Vector3d distance_vector = ee_pose.translation() - collision_sphere;
    double distance = std::max(distance_vector.norm() - radius, 1e-4);
    double force_gain_pot_rep =
                  0.5 + 0.5 * tanh(20.0*(0.05 - distance));
              distance_vector.normalize();
    Vector3d self_collision_force = settings_.k_pot_rep *
                                 distance_vector *
                                 force_gain_pot_rep;
    return self_collision_force;
}


double CFAgent::calculateAtakaGoalRelaxationFactors(double minimal_obstacle_distance) const {
    Eigen::Affine3d ee_pose = robot_dynamic_->getLatestRobotState().getEEPose();
    PCObject* critical_object_tmp;

    // Since there is a critical object, returned values can not be boost::none and are safe to dereference without
    // check. Also the alternate values in Ataka for minimal_obstacle_distance > settings_.obstacle_limit_distance
    // are not used, because if an object is found, the opposite is always true.
    double w1;
    const double ALPHA = 0.3;
    w1 = 1 - std::exp(-(minimal_obstacle_distance - (0.3 * settings_.obstacle_limit_distance)) / (ALPHA));
    Eigen::Affine3d goal = getLatestGoalPose();
    auto ee_position = static_cast<TimedPoint>(robot_dynamic_->getLatestRobotState().getEEPose().translation());
    // The last critical_object_tmp is the ee, because the last fap_pose is always the ee
    PCObject* critical_object_ee = critical_object_tmp;
    double w2;
    if (critical_object_ee != nullptr) {
        Eigen::Vector3d obstacle_minimal_vector = critical_object_ee->getMinimalVector(ee_position).get();
        Eigen::Vector3d robot_goal_vector = goal.translation() - ee_position.point;
        double goal_distance = robot_goal_vector.norm();

        if (goal_distance > 0.0) {
            w2 = 1 - (robot_goal_vector.dot(obstacle_minimal_vector) / (goal_distance * minimal_obstacle_distance));
        }
    } else {
        w2 = 1;
    }

    return w1 * w2;
}

double CFAgent::calculateNonConvexGoalRelaxationFactor(const Eigen::Vector3d& translation_vlc_force,
                                                    const Eigen::Vector3d& circular_fields_force) const {
    const double ZERO_EPSILON = 0.0000005;
    if (almostEqualRelative(circular_fields_force.squaredNorm(), 0.0, ZERO_EPSILON)) {
        return 1.0;
    }

    Vector6d latest_velocity = robot_dynamic_->getLatestRobotState().getEEVel();
    Eigen::Affine3d latest_pose = robot_dynamic_->getLatestRobotState().getEEPose();
    bool is_moving_goal;
    Eigen::Affine3d goal = getLatestGoalPose(&is_moving_goal);
    Eigen::Vector3d latest_translation_velocity = latest_velocity.head(3);

    double angle_factor = latest_translation_velocity.dot(translation_vlc_force);
    double goal_distance = CFGeometry::calculateTranslationGoalDistance(latest_pose, goal).norm();
    const double MIN_TRANSLATIONAL_VELOCITY = 0.1 * settings_.translational_velocity_limit;
    const double MIN_DISTANCE_GOAL = 0.1;

    if (angle_factor <= 0.9 && latest_translation_velocity.norm() < MIN_TRANSLATIONAL_VELOCITY &&
        goal_distance > MIN_DISTANCE_GOAL) {
        return 0.0;
    }
    return 1.0;
}

std::vector<CFAgent::Vector6d> CFAgent::calculateTotalForces() {
    std::vector<Eigen::Affine3d> fap_poses = robot_dynamic_->getLatestRobotState().fap_poses;
    std::vector<Vector6d> fap_velocities = robot_dynamic_->getLatestRobotState().fap_velocities;
    std::vector<Vector6d> fap_forces;

    double minimal_obstacle_distance = settings_.obstacle_limit_distance;
    int j_fap = 0;
    int min_fap_index;
    for (const auto& fap_position : fap_poses) {
        auto position = static_cast<TimedPoint>(fap_position.translation());

        auto fap_obstacle_distance = geometry_->getCriticalObjectMinimalDistance(position); 
        if (fap_obstacle_distance.has_value()) {
            if (fap_obstacle_distance.value() < minimal_obstacle_distance) {
              minimal_obstacle_distance = fap_obstacle_distance.value();
              min_fap_index = j_fap;
            }
        }
        j_fap++;
    }

    robot_dynamic_->getHistory().checkAddMinimalObstacleDistance(minimal_obstacle_distance);
    
    if (minimal_obstacle_distance < settings_.obstacle_limit_distance_repulsive &&
    min_fap_index < fap_poses.size()-1) {
      settings_.close_control_point = true;
    } else {
      settings_.close_control_point = false;
    }

    if (minimal_obstacle_distance < settings_.obstacle_limit_distance_safety &&
    min_fap_index < fap_poses.size()-1) {
      settings_.safety_joint_vel_calculation_method = true;
    } else {
      settings_.safety_joint_vel_calculation_method = false;
    }

    for (size_t i_fap = 0; i_fap < fap_poses.size(); i_fap++) {
        auto position = static_cast<TimedPoint>(fap_poses.at(i_fap).translation());
        if (!is_best_agent_) {
            position.time_step = robot_dynamic_->getHistory().getLatestTimeStep();
        }
        Eigen::Vector3d trans_velocity = fap_velocities.at(i_fap).head(3);

        Vector6d resulting_force = Vector6d::Zero();
        Eigen::Vector3d circular_field_force = Eigen::Vector3d::Zero();
        Eigen::Vector3d repulsive_force = Eigen::Vector3d::Zero();

        std::vector<std::tuple<Eigen::Vector3d, double, Eigen::Vector3d>> electric_currents_square_distances =
            geometry_->getForceCalculationData(position, settings_.internal.rotation_vectors.at(i_fap), trans_velocity,
                                               getLatestGoalPose().translation());

        auto minimal_vector = geometry_->getCriticalObjectMinimalVector(position);
        Eigen::Vector3d object_vec= Eigen::Vector3d::Zero();
        if(minimal_vector.has_value()){
            object_vec = minimal_vector.value(); 
        }
        
        for (const auto& entry : electric_currents_square_distances) {
            Eigen::Vector3d electric_current_vector;
            double squared_distance_to_point;
            Eigen::Vector3d rel_trans_velocity;
            std::tie(electric_current_vector, squared_distance_to_point, rel_trans_velocity) = entry;

            double distance_to_point = std::sqrt(squared_distance_to_point);
            double scaling_distance_to_point =
                std::max(distance_to_point, 1e-4);

	          if (settings_.close_control_point || settings_.invalid_best_agent) {
              double force_gain_pot_rep =
                  0.5 + 0.5 * tanh(18.0*(0.22 - distance_to_point));
              Eigen::Vector3d force_rep_khatib_direction(-object_vec);
              force_rep_khatib_direction.normalize();
              repulsive_force += settings_.k_pot_rep *
                                 force_rep_khatib_direction *
                                 force_gain_pot_rep;
            } else {
              double force_gain_cf_rep =
                  0.5 + 0.5 * tanh(25.0*(0.15 - distance_to_point));
              Eigen::Vector3d dist_cross_rel_vel_normalized =
                  ((-1*object_vec).cross(rel_trans_velocity)).normalized();
              Eigen::Vector3d force_rep_ataka_direction =
                  rel_trans_velocity.cross(
                      dist_cross_rel_vel_normalized);

              repulsive_force += settings_.k_cf_rep *
                                 force_rep_ataka_direction.normalized() *
                                 force_gain_cf_rep;
            }

            double force_gain_cf =
                  0.5 + 0.5 * tanh(18.0*(0.22 - distance_to_point));
                  double dist_gain_cf =
                  0.5 + 0.5 * tanh(60.0*(0.05 - distance_to_point));
            Eigen::Vector3d triple_product = rel_trans_velocity.cross(
                electric_current_vector.cross(rel_trans_velocity));
            circular_field_force += triple_product * (force_gain_cf + dist_gain_cf / scaling_distance_to_point); 
        }

        if (!electric_currents_square_distances.empty()) {
          circular_field_force /= electric_currents_square_distances.size();
          repulsive_force /= electric_currents_square_distances.size();
        }

        // Add attractive force for EE only
        if (i_fap == fap_poses.size() - 1) {
          resulting_force.head(3) += settings_.k_cf_ee * circular_field_force;
          Vector6d vlc_force = calculateVLCForce();
          double w13 = calculateAtakaGoalRelaxationFactors(minimal_obstacle_distance);
          double w4 = calculateNonConvexGoalRelaxationFactor(
              vlc_force.head(3), resulting_force.head(3));
          double w134 = std::max((w13 * w4), 0.0);
          vlc_force.head(3) = w134 * vlc_force.head(3);
          resulting_force += vlc_force;
          resulting_force.head(3) += calculateSelfCollisionAvoidanceForce();
        } else {
          resulting_force.head(3) = settings_.k_cf_body * circular_field_force;
        }
        resulting_force.head(3) += repulsive_force;
        fap_forces.push_back(resulting_force);
    }
    return fap_forces;
}

void CFAgent::calculateStep() {
    if (settings_.internal.moving_goal) {
        updateMovingGoal();
    }

    std::vector<Vector6d> wrenches = calculateTotalForces();
    robot_dynamic_->calculateStep(wrenches, settings_);
}

CFAgent::PredictionExitStatus CFAgent::predict(int no_of_steps) {
    predict_ = true;
    int step_counter = 0;
    while (step_counter < no_of_steps || no_of_steps < 0) {
        if (!predict_) {
            return PredictionExitStatus::ABORTED;
        }
        double estimated_score = calculatePredictionScore(true);
        const double GOAL_REACHED_MIN_SCORE = 100000.0;
        const double COLLISION_MAX_SCORE = 0.2;
        if (estimated_score > GOAL_REACHED_MIN_SCORE) {
            if (!settings_.internal.moving_goal) {
                emit sgnEvolve(settings_);
            }
            double score = calculatePredictionScore(false);
            emit sgnBet(score, settings_, robot_dynamic_->getHistory());
            emit sgnSmooth(settings_, robot_dynamic_->getHistory(), score);
            // VIZ: This is only needed for debug purpose and should be deactivated if not needed, since it is quite
            // heavy
            emit sgnPredictionResult(settings_.internal.id, score, robot_dynamic_->getHistory());
            return PredictionExitStatus::GOAL_REACHED;
        }
        if (estimated_score < COLLISION_MAX_SCORE) {
          return PredictionExitStatus::COLLIDED;
        }
        Eigen::Affine3d last_ee_pose = robot_dynamic_->getLatestRobotState().getEEPose();
        calculateStep();
        Eigen::Affine3d current_ee_pose = robot_dynamic_->getLatestRobotState().getEEPose();
        auto ee_pose_difference = last_ee_pose.translation() - current_ee_pose.translation();
        // If predicted agent moves too fast (e.g. due to discretization error) delete it
        // @ToDo: This should be: settings_.translational_velocity_limit * settings_.prediction_step_time_multiple *
        // settings_.control_loop_cycle_duration
        if (ee_pose_difference.norm() > 0.2) {
            return PredictionExitStatus::COLLIDED;
        }
        step_counter++;
    }

    double score = calculatePredictionScore(false);
    emit sgnBet(score, settings_, robot_dynamic_->getHistory());
    // VIZ: This is only needed for debug purpose and should be deactivated if not needed, since it is quite heavy
    emit sgnPredictionResult(settings_.internal.id, score, robot_dynamic_->getHistory());
    return PredictionExitStatus::MAX_PREDICTION_STEPS_REACHED;
}

CFAgent::PredictionExitStatus CFAgent::calculateLineOfSightShortcuts() {
    predict_ = true;
    int shortcuts_counter = 0;
    const int MAX_LOS_SHORTCUTS = settings_.maximum_line_of_sight_shortcuts;
    const int MAX_BINARY_SEARCH_ITERATIONS = settings_.maximum_line_of_sight_binary_search_iterations;
    boost::circular_buffer<Eigen::Affine3d> shortcut_goals(MAX_LOS_SHORTCUTS + 1);
    int index = settings_.internal.moving_goal->getPoses().size() - 1;
    while (shortcuts_counter < MAX_LOS_SHORTCUTS) {
        if (!predict_) {
            return PredictionExitStatus::ABORTED;
        }
        Eigen::Affine3d shortcut_goal;
        try {
            std::tie(shortcut_goal, index) = lineOfSightBinarySearch(settings_.internal.moving_goal->getPoses(),
                                                                     settings_.internal.moving_goal->getTimeSteps(),
                                                                     index, MAX_BINARY_SEARCH_ITERATIONS);
        } catch (const std::invalid_argument& e) {
            ROS_WARN_STREAM("CFAgent::calculateLineOfSightShortcuts() - " << e.what() << " Aborting.");
            return PredictionExitStatus::ABORTED;
        }

        if (index == 0) {
            break;
        }
        shortcut_goals.push_front(shortcut_goal);
        shortcuts_counter++;
    }

    shortcut_goals.push_front(settings_.internal.goal_pose);
    MovingGoal shortcut_moving_goal;
    shortcut_moving_goal.setOperationMode(MovingGoal::MovingGoalOperationMode::POSITION);
    shortcut_moving_goal.setPoses(shortcut_goals);
    shortcut_moving_goal.nextGoalPose();
    settings_.internal.moving_goal = shortcut_moving_goal;
    return PredictionExitStatus::PRECALCULATION_FINISHED;
}

std::pair<Eigen::Affine3d, int> CFAgent::lineOfSightBinarySearch(const boost::circular_buffer<Eigen::Affine3d>& poses,
                                                                 const boost::circular_buffer<uint64_t>& time_steps,
                                                                 int start_index, int max_search_iterations) const {
    int size = poses.size();
    if (size < 2 || start_index >= size || start_index < 0) {
        throw std::invalid_argument("Given start_index is out of bounds or poses is smaller than 2 elements.");
    }

    int end_index = 0;
    int nearest_index = start_index - 1;
    int farthest_index = end_index;
    bool valid_shortcut;
    int best_index = nearest_index;
    Eigen::Affine3d best_pose = poses.at(best_index);  // Fallback if nothing is valid.

    TimedPoint start_position;
    TimedPoint end_position;
    double threshold;

    int iteration_counter = 0;

    while (iteration_counter < max_search_iterations) {
        std::tie(threshold, start_position, end_position) = calculateThresholdStartEndPoses(poses, time_steps,
                                                                                            start_index, end_index);
        valid_shortcut = geometry_->minimalDistanceToLineIsNotLesserThan(threshold, start_position, end_position);
        if (valid_shortcut) {
            best_index = end_index;
            if (best_index == 0) {
                break;
            }
            nearest_index = end_index - 1;
            int interval = nearest_index - farthest_index;
            if (interval < 0) {
                break;
            }
            int new_end_index = farthest_index + std::lround(std::floor(interval / 2));
            end_index = new_end_index;
        } else {
            farthest_index = end_index + 1;
            int interval = nearest_index - farthest_index;
            if (interval < 0) {
                break;
            }
            int new_end_index = nearest_index - std::lround(std::floor(interval / 2));
            end_index = new_end_index;
        }
        iteration_counter++;
    }

    best_pose = poses.at(best_index);

    return {best_pose, best_index};
}

std::tuple<double, TimedPoint, TimedPoint> CFAgent::calculateThresholdStartEndPoses(
    const boost::circular_buffer<Eigen::Affine3d>& poses, const boost::circular_buffer<uint64_t>& time_steps,
    int start_index, int end_index) const {
    Eigen::Affine3d start_pose = poses.at(start_index);
    TimedPoint start_position;
    start_position.point = start_pose.translation();
    Eigen::Affine3d end_pose = poses.at(end_index);
    TimedPoint end_position;
    end_position.point = end_pose.translation();

    if (!time_steps.empty()) {
        start_position.time_step = time_steps.at(start_index);
        end_position.time_step = time_steps.at(end_index);
    }

    double threshold = settings_.obstacle_limit_distance;
    const double THRESHOLD_SHORTCUT_WEIGHT = settings_.shortcut_under_obstacle_influence_weight;

    auto distance_start = geometry_->getCriticalObjectMinimalDistance(start_position);
    if (distance_start.has_value() && distance_start.value() < threshold) {
            threshold = distance_start.value();
    }

    auto distance_end = geometry_->getCriticalObjectMinimalDistance(end_position);
    if (distance_end.has_value() && distance_end.value() < threshold) {
            threshold = distance_end.value();
    }

    threshold = THRESHOLD_SHORTCUT_WEIGHT * threshold;

    const double TWO_PER_ROOT_THREE = 1.154700538;
    if (threshold > settings_.obstacle_limit_distance / TWO_PER_ROOT_THREE) {
        threshold = settings_.obstacle_limit_distance / TWO_PER_ROOT_THREE;
        ROS_ERROR_STREAM("Fallback for calculateThresholdStartEndPoses is used.");
    }

    if (threshold < settings_.collision_distance) {
        threshold = settings_.collision_distance;
    }

    return {threshold, start_position, end_position};
}

double CFAgent::calculatePredictionScore(bool extreme_estimates_only) const {
    CFRobotState latest_robot_state = robot_dynamic_->getLatestRobotState();

    double result = 0.0;

    // 1 Collision
    boost::optional<double> minimal_obstacle_distance = robot_dynamic_->getHistory().getMinimalObstacleDistance();
    if (minimal_obstacle_distance && minimal_obstacle_distance.get() < settings_.collision_distance) {
        return 0.0;
    }

    // 2 Goal reached
    Eigen::Affine3d ee_pose = latest_robot_state.getEEPose();
    Vector6d artificial_goal_distance = CFGeometry::calculateGoalDistance(ee_pose, settings_.internal.goal_pose);
    const double GOAL_REACHED_LIMIT = 0.1; // rough estimate should be sufficient for prediction
    double goal_distance = artificial_goal_distance.head(3).norm();
    bool goal_reached = goal_distance < GOAL_REACHED_LIMIT;
    if (goal_reached) {
        const double GOAL_REACHED_BONUS_SCORE = 100000.0;
        result += GOAL_REACHED_BONUS_SCORE;
    }

    if (extreme_estimates_only) {
        const double EXTREME_ESTIMATES_DEFAULT_SCORE = 1.0;
        return result + EXTREME_ESTIMATES_DEFAULT_SCORE;
    }

    // 3 Goal distance
    if (!goal_reached) {
        const double GOAL_DISTANCE_BASE_SCORE = 10000.0;
        double goal_distance_score = GOAL_DISTANCE_BASE_SCORE *
                                     std::exp(-((goal_distance-GOAL_REACHED_LIMIT) / settings_.goal_influence_limit_distance));
        result += goal_distance_score;
    }

    // 4 Duration
    const double DURATION_BASE_SCORE = 5000.0;
    double duration_score = DURATION_BASE_SCORE -
                            static_cast<double>(robot_dynamic_->getHistory().getTrackedDuration())*10;
    duration_score = std::max(0.0, duration_score);
    result += duration_score;

    double joint_velocities_total = 0.0;
    double score_joint_torques_limit = 0.0;
    double min_manipulability = 0.0;

    joint_velocities_total = latest_robot_state.score_joint_torques_total;
    score_joint_torques_limit = latest_robot_state.score_joint_torques_limit;
    min_manipulability = latest_robot_state.score_min_manipulability;

    // 5 Joint Limit Avoidance Score
    const double JOINT_LIMIT_BASE_SCORE = 500.0;
    double joint_limit_score = JOINT_LIMIT_BASE_SCORE -
                               JOINT_LIMIT_BASE_SCORE * 25 * (score_joint_torques_limit / joint_velocities_total);
    joint_limit_score = std::max(0.0, joint_limit_score);
    result += joint_limit_score;

    // // 6 Manipulability
    const double MANIPULABILITY_BASE_SCORE = 0.0;
    double manipulability_score = MANIPULABILITY_BASE_SCORE * 100*(min_manipulability);
    manipulability_score = std::min(0.0, manipulability_score);
    result += manipulability_score;

    // 7
    const double MINIMAL_OBSTACLE_DISTANCE_BASE_SCORE = 200.0;
    if (minimal_obstacle_distance) {
        double minimal_obstacle_distance_score = MINIMAL_OBSTACLE_DISTANCE_BASE_SCORE *
                                                 (1 - (std::exp(settings_.collision_distance -
                                                                minimal_obstacle_distance.get())));
        result += minimal_obstacle_distance_score;
    } else {
        result += MINIMAL_OBSTACLE_DISTANCE_BASE_SCORE;
    }

    return result;
}

Eigen::Vector3d CFAgent::calculateNewRotationVectorEndEffector(Eigen::Vector3d robot_vel) {
    robot_vel.normalize();
    Eigen::Vector3d rotation_vector;

    int min_index = 0;
    double min = 2;
    for (size_t i = 0; i < 3; i++) {
        double value = abs(robot_vel[i]);
        if (value < min) {
            min_index = i;
            min = value;
        }
    }

    if (min_index == 0) {
        return robot_vel.cross(Eigen::Vector3d(0.0, -robot_vel.z(), robot_vel.y()));
    }
    if (min_index == 1) {
        return robot_vel.cross(Eigen::Vector3d(robot_vel.z(), 0, -robot_vel.x()));
    }
    if (min_index == 2) {
        return robot_vel.cross(Eigen::Vector3d(-robot_vel.y(), robot_vel.x(), 0.0));
    }

    ROS_ERROR_STREAM("CFAgent::calculateNewRotationVectorEndEffector - Something went terribly wrong.");
    return {};
}

Eigen::Vector3d CFAgent::calculateNewRotationVectorBody(Eigen::Vector3d fap_vel,
                                                        Eigen::Vector3d object_distance_vector) const {
    Eigen::Vector3d vel_normalized = fap_vel.normalized();
    Eigen::Vector3d current_vector = vel_normalized -
                                     ((vel_normalized.transpose() * object_distance_vector) * object_distance_vector) /
                                         (object_distance_vector.norm() * object_distance_vector.norm());

    Eigen::Vector3d rotation_vector = current_vector.cross(object_distance_vector);
    return rotation_vector.normalized();
}

void CFAgent::updateMovingGoal() {
    MovingGoal* moving_goal = &settings_.internal.moving_goal.get();
    Eigen::Vector3d real_pos = robot_dynamic_->getLatestRobotState().getEEPose().translation();
    Eigen::Vector3d latest_goal = moving_goal->getGoalPose().translation();
    double const INTERMEDIATE_GOAL_REACHED_DISTANCE = settings_.intermediate_goal_reached_distance;
    double const INTERMEDIATE_GOAL_REACHED_DISTANCE_SQUARED = INTERMEDIATE_GOAL_REACHED_DISTANCE *
                                                              INTERMEDIATE_GOAL_REACHED_DISTANCE;
    Eigen::Vector3d diff = latest_goal - real_pos;
    double norm = diff.squaredNorm();

    if (moving_goal->getOperationMode() == MovingGoal::MovingGoalOperationMode::TIME) {
        if (norm <= INTERMEDIATE_GOAL_REACHED_DISTANCE_SQUARED) {
            const double INCREMENT_GOAL_REACHED_MULTIPLE = 1.5;
            while (norm <= INCREMENT_GOAL_REACHED_MULTIPLE * INTERMEDIATE_GOAL_REACHED_DISTANCE_SQUARED &&
                   !moving_goal->isLatestTimeStep()) {
                moving_goal->incrementTimeStep();
                latest_goal = moving_goal->getGoalPose().translation();
                diff = latest_goal - real_pos;
                norm = diff.squaredNorm();
            }
        } else {
            moving_goal->incrementTimeStep();
        }
    } else {
        if (norm <= INTERMEDIATE_GOAL_REACHED_DISTANCE_SQUARED) {
            moving_goal->nextGoalPose();
        }
    }
}

const Eigen::Affine3d& CFAgent::getLatestGoalPose(bool* is_moving_goal) const {
    if (settings_.internal.moving_goal && !settings_.internal.moving_goal->isLatestTimeStep()) {
        const Eigen::Affine3d& intermediate_goal_pose = settings_.internal.moving_goal->getGoalPose();
        const double REACHED_THRESHOLD = 0.4;
        bool near_global_goal = false;

        if (REACHED_THRESHOLD > settings_.intermediate_goal_reached_distance) {
            double squared_norm =
                (intermediate_goal_pose.translation() - settings_.internal.goal_pose.translation()).squaredNorm();
            const double SQUARED_THRESHOLD = REACHED_THRESHOLD * REACHED_THRESHOLD;
            near_global_goal = squared_norm < SQUARED_THRESHOLD;
        }

        if (!near_global_goal) {
            if (is_moving_goal != nullptr) {
                *is_moving_goal = true;
            }
            return intermediate_goal_pose;
        }
    }
    if (is_moving_goal != nullptr) {
        *is_moving_goal = false;
    }
    return settings_.internal.goal_pose;
}
