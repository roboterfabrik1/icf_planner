#include <ros/ros.h>
#include <signal.h>

#include <QCoreApplication>
#include <QThread>
#include <QThreadPool>
#include <thread>

#include "cf_motion_planner/cf_planner.h"
#include "cf_motion_planner/cf_planner_ros_interface.h"
#include "cf_motion_planner/cf_types.h"

void signalhandler(int sig) { QCoreApplication::instance()->quit(); }

int main(int argc, char** argv) {
    qRegisterMetaType<AbstractRobotDynamicHistory>();
    qRegisterMetaType<CFRobotState>();
    qRegisterMetaType<CFAgentSettings>();
    qRegisterMetaType<pcl::PointCloud<pcl::PointXYZINormal>::Ptr>();
    qRegisterMetaType<Eigen::Affine3d>();
    qRegisterMetaType<RotationVectorMapVector>();
    qRegisterMetaType<Vector6d>();
    qRegisterMetaType<Vector7d>();
    qRegisterMetaType<pcl_obstacle_generator::MovingObstacles::Ptr>();

    ros::init(argc, argv, "cf_planner");

    QCoreApplication a(argc, argv);
    CFPlannerROSInterface ros_interface;

    CFPlannerSettings planner_settings;
    CFAgentSettings agent_settings;
    std::shared_ptr<AbstractRobotDynamicSettings> dynamic_settings(nullptr);

    ros_interface.loadSettings(planner_settings, agent_settings, dynamic_settings);

    if (!dynamic_settings) {
        ROS_ERROR_STREAM("main() - Dynamic Settings not found."
                         << "Unable to substitute default values");
        throw std::runtime_error("Dynamic Settings not found.");
    }

    CFPlanner planner(&ros_interface, planner_settings, agent_settings, dynamic_settings);

    // For Agent manager thread & system
    QThreadPool::globalInstance()->setMaxThreadCount(QThread::idealThreadCount() - 2);

    signal(SIGQUIT, signalhandler);
    signal(SIGINT, signalhandler);
    signal(SIGTERM, signalhandler);
    signal(SIGHUP, signalhandler);

    return a.exec();
}