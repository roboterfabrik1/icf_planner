# circular_fields_planner

This package contains the circular fields motion planner with predictive multi-agents (CFP).
The planner subscribes to the following topics:

- cf_robot_state: state of the robot
- pcl_obstacle_objects: point cloud obstacles from pcl_obstacle_generator
- cf_goal_pose: target pose of the robot
- rotation_vector_guesses: avoidance directions from the pre-planner

The planner then calculates the virtual circular field forces for guiding the robot to the goal. The planner publishes:

- cf_virtual_fap_forces: forces acting on the robot
- cf_real_robot_settings: settings for the robot control

## Requirements

ROS Noetic (Melodic), circular_fields_planner, point_mass_sim and pcl_obstacle_generator, cf_pre_planner