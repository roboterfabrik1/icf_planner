#ifndef SPHERE_FIBONACCI_GRID_H
#define SPHERE_FIBONACCI_GRID_H

#include <cmath>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>

using namespace std;

void r8mat_transpose_print_sphere(int m, int n, double a[], string title);
void r8mat_transpose_print_some_sphere(int m, int n, double a[], int ilo, int jlo, int ihi, int jhi, string title);
void r8mat_write(string output_filename, int m, int n, double table[]);
void sphere_fibonacci_grid_display(int ng, double xg[], string prefix);
double *sphere_fibonacci_grid_points(double radius, int ng);
void timestamp_sphere();

#endif