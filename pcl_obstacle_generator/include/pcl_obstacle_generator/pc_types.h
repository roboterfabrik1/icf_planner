#include <pcl/point_types.h>
#include <pcl_ros/point_cloud.h>

#ifndef PC_TYPES_H
#define PC_TYPES_H

typedef pcl::PointCloud<pcl::PointXYZI> PointCloud;
typedef pcl::PointCloud<pcl::Normal> PointCloudNormal;
typedef pcl::PointCloud<pcl::PointXYZINormal> PointCloudObject;

#endif