#include <string>
#include <vector>

#include "pcl_obstacle_generator/pc_object.h"
#include "pcl_obstacle_generator/sphere_fibonacci_grid.h"

#ifndef PC_SPHERE_H
#define PC_SPHERE_H

/// @brief A point cloud sphere
class PCSphere : public PCObject {
    /// @brief Sphere radius in m
    double radius_;

    /// @brief Calculate the local points for this object.
    void calculateLocalPoints() override;

public:
    /// @brief Full constructor
    ///
    /// @param parameters Parameter data structure for this object. Please see param descition in struct for details.
    /// @param radius Sphere radius in m
    PCSphere(const PCObjectParameters &parameters, double radius);
};

#endif