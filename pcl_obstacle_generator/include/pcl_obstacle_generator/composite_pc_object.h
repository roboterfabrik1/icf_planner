#include <pcl/common/transforms.h>
#include <pcl/features/normal_3d.h>
#include <pcl_conversions/pcl_conversions.h>

#include <random>
#include <string>
#include <vector>

#include "pc_types.h"
#include "pcl_obstacle_generator/MovingObstacle.h"
#include "pcl_obstacle_generator/abstract_pc_object.h"

#ifndef COMPOSITE_PC_OBJECT_H
#define COMPOSITE_PC_OBJECT_H

/// @brief Parameter structure of settings for an CompositePCObjectParameters
struct CompositePCObjectParameters {
    /// @brief Name of the object
    std::string name = "";

    /// @brief Unique id over all PCObjects
    int object_id = -1;

    /// @brief x,y,z position in m
    std::vector<double> position = {0, 0, 0};

    /// @brief x,y,z latest velocity vector in m/s
    std::vector<double> velocity = {0, 0, 0};

    /// @brief x,y,z maximum velocity vector in m/s
    std::vector<double> max_velocity = {0, 0, 0};

    /// @brief Time step at which the acceleration to max_velocity is started.
    uint64_t acceleration_start_step = 0;

    /// @brief Time step at which max_velocity is reachead.
    uint64_t acceleration_end_step = 0;

    /// @brief Time step at which the deceleration from max_velocity is started.
    uint64_t deceleration_start_step = 0;

    /// @brief Time step at which the object stops.
    uint64_t deceleration_end_step = 0;
};

/// @brief Container class for point cloud Objects that are a composite of other point cloud objects
class CompositePCObject : public AbstractPCObject {
protected:
    /// @brief Object parameters
    CompositePCObjectParameters parameters_;  // NOLINT, because protected is fine here

    /// @brief Time step of the object simulation
    ///
    /// Incremented each time updatePosition is called.
    uint64_t time_step_ = 0;

    /// @brief Objects, of which this container object consists of
    std::vector<std::shared_ptr<AbstractPCObject>> sub_objects_;

    /// @name Calculation results
    /// @{

    /// @brief Latest calculated points
    PointCloud::Ptr global_points_ = nullptr;  // NOLINT, because protected is fine here

    /// @brief Latest calculated normals
    PointCloudNormal::Ptr global_normals_ = nullptr;  // NOLINT, because protected is fine here

    /// @brief Flag, if the data in points_ and normals_ is still valid or needs an update.
    bool global_cache_is_valid_ = false;  // NOLINT, because protected is fine here

    /// @brief Latest calculated points in the local object CS without any rotation or translation
    PointCloud::Ptr local_points_ = nullptr;  // NOLINT, because protected is fine here

    /// @brief Latest calculated normals in the local object CS without any rotation or translation
    PointCloudNormal::Ptr local_normals_ = nullptr;  // NOLINT, because protected is fine here

    /// @brief Flag, if the data in points_ and normals_ is still valid or needs an update.
    bool local_cache_is_valid_ = false;  // NOLINT, because protected is fine here

    /// @brief Flag if the local point cloud did change in comparison to the last call of calculateMovingObstacleMsg()
    bool local_moving_obstacle_point_cloud_did_change_ = true;

    /// @brief Flag if the velocity did change in compariso to the last call of calculateMovingObstacleMsg()
    bool moving_obstacle_twist_did_change_ = true;

    /// @brief Flag if the position did change in comparison to the last call of calculateMovingObstacleMsg()
    bool global_moving_obstacle_position_did_change_ = true;

    /// @}

    /// @brief Calculate the object points and normals in the local object CS without any rotation or translation
    ///
    /// This functions calculates the data for local_points_ and local_normals_
    ///
    /// It simply fetches the data from the sub_objects_ and combines them into one point cloud.
    void calculateLocalPointsAndNormals();

    /// @brief Calculate the object points and normals in the global object CS
    ///
    /// This functions calculates the data for global_points_ and global_normals_
    /// Rotation of objects is not supported for the moment.
    void calculateGlobalPointsAndNormals();

    /// @brief Calculate the latest velocity based on the ac-/deceleration time steps and time_step_
    ///
    /// This utility function is part of updatePosition().
    void updateVelocity();

public:
    /// @brief Constructor
    ///
    /// @param parameters Parameter data structure for this object. Please see param descition in struct for details.
    explicit CompositePCObject(const CompositePCObjectParameters &parameters);

    /// @brief Append a object as a subobject of this composite
    void addSubObject(std::shared_ptr<AbstractPCObject> newObject);

    /// @brief Calculate the latest points and normals and let the pointers point to the result
    ///
    /// This function will use cached data if possible. Therefore
    /// it is very cheap to call, if no changes appeared.
    ///
    /// However, if changes happen, the calculation cost may be heavy.
    ///
    /// The parameters are output parameters only. Make sure they do not override important data.
    ///
    /// @param points Shared Pointer to the resulting point data
    /// @param normals Shared Pointer to the resulting normals data
    void calculatePointsandNormals(PointCloud::Ptr &points, PointCloudNormal::Ptr &normals);

    /// @brief Calculate an obstacle message based on the latest local points, normals and parameters
    ///
    /// This function will use cached data if possible. Therefore
    /// it is very cheap to call, if no changes appeared.
    ///
    /// However, if changes happen, the calculation cost may be heavy.
    ///
    /// The parameter is output parameters only. Make sure they do not override important data.
    ///
    /// @param obstacle_msg Obstacle msg to modify with the object data.
    void calculateMovingObstacleMsg(pcl_obstacle_generator::MovingObstacle &obstacle_msg);

    /// @brief Simulate one step with given velocity vector and a given delta t.
    ///
    /// A call to this function invalidates the global_cache, if an update occurred.
    ///
    /// @param step_time_s Delta t between the last step and this step in seconds
    void updatePosition(double step_time_s);
};

#endif