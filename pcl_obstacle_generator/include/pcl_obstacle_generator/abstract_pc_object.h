#include <pcl/common/transforms.h>
#include <pcl/features/normal_3d.h>
#include <pcl_conversions/pcl_conversions.h>

#include <random>
#include <string>
#include <vector>

#include "pc_types.h"
#include "pcl_obstacle_generator/MovingObstacle.h"

#ifndef ABSTRACT_PC_OBJECT_H
#define ABSTRACT_PC_OBJECT_H

/// @brief Base class for point cloud Objects
class AbstractPCObject {
public:
    /// @brief Calculate the latest points and normals and let the pointers point to the result
    ///
    /// This function will use cached data if possible. Therefore
    /// it is very cheap to call, if no changes appeared.
    ///
    /// However, if changes happen, the calculation cost may be heavy.
    ///
    /// The parameters are output parameters only. Make sure they do not override important data.
    ///
    /// @param points Shared Pointer to the resulting point data
    /// @param normals Shared Pointer to the resulting normals data
    virtual void calculatePointsandNormals(PointCloud::Ptr &points, PointCloudNormal::Ptr &normals) = 0;

    /// @brief Calculate an obstacle message based on the latest local points, normals and parameters
    ///
    /// This function will use cached data if possible. Therefore
    /// it is very cheap to call, if no changes appeared.
    ///
    /// However, if changes happen, the calculation cost may be heavy.
    ///
    /// The parameter is output parameters only. Make sure they do not override important data.
    ///
    /// @param obstacle_msg Obstacle msg to modify with the object data.
    virtual void calculateMovingObstacleMsg(pcl_obstacle_generator::MovingObstacle &obstacle_msg) = 0;

    /// @brief Simulate one step with given velocity vector and a given delta t.
    ///
    ///
    /// A call to this function invalidates the global_cache, if an update occurred.
    ///
    /// @param step_time_s Delta t between the last step and this step in seconds
    virtual void updatePosition(double step_time_s) = 0;
};

#endif