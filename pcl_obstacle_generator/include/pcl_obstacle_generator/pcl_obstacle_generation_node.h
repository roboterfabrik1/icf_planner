#include <ros/ros.h>

#include "pcl_obstacle_generator/ExperimentPipelineObstaclePoint.h"
#include "pcl_obstacle_generator/MovingObstacle.h"
#include "pcl_obstacle_generator/MovingObstacles.h"
#include "pcl_obstacle_generator/abstract_pc_object.h"
#include "pcl_obstacle_generator/composite_pc_object.h"
#include "pcl_obstacle_generator/pc_cuboid.h"
#include "pcl_obstacle_generator/pc_object.h"
#include "pcl_obstacle_generator/pc_sphere.h"
#include "sensor_msgs/PointCloud.h"

#ifndef PCL_OBSTACLE_GENERATION_NODE_H
#define PCL_OBSTACLE_GENERATION_NODE_H

/// @brief Class that constructs a ros node around the PclObstacleGenerator
class PCLObstacleGenerationNode {
    /// @name Ros Utility
    /// Properties for ros interaction
    /// @{
    /// @brief ROS node handle
    ros::NodeHandle node_handle_;

    /// @brief Queue size for all publishers
    const int PUBLISHER_QUEUE_SIZE_ = 1;

    /// @brief Point cloud points publisher
    ///
    /// This publisher publishes the points with intensity as id to the topic raw_pcl_obstacles.
    ros::Publisher points_publisher_;

    /// @brief Point cloud normals publisher
    ///
    /// This publisher publishes the normals to the topic pcl_normals.
    ros::Publisher normal_publisher_;

    /// @brief Publisher of moving obstacles
    ///
    /// This publisher publishes the custom moving obstacles message to pcl_obstacle_objects.
    ros::Publisher obstacles_publisher_;

    /// @brief Publisher, that publishes the point cloud after n seconds once.
    ///
    /// The experiment pipeline can than subscribe to the topic in the rosbag without inflating the rosbag size.
    ros::Publisher experiment_pipeline_points_publisher_;

    /// @}

    /// @brief Step time in seconds for one simulation step. Influences the frequency inside the run() loop.
    const double STEP_TIME_S_ = 1.0 / 10.0;

    /// @brief Point Cloud Objects, to use for the simulation.
    std::vector<std::shared_ptr<AbstractPCObject>> pc_objects_;

    /// @brief Load parameters for and create Point Cloud Objects
    ///
    /// This function querys the ros paramter server for parameters under a given namespace.
    /// For every subname in this namespace, it will create an object with this name.
    /// Every subname needs to provide all parameters for the specific object type (see #PCObject and its subclasses).
    ///
    /// If an object description is invalid, a ROS_ERROR Mesage is generated and construction for this object aborted.
    ///
    /// The created objects are saved to object_container(pc_objects_)
    ///
    /// @example Example parameter structure for a sphere and rectangle:
    /// /PCObjects/Test_Cuboid/dimensions
    /// /PCObjects/Test_Cuboid/discretization
    /// /PCObjects/Test_Cuboid/id
    /// /PCObjects/Test_Cuboid/normals_level
    /// /PCObjects/Test_Cuboid/position
    /// /PCObjects/Test_Cuboid/type
    /// /PCObjects/Test_Cuboid/velocity
    /// /PCObjects/Test_Sphere/discretization
    /// /PCObjects/Test_Sphere/id
    /// /PCObjects/Test_Sphere/normals_level
    /// /PCObjects/Test_Sphere/position
    /// /PCObjects/Test_Sphere/radius
    /// /PCObjects/Test_Sphere/type
    /// /PCObjects/Test_Sphere/velocity
    ///
    /// @param root_path Root path to load obstacles from. Trailing '/' expected.
    /// For the first level usually /PCObjects/
    /// @param object_container Container to append created objects to. For the first level usually pc_objects_.
    /// @param is_nested Is this the first parsing level or a nested one. Nested objects ignore certain parameters.
    void loadPCObjects(const std::string& root_path, std::vector<std::shared_ptr<AbstractPCObject>>& object_container,
                       bool is_nested = false);

    /// @brief Parse and create a composite object
    ///
    /// @param object_root_path Path to the parent root of the object with trailing '/' e.g. /PCObjects/
    /// @param object_name Name of the object e.g. Test_Cuboi. The overall parameters are then read from
    /// object_root_path + object_name + "/" e.g. /PCObjects/Test_Sphere/
    std::shared_ptr<AbstractPCObject> parseCompositePCObject(const std::string& object_root_path,
                                                             const std::string& object_name);

    /// @brief Parse and create a normal pc object
    ///
    /// @param object_root_path Path to the parent root of the object with trailing '/' e.g. /PCObjects/
    /// @param object_name Name of the object e.g. Test_Cuboi. The overall parameters are then read from
    /// object_root_path + object_name + "/" e.g. /PCObjects/Test_Sphere/
    /// @param is_nested Is this object part of a Composite object. If so, velocity and id will be ignored.
    std::shared_ptr<AbstractPCObject> parsePCObject(const std::string& object_root_path, const std::string& object_name,
                                                    bool is_nested);

    /// @brief Publish points as single ExperimentPipelineObstaclePoint messages to be used in the ExperimentPipeline
    ///
    /// The ExperimentPipeline uses rosbags to record its data during runtime. If the points are published as
    /// single points instead of a pcl messages, they can be easily read by pandas.
    ///
    /// This function does not have a purpose for the planner itself. It is for debug / evaluation only.
    ///
    /// @param points shared pointer to point cloud to publish data from.
    void publishExperimentPipelineObstaclePoints(PointCloud::Ptr points);

public:
    PCLObstacleGenerationNode();

    /// @brief Run the node
    void run();
};

#endif