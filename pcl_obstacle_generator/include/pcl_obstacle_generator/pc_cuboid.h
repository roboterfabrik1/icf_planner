#include <string>
#include <vector>

#include "pcl_obstacle_generator/pc_object.h"
#include "pcl_obstacle_generator/square_grid.h"

#ifndef PC_CUBOID_H
#define PC_CUBOID_H

/// @brief A point cloud cuboid
class PCCuboid : public PCObject {
    /// @brief x,y,z dimensions in m
    std::vector<double> dimensions_;

    /// @brief Calculate the local points for this object.
    void calculateLocalPoints() override;

public:
    /// @brief Full constructor
    /// @param parameters Parameter data structure for this object. Please see param descition in struct for details.
    /// @param dimensions x,y,z dimensions in m
    PCCuboid(const PCObjectParameters &parameters, const std::vector<double> &dimensions);
};

#endif