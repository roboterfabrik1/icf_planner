# pcl_obstacle_generator

The node in this package generates and publishes points of a point cloud object and an estimation of each points normal.
The objects, which shall be generated, can be specified through yaml parameter files.
Please see example.yaml for a example specification of a sphere and a cube and example.launch for a simple launch file.
