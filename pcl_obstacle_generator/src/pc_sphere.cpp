#include "pcl_obstacle_generator/pc_sphere.h"

PCSphere::PCSphere(const PCObjectParameters &parameters, double radius) : PCObject(parameters) { radius_ = radius; }

void PCSphere::calculateLocalPoints() {
    local_points_->clear();
    double sphere_area = 4 * M_PI * radius_ * radius_;

    // Very roughly approximate the number of points on the sphere.
    double approx_points = sphere_area / (parameters_.discretization * parameters_.discretization);

    // Round the approximated number of points to get an integer
    int num_points = round(approx_points);

    double *x;

    // Calculate grid for sphere with defined width and length
    x = sphere_fibonacci_grid_points(radius_, num_points);

    local_points_->header.frame_id = "map";
    local_points_->width = num_points;
    local_points_->height = 1;

    // Add points to point cloud
    for (int i = 0; i < 3 * num_points; i = i + 3) {
        pcl::PointXYZI point;
        point.x = x[i];
        point.y = x[i + 1];
        point.z = x[i + 2];
        point.intensity = double(parameters_.object_id);
        local_points_->push_back(point);
    }

    delete[] x;
}