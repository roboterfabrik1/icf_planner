#include "pcl_obstacle_generator/composite_pc_object.h"

CompositePCObject::CompositePCObject(const CompositePCObjectParameters &parameters)
    : AbstractPCObject(),
      parameters_(parameters),
      global_points_(new PointCloud),
      global_normals_(new PointCloudNormal),
      local_points_(new PointCloud),
      local_normals_(new PointCloudNormal) {}

void CompositePCObject::addSubObject(std::shared_ptr<AbstractPCObject> newObject) { sub_objects_.push_back(newObject); }

void CompositePCObject::calculatePointsandNormals(PointCloud::Ptr &points, PointCloudNormal::Ptr &normals) {
    if (!global_cache_is_valid_) {
        calculateGlobalPointsAndNormals();
    }

    points = global_points_;
    normals = global_normals_;
}

void CompositePCObject::calculateMovingObstacleMsg(pcl_obstacle_generator::MovingObstacle &obstacle_msg) {
    obstacle_msg.obstacle_id = parameters_.object_id;

    if (!local_cache_is_valid_) {
        calculateLocalPointsAndNormals();
    }
    obstacle_msg.local_point_cloud_did_change = local_moving_obstacle_point_cloud_did_change_;
    pcl::toROSMsg(*local_points_, obstacle_msg.local_point_cloud);
    pcl::toROSMsg(*local_normals_, obstacle_msg.local_normal_cloud);
    local_moving_obstacle_point_cloud_did_change_ = true;//false

    obstacle_msg.global_twist_did_change = moving_obstacle_twist_did_change_;
    obstacle_msg.global_twist.linear.x = parameters_.velocity.at(0);
    obstacle_msg.global_twist.linear.y = parameters_.velocity.at(1);
    obstacle_msg.global_twist.linear.z = parameters_.velocity.at(2);
    obstacle_msg.global_twist.angular.x = 0.0;
    obstacle_msg.global_twist.angular.y = 0.0;
    obstacle_msg.global_twist.angular.z = 0.0;
    moving_obstacle_twist_did_change_ = true;//false

    obstacle_msg.global_transform_did_change = global_moving_obstacle_position_did_change_;
    obstacle_msg.global_transform.translation.x = parameters_.position.at(0);
    obstacle_msg.global_transform.translation.y = parameters_.position.at(1);
    obstacle_msg.global_transform.translation.z = parameters_.position.at(2);
    obstacle_msg.global_transform.rotation.x = 0.0;
    obstacle_msg.global_transform.rotation.y = 0.0;
    obstacle_msg.global_transform.rotation.z = 0.0;
    obstacle_msg.global_transform.rotation.w = 1.0;
    global_moving_obstacle_position_did_change_ = true;//false
}

void CompositePCObject::calculateLocalPointsAndNormals() {
    PointCloud::Ptr combined_points(new PointCloud);
    PointCloudNormal::Ptr combined_normals(new PointCloudNormal);

    for (auto &object : sub_objects_) {
        PointCloud::Ptr points;
        PointCloudNormal::Ptr normals;

        object->calculatePointsandNormals(points, normals);

        *combined_points = *combined_points + *points;
        *combined_normals = *combined_normals + *normals;
    }

    local_points_ = combined_points;
    local_normals_ = combined_normals;
    local_cache_is_valid_ = true;
    local_moving_obstacle_point_cloud_did_change_ = true;
}

void CompositePCObject::calculateGlobalPointsAndNormals() {
    if (!local_cache_is_valid_) {
        calculateLocalPointsAndNormals();
    }

    Eigen::Affine3f point_transformation;
    point_transformation = Eigen::Affine3f::Identity();
    point_transformation.translation() << parameters_.position.at(0), parameters_.position.at(1),
        parameters_.position.at(2);
    pcl::transformPointCloud(*local_points_, *global_points_, point_transformation);

    global_normals_ = local_normals_;
    global_cache_is_valid_ = true;
}

void CompositePCObject::updateVelocity() {
    if (time_step_ < parameters_.acceleration_start_step ||
        (time_step_ > parameters_.deceleration_end_step && parameters_.deceleration_end_step > 0)) {
        const double DELTA = 0.0000005;
        bool change_reqired = false;
        for (size_t i = 0; i < parameters_.velocity.size(); i++) {
            if (abs(parameters_.velocity[i] - 0.0) > DELTA) {
                change_reqired = true;
            }
        }
        if (change_reqired) {
            parameters_.velocity = {0.0, 0.0, 0.0};
            moving_obstacle_twist_did_change_ = true;
        }
    } else if (time_step_ >= parameters_.acceleration_start_step && time_step_ < parameters_.acceleration_end_step) {
        uint64_t hundred_percent_delta = parameters_.acceleration_end_step - parameters_.acceleration_start_step;
        uint64_t passed_delta = time_step_ - parameters_.acceleration_start_step;
        double percentage = (static_cast<double>(passed_delta) / static_cast<double>(hundred_percent_delta));
        for (size_t i = 0; i < parameters_.velocity.size(); i++) {
            parameters_.velocity[i] = percentage * parameters_.max_velocity[i];
            moving_obstacle_twist_did_change_ = true;
        }
    } else if (time_step_ > parameters_.deceleration_start_step && time_step_ <= parameters_.deceleration_end_step) {
        uint64_t hundred_percent_delta = parameters_.deceleration_end_step - parameters_.deceleration_start_step;
        uint64_t passed_delta = time_step_ - parameters_.deceleration_start_step;
        double percentage = 1.0 - (static_cast<double>(passed_delta) / static_cast<double>(hundred_percent_delta));
        for (size_t i = 0; i < parameters_.velocity.size(); i++) {
            parameters_.velocity[i] = percentage * parameters_.max_velocity[i];
            moving_obstacle_twist_did_change_ = true;
        }
    } else {
        const double DELTA = 0.0000005;
        bool change_reqired = false;
        for (size_t i = 0; i < parameters_.velocity.size(); i++) {
            if (abs(parameters_.velocity[i] - parameters_.max_velocity[i]) > DELTA) {
                change_reqired = true;
            }
        }
        if (change_reqired) {
            parameters_.velocity = parameters_.max_velocity;
            moving_obstacle_twist_did_change_ = true;
        }
    }
}

void CompositePCObject::updatePosition(double step_time_s) {
    const double MIN_CHANGE_VELOCITY = 0.000001;  // m/s
    time_step_ = time_step_ + 1;
    updateVelocity();

    if (step_time_s > 0.0) {
        for (size_t i = 0; i < parameters_.position.size(); i++) {
            double v = parameters_.velocity.at(i);
            if (v > MIN_CHANGE_VELOCITY or v < -MIN_CHANGE_VELOCITY) {
                parameters_.position[i] = parameters_.position.at(i) + (v * step_time_s);
                global_cache_is_valid_ = false;
                global_moving_obstacle_position_did_change_ = true;
            }
        }
    } else {
        ROS_ERROR_STREAM("PCObject::updatePosition - step_time_s needs to be larger than 0.0. Ignoring update.");
    }
}
