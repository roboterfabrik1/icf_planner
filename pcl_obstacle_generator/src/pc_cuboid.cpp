#include "pcl_obstacle_generator/pc_cuboid.h"

PCCuboid::PCCuboid(const PCObjectParameters &parameters, const std::vector<double> &dimensions) : PCObject(parameters) {
    dimensions_ = dimensions;
}

void PCCuboid::calculateLocalPoints() {
    local_points_->clear();

    std::array<int, 2> c = {1, 1};  // The grid centering for each dimension. 1 <= c(*) <= 5. See
                                    // https://people.sc.fsu.edu/~jburkardt/cpp_src/square_grid/square_grid.html
    int x_points = dimensions_.at(0) / parameters_.discretization + 1;
    int y_points = dimensions_.at(1) / parameters_.discretization + 1;
    double x_min = -dimensions_.at(0) / 2;
    double x_max = dimensions_.at(0) / 2;
    double y_min = -dimensions_.at(1) / 2;
    double y_max = dimensions_.at(1) / 2;
    double z_min = -dimensions_.at(2) / 2;
    double z_max = dimensions_.at(2) / 2;
    std::array<double, 2> a = {x_min, y_min};
    std::array<double, 2> b = {x_max, y_max};
    std::array<int, 2> ns = {x_points, y_points};
    int n = ns[0] * ns[1];

    double *x;

    // Calculate grid for square with defined width and length
    x = square_grid(n, ns.data(), a.data(), b.data(), c.data());

    local_points_->header.frame_id = "map";
    local_points_->width = n;
    local_points_->height = 1;

    // Add front face to point cloud
    for (int i = 0; i < 2 * n; i = i + 2) {
        // cuboid_cloud->push_back(pcl::PointXYZ(x[i], x[i+1], z_min));
        // Due to the workaround with using the intensity channel as primitive ID we need to construct the point like
        // this
        pcl::PointXYZI point;
        point.x = x[i];
        point.y = x[i + 1];
        point.z = z_min;
        point.intensity = double(parameters_.object_id);
        local_points_->push_back(point);
    }

    // Add middle faces to point cloud
    double z_coordinate = z_min + parameters_.discretization;
    while (z_coordinate < z_max) {
        for (int j = 0; j < 2 * n; j = j + 2) {
            // Check if we are on the boundaries of the cube
            // if (x[j] == x_min || x[j] == x_max || x[j + 1] == y_min || x[j + 1] == y_max)
            const double EQUALITY_THRESHOLD = 0.001;
            if (std::fabs(x[j] - x_min) < EQUALITY_THRESHOLD || std::fabs(x[j] - x_max) < EQUALITY_THRESHOLD ||
                std::fabs(x[j + 1] - y_min) < EQUALITY_THRESHOLD || std::fabs(x[j + 1] - y_max) < EQUALITY_THRESHOLD) {
                // cuboid_cloud->push_back(pcl::PointXYZ(x[j], x[j+1], i));
                // Due to the workaround with using the intensity channel as primitive ID we need to construct the point
                // like this
                pcl::PointXYZI point;
                point.x = x[j];
                point.y = x[j + 1];
                point.z = z_coordinate;
                point.intensity = double(parameters_.object_id);
                local_points_->push_back(point);
            }
        }
        z_coordinate = z_coordinate + parameters_.discretization;
    }

    // Add last face to pointcloud
    for (int i = 0; i < 2 * n; i = i + 2) {
        // cuboid_cloud->push_back(pcl::PointXYZ(x[i], x[i+1], z_max));
        // Due to the workaround with using the intensity channel as primitive ID we need to construct the point like
        // this
        pcl::PointXYZI point;
        point.x = x[i];
        point.y = x[i + 1];
        point.z = z_max;
        point.intensity = double(parameters_.object_id);
        local_points_->push_back(point);
    }

    delete[] x;
}