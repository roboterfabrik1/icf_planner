#include "pcl_obstacle_generator/pcl_obstacle_generation_node.h"

PCLObstacleGenerationNode::PCLObstacleGenerationNode() {
    // Construct publishers
    points_publisher_ = node_handle_.advertise<PointCloud>("raw_pcl_obstacles", PUBLISHER_QUEUE_SIZE_);
    normal_publisher_ = node_handle_.advertise<PointCloudNormal>("pcl_normals", PUBLISHER_QUEUE_SIZE_);
    obstacles_publisher_ =
        node_handle_.advertise<pcl_obstacle_generator::MovingObstacles>("pcl_obstacle_objects", PUBLISHER_QUEUE_SIZE_);
    const int EXPERIMENT_PUBLISHER_QUEUE = 100000;
    experiment_pipeline_points_publisher_ =
        node_handle_.advertise<pcl_obstacle_generator::ExperimentPipelineObstaclePoint>("experiment_pipeline_obstacles",
                                                                                        EXPERIMENT_PUBLISHER_QUEUE);
}

void PCLObstacleGenerationNode::loadPCObjects(const std::string& root_path,
                                              std::vector<std::shared_ptr<AbstractPCObject>>& object_container,
                                              bool is_nested) {
    XmlRpc::XmlRpcValue result;
    if (node_handle_.hasParam(root_path)) {
        node_handle_.getParam(root_path, result);
        for (auto& element : result) {
            const std::string OBJECT_NAME = element.first;
            std::string type;
            const std::string TYPE_PATH = root_path + OBJECT_NAME + "/type";
            node_handle_.getParam(TYPE_PATH, type);

            if (type == "Sphere" || type == "Cuboid") {
                std::shared_ptr<AbstractPCObject> object = parsePCObject(root_path, OBJECT_NAME, is_nested);
                object_container.push_back(object);
            } else if (type == "Composite") {
                std::shared_ptr<AbstractPCObject> object = parseCompositePCObject(root_path, OBJECT_NAME);
                object_container.push_back(object);
            } else {
                ROS_ERROR_STREAM("PCLObstacleGenerationNode::loadPCObjects - Can not load objects. Type at '"
                                 << TYPE_PATH << "' does not match any known type.");
            }
        }
    } else {
        ROS_ERROR_STREAM("PCLObstacleGenerationNode::loadPCObjects - Can not load objects. Root Path '"
                         << root_path << "' is not found on the ros parameter server.");
    }
}

std::shared_ptr<AbstractPCObject> PCLObstacleGenerationNode::parseCompositePCObject(const std::string& object_root_path,
                                                                                    const std::string& object_name) {
    const std::string OBJECT_PATH = object_root_path + object_name + "/";
    bool parameter_not_missing = true;
    CompositePCObjectParameters common_object_parameters;
    common_object_parameters.name = object_name;

    // Read common params for PCObjects. Expressions that use parameter_not_missing can not use default
    // values and are not optional
    std::string type;
    const std::string TYPE_PATH = OBJECT_PATH + "type";
    parameter_not_missing = node_handle_.getParam(TYPE_PATH, type) && parameter_not_missing;

    const std::string ID_PATH = OBJECT_PATH + "id";
    parameter_not_missing = node_handle_.getParam(ID_PATH, common_object_parameters.object_id) && parameter_not_missing;

    const std::string POSITION_PATH = OBJECT_PATH + "position";
    common_object_parameters.position = node_handle_.param(POSITION_PATH, common_object_parameters.position);

    const std::string VELOCITY_PATH = OBJECT_PATH + "velocity";
    common_object_parameters.max_velocity = node_handle_.param(VELOCITY_PATH, common_object_parameters.max_velocity);

    const std::string ACCELERATION_START_PATH = OBJECT_PATH + "acceleration_start_step";
    common_object_parameters.acceleration_start_step = static_cast<uint64_t>(node_handle_.param(
        ACCELERATION_START_PATH, static_cast<int>(common_object_parameters.acceleration_start_step)));

    const std::string ACCELERATION_END_PATH = OBJECT_PATH + "acceleration_end_step";
    common_object_parameters.acceleration_end_step = static_cast<uint64_t>(
        node_handle_.param(ACCELERATION_END_PATH, static_cast<int>(common_object_parameters.acceleration_end_step)));

    const std::string DECELERATION_START_PATH = OBJECT_PATH + "deceleration_start_step";
    common_object_parameters.deceleration_start_step = static_cast<uint64_t>(node_handle_.param(
        DECELERATION_START_PATH, static_cast<int>(common_object_parameters.deceleration_start_step)));

    const std::string DECELERATION_END_PATH = OBJECT_PATH + "deceleration_end_step";
    common_object_parameters.deceleration_end_step = static_cast<uint64_t>(
        node_handle_.param(DECELERATION_END_PATH, static_cast<int>(common_object_parameters.deceleration_end_step)));

    // Read specialized params for PCObjects and create object based on type
    if (type == "Composite") {
        if (parameter_not_missing) {
            std::shared_ptr<CompositePCObject> object(new CompositePCObject(common_object_parameters));
            const std::string COMPONENTS_PATH = OBJECT_PATH + "components";
            if (node_handle_.hasParam(COMPONENTS_PATH)) {
                std::vector<std::shared_ptr<AbstractPCObject>> components;
                loadPCObjects(COMPONENTS_PATH + "/", components, true);
                for (auto& component : components) {
                    object->addSubObject(component);
                }
            } else {
                ROS_ERROR_STREAM(
                    "PCLObstacleGenerationNode::loadPCObjects - Missing required parameter components for object '"
                    << object_name << "'. Aborting object generation.");
                return nullptr;
            }
            return std::static_pointer_cast<AbstractPCObject>(object);
        }
        ROS_ERROR_STREAM("PCLObstacleGenerationNode::loadPCObjects - Missing required parameters for object '"
                         << object_name << "'. Aborting object generation.");
    } else {
        ROS_ERROR_STREAM("PCLObstacleGenerationNode::loadPCObjects - The required parameter '"
                         << TYPE_PATH << "' is missing for object '" << object_name
                         << "'. Aborting object generation.");
    }
    return nullptr;
}

std::shared_ptr<AbstractPCObject> PCLObstacleGenerationNode::parsePCObject(const std::string& object_root_path,
                                                                           const std::string& object_name,
                                                                           bool is_nested) {
    const std::string OBJECT_PATH = object_root_path + object_name + "/";
    bool parameter_not_missing = true;
    PCObjectParameters common_object_parameters;
    common_object_parameters.name = object_name;

    // Read common params for PCObjects. Expressions that use parameter_not_missing can not use default
    // values and are not optional
    std::string type;
    const std::string TYPE_PATH = OBJECT_PATH + "type";
    parameter_not_missing = node_handle_.getParam(TYPE_PATH, type) && parameter_not_missing;

    const std::string ID_PATH = OBJECT_PATH + "id";
    parameter_not_missing =
        (node_handle_.getParam(ID_PATH, common_object_parameters.object_id) && parameter_not_missing) || is_nested;

    const std::string POSITION_PATH = OBJECT_PATH + "position";
    common_object_parameters.position = node_handle_.param(POSITION_PATH, common_object_parameters.position);

    const std::string VELOCITY_PATH = OBJECT_PATH + "velocity";
    if (is_nested && node_handle_.hasParam(VELOCITY_PATH)) {
        ROS_WARN_STREAM("Velocity for nested object '" << object_name << "' will be ignored.");
    }
    common_object_parameters.max_velocity = node_handle_.param(VELOCITY_PATH, common_object_parameters.max_velocity);

    const std::string ACCELERATION_START_PATH = OBJECT_PATH + "acceleration_start_step";
    if (is_nested && node_handle_.hasParam(ACCELERATION_START_PATH)) {
        ROS_WARN_STREAM("Acceleration_start_step for nested object '" << object_name << "' will be ignored.");
    }
    common_object_parameters.acceleration_start_step = static_cast<uint64_t>(node_handle_.param(
        ACCELERATION_START_PATH, static_cast<int>(common_object_parameters.acceleration_start_step)));

    const std::string ACCELERATION_END_PATH = OBJECT_PATH + "acceleration_end_step";
    if (is_nested && node_handle_.hasParam(ACCELERATION_END_PATH)) {
        ROS_WARN_STREAM("Acceleration_end_step for nested object '" << object_name << "' will be ignored.");
    }
    common_object_parameters.acceleration_end_step = static_cast<uint64_t>(
        node_handle_.param(ACCELERATION_END_PATH, static_cast<int>(common_object_parameters.acceleration_end_step)));

    const std::string DECELERATION_START_PATH = OBJECT_PATH + "deceleration_start_step";
    if (is_nested && node_handle_.hasParam(DECELERATION_START_PATH)) {
        ROS_WARN_STREAM("Deceleration_start_step for nested object '" << object_name << "' will be ignored.");
    }
    common_object_parameters.deceleration_start_step = static_cast<uint64_t>(node_handle_.param(
        DECELERATION_START_PATH, static_cast<int>(common_object_parameters.deceleration_start_step)));

    const std::string DECELERATION_END_PATH = OBJECT_PATH + "deceleration_end_step";
    if (is_nested && node_handle_.hasParam(DECELERATION_END_PATH)) {
        ROS_WARN_STREAM("Deceleration_end_step for nested object '" << object_name << "' will be ignored.");
    }
    common_object_parameters.deceleration_end_step = static_cast<uint64_t>(
        node_handle_.param(DECELERATION_END_PATH, static_cast<int>(common_object_parameters.deceleration_end_step)));

    const std::string DISCRETIZATION_PATH = OBJECT_PATH + "discretization";
    common_object_parameters.discretization =
        node_handle_.param(DISCRETIZATION_PATH, common_object_parameters.discretization);
    const std::string NORMALS_LEVEL_PATH = OBJECT_PATH + "normals_level";
    common_object_parameters.normals_level =
        node_handle_.param(NORMALS_LEVEL_PATH, common_object_parameters.normals_level);

    // Read specialized params for PCObjects and create object based on type
    if (type == "Sphere") {
        double radius;
        const std::string RADIUS_PATH = OBJECT_PATH + "radius";
        parameter_not_missing = node_handle_.getParam(RADIUS_PATH, radius) && parameter_not_missing;

        if (parameter_not_missing) {
            std::shared_ptr<AbstractPCObject> object(new PCSphere(common_object_parameters, radius));

            return object;
        }
        ROS_ERROR_STREAM("PCLObstacleGenerationNode::loadPCObjects - Missing required parameters for object '"
                         << object_name << "'. Aborting object generation.");

    } else if (type == "Cuboid") {
        std::vector<double> dimensions;
        const std::string DIMENSIONS_PATH = OBJECT_PATH + "dimensions";
        parameter_not_missing = node_handle_.getParam(DIMENSIONS_PATH, dimensions) && parameter_not_missing;

        if (parameter_not_missing) {
            std::shared_ptr<AbstractPCObject> object(new PCCuboid(common_object_parameters, dimensions));

            return object;
        }
        ROS_ERROR_STREAM("PCLObstacleGenerationNode::loadPCObjects - Missing required parameters for object '"
                         << object_name << "'. Aborting object generation.");

    } else {
        ROS_ERROR_STREAM("PCLObstacleGenerationNode::loadPCObjects - The required parameter '"
                         << TYPE_PATH << "' is missing for object '" << object_name
                         << "'. Aborting object generation.");
    }

    return nullptr;
}

void PCLObstacleGenerationNode::run() {
    const std::string ROOT_PATH = "PCObjects/";
    loadPCObjects(ROOT_PATH, pc_objects_);

    PointCloud::Ptr combined_points(new PointCloud);
    PointCloudNormal::Ptr combined_normals(new PointCloudNormal);
    pcl_obstacle_generator::MovingObstacles::Ptr moving_obstacles(new pcl_obstacle_generator::MovingObstacles);
    moving_obstacles->obstacles.resize(pc_objects_.size());

    combined_points->header.frame_id = "map";
    combined_normals->header.frame_id = "map";

    ros::Rate loop_rate(1 / STEP_TIME_S_);
    int loop_counter = 0;
    while (node_handle_.ok()) {
        combined_points->clear();
        combined_normals->clear();
        int i = 0;
        for (auto& pc_object : pc_objects_) {
            PointCloud::Ptr points;
            PointCloudNormal::Ptr normals;
            pc_object->updatePosition(STEP_TIME_S_);
            pc_object->calculatePointsandNormals(points, normals);
            pc_object->calculateMovingObstacleMsg(moving_obstacles->obstacles[i]);

            *combined_points = *combined_points + *points;
            //*combined_normals = *combined_normals + *normals;
            i++;
        }

        pcl_conversions::toPCL(ros::Time::now(), combined_points->header.stamp);
        points_publisher_.publish(combined_points);
        const int EXPERIMENT_PIPELINE_PUBLISH_POINT = 5;
        if (loop_counter % EXPERIMENT_PIPELINE_PUBLISH_POINT == 0) {
            publishExperimentPipelineObstaclePoints(combined_points);
        }
        loop_counter++;

        pcl_conversions::toPCL(ros::Time::now(), combined_normals->header.stamp);
        normal_publisher_.publish(combined_normals);

        obstacles_publisher_.publish(moving_obstacles);

        ros::spinOnce();
        loop_rate.sleep();
    }
}

void PCLObstacleGenerationNode::publishExperimentPipelineObstaclePoints(PointCloud::Ptr points) {
    for (auto& pcl_point : *points) {
        pcl_obstacle_generator::ExperimentPipelineObstaclePoint point;
        point.x = pcl_point.x;
        point.y = pcl_point.y;
        point.z = pcl_point.z;
        point.object_id = static_cast<int16_t>(std::lround(pcl_point.intensity));
        experiment_pipeline_points_publisher_.publish(point);
    }
}