#include <ros/ros.h>

#include "pcl_obstacle_generator/pcl_obstacle_generation_node.h"

int main(int argc, char **argv) {
    // Init ROS and publishers
    ros::init(argc, argv, "pub_pcl_raw_obstacles");
    PCLObstacleGenerationNode node;
    node.run();
}