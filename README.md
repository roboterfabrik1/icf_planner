# Informed Circular Fields Planner
This repository contains the code regarding the paper:   
Marvin Becker, Philipp Caspers, Torsten Lilge, Sami Haddadin, and Matthias A. Müller. "Informed Circular Fields for Global Reactive Obstacle Avoidance of Robotic Manipulators", 2024.

![center_sphere_predictions](/uploads/7200cb68813683ac3653ab53d92f7a88/center_sphere_predictions.png)

The repository contains all the code necessary for running the informed circular fields planner described in the paper. 

### Video

The accompanying videos for the work can be found [here](https://doi.org/10.25835/sr51tdkd). 

### Citation

If you find our work useful in your research, please consider citing:
``` bash
@article{BeckerCasLilHad2024,
  author={Becker, Marvin and Caspers, Philipp and Lilge, Torsten and Haddadin, Sami and Müller, Matthias A.},
  journal={in preparation}, 
  title={Informed Circular Fields for Global Reactive Obstacle Avoidance of Robotic Manipulators}, 
  year={2024}}
```

# Requirements
- Ubuntu 20.04
- ROS noetic [http://wiki.ros.org/noetic/Installation/Ubuntu](http://wiki.ros.org/noetic/Installation/Ubuntu)
- Catkin Command Line Tools [https://catkin-tools.readthedocs.io/en/latest/installing.html](https://catkin-tools.readthedocs.io/en/latest/installing.html)

## Additional requirement
The ICF planner requires some packages that need to be installed using the following command:
```bash
sudo apt-get install ros-noetic-moveit ros-noetic-franka-hw ros-noetic-franka-gripper ros-noetic-franka-description ros-noetic-moveit-planners-ompl python3-catkin-tools packagekit-gtk3-module ros-noetic-moveit-visual-tools
```

# Installation

1. Create a new workspace with `src` folder using the following command:
```bash
mkdir -p catkin_ws/src
```

2. Initialize the workspace and setup build type 'Release' for maximum performance:
```bash
cd catkin_ws
catkin init
catkin config --cmake-args -DCMAKE_BUILD_TYPE=Release
```
    
3. Clone this repository using the following command (and checkout the correct branch):
```bash
cd src 
git clone https://gitlab.com/roboterfabrik1/icf_planner.git .
```

4. Build the packages using catkin:
```bash
cd .. 
catkin build
```

5. Test the code using an example launch-file
```bash
source devel/setup.bash
roslaunch circular_fields_simulations center_sphere.launch
```

# Overview
The launch files for the different scenarios are stored under circular_fields_simulation/launch/panda.
The config file is located at
circular_fields_simulation/cfg/planner/sim_robot_planner.yaml

# Troubleshooting
We got reports of compilation errors on some computers.
Specifically, users reported internal compiler errors during compilation. 
However, repeatedly calling `catkin build` solved the problems.

# Contributors
Marvin Becker\
Dwayne Steinke\
Tom Hattendorf\
Philipp Caspers\
Haosen Tang\
Fabrice Zeug

# Disclaimer
The code in moveit_perception, pcl_obstacle_generator/src/sphere_fibonacci_grid.cpp and pcl_obstacle_generator/src/square_grip.cpp is not originally written or owned by us. 
It originates from third parties and belongs to the MoveIt motion planning framework and John Burkhardt, for more details see:
- [https://moveit.ros.org/](https://moveit.ros.org/)
- [https://people.sc.fsu.edu/~jburkardt/cpp_src/sphere_fibonacci_grid/sphere_fibonacci_grid.html](https://people.sc.fsu.edu/~jburkardt/cpp_src/sphere_fibonacci_grid/sphere_fibonacci_grid.html)
