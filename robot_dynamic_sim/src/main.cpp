#include <ros/ros.h>

#include "robot_dynamic_sim/robot_dynamic_sim.h"

int main(int argc, char** argv) {
    ros::init(argc, argv, "robot_dynamics");
    ros::NodeHandle n;
    RobotDynamicSim robotSim(n);

    // set ros::spin frequency
    ros::Rate ros_rate(1 / robotSim.getStepTime());
    ROS_INFO_STREAM("Ros Rate: " << ros_rate.expectedCycleTime().toSec());

    while (ros::ok()) {
        ros::spinOnce();

        robotSim.calculateStep();

        ros_rate.sleep();
    }

    return 0;
}