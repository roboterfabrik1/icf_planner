#include "robot_dynamic_sim/robot_dynamic_sim.h"

RobotDynamicSim::RobotDynamicSim(ros::NodeHandle& node_handle) : n_(&node_handle) {
    virtual_fap_forces_sub_ = n_->subscribe("cf_virtual_fap_forces", 1, &RobotDynamicSim::virtualFAPForceCallback, this,
                                            ros::TransportHints().tcpNoDelay());
    robot_settings_sub_ = n_->subscribe("cf_real_robot_settings", 1, &RobotDynamicSim::RealRobotSettingsCallback, this,
                                        ros::TransportHints().tcpNoDelay());
    goal_pose_sub_ = n_->subscribe("cf_goal_pose", 1,
                                   &RobotDynamicSim::goalPoseCallback, this);

    const int SMALL_QUEUE_SIZE = 1;
    const int LARGE_QUEUE_SIZE = 1000;
    joint_state_pub_ = n_->advertise<sensor_msgs::JointState>("joint_states", SMALL_QUEUE_SIZE);
    robot_state_pub_ = n_->advertise<cf_planner_msgs::RobotState>("cf_robot_state", SMALL_QUEUE_SIZE);

    ee_pub_ = n_->advertise<geometry_msgs::PoseStamped>("ee_pose", SMALL_QUEUE_SIZE);

    path_pub_ = n_->advertise<nav_msgs::Path>("robot_path", LARGE_QUEUE_SIZE);
    path_pub_timer_ = n_->createTimer(ros::Duration(path_publish_time_s_), &RobotDynamicSim::pathPublisherCallback,
                                      this);

    fap_positions_publisher_ = n_->advertise<visualization_msgs::Marker>("fap_positions", SMALL_QUEUE_SIZE);
    fap_forces_publisher_ = n_->advertise<visualization_msgs::Marker>("fap_forces", SMALL_QUEUE_SIZE);

    getParams();
}

void RobotDynamicSim::pathPublisherCallback(const ros::TimerEvent& event) {
    ((void)(event));  // Event is not used.

    const int MAX_POSES_IN_PATH = 10000;
    if (robot_path_.poses.size() > MAX_POSES_IN_PATH) {
        nav_msgs::Path downsampled_robot_path;
        downsampled_robot_path.header = robot_path_.header;
        // https://github.com/ros-visualization/rviz/issues/1107 Rviz used to crash for patrobot_path_h
        // messages greater than 16384. If robot_path_ has more than 16384 poses, the index will
        // increase by 2 every time, to halve the amount. For more than double the max the path will be
        // divided by 3 etc.. However since it it fixed, it should be unnecessary..
        for (size_t i = 0; i < robot_path_.poses.size(); i += std::floor(robot_path_.poses.size() / 10000.0) + 1) {
            downsampled_robot_path.poses.push_back(robot_path_.poses.at(i));
        }
        path_pub_.publish(downsampled_robot_path);
    } else {
        path_pub_.publish(robot_path_);
    }
}

void RobotDynamicSim::goalPoseCallback(
    const geometry_msgs::PoseStamped& latest_goal_pose_msg) {
  goal_pose_ = latest_goal_pose_msg;
  cf_robot_calculator_.setGoalPose(goal_pose_);

  if (!received_goal_pose_) {
    received_goal_pose_ = true;
  }
}

void RobotDynamicSim::RealRobotSettingsCallback(const cf_planner_msgs::AgentSettings& real_robot_settings_msg) {
    robot_settings_.k_jla = real_robot_settings_msg.k_jla;
    robot_settings_.k_man = real_robot_settings_msg.k_man;
    robot_settings_.d_null = real_robot_settings_msg.d_null;
    robot_settings_.d_jla = real_robot_settings_msg.d_jla;
    robot_settings_.scale_dynamic = real_robot_settings_msg.scale_dynamic;
    robot_settings_.nullspace_projection = real_robot_settings_msg.nullspace_projection;
    robot_settings_.manipulability_gradient = real_robot_settings_msg.manipulability_gradient;
    robot_settings_.invalid_best_agent = real_robot_settings_msg.invalid_best_agent;
    robot_settings_.safety_joint_vel_calculation_method = real_robot_settings_msg.safety_joint_vel_calculation_method;
    robot_settings_.close_control_point = real_robot_settings_msg.close_control_point;
    if (real_robot_settings_msg.fap_frame_ids.empty()) {
        robot_settings_.fap_frame_ids = {-1};
        return;
    }
    std::vector<int> fap_frame_ids;
    for (auto& frame_id : real_robot_settings_msg.fap_frame_ids) {
        fap_frame_ids.push_back(frame_id);
    }
    robot_settings_.fap_frame_ids = fap_frame_ids;
    if (!received_first_robot_settings_) {
      received_first_robot_settings_ = true;
    }
}

void RobotDynamicSim::virtualFAPForceCallback(const cf_planner_msgs::WrenchArray& force_msg) {
    std::vector<Vector6d> fap_forces;
    Vector6d wrench;
    for (auto& wrench_msg : force_msg.wrenches) {
        Vector6d wrench;
        tf::wrenchMsgToEigen(wrench_msg, wrench);

        fap_forces.push_back(wrench);
    }
    current_fap_wrenches_ = fap_forces;
}

void RobotDynamicSim::getParams() {
    std::vector<double> start_joints;
    std::vector<double> dh_a;
    std::vector<double> dh_d;
    std::vector<double> dh_theta;
    std::vector<double> dh_alpha;
    std::vector<double> q_min;
    std::vector<double> q_max;
    std::vector<double> dq_max;
    std::vector<double> ddq_max;
    std::vector<int> joint_types;
    bool sim_only;

    std::string ROOT_PATH = "CFPlanner/CFAgentSettings";

    if (n_->hasParam(ROOT_PATH)) {
        n_->getParam(ROOT_PATH + "/only_two_dimensions", robot_settings_.only_two_dimensions);
        n_->getParam(ROOT_PATH + "/k_g", robot_settings_.k_g);
        n_->getParam(ROOT_PATH + "/k_d", robot_settings_.k_d);
        n_->getParam(ROOT_PATH + "/k_cf_ee", robot_settings_.k_cf_ee);
        n_->getParam(ROOT_PATH + "/k_cf_body", robot_settings_.k_cf_body);
        n_->getParam(ROOT_PATH + "/k_cf_rep", robot_settings_.k_cf_rep);
        n_->getParam(ROOT_PATH + "/obstacle_limit_distance_repulsive",
                     robot_settings_.obstacle_limit_distance_repulsive);
        n_->getParam(ROOT_PATH + "/obstacle_limit_distance_safety",
                     robot_settings_.obstacle_limit_distance_safety);
        n_->getParam(ROOT_PATH + "/collision_distance", robot_settings_.collision_distance);
        n_->getParam(ROOT_PATH + "/goal_influence_limit_distance", robot_settings_.goal_influence_limit_distance);
        n_->getParam(ROOT_PATH + "/intermediate_goal_reached_distance",
                     robot_settings_.intermediate_goal_reached_distance);
        n_->getParam(ROOT_PATH + "/maximum_line_of_sight_shortcuts", robot_settings_.maximum_line_of_sight_shortcuts);
        n_->getParam(ROOT_PATH + "/maximum_line_of_sight_binary_search_iterations",
                     robot_settings_.maximum_line_of_sight_binary_search_iterations);
        n_->getParam(ROOT_PATH + "/shortcut_under_obstacle_influence_weight",
                     robot_settings_.shortcut_under_obstacle_influence_weight);
        n_->getParam(ROOT_PATH + "/translational_velocity_limit", robot_settings_.translational_velocity_limit);
        n_->getParam(ROOT_PATH + "/rotational_velocity_limit", robot_settings_.rotational_velocity_limit);
        n_->getParam(ROOT_PATH + "/fap_frame_ids", robot_settings_.fap_frame_ids);
        n_->getParam(ROOT_PATH + "/manipulability_gradient", robot_settings_.manipulability_gradient);
        n_->getParam(ROOT_PATH + "/nullspace_projection", robot_settings_.nullspace_projection);
        n_->getParam(ROOT_PATH + "/scale_dynamic", robot_settings_.scale_dynamic);
    } else {
        ROS_ERROR_STREAM("RobotDynamicSim::getParams - Can not load agent settings. Root Path '"
                         << ROOT_PATH << "' is not found on the ros parameter server.");
    }

    ROOT_PATH = "CFPlanner/SimpleRobotDynamicSettings";

    if (n_->hasParam(ROOT_PATH)) {
        n_->getParam(ROOT_PATH + "/dh_d", dh_d);
        n_->getParam(ROOT_PATH + "/dh_theta", dh_theta);
        n_->getParam(ROOT_PATH + "/dh_alpha", dh_alpha);
        n_->getParam(ROOT_PATH + "/dh_a", dh_a);
        n_->getParam(ROOT_PATH + "/joint_types", joint_types);
        n_->getParam(ROOT_PATH + "/q_min", q_min);
        n_->getParam(ROOT_PATH + "/q_max", q_max);
        n_->getParam(ROOT_PATH + "/dq_max", dq_max);
        n_->getParam(ROOT_PATH + "/ddq_max", ddq_max);
        n_->getParam(ROOT_PATH + "/k_jla", robot_settings_.k_jla);
        n_->getParam(ROOT_PATH + "/d_jla", robot_settings_.d_jla);
        n_->getParam(ROOT_PATH + "/k_man", robot_settings_.k_man);
        n_->getParam(ROOT_PATH + "/d_null", robot_settings_.d_null);
        if (!n_->param(ROOT_PATH + "/sim_only", sim_only, true)) {
            ROS_WARN_STREAM(
                "RobotDynamicSim::getParams - Can not load sim_only. Falling back to default behavior: simulation "
                "only.");
        }
    } else {
        ROS_ERROR_STREAM("RobotDynamicSim::getParams - Can not load robot settings. Root Path '"
                         << ROOT_PATH << "' is not found on the ros parameter server.");
    }
    if (n_->hasParam("start_joints")) {
        n_->getParam("start_joints", start_joints);
    } else {
        ROS_ERROR_STREAM("RobotDynamicSim::getParams - Can not load start_joints.");
    }
    if (n_->hasParam("joint_names")) {
        n_->getParam("joint_names", joint_names_);
    } else {
        ROS_ERROR_STREAM("RobotDynamicSim::getParams - Can not load start_joints.");
    }

    cf_robot_calculator_.buildChain(joint_types, dh_a, dh_alpha, dh_d, dh_theta);
    cf_robot_calculator_.setParams(step_time_, q_min, q_max, dq_max, ddq_max);

    // if (!received_goal_pose_) {
    //   geometry_msgs::PoseStamped::ConstPtr goal_pose_msg =
    //       ros::topic::waitForMessage<geometry_msgs::PoseStamped>(
    //           "cf_goal_pose");

    //   goal_pose_ = *goal_pose_msg;
    //   cf_robot_calculator_.setGoalPose(goal_pose_);
    //   received_goal_pose_ = true;
    // }

    if (!sim_only) {
      sensor_msgs::JointState::ConstPtr initial_joint_states;
      initial_joint_states =
          ros::topic::waitForMessage<sensor_msgs::JointState>(
              "/franka_state_controller/joint_states");

      if (initial_joint_states != NULL) {
        start_joints = initial_joint_states->position;
      } else {
        ROS_ERROR_STREAM(
            "RobotDynamicSim::getParams - Did not receive joint states from "
            "real robot.");
      }
    }
    robot_state_.joint_positions = Eigen::Map<Vector7d>(start_joints.data());
    robot_state_.joint_velocities = Vector7d::Zero();
    robot_state_.joint_velocities_null = Vector7d::Zero();

    int amount_of_faps = robot_settings_.fap_frame_ids.size();

    std::vector<CFRobotState::Matrix6x7> empty_fap_jacobians;
    for (size_t i = 0; i < amount_of_faps; i++) {
        empty_fap_jacobians.push_back(CFRobotState::Matrix6x7::Zero());
    }
    robot_state_.fap_jacobians = empty_fap_jacobians;

    std::vector<Eigen::Affine3d> empty_fap_poses;
    for (size_t i = 0; i < amount_of_faps; i++) {
        empty_fap_poses.push_back(Eigen::Affine3d::Identity());
    }
    robot_state_.fap_poses = empty_fap_poses;

    std::vector<Vector6d> empty_fap_velocities;
    for (size_t i = 0; i < amount_of_faps; i++) {
        empty_fap_velocities.push_back(Vector6d::Zero());
    }
    robot_state_.fap_velocities = empty_fap_velocities;

    current_fap_wrenches_.reserve(amount_of_faps);
    for (size_t i = 0; i < amount_of_faps; i++) {
        current_fap_wrenches_.push_back(Vector6d::Zero());
    }
}

void RobotDynamicSim::publishJointStates() {
    sensor_msgs::JointState joint_msg;
    joint_msg.header.stamp = ros::Time::now();
    int num_joints = robot_state_.joint_positions.col(0).size();
    for (size_t i = 0; i < num_joints; i++) {
        joint_msg.position.push_back(robot_state_.joint_positions(i, 0));
        joint_msg.velocity.push_back(robot_state_.joint_velocities(i, 0));
        joint_msg.name.push_back(joint_names_.at(i));
    }
    // In order to display the fingers in rviz
    joint_msg.name.push_back("panda_finger_joint1");
    joint_msg.position.push_back(0.04);
    joint_msg.name.push_back("panda_finger_joint2");
    joint_msg.position.push_back(0.04);
    joint_state_pub_.publish(joint_msg);
}

void RobotDynamicSim::publishFAPPositions() {
    fap_position_counter_ = 0;
    const double LIFETIME_S = 0.5;

    for (auto& fap_pose : robot_state_.fap_poses) {
        visualization_msgs::Marker marker_sphere;
        marker_sphere.action = visualization_msgs::Marker::ADD;
        marker_sphere.type = visualization_msgs::Marker::SPHERE;
        marker_sphere.header.frame_id = "map";
        marker_sphere.header.stamp = ros::Time::now();
        marker_sphere.lifetime = ros::Duration(LIFETIME_S);
        double scale = 0.05;  // NOLINT(readability-magic-numbers) : Style value initialization

        marker_sphere.scale.x = scale;
        marker_sphere.scale.y = scale;
        marker_sphere.scale.z = scale;

        marker_sphere.color.a = 1.0;
        marker_sphere.color.r = 0.9;  // NOLINT(readability-magic-numbers) : Style value initialization
        marker_sphere.color.g = 0.6;  // NOLINT(readability-magic-numbers) : Style value initialization
        marker_sphere.color.b = 0.0;

        marker_sphere.pose.position.x = fap_pose.translation().x();
        marker_sphere.pose.position.y = fap_pose.translation().y();
        marker_sphere.pose.position.z = fap_pose.translation().z();
        marker_sphere.pose.orientation.w = 1.0;

        marker_sphere.ns = "fap_position" + std::to_string(fap_position_counter_);
        marker_sphere.id = fap_position_counter_;

        fap_positions_publisher_.publish(marker_sphere);
        fap_position_counter_++;
    }
    int number_of_poses = robot_state_.fap_poses.size();
    int number_of_forces = current_fap_wrenches_.size();
    if (number_of_forces == number_of_poses || number_of_forces == number_of_poses - 1) {
        visualization_msgs::Marker fap_forces;
        fap_forces.header.frame_id = "map";
        fap_forces.header.stamp = ros::Time::now();
        fap_forces.lifetime = ros::Duration(LIFETIME_S);
        fap_forces.scale.x = 0.01;  // NOLINT(readability-magic-numbers) : Style value initialization
        fap_forces.color.a = 1.0;
        fap_forces.color.r = 0.9;  // NOLINT(readability-magic-numbers) : Style value initialization
        fap_forces.color.g = 0.6;  // NOLINT(readability-magic-numbers) : Style value initialization
        fap_forces.color.b = 0.0;
        fap_forces.pose.orientation.w = 1.0;

        fap_forces.ns = "fap_forces" + std::to_string(fap_position_counter_);
        fap_forces.id = 1;
        fap_forces.action = visualization_msgs::Marker::ADD;
        fap_forces.type = visualization_msgs::Marker::LINE_LIST;

        int point_index = 0;
        fap_forces.points.reserve(2 * number_of_forces);

        for (const auto& wrench : current_fap_wrenches_) {
            Eigen::Vector3d anchor_point = robot_state_.fap_poses[point_index].translation();
            geometry_msgs::Point anchor_point_msg;
            tf::pointEigenToMsg(anchor_point, anchor_point_msg);
            fap_forces.points.push_back(anchor_point_msg);

            Eigen::Vector3d head_point = anchor_point + wrench.head(3);
            geometry_msgs::Point head_point_msg;
            tf::pointEigenToMsg(head_point, head_point_msg);
            fap_forces.points.push_back(head_point_msg);

            point_index++;
        }
        fap_forces_publisher_.publish(fap_forces);
    }
}

void RobotDynamicSim::publishDynamics() {
    cf_planner_msgs::RobotState robot_state_msg;
    robot_state_msg.header.stamp = ros::Time::now();
    robot_state_msg.header.frame_id = "map";
    for (auto& twist : robot_state_.fap_velocities) {
        geometry_msgs::Twist twist_msg;
        tf::twistEigenToMsg(twist, twist_msg);
        robot_state_msg.fap_velocities.push_back(twist_msg);
    }

    for (auto& pos : robot_state_.fap_poses) {
        geometry_msgs::Pose pose_msg;
        tf::poseEigenToMsg(pos, pose_msg);
        robot_state_msg.fap_poses.push_back(pose_msg);
    }

    int num_joints = robot_state_.joint_positions.size();
    for (size_t i = 0; i < num_joints; i++) {
        robot_state_msg.joint_positions.push_back(robot_state_.joint_positions(i, 0));
        robot_state_msg.joint_velocities.push_back(robot_state_.joint_velocities(i, 0));
        robot_state_msg.joint_velocities_null.push_back(robot_state_.joint_velocities_null(i, 0));
    }

    if (robot_state_.fap_poses.size() > 0) {
        geometry_msgs::PoseStamped fap_debug;
        fap_debug.header.stamp = ros::Time::now();
        fap_debug.header.frame_id = "map";
        Eigen::Quaterniond q(robot_state_.fap_poses.back().rotation());
        tf::quaternionEigenToMsg(q, fap_debug.pose.orientation);
        tf::pointEigenToMsg(robot_state_.fap_poses.back().translation(), fap_debug.pose.position);
        ee_pub_.publish(fap_debug);
    }

    robot_path_.header.stamp = ros::Time::now();
    robot_path_.header.frame_id = "map";

    geometry_msgs::PoseStamped ee_position;
    ee_position.header.stamp = ros::Time::now();
    ee_position.header.frame_id = "map";
    Eigen::Quaterniond q(robot_state_.getEEPose().rotation());
    tf::quaternionEigenToMsg(q, ee_position.pose.orientation);
    tf::pointEigenToMsg(robot_state_.getEEPose().translation(), ee_position.pose.position);

    robot_path_.poses.push_back(ee_position);

    robot_state_pub_.publish(robot_state_msg);
}

void RobotDynamicSim::calculateStep() {
  if (received_first_robot_settings_){
    cf_robot_calculator_.calculateDynamicStep(current_fap_wrenches_, robot_state_, robot_settings_);
  }

  publishJointStates();
  publishDynamics();
  publishFAPPositions();
}
