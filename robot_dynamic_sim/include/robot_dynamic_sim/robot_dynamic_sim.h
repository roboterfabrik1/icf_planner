#ifndef ROBOT_DYNAMIC_SIM_H
#define ROBOT_DYNAMIC_SIM_H

#include <eigen_conversions/eigen_msg.h>
#include <nav_msgs/Path.h>
#include <sensor_msgs/JointState.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/transform_broadcaster.h>

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <Eigen/StdVector>
#include <boost/algorithm/clamp.hpp>
#include <iostream>

#include "cf_motion_planner/cf_robot_calculations.h"
#include "cf_planner_msgs/AgentSettings.h"
#include "cf_planner_msgs/ForceApplicationPoints.h"
#include "cf_planner_msgs/RobotState.h"
#include "cf_planner_msgs/WrenchArray.h"
#include "eigen_conversions/eigen_kdl.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/PoseArray.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/Wrench.h"
#include "geometry_msgs/WrenchStamped.h"
#include "ros/ros.h"
#include "sensor_msgs/JointState.h"
#include "visualization_msgs/Marker.h"

/// @brief Class for calculating localization and velocity of a robot receiving virtual forces from circular fields.
///
/// This class publishes the following messages:
/// - /point_mass_pos: geometry_msgs::PoseStamped of the latest robot position
/// - /point_mass_vel: geometry_msgs::TwistStamped of the latest robot velocity
///
/// This class subscribes to the following messages:
/// - /cf_virtual_forces: geometry_msgs::WrenchStamped Virtual force on the EE for dynamic calculation
///
/// This node can be configured through rosparam with the following parameters:
/// - SIMULATION_STEP_TIME_S
/// - START_JOINTS
/// - DH_PARAMETERS_A
/// - DH_PARAMETERS_ALPHA
/// - DH_PARAMETERS_D
/// - JOINTS_LOWER
/// - JOINTS_UPPER
/// - MAX_VEL
/// - MAX_TAU
/// Please see the documentation of the member variables with the same name.
///
/// The main calculation of this node can be found in publishDynamics().
class RobotDynamicSim {
    typedef Eigen::Matrix<double, 6, 1> Vector6d;  // NOLINT

    typedef Eigen::Matrix<double, 7, 1> Vector7d;  // NOLINT

    /// @brief Current robot settings
    CFAgentSettings robot_settings_;

    /// @brief Object for robot related calculations
    CFRobotCalculations cf_robot_calculator_;

    /// @name Ros Utility
    /// Properties for ros interaction
    /// @{
    /// @brief ROS node handle
    ros::NodeHandle* n_;

    /// @brief Subscriber to virtual forces of circular field. Calculates velocities and calls publisher
    ros::Subscriber virtual_ee_forces_sub_;

    /// @brief Subscriber to virtual forces of circular field. Calculates velocities and calls publisher
    ros::Subscriber virtual_fap_forces_sub_;

    /// @brief Subscriber to the robot settings
    ros::Subscriber robot_settings_sub_;

    /// @brief Subscriber to the FAP positions
    ros::Subscriber fap_positions_sub_;

    /// @brief Subscriber to the goal pose of circular field. Calculates pose and calls publisher
    ros::Subscriber goal_pose_sub_;

    /// @brief Publishes the current robot state
    ros::Publisher robot_state_pub_;

    /// @brief Publishes current postion ot the force application point
    ros::Publisher ee_pub_;

    /// @brief Publishes current joint state of robot
    ros::Publisher joint_state_pub_;

    /// @brief Publishes current and recent positions of end effector
    ros::Publisher path_pub_;

    /// @brief Publishes current positions of force application points
    ros::Publisher fap_positions_publisher_;

    /// @brief Publishes current forces acting on the force application points
    ros::Publisher fap_forces_publisher_;

    /// @brief Auxilary variable for the number of fap positions
    int fap_position_counter_;

    /// @brief Time interval in s, in which  the robot_path_ is published
    double path_publish_time_s_ = 0.1;  // NOLINT

    /// @brief Timer for publishing the path
    ros::Timer path_pub_timer_;

    /// @brief Current robot state
    CFRobotState robot_state_;

    /// @brief Step time of the simulation
    double step_time_ = 0.005;

    /// @brief Wrenches acting on defined force application points
    std::vector<Vector6d> current_fap_wrenches_;

    /// @brief Vector containing the joint names of the robot
    std::vector<std::string> joint_names_;

    /// @brief msg that stores the path of the robot
    nav_msgs::Path robot_path_;

    /// @brief Goal pose for the planner
    geometry_msgs::PoseStamped goal_pose_;

    /// @brief Check for goal pose initialization
    bool received_goal_pose_ = false;

    /// @brief Check for robot settings initialization
    bool received_first_robot_settings_ = false;

    /// @name Utility Functions and callbacks
    /// @{
    /// @brief Get all the required parameters for this node
    ///
    /// This function is a utility function for the constructor
    void getParams();

    /// @brief Converts into and publishes joint positions, velocities and efforts in a jointstate-message
    void publishJointStates();

    /// @brief Converts into and publishes robot state
    void publishDynamics();

    /// @brief Publishes the positions of the force application points
    void publishFAPPositions();

    /// @brief Timer callback for publishing the robot_path_.
    ///
    /// rviz load is quite heavy when publishing frequently. Therefore it is only published every
    /// path_publish_time_s_ and downsampled, if the poses exceed a limit.
    void pathPublisherCallback(const ros::TimerEvent& event);

    /// @brief Transforms the FAP positions from msg to Eigen::Affine3d format
    ///
    /// @param fap_positions_msg Incoming message containing the FAP positions in World_CS
    void FAPPositionsCallback(const cf_planner_msgs::ForceApplicationPoints& fap_positions_msg);

    /// @brief Transforms the end effector goal pose from message format to a 6-Dimensional vector
    ///
    /// @param msg Incoming geometry message containing the end effector goal pose
    void goalPoseCallback(const geometry_msgs::PoseStamped& msg);

    /// @brief Calculates current_accel_ from virtual forces
    ///
    /// @param force_msg Incoming virtual forces from motion planning algorithm
    void virtualEEForceCallback(const geometry_msgs::WrenchStamped& force_msg);

    /// @brief Calculates current_accel_ from virtual forces
    ///
    /// @param force_msg Incoming virtual forces from motion planning algorithm
    void virtualFAPForceCallback(const cf_planner_msgs::WrenchArray& force_msg);
    /// @}

    /// @brief Sets the robot settings to the setting from the cf_planners best agent
    ///
    /// @param real_robot_settings_msg Incoming robot settings
    void RealRobotSettingsCallback(const cf_planner_msgs::AgentSettings& real_robot_settings_msg);

public:
    /// @brief Construct a new Dynamics object
    ///
    /// @param node_handle The node handle to use
    ///
    explicit RobotDynamicSim(ros::NodeHandle& node_handle);

    /// @brief Get the step time of this robot
    double getStepTime() const { return step_time_; }
    // @}

    /// @brief Calculate a dynamic step and published the updated values /point_mass_pos, /point_mass_vel, (/tf)
    ///
    /// @attention In order to calculate valid values, this function has to be called every step_time_ by
    /// the overlying node loop in main.
    void calculateStep();
};

#endif  // DYNAMICS_H
