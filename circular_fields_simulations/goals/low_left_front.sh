#!/bin/bash
# Utility script to publish the goal pose once.
# This is a warpper around the rostopic pub --once shell command.
# Unfortunatly, the normal command does litter the bash history due to the multiline input. 
# This script prevents this.

#############
# Execution
#############

rostopic pub --once /cf_goal_pose geometry_msgs/PoseStamped "header:
  seq: 0
  stamp:
    secs: 0
    nsecs: 0
  frame_id: 'map'
pose:
  position:
    x: 0.45
    y: 0.4
    z: 0.3
  orientation:
    x: 1.0
    y: 0.0
    z: 0.0
    w: 0.0" 


echo "Done"
